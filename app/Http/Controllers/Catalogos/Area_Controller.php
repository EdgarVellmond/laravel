<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Area_Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Area_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Área
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Áreas
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.catalogos.area")) {
            return Redirect::to('/');
        }

        $arrayArea = Area_Model::select(Area_Model::$id, Area_Model::$nombre)->get();

        return view('Catalogos.Area.index', compact('arrayArea'));

    }

    /**
     * Función para crear o editar un registro en el Catálogo de Áreas
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Area_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.area")) {
                return "No tienes permiso para agregar.";
            }

            $area = new Area_Model();
            $area->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.area")) {
                return "No tienes permiso para editar.";
            }

            $area = Area_Model::find($data[Area_Model::$id]);
            $area->id_usuario_edicion = Auth::id();

        }

        $area->{Area_Model::$nombre} = $data[Area_Model::$nombre];

        if ($area->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el área
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerAreaEditar()
    {

        $data = request()->all();
        $id = $data[Area_Model::$id];

        $area = Area_Model::FindOrFail($id);

        return $area;

    }

    /**
     * Función para eliminar el área
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.area")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Area_Model::$id];
        $area = Area_Model::find($id);

        IF ($area->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}