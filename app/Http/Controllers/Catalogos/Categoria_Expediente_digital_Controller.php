<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Categoria_Expediente_digital_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Categoria_Expediente_Digital_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Categoría del Expediente Digital
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Categoría del Expediente Digital
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.catalogos.categoria_expediente_digital")) {
            return Redirect::to('/');
        }

        $arrayCategoriaExpedienteDigital = Categoria_Expediente_Digital_Model::select(Categoria_Expediente_Digital_Model::$id, Categoria_Expediente_Digital_Model::$nombre)->get();

        return view('Catalogos.Categoria_Expediente_Digital.index', compact('arrayCategoriaExpedienteDigital'));

    }

    /**
     * Función para crear o editar un registro en el Catálogo de Categoría del Expediente Digital
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Categoria_Expediente_Digital_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.categoria_expediente_digital")) {
                return "No tienes permiso para agregar.";
            }

            $categoriaExpedienteDigital = new Categoria_Expediente_Digital_Model();
            $categoriaExpedienteDigital->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.categoria_expediente_digital")) {
                return "No tienes permiso para editar.";
            }

            $categoriaExpedienteDigital = Categoria_Expediente_Digital_Model::find($data[Categoria_Expediente_Digital_Model::$id]);
            $categoriaExpedienteDigital->id_usuario_edicion = Auth::id();

        }

        $categoriaExpedienteDigital->{Categoria_Expediente_Digital_Model::$nombre} = $data[Categoria_Expediente_Digital_Model::$nombre];

        if ($categoriaExpedienteDigital->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener la Categoría del Expediente Digital
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerCategoriaExpedienteDigitalEditar()
    {

        $data = request()->all();
        $id = $data[Categoria_Expediente_Digital_Model::$id];

        $categoriaExpedienteDigital = Categoria_Expediente_Digital_Model::FindOrFail($id);

        return $categoriaExpedienteDigital;

    }

    /**
     * Función para eliminar la Categoría del Expediente Digital
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.categoria_expediente_digital")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Categoria_Expediente_Digital_Model::$id];
        $categoriaExpedienteDigital = Categoria_Expediente_Digital_Model::find($id);

        IF ($categoriaExpedienteDigital->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}