<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Clasificacion_Obra_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Clasificacion_Obra_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Clasificación de Obra
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Clasificación de Obra
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.clasificacion_obra")) {
            return Redirect::to('/');
        }

        $arrayClasificacionObra = Clasificacion_Obra_Model::select(Clasificacion_Obra_Model::$id, Clasificacion_Obra_Model::$nombre)->get();

        return view('Catalogos.Clasificacion_Obra.index', compact('arrayClasificacionObra'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Clasificación de Obra
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Clasificacion_Obra_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.clasificacion_obra")) {
                return "No tienes permiso para agregar.";
            }

            $clasificacionObra = new Clasificacion_Obra_Model();
            $clasificacionObra->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.clasificacion_obra")) {
                return "No tienes permiso para editar.";
            }

            $clasificacionObra = Clasificacion_Obra_Model::find($data[Clasificacion_Obra_Model::$id]);
            $clasificacionObra->id_usuario_edicion = Auth::id();

        }

        $clasificacionObra->{Clasificacion_Obra_Model::$nombre} = $data[Clasificacion_Obra_Model::$nombre];

        if ($clasificacionObra->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener la Clasificación de Obra
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerClasificacionObraEditar()
    {

        $data = request()->all();
        $id = $data[Clasificacion_Obra_Model::$id];

        $clasificacionObra = Clasificacion_Obra_Model::FindOrFail($id);

        return $clasificacionObra;

    }

    /**
     * Función para eliminar la Clasificación de Obra
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.clasificacion_obra")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Clasificacion_Obra_Model::$id];
        $clasificacionObra = Clasificacion_Obra_Model::find($id);

        IF ($clasificacionObra->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}