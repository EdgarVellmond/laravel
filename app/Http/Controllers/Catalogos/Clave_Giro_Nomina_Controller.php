<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Clave_Giro_Nomina_Model;
use App\Models\Catalogos\Empresa_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;


class Clave_Giro_Nomina_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Retorna la vista del index del catálogo de Cuentas de Nómina del Sat
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Cuentas de Nómina del Sat
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.clave_giro_nomina")) {
            return Redirect::to('/');
        }

        $arrayClaveGiroNomina = Clave_Giro_Nomina_Model::select(Clave_Giro_Nomina_Model::$tabla. self::PUNTO .Clave_Giro_Nomina_Model::$id,
            Clave_Giro_Nomina_Model::$tabla. self::PUNTO .Clave_Giro_Nomina_Model::$cuenta,
            Clave_Giro_Nomina_Model::$tabla. self::PUNTO .Clave_Giro_Nomina_Model::$nombre,
            Clave_Giro_Nomina_Model::$tabla. self::PUNTO .Clave_Giro_Nomina_Model::$tipoGiro,
            Clave_Giro_Nomina_Model::$tabla. self::PUNTO .Clave_Giro_Nomina_Model::$prestacion,
            DB::raw(Empresa_Model::$tabla.self::PUNTO.Empresa_Model::$razonSocial." as nombreEmpresa"))
            ->join(Empresa_Model::$tabla,
                Clave_Giro_Nomina_Model::$tabla.self::PUNTO.Clave_Giro_Nomina_Model::$idEmpresa,
                "=",
                Empresa_Model::$tabla.self::PUNTO.Empresa_Model::$id)
            ->get();

        $arrayEmpresa = Empresa_Model::select(Empresa_Model::$id,
            Empresa_Model::$razonSocial)
            ->where(Empresa_Model::$patron, 1)
            ->lists(Empresa_Model::$razonSocial , Empresa_Model::$id);

        return view('Catalogos.Clave_Giro_Nomina.index', compact('arrayClaveGiroNomina', 'permiso', 'arrayEmpresa'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Clave de Giro de Nómina
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Clave_Giro_Nomina_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.clave_giro_nomina")) {
                return "No tienes permiso para agregar.";
            }

            $clave_giro_nomina = new Clave_Giro_Nomina_Model();
            $clave_giro_nomina->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.clave_giro_nomina")) {
                return "No tienes permiso para editar.";
            }

            $clave_giro_nomina = Clave_Giro_Nomina_Model::find($data[Clave_Giro_Nomina_Model::$id]);
            $clave_giro_nomina->id_usuario_edicion = Auth::id();

        }

        $clave_giro_nomina->{Clave_Giro_Nomina_Model::$cuenta} = $data[Clave_Giro_Nomina_Model::$cuenta];
        $clave_giro_nomina->{Clave_Giro_Nomina_Model::$nombre} = $data[Clave_Giro_Nomina_Model::$nombre];
        $clave_giro_nomina->{Clave_Giro_Nomina_Model::$tipoGiro} = $data[Clave_Giro_Nomina_Model::$tipoGiro];
        $clave_giro_nomina->{Clave_Giro_Nomina_Model::$idEmpresa} = $data[Clave_Giro_Nomina_Model::$idEmpresa];
        if(isset($data[Clave_Giro_Nomina_Model::$prestacion])){
            $clave_giro_nomina->{Clave_Giro_Nomina_Model::$prestacion} = 1;
        }else{
            $clave_giro_nomina->{Clave_Giro_Nomina_Model::$prestacion} = 0;
        }


        if ($clave_giro_nomina->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener la clave_giro_nomina
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerClaveGiroNominaEditar()
    {

        $data = request()->all();
        $id = $data[Clave_Giro_Nomina_Model::$id];

        $clave_giro_nomina = Clave_Giro_Nomina_Model::FindOrFail($id);

        return $clave_giro_nomina;

    }

    /**
     * Función para eliminar la clave_giro_nomina
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.clave_giro_nomina")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Clave_Giro_Nomina_Model::$id];
        $clave_giro_nomina = Clave_Giro_Nomina_Model::find($id);

        IF ($clave_giro_nomina->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}