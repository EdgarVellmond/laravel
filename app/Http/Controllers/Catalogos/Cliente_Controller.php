<?php

namespace App\Http\Controllers\Catalogos;

use App\Models\Catalogos\Banco_Model;
use App\Models\Catalogos\Cliente_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Metodo_Pago_Model;
use App\Models\Catalogos\Municipio_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;


class Cliente_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de clientes
     *
     * @author Oscar Vargas
     * @return Vista index del cliente
     */
    public function index()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.cliente")) {
            return Redirect::to('/');
        }

        //Obtener lista de Clientes
        $arrayCliente = Cliente_Model::select(Cliente_Model::$id, Cliente_Model::$nombre,
            Cliente_Model::$apellidoPaterno, Cliente_Model::$apellidoMaterno, Cliente_Model::$rfc,
            Cliente_Model::$telefono, Cliente_Model::$correo, Cliente_Model::$id)
            ->get();

        //Obtener lista de Estados
        $arrayEstado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);

        //Obtener lista de Bancos
        $arrayBanco = Banco_Model::lists(Banco_Model::$nombre, Banco_Model::$id);

        //Obtener lista de Metodos de pago
        $arrayMetodoPago = Metodo_Pago_Model::lists(Metodo_Pago_Model::$nombre, Metodo_Pago_Model::$id);

        return view('Catalogos.Cliente.index', compact('arrayCliente', 'arrayTipoPersona', 'arrayEstado', 'arrayBanco', 'arrayMetodoPago'));

    }

    /**
     * Metodo que sirve para guardar o editar un cliente
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Altas de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.cliente")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data['id_cliente'] == "") {

            $proveedor = new Cliente_Model();

        } else {

            $proveedor = Cliente_Model::find($data[Cliente_Model::$id]);

        }

        $proveedor->{Cliente_Model::$nombre} = $data[Cliente_Model::$nombre];
        $proveedor->{Cliente_Model::$apellidoPaterno} = $data[Cliente_Model::$apellidoPaterno];
        $proveedor->{Cliente_Model::$apellidoMaterno} = $data[Cliente_Model::$apellidoMaterno];
        $proveedor->{Cliente_Model::$rfc} = $data[Cliente_Model::$rfc];
        if (isset($data[Cliente_Model::$credito])) {
            $proveedor->{Cliente_Model::$credito} = $data[Cliente_Model::$credito];
        } else {
            $proveedor->{Cliente_Model::$credito} = 0;
        }
        $proveedor->{Cliente_Model::$diasCredito} = $data[Cliente_Model::$diasCredito];
        $proveedor->{Cliente_Model::$limiteCredito} = $data[Cliente_Model::$limiteCredito];
        $proveedor->{Cliente_Model::$telefono} = $data[Cliente_Model::$telefono];
        $proveedor->{Cliente_Model::$correo} = $data[Cliente_Model::$correo];
        $proveedor->{Cliente_Model::$calle} = $data[Cliente_Model::$calle];
        $proveedor->{Cliente_Model::$colonia} = $data[Cliente_Model::$colonia];
        $proveedor->{Cliente_Model::$numeroInterior} = $data[Cliente_Model::$numeroInterior];
        $proveedor->{Cliente_Model::$numeroExterior} = $data[Cliente_Model::$numeroExterior];
        $proveedor->{Cliente_Model::$cp} = $data[Cliente_Model::$cp];
        $proveedor->{Cliente_Model::$tipoPersona} = $data[Cliente_Model::$tipoPersona];
        $proveedor->{Cliente_Model::$idMetodoPago} = $data[Cliente_Model::$idMetodoPago];
        $proveedor->{Cliente_Model::$idBanco} = $data[Cliente_Model::$idBanco];
        $proveedor->{Cliente_Model::$idMunicipio} = $data[Cliente_Model::$idMunicipio];


        IF ($proveedor->save()) {
            return "Se guardo correctamente el Cliente";
        } else {
            return "No se guardo exitosamente el Cliente";
        }

    }

    /**
     * Metodo que sirve para enviar la informacion de la direccion de un Cliente
     *
     * @param POST id_cliente identificador unico
     * @author Oscar Vargas
     * @return json con la informacion de domicilio de cliente
     */
    public function obtenerDomicilioCliente()
    {

        $data = request()->all();

        $arrayDomicilio = Cliente_Model::select(Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$calle,
            Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$colonia,
            Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$numeroExterior,
            Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$numeroInterior,
            Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$cp,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . ' as nombreMunicipio'),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . ' as nombreEstado'))
            ->join(Municipio_Model::$tabla,
                Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                "=",
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->where(Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$id, $data['id_cliente'])->get();

        return $arrayDomicilio;

    }

    /**
     * Metodo que sirve para enviar la informacion del credito y forma de pago de un Cliente
     *
     * @param POST id_cliente identificador unico
     * @author Oscar Vargas
     * @return json con la informacion de credito y forma de pago de cliente
     */
    public function obtenerCreditoCliente()
    {

        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("credito.catalogos.cliente")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        $arrayCredito = Cliente_Model::select(Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$credito,
            Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$diasCredito,
            Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$limiteCredito,
            DB::raw(Metodo_Pago_Model::$tabla . self::PUNTO . Metodo_Pago_Model::$nombre . ' as nombreMetodo'),
            DB::raw(Banco_Model::$tabla . self::PUNTO . Banco_Model::$nombre . ' as nombreBanco'))
            ->join(Metodo_Pago_Model::$tabla,
                Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$idMetodoPago,
                "=",
                Metodo_Pago_Model::$tabla . self::PUNTO . Metodo_Pago_Model::$id)
            ->join(Banco_Model::$tabla,
                Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$idBanco,
                "=",
                Banco_Model::$tabla . self::PUNTO . Banco_Model::$id)
            ->where(Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$id, $data['id_cliente'])->get();

        return $arrayCredito;

    }

    /**
     * Metodo que sirve para eliminar un Cliente
     *
     * @param POST id_cliente identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarCliente()
    {

        //Permiso de Bajas de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.cliente")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Cliente_Model::$id];
        $cliente = Cliente_Model::find($id);

        IF ($cliente->delete()) {
            return "Se eliminó correctamente el Cliente";
        } else {
            return "No se eliminó exitosamente el Cliente";
        }

    }

    /**
     * Metodo que sirve para obtener un cliente
     *
     * @param POST id_cliente identificador unico
     * @author Oscar Vargas
     * @return json con el cliente solicitado
     */
    public function obtenerCliente()
    {

        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.cliente")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Cliente_Model::$id];
        $cliente = Cliente_Model::where(Cliente_Model::$id, $id)
            ->join(Municipio_Model::$tabla,
                Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)->get();

        return $cliente;

    }



}
