<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Cuentas_Nomina_Sat_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Cuentas_Nomina_Sat_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Cuentas de Nómina del Sat
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Cuentas de Nómina del Sat
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.cuentas_nomina_sat")) {
            return Redirect::to('/');
        }

        $arrayCuentasNominaSat = Cuentas_Nomina_Sat_Model::select(Cuentas_Nomina_Sat_Model::$id,
            Cuentas_Nomina_Sat_Model::$cuenta,
            Cuentas_Nomina_Sat_Model::$nombre)->get();

        return view('Catalogos.Cuentas_Nomina_Sat.index', compact('arrayCuentasNominaSat', 'permiso'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Cuentas de Nómina del Sat
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Cuentas_Nomina_Sat_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.cuentas_nomina_sat")) {
                return "No tienes permiso para agregar.";
            }

            $cuentas_nomina_sat = new Cuentas_Nomina_Sat_Model();
            $cuentas_nomina_sat->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.cuentas_nomina_sat")) {
                return "No tienes permiso para editar.";
            }

            $cuentas_nomina_sat = Cuentas_Nomina_Sat_Model::find($data[Cuentas_Nomina_Sat_Model::$id]);
            $cuentas_nomina_sat->id_usuario_edicion = Auth::id();

        }

        $cuentas_nomina_sat->{Cuentas_Nomina_Sat_Model::$cuenta} = $data[Cuentas_Nomina_Sat_Model::$cuenta];
        $cuentas_nomina_sat->{Cuentas_Nomina_Sat_Model::$nombre} = $data[Cuentas_Nomina_Sat_Model::$nombre];

        if ($cuentas_nomina_sat->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el cuentas_nomina_sat
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerCuentasNominaSatEditar()
    {

        $data = request()->all();
        $id = $data[Cuentas_Nomina_Sat_Model::$id];

        $cuentas_nomina_sat = Cuentas_Nomina_Sat_Model::FindOrFail($id);

        return $cuentas_nomina_sat;

    }

    /**
     * Función para eliminar el cuentas_nomina_sat
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.cuentas_nomina_sat")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Cuentas_Nomina_Sat_Model::$id];
        $cuentas_nomina_sat = Cuentas_Nomina_Sat_Model::find($id);

        IF ($cuentas_nomina_sat->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}