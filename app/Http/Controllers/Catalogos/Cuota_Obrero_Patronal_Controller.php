<?php

namespace App\Http\Controllers\Catalogos;

use App\Models\Catalogos\Cuota_Obrero_Patronal_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;

class Cuota_Obrero_Patronal_Controller extends Controller
{

    /**
     * Index del catalogo de cuotas
     *
     * @author Oscar Vargas
     * @return Vista index del cuota
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.cuota_obrero_patronal")) {
            return Redirect::to('/');
        }

        //Obtener lista de Cuotas Obrero Patronal
        $arrayCuota = Cuota_Obrero_Patronal_Model::select(Cuota_Obrero_Patronal_Model::$id,
            Cuota_Obrero_Patronal_Model::$ramo,
            Cuota_Obrero_Patronal_Model::$valor,
            Cuota_Obrero_Patronal_Model::$tipoCuota)->get();

        return view('Catalogos.Cuota_Obrero_Patronal.index', compact('arrayCuota'));

    }

    /**
     * Metodo que sirve para guardar o editar una cuota
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.cuota_obrero_patronal")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Cuota_Obrero_Patronal_Model::$id] == "") {

            $cuota = new Cuota_Obrero_Patronal_Model();
            $cuota->id_usuario_creacion = Auth::id();

        } else {

            $cuota = Cuota_Obrero_Patronal_Model::find($data[Cuota_Obrero_Patronal_Model::$id]);
            $cuota->id_usuario_edicion = Auth::id();

        }

        $cuota->{Cuota_Obrero_Patronal_Model::$ramo} = $data[Cuota_Obrero_Patronal_Model::$ramo];
        $cuota->{Cuota_Obrero_Patronal_Model::$valor} = $data[Cuota_Obrero_Patronal_Model::$valor];
        $cuota->{Cuota_Obrero_Patronal_Model::$tipoCuota} = $data[Cuota_Obrero_Patronal_Model::$tipoCuota];

        if ($cuota->save()) {
            return "Se guardo correctamente la cuota obrero patronal";
        } else {
            return "No se guardo exitosamente la cuota obrero patronal";
        }

    }

    /**
     * Metodo que sirve para obtener una cuota obrero patronal
     *
     * @param POST id_cuota_obrero_patronal identificador unico
     * @author Oscar Vargas
     * @return json con la cuota solicitado
     */
    public function obtenerCuotaObreroPatronal()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.cuota_obrero_patronal")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Cuota_Obrero_Patronal_Model::$id];

        $cuota = Cuota_Obrero_Patronal_Model::FindOrFail($id);

        return $cuota;

    }

    /**
     * Metodo que sirve para eliminar una cuota
     *
     * @param POST id_cuota_obrero_patronal identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarCuotaObreroPatronal()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.cuota_obrero_patronal")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Cuota_Obrero_Patronal_Model::$id];
        $cuota = Cuota_Obrero_Patronal_Model::find($id);

        IF ($cuota->delete()) {
            return "Se eliminó correctamente la cuota obrero patronal";
        } else {
            return "No se eliminó exitosamente la cyota obrero patronal";
        }

    }
}
