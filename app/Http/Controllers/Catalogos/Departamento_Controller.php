<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Departamento_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Departamento_Controller extends Controller
{

    const EQUALS = "=";
    const PUNTO = ".";

    /**
     * Retorna la vista del index del catálogo del Departamento
     * @author Erik Villarreal
     * @return view #Vista index del catálogo del Departamento
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.catalogos.departamento")) {
            return Redirect::to('/');
        }

        $arrayDepartamento = Departamento_Model::select(Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$id,
            Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$nombre,
            //id del Área usado como nombre para diferenciarlo del nombre del Departamento
            DB::raw(Area_Model::$tabla . self::PUNTO . Area_Model::$nombre . " as " . Area_Model::$id))
            ->join(Area_Model::$tabla,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$idArea,
                self::EQUALS,
                Area_Model::$tabla . self::PUNTO . Area_Model::$id)
            ->get();

        $area = Area_Model::lists(Area_Model::$nombre, Area_Model::$id);

        return view('Catalogos.departamento.index', compact('arrayDepartamento', 'area'));

    }

    /**
     * Función para crear o editar un registro en el Catálogo de Departamento
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Departamento_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.departamento")) {
                return "No tienes permiso para agregar.";
            }

            $departamento = new Departamento_Model();
            $departamento->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.departamento")) {
                return "No tienes permiso para editar.";
            }

            $departamento = Departamento_Model::find($data[Departamento_Model::$id]);
            $departamento->id_usuario_edicion = Auth::id();

        }

        $departamento->{Departamento_Model::$nombre} = $data[Departamento_Model::$nombre];
        $departamento->{Departamento_Model::$idArea} = $data[Area_Model::$id];

        if ($departamento->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el área
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerDepartamentoEditar()
    {

        $data = request()->all();
        $id = $data[Departamento_Model::$id];

        $departamento = Departamento_Model::FindOrFail($id);

        return $departamento;

    }

    /**
     * Metodo que sirve para llenar un select de departamentos de cierta Área
     * @param POST id del estado
     * @author Oscar Vargas
     * @return mixed
     */
    public function obtenerDepartamentos()
    {
        $data = request()->all();

        //Obtener lista de Departamentos
        $arraDepartamentos = Departamento_Model::where(Departamento_Model::$idArea, $data[Departamento_Model::$idArea])->lists(Departamento_Model::$nombre, Departamento_Model::$id);

        return $arraDepartamentos;
    }

    /**
     * Función para eliminar el área
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.departamento")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Departamento_Model::$id];
        $departamento = Departamento_Model::find($id);

        IF ($departamento->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}