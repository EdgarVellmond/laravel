<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Dia_Festivo_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Dia_Festivo_Controller extends Controller 
{

    /**
     * Retorna la vista del index del catálogo de Día Festivo
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Día Festivo
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.dia_festivo")) {
            return Redirect::to('/');
        }

        $arrayDiaFestivo = Dia_Festivo_Model::select(Dia_Festivo_Model::$id, Dia_Festivo_Model::$nombre, Dia_Festivo_Model::$fecha)->get();

        return view('Catalogos.Dia_Festivo.index', compact('arrayDiaFestivo'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Día Festivo
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Dia_Festivo_Model::$id] == "") {
            
            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.dia_festivo")) {
                return "No tienes permiso para agregar.";
            }

            $diaFestivo = new Dia_Festivo_Model();
            $diaFestivo->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.dia_festivo")) {
                return "No tienes permiso para editar.";
            }

            $diaFestivo = Dia_Festivo_Model::find($data[Dia_Festivo_Model::$id]);
            $diaFestivo->id_usuario_edicion = Auth::id();

        }

        $diaFestivo->{Dia_Festivo_Model::$nombre} = $data[Dia_Festivo_Model::$nombre];
        $diaFestivo->{Dia_Festivo_Model::$fecha} = $data[Dia_Festivo_Model::$fecha];

        if ($diaFestivo->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el puesto
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerDiaFestivoEditar()
    {

        $data = request()->all();
        $id = $data[Dia_Festivo_Model::$id];

        $diaFestivo = Dia_Festivo_Model::FindOrFail($id);

        return $diaFestivo;

    }

    /**
     * Función para eliminar el puesto
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.dia_festivo")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Dia_Festivo_Model::$id];
        $diaFestivo = Dia_Festivo_Model::find($id);

        IF ($diaFestivo->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}