<?php

namespace App\Http\Controllers\Catalogos;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Genericos\Archivo_Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Empresa_Imagen_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class Empresa_Controller extends Controller
{
    const EQUALS = '=';
    const PUNTO = '.';

    /**
     * Muestra el catálogo de Empresa
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.empresa")) {
            return Redirect::to('/');
        }

        $empresas = Empresa_Model::select(
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$id,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$clave,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$rfc,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$razonSocial,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$correo,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$idEmpresaImagen,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$patron,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$operacion,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$tipoNomina,
            Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$uuid,
            Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$ruta,
            Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$extension
        )
            ->leftjoin(Empresa_Imagen_Model::$tabla,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$idEmpresaImagen,
                self::EQUALS,
                Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$id)
            ->get();

        $estado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);

        return view('Catalogos.Empresa.index', compact("empresas", "estado"));

    }

    /**
     * Función para obtener la dirección de la Empresa
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function obtenerDireccionEmpresa()
    {
        $data = request()->all();

        return $direccion = Empresa_Model::select(Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$calle,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$numeroExterior,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$numeroInterior,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$colonia,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$cp,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . ' as ' . Municipio_Model::$id),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . ' as ' . Estado_Model::$id))
            ->join(Municipio_Model::$tabla,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$idMunicipio,
                self::EQUALS,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                self::EQUALS,
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->where(Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$id, $data[Empresa_Model::$id])
            ->get();

    }

    /**
     * Función para obtener la dirección de la Empresa
     * @author Erik Villarreal
     * @return JSON #Información completa de la empresa como objeto JSON
     */
    public function obtenerEmpresa()
    {
        $data = request()->all();

        return $direccion = Empresa_Model::select(
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$clave,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$rfc,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$correo,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$razonSocial,
            Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$ruta,
            Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$uuid,
            Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$extension,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$calle,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$numeroExterior,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$numeroInterior,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$colonia,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$cp,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$patron,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$operacion,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$tipoNomina,
            Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id,
            Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->join(Municipio_Model::$tabla,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$idMunicipio,
                self::EQUALS,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                self::EQUALS,
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->leftjoin(Empresa_Imagen_Model::$tabla,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$idEmpresaImagen,
                self::EQUALS,
                Empresa_Imagen_Model::$tabla . self::PUNTO . Empresa_Imagen_Model::$id)
            ->where(Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$id, $data[Empresa_Model::$id])
            ->get();

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Empresa o Editarlo
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        $data = $request->all();
        
        if ($data[Empresa_Model::$id] == "") {

            //Permiso de Despliegue de este Modulo
            if (!Helpers::get_permiso("alta.catalogos.empresa")) {
                return Redirect::to('/');
            }

            $empresa = new Empresa_Model();
            $empresa->{Empresa_Model::$idUsuarioCreacion} = Auth::id();
        } else {

            //Permiso de Despliegue de este Modulo
            if (!Helpers::get_permiso("edicion.catalogos.empresa")) {
                return Redirect::to('/');
            }

            $empresa = Empresa_Model::find($data[Empresa_Model::$id]);
            $empresa->{Empresa_Model::$idUsuarioEdicion} = Auth::id();
        }

        if ($request->hasFile(Empresa_Model::$idEmpresaImagen)) {

            $instancia = new Empresa_Imagen_Model();

            $imagenInsertada = Archivo_Controller::guardarArchivoServidor($request->file(Empresa_Model::$idEmpresaImagen), "empresa", "app\\Catalogos\\Empresa\\", $instancia, "", "");

            if ($imagenInsertada) {

                $empresa->{Empresa_Model::$idEmpresaImagen} = $imagenInsertada;

            } else {
                return Redirect::back()->with("error" , "Error al guardar la imagen");
            }

        }

        $empresa->{Empresa_Model::$clave} = $data[Empresa_Model::$clave];
        $empresa->{Empresa_Model::$calle} = $data[Empresa_Model::$calle];
        $empresa->{Empresa_Model::$rfc} = $data[Empresa_Model::$rfc];
        $empresa->{Empresa_Model::$numeroExterior} = $data[Empresa_Model::$numeroExterior];
        $empresa->{Empresa_Model::$correo} = $data[Empresa_Model::$correo];
        $empresa->{Empresa_Model::$numeroInterior} = $data[Empresa_Model::$numeroInterior];
        $empresa->{Empresa_Model::$razonSocial} = $data[Empresa_Model::$razonSocial];
        $empresa->{Empresa_Model::$colonia} = $data[Empresa_Model::$colonia];
        $empresa->{Empresa_Model::$cp} = $data[Empresa_Model::$cp];
        $empresa->{Empresa_Model::$tipoNomina} = $data[Empresa_Model::$tipoNomina];
        if (isset($data[Empresa_Model::$patron])) {
            if ($data[Empresa_Model::$patron] == "on") {
                $empresa->{Empresa_Model::$patron} = 1;
            } else {
                $empresa->{Empresa_Model::$patron} = 0;
            }
        }else{
            $empresa->{Empresa_Model::$operacion} = 0;
        }
        if (isset($data[Empresa_Model::$operacion])) {
            if ($data[Empresa_Model::$operacion] == "on") {
                $empresa->{Empresa_Model::$operacion} = 1;
            } else {
                $empresa->{Empresa_Model::$operacion} = 0;
            }
        }else{
            $empresa->{Empresa_Model::$operacion} = 0;
        }

        $empresa->{Empresa_Model::$idMunicipio} = $data[Empresa_Model::$idMunicipio]; 
        
        if($empresa->save()){

            return Redirect::back()->with("success" , "Se guardo la empresa correctamente");

        }else{

            return Redirect::back()->with("error" , "No se pudo guardar el empleado correctamente");

        }

    }

    /**
     * Función para borrar un registro en el Catálogo de Empresa (Borrado lógico)
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.empresa")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();

        $empresa = Empresa_Model::find($data[Empresa_Model::$id]);
        $imagen = Empresa_Imagen_Model::find($empresa->{Empresa_Model::$idEmpresaImagen});

        if ($imagen) {
            if (Archivo_Controller::borrar('empresa', $imagen->{Empresa_Imagen_Model::$uuid}, $imagen->{Empresa_Imagen_Model::$extension})) {
                $imagen->delete();
            }
        }

        if ($empresa->delete()) {
            return "Borrado correctamente.";
        } else {
            return "Problemas al borrar.";
        }

    }

}
