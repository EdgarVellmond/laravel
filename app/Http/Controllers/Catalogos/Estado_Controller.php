<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Empleado_Model;
use App\Models\Catalogos\Estado_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Estado_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de estado
     *
     * @author Oscar Vargas
     * @return Vista index del estado
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.estado")) {
            return Redirect::to('/');
        }

        //Obtener lista de Estados
        $arrayEstado = Estado_Model::select(Estado_Model::$id,
            Estado_Model::$nombre)->get();

        return view('Catalogos.Estado.index', compact('arrayEstado'));

    }

    /**
     * Metodo que sirve para guardar o editar un estado
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.estado")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Estado_Model::$id] == "") {

            $estado = new Estado_Model();
            $estado->id_usuario_creacion = Auth::id();

        } else {

            $estado = Estado_Model::find($data[Estado_Model::$id]);
            $estado->id_usuario_edicion = Auth::id();

        }

        $estado->{Estado_Model::$nombre} = $data[Estado_Model::$nombre];

        if ($estado->save()) {
            return "Se guardo correctamente el Estado";
        } else {
            return "No se guardo exitosamente el Estado";
        }

    }

    /**
     * Metodo que sirve para obtener un Estado
     *
     * @param POST id_estado identificador unico
     * @author Oscar Vargas
     * @return json con el estado solicitado
     */
    public function obtenerEstado()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.estado")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Estado_Model::$id];

        $estado = Estado_Model::FindOrFail($id);

        return $estado;

    }

    /**
     * Metodo que sirve para eliminar un Estado
     *
     * @param POST id_estado identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarEstado()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.estado")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Estado_Model::$id];
        $estado = Estado_Model::find($id);

        IF ($estado->delete()) {
            return "Se eliminó correctamente el Estado";
        } else {
            return "No se eliminó exitosamente el Estado";
        }

    }

}