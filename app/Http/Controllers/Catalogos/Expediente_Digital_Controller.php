<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Categoria_Expediente_Digital_Model;
use App\Models\Catalogos\Expediente_Digital_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Expediente_Digital_Controller extends Controller
{

    const EQUALS = "=";
    const PUNTO = ".";

    /**
     * Retorna la vista del index del catálogo del Expediente Digital
     * @author Erik Villarreal
     * @return view #Vista index del catálogo del Expediente Digital
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.catalogos.expediente_digital")) {
            return Redirect::to('/');
        }

        $arrayExpedienteDigital = Expediente_Digital_Model::select(Expediente_Digital_Model::$tabla . self::PUNTO . Expediente_Digital_Model::$id,
            Expediente_Digital_Model::$tabla . self::PUNTO . Expediente_Digital_Model::$nombre,
            //id del Área usado como nombre para diferenciarlo del nombre del Expediente Digital
            DB::raw(Categoria_Expediente_Digital_Model::$tabla . self::PUNTO . Categoria_Expediente_Digital_Model::$nombre . " as " . Categoria_Expediente_Digital_Model::$id))
            ->join(Categoria_Expediente_Digital_Model::$tabla,
                Expediente_Digital_Model::$tabla . self::PUNTO . Expediente_Digital_Model::$idCategoriaExpedienteDigital,
                self::EQUALS,
                Categoria_Expediente_Digital_Model::$tabla . self::PUNTO . Categoria_Expediente_Digital_Model::$id)
            ->get();

        $categoriaExpedienteDigital = Categoria_Expediente_Digital_Model::lists(Categoria_Expediente_Digital_Model::$nombre, Categoria_Expediente_Digital_Model::$id);

        return view('Catalogos.Expediente_Digital.index', compact('arrayExpedienteDigital', 'categoriaExpedienteDigital'));

    }

    /**
     * Función para crear o editar un registro en el Catálogo de Expediente Digital
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Expediente_Digital_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.expediente_digital")) {
                return "No tienes permiso para agregar.";
            }

            $expedienteDigital = new Expediente_Digital_Model();
            $expedienteDigital->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.expediente_digital")) {
                return "No tienes permiso para editar.";
            }

            $expedienteDigital = Expediente_Digital_Model::find($data[Expediente_Digital_Model::$id]);
            $expedienteDigital->id_usuario_edicion = Auth::id();

        }

        $expedienteDigital->{Expediente_Digital_Model::$nombre} = $data[Expediente_Digital_Model::$nombre];
        $expedienteDigital->{Expediente_Digital_Model::$idCategoriaExpedienteDigital} = $data[Categoria_Expediente_Digital_Model::$id];

        if ($expedienteDigital->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el área
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerExpedienteDigitalEditar()
    {

        $data = request()->all();
        $id = $data[Expediente_Digital_Model::$id];

        $expedienteDigital = Expediente_Digital_Model::FindOrFail($id);

        return $expedienteDigital;

    }

    /**
     * Metodo que sirve para obtener el expediente digital de cierta Categoría
     * @param POST id del estado
     * @author Oscar Vargas
     * @return mixed
     */
    public function obtenerExpedienteDigital()
    {
        $data = request()->all();

        //Obtener lista de Expediente digital
        $arrayExpedienteDigital = Expediente_Digital_Model::where(Expediente_Digital_Model::$idCategoriaExpedienteDigital, $data[Expediente_Digital_Model::$idCategoriaExpedienteDigital])->lists(Expediente_Digital_Model::$nombre, Expediente_Digital_Model::$id);

        return $arrayExpedienteDigital;
    }

    /**
     * Función para eliminar el área
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.expediente_digital")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Expediente_Digital_Model::$id];
        $expedienteDigital = Expediente_Digital_Model::find($id);

        IF ($expedienteDigital->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}