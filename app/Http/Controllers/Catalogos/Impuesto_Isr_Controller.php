<?php

namespace App\Http\Controllers\Catalogos;

use App\Models\Catalogos\Impuesto_Isr_Model;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Impuesto_Isr_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de impuesto isr
     *
     * @author Oscar Vargas
     * @return Vista index del impuesto isr
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.impuesto_isr")) {
            return Redirect::to('/');
        }

        //Obtener lista de impuesto isr
        $arrayImpuesto = Impuesto_Isr_Model::select(Impuesto_Isr_Model::$id,
            Impuesto_Isr_Model::$limiteInferior,
            Impuesto_Isr_Model::$limiteSuperior,
            Impuesto_Isr_Model::$cuotaFija,
            Impuesto_Isr_Model::$porcentaje,
            Impuesto_Isr_Model::$tipoNomina)->get();

        return view('Catalogos.Impuesto_Isr.index', compact('arrayImpuesto'));

    }

    /**
     * Metodo que sirve para guardar o editar un impuesto isr
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.impuesto_isr")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Impuesto_Isr_Model::$id] == "") {

            $impuesto = new Impuesto_Isr_Model();
            $impuesto->id_usuario_creacion = Auth::id();

        } else {

            $impuesto = Impuesto_Isr_Model::find($data[Impuesto_Isr_Model::$id]);
            $impuesto->id_usuario_edicion = Auth::id();

        }

        $impuesto->{Impuesto_Isr_Model::$limiteInferior} = $data[Impuesto_Isr_Model::$limiteInferior];
        $impuesto->{Impuesto_Isr_Model::$limiteSuperior} = $data[Impuesto_Isr_Model::$limiteSuperior];
        $impuesto->{Impuesto_Isr_Model::$cuotaFija} = $data[Impuesto_Isr_Model::$cuotaFija];
        $impuesto->{Impuesto_Isr_Model::$porcentaje} = $data[Impuesto_Isr_Model::$porcentaje];
        $impuesto->{Impuesto_Isr_Model::$tipoNomina} = $data[Impuesto_Isr_Model::$tipoNomina];

        if ($impuesto->save()) {
            return "Se guardo correctamente el Impuesto ISR";
        } else {
            return "No se guardo exitosamente el Impuesto ISR";
        }

    }

    /**
     * Metodo que sirve para obtener un Impuesto isr
     *
     * @param POST id_impuesto_isr identificador unico
     * @author Oscar Vargas
     * @return json con el impuesto isr solicitado
     */
    public function obtenerImpuestoIsr()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.impuesto_isr")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Impuesto_Isr_Model::$id];

        $impuesto = Impuesto_Isr_Model::FindOrFail($id);

        return $impuesto;

    }

    /**
     * Metodo que sirve para eliminar un Impuesto isr
     *
     * @param POST id_impuesto_isr identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarImpuestoIsr()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.impuesto_isr")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Impuesto_Isr_Model::$id];
        $impuesto = Impuesto_Isr_Model::find($id);

        IF ($impuesto->delete()) {
            return "Se eliminó correctamente el Impuesto ISR";
        } else {
            return "No se eliminó exitosamente el Impuesto ISR";
        }

    }
}
