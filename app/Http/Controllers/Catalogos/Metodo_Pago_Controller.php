<?php

namespace App\Http\Controllers\Catalogos;

use App\Models\Catalogos\Metodo_Pago_Model;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Metodo_Pago_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de metodo pago
     *
     * @author Oscar Vargas
     * @return Vista index del metodo pago
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.metodo_pago")) {
            return Redirect::to('/');
        }

        //Obtener lista de metodos de pago
        $arrayMetodoPago = Metodo_Pago_Model::select(Metodo_Pago_Model::$id,
            Metodo_Pago_Model::$nombre,
            Metodo_Pago_Model::$tipoPago,
            Metodo_Pago_Model::$formaPago)
            ->get();

        return view('Catalogos.Metodo_Pago.index', compact('arrayMetodoPago'));

    }

    /**
     * Metodo que sirve para guardar o editar un metodo pago
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {


        $data = request()->all();

        if ($data[Metodo_Pago_Model::$id] == "") {

            //Permiso de Despliegue de este Modulo
            if (!Helpers::get_permiso("alta.catalogos.metodo_pago")) {
                return "No tienes permisos para agregar";
            }

            $metodoPago = new Metodo_Pago_Model();
            $metodoPago->id_usuario_creacion = Auth::id();

        } else {

            //Permiso de Despliegue de este Modulo
            if (!Helpers::get_permiso("edicion.catalogos.metodo_pago")) {
                return "No tienes permisos para editar";
            }

            $metodoPago = Metodo_Pago_Model::find($data[Metodo_Pago_Model::$id]);
            $metodoPago->id_usuario_edicion = Auth::id();

        }

        $metodoPago->{Metodo_Pago_Model::$nombre} = $data[Metodo_Pago_Model::$nombre];
        $metodoPago->{Metodo_Pago_Model::$tipoPago} = $data[Metodo_Pago_Model::$tipoPago];
        $metodoPago->{Metodo_Pago_Model::$formaPago} = $data[Metodo_Pago_Model::$formaPago];

        if ($metodoPago->save()) {
            return "Se guardo correctamente el Método de Pago";
        } else {
            return "No se guardo exitosamente el Método de Pago";
        }

    }

    /**
     * Metodo que sirve para eliminar un metodo de pago
     *
     * @param POST id_metodo_pago identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.metodo_pago")) {
            return "No tienes permisos para eliminar";
        }

        $data = request()->all();
        $id = $data[Metodo_Pago_Model::$id];
        $metodoPago = Metodo_Pago_Model::find($id);

        IF ($metodoPago->delete()) {
            return "Se eliminó correctamente el Método de Pago";
        } else {
            return "No se eliminó exitosamente el Método de Pago";
        }

    }

    /**
     * Metodo que sirve para obtener un metodo de pago
     *
     * @param POST id_metodo_pago identificador unico
     * @author Oscar Vargas
     * @return json con el metodo de pago solicitado
     */
    public function obtenerMetodoPago()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.metodo_pago")) {
            return "No tienes permisos para editar";
        }

        $data = request()->all();
        $id = $data[Metodo_Pago_Model::$id];

        $metodoPago = Metodo_Pago_Model::FindOrFail($id);

        return $metodoPago;

    }


}
