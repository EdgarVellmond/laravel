<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Municipio_Model;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;


class Municipio_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de municipio
     *
     * @author Oscar Vargas
     * @return Vista index del municipio
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.municipio")) {
            return Redirect::to('/');
        }

        //Obtener lista de municipio
        $arrayMunicipio = Municipio_Model::select(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id,
            Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre,
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . " as nombreEstado"))
            ->leftjoin(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                "=",
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)->get();

        //Obtener lista de Estados
        $arrayEstado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);

        return view('Catalogos.Municipio.index', compact('arrayMunicipio', 'arrayEstado'));

    }

    /**
     * Metodo que sirve para llenar un select de municipios de cierto estado
     * @param POST id del estado
     * @author Oscar Vargas
     * @return mixed
     */
    public function obtenerMunicipios()
    {
        $data = request()->all();

        //Obtener lista de Municipios
        $arrayMunicipio = Municipio_Model::where(Municipio_Model::$idEstado, $data[Municipio_Model::$idEstado])->lists(Municipio_Model::$nombre, Municipio_Model::$id);

        return $arrayMunicipio;
    }

    /**
     * Metodo que sirve para guardar o editar un municipio
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.municipio")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Municipio_Model::$id] == "") {

            $municipio = new Municipio_Model();
            $municipio->id_usuario_creacion = Auth::id();

        } else {

            $municipio = Municipio_Model::find($data[Municipio_Model::$id]);
            $municipio->id_usuario_edicion = Auth::id();

        }

        $municipio->{Municipio_Model::$nombre} = $data[Municipio_Model::$nombre];
        $municipio->{Municipio_Model::$idEstado} = $data[Municipio_Model::$idEstado];

        if ($municipio->save()) {
            return "Se guardo correctamente el Municipio";
        } else {
            return "No se guardo exitosamente el Municipio";
        }

    }

    /**
     * Metodo que sirve para obtener un Municipio
     *
     * @param POST id_municipio identificador unico
     * @author Oscar Vargas
     * @return json con el municipio solicitado
     */
    public function obtenerMunicipioEditar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.municipio")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Municipio_Model::$id];

        $municipio = Municipio_Model::FindOrFail($id);

        return $municipio;

    }

    /**
     * Metodo que sirve para eliminar un Municipio
     *
     * @param POST id_municipio identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarMunicipio()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.municipio")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Municipio_Model::$id];
        $municipio = Municipio_Model::find($id);

        if ($municipio->delete()) {
            return "Se eliminó correctamente el Municipio";
        } else {
            return "No se eliminó exitosamente el Municipio";
        }

    }


}