<?php

namespace App\Http\Controllers\Catalogos;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Genericos\Pdf_Controller;
use App\Models\Catalogos\Plantilla_Model;
use App\Models\Catalogos\Tipo_Plantilla_Model;
use App\Models\Sistema\Variables_Sistema_Model;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Plantilla_Controller extends Controller
{

    const PUNTO = ".";
    const EQUALS = "=";

    /**
     * Muestra la Pantalla Principal
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.plantilla")) {
            return Redirect::to('/');
        }

        $plantilla = Plantilla_Model::select(
                                        Plantilla_Model::$tabla.self::PUNTO.Plantilla_Model::$id,
                                        Plantilla_Model::$tabla.self::PUNTO.Plantilla_Model::$nombre,
                                        DB::raw(Tipo_Plantilla_Model::$tabla.self::PUNTO.Tipo_Plantilla_Model::$nombre . " as " . Tipo_Plantilla_Model::$id)
                                        )

                                        ->join(Tipo_Plantilla_Model::$tabla,
                                            Plantilla_Model::$tabla.self::PUNTO.Plantilla_Model::$idTipoPlantilla,
                                            self::EQUALS,
                                            Tipo_Plantilla_Model::$tabla.self::PUNTO.Tipo_Plantilla_Model::$id
                                            )

                                        ->get();

        $tipoPlantilla = Tipo_Plantilla_Model::lists(Tipo_Plantilla_Model::$nombre, Tipo_Plantilla_Model::$id);

        return view('Catalogos.Plantilla.index', compact("permiso", "plantilla", "tipoPlantilla"));
    }

    /**
     * Muestra la Pantalla para crear una nueva Plantilla
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function nuevaPlantilla()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.plantilla")) {
            return Redirect::to('/');
        }

        $tipoPlantilla = Tipo_Plantilla_Model::lists(Tipo_Plantilla_Model::$nombre, Tipo_Plantilla_Model::$id);

        $arrayTablas = Variables_Sistema_Model::where(Variables_Sistema_Model::$idCategoria, 2)
                                                        ->groupBy(Variables_Sistema_Model::$aliasTablaOrigen)
                                                        ->lists(Variables_Sistema_Model::$aliasTablaOrigen, Variables_Sistema_Model::$aliasTablaOrigen)
                                                        ->toArray();

        $contenido = "";

        return view('Catalogos.Plantilla.nuevaPlantilla', compact("permiso", "tipoPlantilla", "arrayTablas", "contenido"));
    }

    /**
     * Muestra la Pantalla para editar una Plantilla
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function editarPlantilla()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.plantilla")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        $idPlantilla = $data[Plantilla_Model::$id];

        $tipoPlantilla = Tipo_Plantilla_Model::lists(Tipo_Plantilla_Model::$nombre, Tipo_Plantilla_Model::$id);

        $arrayTablas = Variables_Sistema_Model::where(Variables_Sistema_Model::$idCategoria, 2)
                                                        ->groupBy(Variables_Sistema_Model::$aliasTablaOrigen)
                                                        ->lists(Variables_Sistema_Model::$aliasTablaOrigen, Variables_Sistema_Model::$aliasTablaOrigen)
                                                        ->toArray();

        return view('Catalogos.Plantilla.nuevaPlantilla', compact("permiso", "tipoPlantilla", "arrayTablas", "contenido", "idPlantilla"));
    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Plantilla
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Plantilla_Model::$id] == "") {
            
            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.plantilla")) {
                return "No tienes permiso para agregar.";
            }

            $plantilla = new Plantilla_Model();
            $plantilla->id_usuario_creacion = Auth::id();
            $plantilla->{Plantilla_Model::$contenido} = $data[Plantilla_Model::$contenido];

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.plantilla")) {
                return "No tienes permiso para editar.";
            }

            $plantilla = Plantilla_Model::find($data[Plantilla_Model::$id]);
            $plantilla->id_usuario_edicion = Auth::id();

        }

        $plantilla->{Plantilla_Model::$nombre} = $data[Plantilla_Model::$nombre];
        $plantilla->{Plantilla_Model::$idTipoPlantilla} = $data[Plantilla_Model::$idTipoPlantilla];


        if ($plantilla->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el Plantilla
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerPlantillaEditar()
    {

        $data = request()->all();
        $id = $data[Plantilla_Model::$id];

        $plantilla = Plantilla_Model::select(Plantilla_Model::$nombre, Plantilla_Model::$idTipoPlantilla)
                                    ->where(Plantilla_Model::$id, $id)
                                    ->get();

        return $plantilla;

    }

    /**
     * Función para obtener el Plantilla
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerPlantilla()
    {

        $data = request()->all();
        $id = $data[Plantilla_Model::$id];

        $plantilla = Plantilla_Model::select(Plantilla_Model::$nombre, Plantilla_Model::$idTipoPlantilla, Plantilla_Model::$contenido)
                                    ->where(Plantilla_Model::$id, $id)
                                    ->get();

        return $plantilla;

    }

    /**
     * Función para obtener el Plantilla
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function vistaPrevia($id)
    {

        return Pdf_Controller::generarPlantillaPdf($id);

    }

    /**
     * Función para eliminar el Plantilla
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.plantilla")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Plantilla_Model::$id];
        $plantilla = Plantilla_Model::find($id);

        IF ($plantilla->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

    /**
     * Genera la plantilla de Plantilla como documento
     * @author Erik Villarreal
     * @return PDF view
     */
    public static function generarPlantillaPdf($idPlantilla)
    {
        $idTemporalEmpleado=5;
        $plantilla = Plantilla_Model::find($idPlantilla);

        $contenido = Pdf_Controller::reemplazarEnContenido($plantilla->{Plantilla_Model::$contenido}, $idTemporalEmpleado, "p");

        $contenido = Pdf_Controller::reemplazarEnContenido($contenido, 0, "e");

        $pdf = \PDF::loadView('Genericos.vacio',compact('contenido'))
            ->setPaper('Letter')
            ->setOption('margin-top', '40mm')
            ->setOption('margin-bottom', '10mm')
            //->setOrientation('landscape')
            //->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-left',utf8_decode('Usuario: '.Auth::user()->nombre))
            ->setOption('footer-right',utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size','10');

        return  $pdf->stream('Contrato.pdf');

    }

}
