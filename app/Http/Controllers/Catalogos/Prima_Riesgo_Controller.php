<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Models\Catalogos\Prima_Riesgo_Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\Helpers;

class Prima_Riesgo_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de prima riesgo
     *
     * @author Oscar Vargas
     * @return Vista index del estado
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.prima_riesgo")) {
            return Redirect::to('/');
        }

        //Obtener lista de Estados
        $arrayPrima = Prima_Riesgo_Model::select(Prima_Riesgo_Model::$id,
            Prima_Riesgo_Model::$nombre,
            Prima_Riesgo_Model::$valor)->get();

        return view('Catalogos.Prima_Riesgo.index', compact('arrayPrima'));

    }

    /**
     * Metodo que sirve para guardar o editar una prima de riesgo
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.prima_riesgo")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Prima_Riesgo_Model::$id] == "") {

            $primaRiesgo = new Prima_Riesgo_Model();
            $primaRiesgo->id_usuario_creacion = Auth::id();

        } else {

            $primaRiesgo = Prima_Riesgo_Model::find($data[Prima_Riesgo_Model::$id]);
            $primaRiesgo->id_usuario_edicion = Auth::id();

        }

        $primaRiesgo->{Prima_Riesgo_Model::$nombre} = $data[Prima_Riesgo_Model::$nombre];
        $primaRiesgo->{Prima_Riesgo_Model::$valor} = $data[Prima_Riesgo_Model::$valor];

        if ($primaRiesgo->save()) {
            return "Se guardo correctamente la Prima de Riesgo";
        } else {
            return "No se guardo exitosamente la Prima de Riesgo";
        }

    }

    /**
     * Metodo que sirve para obtener una Prima de Riesgo
     *
     * @param POST id_prima_riesgo identificador unico
     * @author Oscar Vargas
     * @return json con la prima de riesgo solicitado
     */
    public function obtenerPrimaRiesgo()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.prima_riesgo")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Prima_Riesgo_Model::$id];

        $primaRiesgo = Prima_Riesgo_Model::FindOrFail($id);

        return $primaRiesgo;

    }

    /**
     * Metodo que sirve para eliminar una prima de riesgo
     *
     * @param POST id_prima_riesgo identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarPrimaRiesgo()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.prima_riesgo")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Prima_Riesgo_Model::$id];
        $primaRiesgo = Prima_Riesgo_Model::find($id);

        IF ($primaRiesgo->delete()) {
            return "Se eliminó correctamente la Prima de Riesgo";
        } else {
            return "No se eliminó exitosamente la Prima de Riesgo";
        }

    }

}
