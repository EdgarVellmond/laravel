<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Banco_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Metodo_Pago_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Proveedor_Model;
use App\Models\Catalogos\Tipo_Persona_Model;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class Proveedor_Controller extends Controller
{
    const PUNTO = ".";

    /**
     * Index del catalogo de proveedores
     *
     * @author Oscar Vargas
     * @return Vista index del proveedor
     */
    public function index()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.proveedor")) {
            return Redirect::to('/');
        }

        //Obtener lista de Proveedores
        $arrayProveedor = Proveedor_Model::select(Proveedor_Model::$numeroProveedor, Proveedor_Model::$nombre,
            Proveedor_Model::$apellidoPaterno, Proveedor_Model::$apellidoMaterno, Proveedor_Model::$rfc,
            Proveedor_Model::$telefono, Proveedor_Model::$correo, Proveedor_Model::$id)
            ->get();

        //Obtener lista de Estados
        $arrayEstado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);

        //Obtener lista de Bancos
        $arrayBanco = Banco_Model::lists(Banco_Model::$nombre, Banco_Model::$id);

        //Obtener lista de Metodos de pago
        $arrayMetodoPago = Metodo_Pago_Model::lists(Metodo_Pago_Model::$nombre, Metodo_Pago_Model::$id);

        return view('Catalogos.Proveedor.index', compact('arrayProveedor', 'arrayTipoPersona', 'arrayEstado', 'arrayBanco', 'arrayMetodoPago'));

    }

    /**
     * Metodo que sirve para guardar o editar un proveedor
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Altas de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.proveedor")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data['id_proveedor'] == "") {

            $proveedor = new Proveedor_Model();

        } else {

            $proveedor = Proveedor_Model::find($data[Proveedor_Model::$id]);

        }

        $proveedor->{Proveedor_Model::$nombre} = $data[Proveedor_Model::$nombre];
        $proveedor->{Proveedor_Model::$apellidoPaterno} = $data[Proveedor_Model::$apellidoPaterno];
        $proveedor->{Proveedor_Model::$apellidoMaterno} = $data[Proveedor_Model::$apellidoMaterno];
        $proveedor->{Proveedor_Model::$rfc} = $data[Proveedor_Model::$rfc];
        if (isset($data[Proveedor_Model::$credito])) {
            $proveedor->{Proveedor_Model::$credito} = $data[Proveedor_Model::$credito];
        } else {
            $proveedor->{Proveedor_Model::$credito} = 0;
        }
        $proveedor->{Proveedor_Model::$diasCredito} = $data[Proveedor_Model::$diasCredito];
        $proveedor->{Proveedor_Model::$limiteCredito} = $data[Proveedor_Model::$limiteCredito];
        $proveedor->{Proveedor_Model::$telefono} = $data[Proveedor_Model::$telefono];
        $proveedor->{Proveedor_Model::$correo} = $data[Proveedor_Model::$correo];
        $proveedor->{Proveedor_Model::$calle} = $data[Proveedor_Model::$calle];
        $proveedor->{Proveedor_Model::$colonia} = $data[Proveedor_Model::$colonia];
        $proveedor->{Proveedor_Model::$numeroInterior} = $data[Proveedor_Model::$numeroInterior];
        $proveedor->{Proveedor_Model::$numeroExterior} = $data[Proveedor_Model::$numeroExterior];
        $proveedor->{Proveedor_Model::$cp} = $data[Proveedor_Model::$cp];
        $proveedor->{Proveedor_Model::$tipoPersona} = $data[Proveedor_Model::$tipoPersona];
        $proveedor->{Proveedor_Model::$idMetodoPago} = $data[Proveedor_Model::$idMetodoPago];
        $proveedor->{Proveedor_Model::$idBanco} = $data[Proveedor_Model::$idBanco];
        $proveedor->{Proveedor_Model::$idMunicipio} = $data[Proveedor_Model::$idMunicipio];


        IF ($proveedor->save()) {
            return "Se guardo correctamente el Proveedor";
        } else {
            return "No se guardo exitosamente el Proveedor";
        }

    }

    /**
     * Metodo que sirve para enviar la informacion de la direccion de un Proveedor
     *
     * @param POST id_proveedor identificador unico
     * @author Oscar Vargas
     * @return json con la informacion de domicilio de proveedor
     */
    public function obtenerDomicilioProveedor()
    {

        $data = request()->all();

        $arrayDomicilio = Proveedor_Model::select(Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$calle,
            Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$colonia,
            Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$numeroExterior,
            Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$numeroInterior,
            Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$cp,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . ' as nombreMunicipio'),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . ' as nombreEstado'))
            ->join(Municipio_Model::$tabla,
                Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                "=",
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->where(Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$id, $data['id_proveedor'])->get();

        return $arrayDomicilio;

    }

    /**
     * Metodo que sirve para enviar la informacion del credito y forma de pago de un Proveedor
     *
     * @param POST id_proveedor identificador unico
     * @author Oscar Vargas
     * @return json con la informacion de credito y forma de pago de proveedor
     */
    public function obtenerCreditoProveedor()
    {

        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("credito.catalogos.proveedor")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        $arrayCredito = Proveedor_Model::select(Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$credito,
            Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$diasCredito,
            Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$limiteCredito,
            DB::raw(Metodo_Pago_Model::$tabla . self::PUNTO . Metodo_Pago_Model::$nombre . ' as nombreMetodo'),
            DB::raw(Banco_Model::$tabla . self::PUNTO . Banco_Model::$nombre . ' as nombreBanco'))
            ->join(Metodo_Pago_Model::$tabla,
                Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$idMetodoPago,
                "=",
                Metodo_Pago_Model::$tabla . self::PUNTO . Metodo_Pago_Model::$id)
            ->join(Banco_Model::$tabla,
                Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$idBanco,
                "=",
                Banco_Model::$tabla . self::PUNTO . Banco_Model::$id)
            ->where(Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$id, $data['id_proveedor'])->get();

        return $arrayCredito;

    }

    /**
     * Metodo que sirve para eliminar un Proveedor
     *
     * @param POST id_proveedor identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarProveedor()
    {

        //Permiso de Bajas de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.proveedor")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Proveedor_Model::$id];
        $proveedor = Proveedor_Model::find($id);

        IF ($proveedor->delete()) {
            return "Se eliminó correctamente el Proveedor";
        } else {
            return "No se eliminó exitosamente el Proveedor";
        }

    }

    /**
     * Metodo que sirve para obtener un Proveedor
     *
     * @param POST id_proveedor identificador unico
     * @author Oscar Vargas
     * @return json con el proveedor solicitado
     */
    public function obtenerProveedor()
    {

        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.proveedor")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Proveedor_Model::$id];
        $proveedor = Proveedor_Model::where(Proveedor_Model::$id, $id)
            ->join(Municipio_Model::$tabla,
                Proveedor_Model::$tabla . self::PUNTO . Proveedor_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)->get();

        return $proveedor;

    }

}