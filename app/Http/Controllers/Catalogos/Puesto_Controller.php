<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Puesto_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Puesto_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Puestos
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Puestos
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.puesto")) {
            return Redirect::to('/');
        }

        $arrayPuesto = Puesto_Model::select(Puesto_Model::$id, Puesto_Model::$nombre)->get();

        return view('Catalogos.Puesto.index', compact('arrayPuesto'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Puestos
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Puesto_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.puesto")) {
                return "No tienes permiso para agregar.";
            }

            $puesto = new Puesto_Model();
            $puesto->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.puesto")) {
                return "No tienes permiso para editar.";
            }

            $puesto = Puesto_Model::find($data[Puesto_Model::$id]);
            $puesto->id_usuario_edicion = Auth::id();

        }

        $puesto->{Puesto_Model::$nombre} = $data[Puesto_Model::$nombre];

        if ($puesto->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el puesto
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerPuestoEditar()
    {

        $data = request()->all();
        $id = $data[Puesto_Model::$id];

        $puesto = Puesto_Model::FindOrFail($id);

        return $puesto;

    }

    /**
     * Función para eliminar el puesto
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.puesto")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Puesto_Model::$id];
        $puesto = Puesto_Model::find($id);

        IF ($puesto->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}