<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Rol_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Rol_Controller extends Controller
{


    /**
     * Retorna la vista del index del catálogo de Roles
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Roles
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.rol")) {
            return Redirect::to('/');
        }

        $arrayRol = Rol_Model::select(Rol_Model::$id, Rol_Model::$nombre)->get();

        return view('Catalogos.Rol.index', compact('arrayRol'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Roles
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Rol_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.rol")) {
                return "No tienes permiso para agregar.";
            }

            $rol = new Rol_Model();
            $rol->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.rol")) {
                return "No tienes permiso para editar.";
            }

            $rol = Rol_Model::find($data[Rol_Model::$id]);
            $rol->id_usuario_edicion = Auth::id();

        }

        $rol->{Rol_Model::$nombre} = $data[Rol_Model::$nombre];

        if ($rol->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el rol
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerRolEditar()
    {

        $data = request()->all();
        $id = $data[Rol_Model::$id];

        $rol = Rol_Model::FindOrFail($id);

        return $rol;

    }

    /**
     * Función para eliminar el rol
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.rol")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Rol_Model::$id];
        $rol = Rol_Model::find($id);

        if($rol->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}