<?php

namespace App\Http\Controllers\Catalogos;

use App\Models\Catalogos\Salario_Minimo_Model;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Salario_Minimo_Controller extends Controller
{

    /**
     * Index del catalogo de salario minimo
     *
     * @author Oscar Vargas
     * @return Vista index del salario minimo
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.salario_minimo")) {
            return Redirect::to('/');
        }

        //Obtener lista de Salarios Minimos
        $arraySalarioMinimo = Salario_Minimo_Model::select(Salario_Minimo_Model::$id,
            Salario_Minimo_Model::$valor,
            Salario_Minimo_Model::$anio)->get();

        return view('Catalogos.Salario_Minimo.index', compact('arraySalarioMinimo'));

    }

    /**
     * Metodo que sirve para guardar o editar un salario minimo
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.salario_minimo")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Salario_Minimo_Model::$id] == "") {

            $salario = new Salario_Minimo_Model();
            $salario->id_usuario_creacion = Auth::id();

        } else {

            $salario = Salario_Minimo_Model::find($data[Salario_Minimo_Model::$id]);
            $salario->id_usuario_edicion = Auth::id();

        }

        $salario->{Salario_Minimo_Model::$valor} = $data[Salario_Minimo_Model::$valor];
        $salario->{Salario_Minimo_Model::$anio} = $data[Salario_Minimo_Model::$anio];

        if ($salario->save()) {
            return "Se guardo correctamente el Salario Mínimo";
        } else {
            return "No se guardo exitosamente el Salario Mínimo";
        }

    }

    /**
     * Metodo que sirve para obtener un Salario Minimo
     *
     * @param POST id_salario_minimo identificador unico
     * @author Oscar Vargas
     * @return json con el salario minimo solicitado
     */
    public function obtenerSalarioMinimo()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.salario_minimo")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Salario_Minimo_Model::$id];

        $salario = Salario_Minimo_Model::FindOrFail($id);

        return $salario;

    }

    /**
     * Metodo que sirve para eliminar un Salario minimo
     *
     * @param POST id_salario_minimo identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarSalarioMinimo()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.salario_minimo")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Salario_Minimo_Model::$id];
        $salario = Salario_Minimo_Model::find($id);

        IF ($salario->delete()) {
            return "Se eliminó correctamente el Salario Mínimo";
        } else {
            return "No se eliminó exitosamente el Salario Mínimo";
        }

    }

}
