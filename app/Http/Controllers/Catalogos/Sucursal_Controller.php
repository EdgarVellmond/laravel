<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Sucursal_Model;
use Illuminate\Support\Facades\DB;

class Sucursal_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de sucursal
     *
     * @author Oscar Vargas
     * @return Vista index del sucursal
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.sucursal")) {
            return Redirect::to('/');
        }

        //Obtener lista de Sucursales
        $arraySucursal = Sucursal_Model::select(Sucursal_Model::$id,
            Sucursal_Model::$nombre,
            Sucursal_Model::$clave,
            Sucursal_Model::$correo,
            Sucursal_Model::$telefono)
            ->get();

        //Obtener lista de Estados
        $arrayEstado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);

        //Obtener lista de Empresas
        $arrayEmpresa = Empresa_Model::where(Empresa_Model::$operacion, 1)->lists(Empresa_Model::$razonSocial, Empresa_Model::$id);

        return view('Catalogos.Sucursal.index', compact('arraySucursal', 'arrayEstado', 'arrayEmpresa'));

    }

    /**
     * Metodo que sirve para guardar o editar una sucursal
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.catalogos.sucursal")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data['id_sucursal'] == "") {

            $sucursal = new Sucursal_Model();
            $sucursal->{Sucursal_Model::$estatus} = 1;

        } else {

            $sucursal = Sucursal_Model::find($data[Sucursal_Model::$id]);

        }

        $sucursal->{Sucursal_Model::$nombre} = $data[Sucursal_Model::$nombre];
        $sucursal->{Sucursal_Model::$clave} = $data[Sucursal_Model::$clave];
        $sucursal->{Sucursal_Model::$correo} = $data[Sucursal_Model::$correo];
        $sucursal->{Sucursal_Model::$telefono} = $data[Sucursal_Model::$telefono];

        $sucursal->{Sucursal_Model::$calle} = $data[Sucursal_Model::$calle];
        $sucursal->{Sucursal_Model::$colonia} = $data[Sucursal_Model::$colonia];
        $sucursal->{Sucursal_Model::$numeroInterior} = $data[Sucursal_Model::$numeroInterior];
        $sucursal->{Sucursal_Model::$numeroExterior} = $data[Sucursal_Model::$numeroExterior];
        $sucursal->{Sucursal_Model::$cp} = $data[Sucursal_Model::$cp];
        $sucursal->{Sucursal_Model::$idEmpresa} = $data[Sucursal_Model::$idEmpresa];
        $sucursal->{Sucursal_Model::$idMunicipio} = $data[Sucursal_Model::$idMunicipio];

        IF ($sucursal->save()) {
            return "Se guardo correctamente la Sucursal";
        } else {
            return "No se guardo exitosamente la Sucursal";
        }

    }

    /**
     * Metodo que sirve para eliminar una Sucursal
     *
     * @param POST id_sucursal identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminarSucursal()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.catalogos.sucursal")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Sucursal_Model::$id];
        $sucursal = Sucursal_Model::find($id);

        IF ($sucursal->delete()) {
            return "Se eliminó correctamente la Sucursal";
        } else {
            return "No se eliminó exitosamente la Sucursal";
        }

    }

    /**
     * Metodo que sirve para enviar la informacion de la direccion de una Sucursal
     *
     * @param POST id_sucursal identificador unico
     * @author Oscar Vargas
     * @return json con la informacion de domicilio de una sucursal
     */
    public function obtenerDomicilioSucursal()
    {

        $data = request()->all();

        $arrayDomicilio = Sucursal_Model::select(Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$calle,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$colonia,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$numeroExterior,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$numeroInterior,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$cp,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . ' as nombreMunicipio'),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . ' as nombreEstado'))
            ->join(Municipio_Model::$tabla,
                Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                "=",
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->where(Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$id, $data['id_sucursal'])->get();

        return $arrayDomicilio;

    }

    /**
     * Metodo que sirve para obtener una Sucursal
     *
     * @param POST id_sucursal identificador unico
     * @author Oscar Vargas
     * @return json con la sucursal solicitado
     */
    public function obtenerSucursal()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.catalogos.sucursal")) {
            //return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Sucursal_Model::$id];

        $sucursal = Sucursal_Model::select(Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$nombre,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$clave,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$correo,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$telefono,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$calle,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$numeroInterior,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$numeroExterior,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$colonia,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$cp,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$idMunicipio,
            Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$idEmpresa,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado . ' as id_estado'),
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . ' as nombreMunicipio'),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . ' as nombreEstado'))
            ->where(Sucursal_Model::$id, $id)
            ->join(Municipio_Model::$tabla,
                Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                "=",
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)->get();

        return $sucursal;

    }
}
