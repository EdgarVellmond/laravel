<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Tipo_Plantilla_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Tipo_Plantilla_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de tipo_plantilla
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de tipo_plantilla
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.tipo_plantilla")) {
            return Redirect::to('/');
        }

        $arrayTipoPlantilla = Tipo_Plantilla_Model::select(Tipo_Plantilla_Model::$id, Tipo_Plantilla_Model::$nombre)->get();

        return view('Catalogos.Tipo_Plantilla.index', compact('arrayTipoPlantilla'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de tipo_plantilla
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Tipo_Plantilla_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.tipo_plantilla")) {
                return "No tienes permiso para agregar.";
            }

            $tipo_plantilla = new Tipo_Plantilla_Model();
            $tipo_plantilla->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.tipo_plantilla")) {
                return "No tienes permiso para editar.";
            }

            $tipo_plantilla = Tipo_Plantilla_Model::find($data[Tipo_Plantilla_Model::$id]);
            $tipo_plantilla->id_usuario_edicion = Auth::id();

        }

        $tipo_plantilla->{Tipo_Plantilla_Model::$nombre} = $data[Tipo_Plantilla_Model::$nombre];

        if ($tipo_plantilla->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el tipo_plantilla
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerTipoPlantillaEditar()
    {

        $data = request()->all();
        $id = $data[Tipo_Plantilla_Model::$id];

        $tipo_plantilla = Tipo_Plantilla_Model::FindOrFail($id);

        return $tipo_plantilla;

    }

    /**
     * Función para eliminar el tipo_plantilla
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.tipo_plantilla")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Tipo_Plantilla_Model::$id];
        $tipo_plantilla = Tipo_Plantilla_Model::find($id);

        IF ($tipo_plantilla->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}