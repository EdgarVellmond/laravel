<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Tipo_Solicitud_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Tipo_Solicitud_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Tipo de Solicitud
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Tipo de Solicitud
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.catalogos.tipo_solicitud")) {
            return Redirect::to('/');
        }

        $arrayTipoSolicitud = Tipo_Solicitud_Model::select(Tipo_Solicitud_Model::$id, Tipo_Solicitud_Model::$nombre)->get();

        return view('Catalogos.Tipo_Solicitud.index', compact('arrayTipoSolicitud'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Tipo de Solicitud
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Tipo_Solicitud_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.catalogos.tipo_solicitud")) {
                return "No tienes permiso para agregar.";
            }

            $tipo_solicitud = new Tipo_Solicitud_Model();
            $tipo_solicitud->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.catalogos.tipo_solicitud")) {
                return "No tienes permiso para editar.";
            }

            $tipo_solicitud = Tipo_Solicitud_Model::find($data[Tipo_Solicitud_Model::$id]);
            $tipo_solicitud->id_usuario_edicion = Auth::id();

        }

        $tipo_solicitud->{Tipo_Solicitud_Model::$nombre} = $data[Tipo_Solicitud_Model::$nombre];

        if ($tipo_solicitud->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener el tipo de solicitud
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerTipoSolicitudEditar()
    {

        $data = request()->all();
        $id = $data[Tipo_Solicitud_Model::$id];

        $tipo_solicitud = Tipo_Solicitud_Model::FindOrFail($id);

        return $tipo_solicitud;

    }

    /**
     * Función para eliminar el tipo de solicitud
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.catalogos.tipo_solicitud")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Tipo_Solicitud_Model::$id];
        $tipo_solicitud = Tipo_Solicitud_Model::find($id);

        IF ($tipo_solicitud->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}