<?php

namespace App\Http\Controllers\Genericos;


use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Empresa_Imagen_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class Archivo_Controller extends Controller
{
    const EQUALS = '=';
    const PUNTO = '.';

    /**
     * Función para guardar una archivo en el servidor
     * @author Erik Villarreal
     * @param  $archivo #archivo a subir
     * @param  $pathDisk #Es la ruta que usa Storage a la hora de guardar el archivo (Una palabra generalmente)
     * @param  $pathBD #Es la ruta que se guardará en la Base de datos de la ubicación del archivo
     * @param  $id #Es el id de la tabla a la que pertenece la imagen (Puede ser null)
     * @param  $campo #Es el nombre del campo id de la tabla a la que pertenece el archivo
     * @return boolean || objeto de Instancia
     */
    public static function guardarArchivoServidor($archivo, $pathDisk, $pathBD, $modelo, $id, $campo)
    {

        $fileUUID = Helpers::generate_uuid();
        $clientOriginalName = $archivo->getClientOriginalName();
        $mime_archivo = $archivo->getClientMimeType();
        $ext_archivo = $archivo->getClientOriginalExtension();

        if (Storage::disk($pathDisk)->put($fileUUID . '.' . $ext_archivo, File::get($archivo))) {
            return self::guardarInformacionArchivo($modelo, $clientOriginalName, $fileUUID, $mime_archivo, $ext_archivo, $pathBD, $id, $campo);
        } else {
            return false;
        }

    }

    /**
     * Función para guardar la información del archivo subido a servidor dentro de la BD
     * @author Erik Villarreal, 'onSubmit' => 'return guardarEmpresa()'
     * @param  $archivo #instancia del modelo
     * @param  $clienteOriginalName #Nombre original del archivo que se sube
     * @param  $fileUUID #Nombre generado como UUID
     * @param  $mime_archivo #mime del archivo
     * @param  $ext_archivo #extensión del archivo a subir
     * @param  $path #Es la ruta que se guardará en la Base de datos de la ubicación del archivo
     * @param  $id #Es el id de la tabla a la que pertenece la imagen (Puede ser null)
     * @param  $campo #Es el nombre del campo de la tabla a la que pertenece el archivo
     * @return boolean || objeto de instancia
     */
    public static function guardarInformacionArchivo($archivo, $clientOriginalName, $fileUUID, $mime_archivo, $ext_archivo, $path, $id, $campo)
    {

        if ($id <> "") {
            $archivo->{$campo} = $id;
        }

        $archivo->{$archivo::$mime} = $mime_archivo;
        $archivo->{$archivo::$ruta} = $path;
        $archivo->{$archivo::$nombre} = $clientOriginalName;
        $archivo->{$archivo::$extension} = $ext_archivo;
        $archivo->{$archivo::$uuid} = $fileUUID;

        if ($archivo->save()) {
            return $archivo->{$archivo::$id};
        } else {
            return false;
        }

    }

    /**
     * Función para borrar un archivo del servidor(Inrrecuperable)
     * @author Erik Villarreal
     * @param  $pathDisk #Es la ruta que usa Storage a la hora de guardar el archivo (Una palabra generalmente)
     * @param  $uuid #Nombre generado como UUID
     * @return boolean
     */
    public static function borrar($pathDisk, $uuid, $extension)
    {
        if (Storage::disk($pathDisk)->delete($uuid . '.' . $extension)) {
            return true;
        } else {
            return false;
        }

    }

}
