<?php

namespace App\Http\Controllers\Genericos;


use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Empresa_Imagen_Model;
use App\Models\Genericos\Documento_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class Documento_Controller extends Controller
{
    const EQUALS = '=';
    const PUNTO = '.';

    /**
     * Función para guardar una archivo del catálogo de Empresa
     * @author Erik Villarreal
     * @param  $archivo #archivo a subir
     * @param  $pathDisk #Es la ruta que usa Storage a la hora de guardar el archivo (Una palabra generalmente)
     * @param  $pathBD #Es la ruta que se guardará en la Base de datos de la ubicación del archivo
     * @param  $idPropietario #Es el id del propietario del documento
     * @param  $tabla #Nombre de la tabla a la que pertenece el propietario de la imagen
     * @param  $descripcion #Descripción del documento que se está subiendo
     * @param  $rol #Rol que puede tomar el documento, basado en el catálogo de rol
     * @return boolean || objeto de Instancia
     */
    public static function guardarDocumentoServidor($archivo, $pathDisk, $pathBD, $modelo, $idPropietario, $tabla, $descripcion)
    {

        $fileUUID = Helpers::generate_uuid();
        $clientOriginalName = $archivo->getClientOriginalName();
        $mime_archivo = $archivo->getClientMimeType();
        $ext_archivo = $archivo->getClientOriginalExtension();

        if (Storage::disk($pathDisk)->put($fileUUID . '.' . $ext_archivo, File::get($archivo))) {
            return self::guardarInformacionDocumento($modelo, $clientOriginalName, $fileUUID, $mime_archivo, $ext_archivo, $pathBD, $idPropietario, $tabla, $descripcion);
        } else {
            return false;
        }

    }

    /**
     * Función para guardar la información de  la archivo subida a servidor dentro de la BD
     * @author Erik Villarreal, 'onSubmit' => 'return guardarEmpresa()'
     * @param  $archivo #instancia del modelo
     * @param  $clienteOriginalName #Nombre original del archivo que se sube
     * @param  $fileUUID #Nombre generado como UUID
     * @param  $mime_archivo #mime del archivo
     * @param  $ext_archivo #extensión del archivo a subir
     * @param  $path #Es la ruta que se guardará en la Base de datos de la ubicación del archivo
     * @param  $id #Es el id de la tabla a la que pertenece la imagen (Puede ser null)
     * @param  $tabla #Es el nombre de la tabla a la que pertenece el archivo
     * @param  $descripcion #Descripción del documento que se está subiendo
     * @return boolean || objeto de instancia
     */
    public static function guardarInformacionDocumento($archivo, $clientOriginalName, $fileUUID, $mime_archivo, $ext_archivo, $path, $id, $tabla,$descripcion)
    {

        $archivo->{$archivo::$tablaOrigen} = $tabla;
        $archivo->{$archivo::$idPropietario} = $id;
        $archivo->{$archivo::$descripcion} = $descripcion;
        $archivo->{$archivo::$mime} = $mime_archivo;
        $archivo->{$archivo::$ruta} = $path;
        $archivo->{$archivo::$nombre} = $clientOriginalName;
        $archivo->{$archivo::$extension} = $ext_archivo;
        $archivo->{$archivo::$uuid} = $fileUUID;

        if ($archivo->save()) {
            return $archivo->{$archivo::$id};
        } else {
            return false;
        }

    }

    /**
     * Función para borrar un documento (Borrado lógico)
     * @author Erik Villarreal
     * @param  $pathDisk #Es la ruta que usa Storage a la hora de guardar el archivo (Una palabra generalmente)
     * @param  $uuid #Nombre generado como UUID
     * @return boolean
     */
    public static function borrar($pathDisk, $uuid, $extension)
    {
        if (Storage::disk($pathDisk)->delete($uuid . '.' . $extension)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Función para descargar documentos
     * @author Erik Villarreal
     * @param  $fileSystem #Es la ruta que usa Storage a la hora de guardar el archivo (Una palabra generalmente)
     * @param  $id #ID del propietario de los documentos
     * @param  $tabla #Tabla a la que pertenece el propietario
     * @return boolean
     */
    public function descargarDocumentos($id, $tabla, $fileSystem)
    {

        $documentos = Documento_Model::select(Documento_Model::$uuid,
                                            Documento_Model::$extension,
                                            Documento_Model::$mime,
                                            Documento_Model::$nombre)
                                    ->where(Documento_Model::$idPropietario, '=', $id)
                                    ->where(Documento_Model::$tablaOrigen, '=', $tabla)
                                    ->get();

        if(count($documentos) > 0){
            $zip = new \ZipArchive;
        
            if ($zip->open("documentos_".$id.".zip", \ZipArchive::CREATE)!==TRUE) {
                exit("cannot open <$filename>\n");
            }
            foreach ($documentos as $key => $value) {
            
                $file = $value->{Documento_Model::$uuid}.self::PUNTO.$value->{Documento_Model::$extension};
                $fs = Storage::disk($fileSystem)->getDriver()->getAdapter()->applyPathPrefix($file);
                $zip->addFile($fs,$value->{Documento_Model::$nombre}.self::PUNTO.$value->{Documento_Model::$extension});

            }
            
            $zip->close();

            header("Content-Type: application/octet-stream");
            header("Content-disposition: attachment; filename=documentos_6.zip");
            readfile(public_path("/documentos_".$id.".zip"));
            @unlink(public_path("/documentos_".$id.".zip"));
            exit();
        }else{
            echo "<script>alert('No tiene documentos adjuntos');window.close();</script>";
        }           

    }

    /**
     * Función para descargar documentos
     * @author Erik Villarreal
     * @param  $fileSystem #Es la ruta que usa Storage a la hora de guardar el archivo (Una palabra generalmente)
     * @param  $id #ID del propietario de los documentos
     * @param  $tabla #Tabla a la que pertenece el propietario
     * @return boolean
     */
    public function obtenerDocumentos($id, $tabla, $fileSystem)
    {
        $documentos = Documento_Model::select(Documento_Model::$uuid,
                                            Documento_Model::$extension,
                                            Documento_Model::$mime,
                                            Documento_Model::$nombre)
                                    ->where(Documento_Model::$idPropietario, '=', $id)
                                    ->where(Documento_Model::$tablaOrigen, '=', $tabla)
                                    ->first();

        if(count($documentos) > 0){
                //return $documentos->{Documento_Model::$uuid};
                $file = $documentos->{Documento_Model::$uuid}.self::PUNTO.$documentos->{Documento_Model::$extension};
                $fs = Storage::disk($fileSystem)->getDriver()->getAdapter()->applyPathPrefix($file);
                $mime = $documentos->{Documento_Model::$mime};
                $extension = $documentos->{Documento_Model::$extension};

            header("Content-Type: " . $mime);
            header("Content-disposition: online; ");
        }else{
            echo "<script>alert('No tiene documentos adjuntos esta Solicitud');window.close();</script>";
        }           

    }
}

