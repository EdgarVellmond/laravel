<?php

namespace App\Http\Controllers\Genericos;


use App\Http\Controllers\Controller;
use App\Models\Catalogos\Plantilla_Model;
use App\Models\Catalogos\Tipo_Plantilla_Model;
use App\Models\Catalogos\Rol_Model;
use App\Models\Sistema\Variables_Sistema_Model;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Pdf_Controller extends Controller
{

    const PUNTO = ".";
    const EQUALS = "=";
    const PERMISO = "catalogo_plantilla";
    const INICIO_PARAMETRO = '$p{';
    const INICIO_ESPECIAL = '$e{';
    /**
     * Genera la plantilla de plantilla como documento
     * @author Erik Villarreal
     * @param  int #id de la plantilla que se tomará para la sustitución
     * @param  int #id del propietario del documento que se genera
     * @return PDF view
     */
    public static function generarPlantillaPdf($idPlantilla, $idPropietario)
    {

        $plantilla = Plantilla_Model::find($idPlantilla);

        $contenido = self::reemplazarEnContenido($plantilla->{Plantilla_Model::$contenido}, $idPropietario, "p");

        $contenido = self::reemplazarEnContenido($contenido, 0, "e");

        $pdf = \PDF::loadView('Genericos.vacio',compact('contenido'))
            ->setPaper('Letter')
            ->setOption('margin-top', '40mm')
            ->setOption('margin-bottom', '10mm')
            //->setOrientation('landscape')
            //->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-left',utf8_decode('Usuario: '.Auth::user()->nombre))
            ->setOption('footer-right',utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size','10');

        return  $pdf->stream('Contrato.pdf');

    }

    /**
     * Genera el contenido de la plantilla con variables sustituidas
     * @author Erik Villarreal
     * @param  int #id de la plantilla que se tomará para la sustitución
     * @param  int #id del propietario del documento que se genera
     * @return string #longText #Contenido de la plantilla como texto con las variables ya sustituidas
     */
    public static function generarContenidoPlantilla($idPlantilla, $idPropietario)
    {
        
        $plantilla = Plantilla_Model::find($idPlantilla);

        $contenido = self::reemplazarEnContenido($plantilla->{Plantilla_Model::$contenido}, $idPropietario, "p");


        $contenido = self::reemplazarEnContenido($contenido, 0, "e");

        return $contenido;

    }

    /**
     * Función encargada de reemplazar los parámetros identificados como '$p{}' sobre el contenido 
     * de la plantilla con la información a la que corresponde.
     * @author Erik Villarreal
     * @param  string #Cadena del contenido con los parámetros sin reemplazar
     * @param  id #id del propietario para el reemplazo de las variables Parámetro sólamente, 0 si se hará reemplazo Especial
     * @param  string #Modo de funcionamiento, "e" para Especial y "p" para Parámetro
     * @return string #Cadena del contenido con los parámetros ya reemplazados
     */
    public static function reemplazarEnContenido($contenido, $id, $modo){
        
        $tablasNecesarias = null;
        $auxiliarCamposNecesarios = null;
        $camposNecesarios = null;
        $camposEspeciales = null;
        $rol = null;

        //Ciclo para obtener un array con tablas y campos necesarios a base de 3 niveles de explode
        
        if($modo == "e"){
            $nivel1[] = explode(self::INICIO_ESPECIAL, $contenido);
        }else{
            $nivel1[] = explode(self::INICIO_PARAMETRO, $contenido);
        }
        
        if(count($nivel1[0]) > 1){

        for ($i = 1; $i < count($nivel1[0]) ; $i++ ) {
            
            $nivel2[$i] = explode('}', $nivel1[0][$i]);
            $nivel3[$i] = explode('.', $nivel2[$i][0]);

            if($tablasNecesarias != null){
                
                if(!in_array($nivel3[$i][0], $tablasNecesarias)){
                    $tablasNecesarias[] = $nivel3[$i][0];
                }

                if($auxiliarCamposNecesarios != null){
                
                    if(!in_array($nivel3[$i][1], $auxiliarCamposNecesarios)){
                        $auxiliarCamposNecesarios[] = $nivel3[$i][1];
                        $camposNecesarios[$nivel3[$i][0]][] = $nivel3[$i][1];
                        if(count($nivel3[$i]) > 2){
                            $rol[$nivel3[$i][0]][] = $nivel3[$i][2];
                            $camposEspeciales[] = $nivel3[$i][2];
                        }
                    }

                }else{
                    $auxiliarCamposNecesarios[] = $nivel3[$i][1];
                    $camposNecesarios[$nivel3[$i][0]][] = $nivel3[$i][1];
                    if(count($nivel3[$i]) > 2){
                        $rol[$nivel3[$i][0]][] = $nivel3[$i][2];
                        $camposEspeciales[] = $nivel3[$i][2];
                    }
                }

            }else{
                $tablasNecesarias[] = $nivel3[$i][0];
                $auxiliarCamposNecesarios[] = $nivel3[$i][1];
                $camposNecesarios[$nivel3[$i][0]][] = $nivel3[$i][1];
                if(count($nivel3[$i]) > 2){
                    $rol[$nivel3[$i][0]][] = $nivel3[$i][2];
                    $camposEspeciales[] = $nivel3[$i][2];
                }
            }

        }
        
        //Consulta de los ID para la futura consulta de los valores que reemplazarán las variables especiales
        if($modo == "e"){
            foreach ($rol as $key => $value) {
                $rol[$key] = Rol_Model::where(Rol_Model::$nombre, $value)->get();
                $rol[$key] = $rol[$key][0]->{Rol_Model::$idPropietario};
            }
        }

        $informacionTablas = null;
        $camposConsultar = "";

        //Ciclo que arma el Query SQL para las consultas de información y realiza el reemplazo sobre el contenido

        for ($i = 0 ; $i < count($tablasNecesarias) ; $i++ ) {
            
            $tablasRelacionales = explode(",", $tablasNecesarias[$i]);

            if(count($tablasRelacionales) > 1){

                $j=0;
                foreach ($camposNecesarios[$tablasNecesarias[$i]] as $key => $value) {
                    
                    if($j == 0){
                        $auxiliarCamposRelacionalesTemporal = $value;
                    }else{
                        $auxiliarCamposRelacionalesTemporal .= explode(",", $value)[1];
                    }
                    $j++;
                }

                $j = 0; 
                foreach ($tablasRelacionales as $key => $value) {
                    
                    
                    $camposRelacionales = explode(",", $auxiliarCamposRelacionalesTemporal);

                    if($j == 0){

                        $informacionTablasRelacionadas[$j] = DB::select("select id_" . $tablasRelacionales[$j + 1] . "," . $camposRelacionales[$j] . " from " . $tablasRelacionales[$j] . " where id_" . $tablasRelacionales[$j] . "=" . $id);

                    }else if($j < count($tablasRelacionales) - 1){

                        $idTemporal = (array) $informacionTablasRelacionadas[$j - 1][0];
                        $informacionTablasRelacionadas[$j] = DB::select("select id_" . $tablasRelacionales[$j + 1] . "," . $camposRelacionales[$j] . " from " . $tablasRelacionales[$j] . " where id_" . $tablasRelacionales[$j] . "=" . $idTemporal["id_" . $tablasRelacionales[$j]]);

                    }else{

                        $idTemporal = (array) $informacionTablasRelacionadas[$j - 1][0];
                        $informacionTablasRelacionadas[$j] = DB::select("select " . $camposRelacionales[$j] . " from " . $tablasRelacionales[$j] . " where id_" . $tablasRelacionales[$j] . "=" . $idTemporal["id_" . $tablasRelacionales[$j]]);
                        $valorFinal[$j] = (array) $informacionTablasRelacionadas[$j][0];
                        $valorFinal[$j] = $valorFinal[$j][$camposRelacionales[$j]];
                    }
                    
                    $j++;
                }
                $j = 0;
                foreach ($valorFinal as $key => $value) {
                    $contenido = str_replace(self::INICIO_PARAMETRO . $tablasNecesarias[$i] . self::PUNTO . $camposNecesarios[$tablasNecesarias[$i]][$j] . "}", $value, $contenido);
                    $j++;
                }


            }else{

                $j = 1;
                foreach ($camposNecesarios[$tablasNecesarias[$i]] as $key => $value) {

                    $camposConsultar = $camposConsultar . $value;

                    if(count($camposNecesarios[$tablasNecesarias[$i]]) > 1 && $j < count($camposNecesarios[$tablasNecesarias[$i]])){
                        $camposConsultar = $camposConsultar . ", ";
                    }

                    $j++;

                }  

                if($modo == "e"){

                    $informacionTablas = DB::select("select " . $camposConsultar . " from " . $tablasNecesarias[$i] . " where id_" . $tablasNecesarias[$i] . "=" . $rol[$tablasNecesarias[$i]]);
                    foreach ($informacionTablas[0] as $key => $value) {
                        $contenido = str_replace(self::INICIO_ESPECIAL . $tablasNecesarias[$i] . self::PUNTO . $key . self::PUNTO . $camposEspeciales[$i] . "}", $value, $contenido);
                    }

                }else{

                    $informacionTablas = DB::select("select " . $camposConsultar . " from " . $tablasNecesarias[$i] . " where id_" . $tablasNecesarias[$i] . "=" . $id);

                    foreach ($informacionTablas[0] as $key => $value) {
                        $contenido = str_replace(self::INICIO_PARAMETRO . $tablasNecesarias[$i] . self::PUNTO . $key . "}", $value, $contenido);
                    }

                }  

            }
            
        }

        }

        return $contenido;

    }

    /**
     * Genera una Vista previa de la plantilla en PDF
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public static function generarVistaPreviaPdf($idPlantilla)
    {

        $plantilla = Plantilla_Model::find($idPlantilla);

        $contenido = $plantilla->{Plantilla_Model::$contenido};

        $pdf = \PDF::loadView('Genericos.vacio',compact('contenido'))
            ->setPaper('Letter')
            ->setOption('margin-top', '40mm')
            ->setOption('margin-bottom', '10mm')
            //->setOrientation('landscape')
            //->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-left',utf8_decode('Usuario: '.Auth::user()->nombre))
            ->setOption('footer-right',utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size','10');

        return  $pdf->stream('Corte por Servicio.pdf');

    }

}
