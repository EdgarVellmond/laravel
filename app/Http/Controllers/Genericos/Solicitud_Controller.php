<?php

namespace App\Http\Controllers\Genericos;

use App\Http\Controllers\Controller;
use App\Models\Catalogos\Tipo_Solicitud_Model;
use App\Models\Genericos\Historial_Solicitud_Model;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Sistema\Usuarios_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Genericos\Documento_Model;
use App\Models\Catalogos\Area_Model;
use Illuminate\Support\Facades\Session;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;


class Solicitud_Controller extends Controller
{
    const PENDIENTE = 'Pendiente';
    const EN_PROCESO = 'En proceso';
    const PUNTO = '.';
    const EQUALS = '=';

    /**
     * Vista para generar Solicitudes de Empleado por parte del Residente
     * @author Erik Villarreal
     * @return Index
     */
    public function index()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.generico.solicitud")) {
            return Redirect::to('/');
        }

        $solicitud = self::obtenerSolicitudesResidente(Auth::id());
        $tipos_solicitud = Tipo_Solicitud_Model::lists(Tipo_Solicitud_Model::$nombre, Tipo_Solicitud_Model::$id);
        $areas = Area_Model::lists(Area_Model::$nombre, Area_Model::$id);

        return view('Genericos.Solicitud.index', compact('solicitud', 'tipos_solicitud', 'areas'));

    }

    /**
     * Vista para la bitacora de Solicitudes
     * @author Oscar Vargas
     * @return Index
     */
    public function indexBitacora()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.generico.bitacora_solicitud")) {
            return Redirect::to('/');
        }

        $solicitud = self::obtenerSolicitudesBitacora();

        return view('Genericos.Solicitud.index_bitacora', compact('solicitud'));

    }

    /**
     * Metodo que sirve para obtener todas las solicitudes para la bitacora
     *
     * @author Oscar Vargas
     * @return objeto con todas las solicitudes
     */
    public static function obtenerSolicitudesBitacora()
    {

        return Solicitud_Model::select(DB::raw(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$estatus . " as estatusSolicitud"),
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$asunto,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$observaciones,
            Tipo_Solicitud_Model::$tabla . self::PUNTO . Tipo_Solicitud_Model::$nombre,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$createdAt,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," . Empleado_Model::$apellidoPaterno . ",' '," . Empleado_Model::$apellidoMaterno . ") as nombreEmpleado"))
            ->join(Tipo_Solicitud_Model::$tabla,
                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idTipoSolicitud,
                "=",
                Tipo_Solicitud_Model::$tabla . self::PUNTO . Tipo_Solicitud_Model::$id)
            ->join(Usuarios_Model::$tabla,
                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idUsuarioCreacion,
                "=",
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$id)
            ->join(Empleado_Model::$tabla,
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->orderBy(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$createdAt, "dec")
            ->limit(1000)
            ->get();
    }

    /**
     * Metodo que sirve para obtener todas las solicitudes con estatus pendiente y en proceso para el Residente
     *
     * @author Oscar Vargas
     * @param  int #Identificador del Residente
     * @return objeto con todas las solicitudes
     */
    public static function obtenerSolicitudesResidente($id)
    {

        return Solicitud_Model::select(DB::raw(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$estatus . " as estatusSolicitud"),
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$asunto,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$observaciones,
            Tipo_Solicitud_Model::$tabla . self::PUNTO . Tipo_Solicitud_Model::$nombre,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," . Empleado_Model::$apellidoPaterno . ",' '," . Empleado_Model::$apellidoMaterno . ") as nombreEmpleado"))
            ->where(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idUsuarioCreacion, $id)
            ->join(Tipo_Solicitud_Model::$tabla,
                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idTipoSolicitud,
                "=",
                Tipo_Solicitud_Model::$tabla . self::PUNTO . Tipo_Solicitud_Model::$id)
            ->join(Usuarios_Model::$tabla,
                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idUsuarioCreacion,
                "=",
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$id)
            ->join(Empleado_Model::$tabla,
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->orderBy(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id, "dec")
            ->get();
    }

    /**
     * Metodo que sirve para obtener todas las solicitudes con estatus pendiente y en proceso para el depto. de Recursos Humanos
     *
     * @author Oscar Vargas
     * @return objeto con todas las solicitudes
     */
    public static function obtenerSolicitudesRecursosHumanos()
    {

        return Solicitud_Model::select(DB::raw(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$estatus . " as estatusSolicitud"),
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$asunto,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id,
            Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$observaciones,
            Tipo_Solicitud_Model::$tabla . self::PUNTO . Tipo_Solicitud_Model::$nombre,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," . Empleado_Model::$apellidoPaterno . ",' '," . Empleado_Model::$apellidoMaterno . ") as nombreEmpleado"))
            ->where(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idArea, \Config::get('variables_sistema.solicitud_id_area_recursos_humanos'))
            ->join(Tipo_Solicitud_Model::$tabla,
                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idTipoSolicitud,
                "=",
                Tipo_Solicitud_Model::$tabla . self::PUNTO . Tipo_Solicitud_Model::$id)
            ->join(Usuarios_Model::$tabla,
                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idUsuarioCreacion,
                "=",
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$id)
            ->join(Empleado_Model::$tabla,
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->orderBy(Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id, "dec")
            ->get();
    }

    /**
     * Metodo que sirve para cambiar el estatus de una solicitud dependiendo del parametro que se le mande
     *
     * @author Oscar Vargas
     * @return redirije a la pagina anterior
     */
    public function cambiarEstatus()
    {

        $data = request()->all();

        $solicitud = Solicitud_Model::find($data[Solicitud_Model::$id]);

        if($solicitud->{Solicitud_Model::$estatus} == $data["estatusAnterior"]) {

            if (isset($data[Solicitud_Model::$observaciones])) {
                $solicitud->{Solicitud_Model::$observaciones} = $data[Solicitud_Model::$observaciones];
            }
            $solicitud->{Solicitud_Model::$estatus} = $data[Solicitud_Model::$estatus];

            $solicitud->save();

            // Se guarda el historial de cambios
            self::historialSolicitud( $solicitud->{Solicitud_Model::$id}, "Cambio a estatus de ".$solicitud->{Solicitud_Model::$estatus});

            return Redirect::back()->with("message", "Se cambió el estatus correctamente");

        }else{

            return Redirect::back()->with("error", "No se pudo cambiar el estatus porque ha sido cambiado por otro usuario, actualiza la página");

        }

    }

    /**
     * Metodo que sirve para obtener la información de una Solicitud para editar
     *
     * @author Eriik Villarreal
     * @return {JSON} #Objeto con la información de la solicitud y sus documentos
     */
    public function obtenerSolicitudEditar()
    {

        $data = request()->all();

        return $solicitud = Solicitud_Model::select(
                                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id,
                                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$asunto,
                                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idArea,
                                Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$idTipoSolicitud,
                                Documento_Model::$tabla . self::PUNTO . Documento_Model::$ruta,
                                Documento_Model::$tabla . self::PUNTO . Documento_Model::$uuid,
                                Documento_Model::$tabla . self::PUNTO . Documento_Model::$extension
                                )
                            ->leftJoin(Documento_Model::$tabla,
                                    Solicitud_Model::$tabla . self::PUNTO . Solicitud_Model::$id,
                                    self::EQUALS,
                                    Documento_Model::$tabla . self::PUNTO . Documento_Model::$idPropietario)
                            ->where(Solicitud_Model::$id, $data[Solicitud_Model::$id])
                            ->get();
        
    }

    /**
     * Función para crear una nueva Solicitud o editarla
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function guardarSolicitud(Request $request)
    {

        $data = $request->all();

        if ($data[Solicitud_Model::$id] == "") {

            //Permiso de Despliegue de este Modulo
            if (!Helpers::get_permiso("alta.generico.solicitud")) {
                return Redirect::to('/');
            }

            $solicitud = new Solicitud_Model();
            $solicitud->{Solicitud_Model::$idUsuarioCreacion} = Auth::id();
        } else {

            //Permiso de Despliegue de este Modulo
            if (!Helpers::get_permiso("edicion.generico.solicitud")) {
                return Redirect::to('/');
            }

            $solicitud = Solicitud_Model::find($data[Solicitud_Model::$id]);
            $solicitud->{Solicitud_Model::$idUsuarioEdicion} = Auth::id();

            if($solicitud->{Solicitud_Model::$estatus} != "Pendiente" && $solicitud->{Solicitud_Model::$estatus} != "Rechazada") {
                return Redirect::back()->with("error" , "No se pudo guardar porque el estatus ha sido cambiado por alguien más.");
            }

        }

        $solicitud->{Solicitud_Model::$asunto} = $data[Solicitud_Model::$asunto];
        $solicitud->{Solicitud_Model::$idArea} = $data[Solicitud_Model::$idArea];
        $solicitud->{Solicitud_Model::$idTipoSolicitud} = $data[Solicitud_Model::$idTipoSolicitud];
        
        if(isset($data[Solicitud_Model::$observaciones])){
            $solicitud->{Solicitud_Model::$observaciones} = $data[Solicitud_Model::$observaciones];
        }

        $solicitud->{Solicitud_Model::$estatus} = $data[Solicitud_Model::$estatus];

        if($solicitud->save()){
            if ($request->hasFile(Documento_Model::$id)) {
            
                foreach ($request->file(Documento_Model::$id) as $key => $archivo) {

                    $instancia = new Documento_Model();

                    $imagenInsertada = Documento_Controller::guardarDocumentoServidor($archivo, "solicitud", "app\\Genericos\\Solicitud\\", $instancia, $solicitud->{Solicitud_Model::$id} , "solicitud","");

                    if (!$imagenInsertada) {
                      return Redirect::back()->with("error" , "Error al guardar la imagen");
                    }
                }
            
            }

            if ($data[Solicitud_Model::$id] == "") {
                self::historialSolicitud($solicitud->{Solicitud_Model::$id} , "Se generó nueva Solicitud");
            }else{
                self::historialSolicitud($solicitud->{Solicitud_Model::$id} , "Se editó la Solicitud ó reenvió");
            }

            return Redirect::back()->with("success" , "Se guardó la solicitud correctamente.");

        }else{
            return Redirect::back()->with("error" , "No se pudo guardar la solicitud correctamente.");
        }
    }

    /**
     * Metodo que sirve para guardar el historial que tiene la solicitud
     *
     * @author Oscar Vargas
     * @param $idSolicitud variable con el identificar unico de la solicitud
     * @param $accion variable con la accion que hizo el usuario
     * @return void
     */
    public function historialSolicitud($idSolicitud , $accionSolicitud){

        $historial = new Historial_Solicitud_Model();

        $historial->{Historial_Solicitud_Model::$idSolicitud} = $idSolicitud;
        $historial->{Historial_Solicitud_Model::$accion} = $accionSolicitud;
        $historial->{Historial_Solicitud_Model::$idUsuarioCreacion} = Auth::id();

        $historial->save();

    }

}
