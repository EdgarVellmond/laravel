<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ImpresionesController extends Controller
{
    public function Sis_Logo_Actual(){
        $SisLogos = DB::table("sistema_logos")->where("activo",1)->get();
        return '<img alt="Logo" src="data:'.$SisLogos[0]->mime.';base64,'.base64_encode($SisLogos[0]->logo).'" height="75px"  class="logo"/>';
    }
    public function Sis_Logo($id){
        $SisLogos = DB::table("sistema_logos")->find($id);
        return '<img alt="Logo" src="data:'.$SisLogos->mime.';base64,'.base64_encode($SisLogos->logo).'" height="75px"  class="logo"/>/>';
    }

    /**
     * EJEMPLOS PARA IMPRESION DE PDFs
     * @author: Luis Chavez
     * @Nota No sirven por que no existe el model
     */
    public function movimiento_equipo($id){
        $MovEquipos = MovEquipos::find($id);
        $MovEquiposDetalles = MovEquiposDetalles::where("idMovEquipo",$id)->get();
        $LogoActual = $this->Sis_Logo_Actual();
        $TipoMovimiento = array(1 => 'Cambio de Resguardo', 2 => 'Baja de Equipo', 3 => 'Alta de Equipo');
        $htmlheader = view('impresiones.header_movimientos',compact('MovEquipos','MovEquiposDetalles','TipoMovimiento','LogoActual'));
        $htmlfooter = view('impresiones.footer_movimientos_'.$MovEquipos->TipoMovimiento,compact('MovEquipos','MovEquiposDetalles','TipoMovimiento'));
        $pdf = \PDF::loadView('impresiones.equipos_movimiento', compact('MovEquipos','MovEquiposDetalles','TipoMovimiento'))
            ->setPaper('Letter')
            ->setOrientation('landscape')
            ->setOption('margin-top', '40mm')
            ->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-html',$htmlfooter);
        return $pdf->stream('Movimiento de Equipo.pdf');
        //return $pdf->download('Movimiento de Equipo.pdf');
    }
    public function salida_consumible($id){
        $Movimiento = MovConsumibles::find($id);
        $Consumible = $Movimiento->Consumible;
        $LogoActual = $this->Sis_Logo_Actual();
        $htmlheader = view('impresiones.header_consumibles',compact('Movimiento','LogoActual'));
        $pdf = \PDF::loadView('impresiones.salida_consumible',compact('Movimiento','Consumible'))
            ->setPaper('Letter')
            ->setOption('margin-top', '40mm')
            ->setOption('margin-bottom', '10mm')
            ->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-left',utf8_decode('Usuario: '.Auth::user()->nombre))
            ->setOption('footer-right',utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size','10');
        return  $pdf->stream('Salida de Consumible.pdf');
    }
    public function salida_accesorio($id){
        $Movimiento = MovAccesorios::find($id);
        $Accesorio = $Movimiento->Accesorio;
        $LogoActual = $this->Sis_Logo_Actual();
        $htmlheader = view('impresiones.header_accesorios',compact('Movimiento','LogoActual'));
        $pdf = \PDF::loadView('impresiones.salida_accesorio',compact('Movimiento','Accesorio'))
            ->setPaper('Letter')
            ->setOption('margin-top', '40mm')
            ->setOption('margin-bottom', '10mm')
            ->setOption('header-html',$htmlheader)->setOption('header-spacing',3)
            ->setOption('footer-left',utf8_decode('Usuario: '.Auth::user()->nombre))
            ->setOption('footer-right',utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size','10');
        return  $pdf->stream('Salida de Accesorio.pdf');
    }
}
