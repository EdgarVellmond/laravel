<?php

namespace App\Http\Controllers\Indicadores;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Actividad_Controller extends Controller
{
    //
        public function index()
    {

        //Permiso de Despliegue de este Módulo
        //if (!Helpers::get_permiso("vista.catalogos.area")) {
          //  return Redirect::to('/');
        //}
        $arrayActividad = Actividad_Model::select(Actividad_Model::$id,
            Actividad_Model::$proyectoNombre,
        	Actividad_Model::$cliente,
        	Actividad_Model::$fechaInicial,
        	Actividad_Model::$fechaTermino,
        	Actividad_Model::$observaciones,
            Actividad_Model::$estatus,
            Actividad_Model::$tipo,
            Actividad_Model::$duracion,
            Actividad_Model::$gastobra,
            Actividad_Model::$prioridad,
            Actividad_Model::$montocontr,
            Actividad_Model::$montoejercido,
            Actividad_Model::$ubicacion)->get();
        return view('Indicadores.Banco_Proyecto.index' , compact("arrayActividad"));
    }
}
