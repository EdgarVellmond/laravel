<?php
namespace App\Http\Controllers\Indicadores;
use App\Models\Indicadores\Proyectos_Model;
use App\Models\Indicadores\Actividad_Model;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Proyectos_Controller extends Controller
{

    const PUNTO = '.';
    
    /**
     * Retorna la vista del index del catálogo de Área
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Áreas
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.indicadores.proyecto")) {
           return Redirect::to('/');
        }
        $arrayProyecto = Proyectos_Model::select(Proyectos_Model::$id,
            Proyectos_Model::$proyectoNombre,
        	Proyectos_Model::$cliente,
        	Proyectos_Model::$fechaInicial,
        	Proyectos_Model::$fechaTermino,
        	Proyectos_Model::$observaciones,
            Proyectos_Model::$estatus,
            Proyectos_Model::$tipo,
            Proyectos_Model::$duracion,
            Proyectos_Model::$gastobra,
            Proyectos_Model::$prioridad,
            Proyectos_Model::$montocontr,
            Proyectos_Model::$montoejercido,
            Proyectos_Model::$saldoEjercer,
            Proyectos_Model::$ubicacion)->get();
        return view('Indicadores.Banco_Proyecto.index' , compact("arrayProyecto"));
    }

    public function obtener(){
          //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.indicadores.proyecto")) {
            return "Sin permiso para ver el historial";
        }
    	$data = request()->all();

        $id = $data[Proyectos_Model::$id];

        $proyecto = Proyectos_Model::FindOrFail($id);

        return $proyecto;
    }



     public function guardar()
    {


        $data = request()->all();

        if ($data[Proyectos_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.indicadores.proyecto")) {
                return "No tienes permiso para agregar.";
            }

            $proyecto = new Proyectos_Model();
            $proyecto->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.indicadores.proyecto")) {
                return "No tienes permiso para editar.";
            }

            $proyecto = Proyectos_Model::find($data[Proyectos_Model::$id]);
            $proyecto->id_usuario_edicion = Auth::id();

        }

        $proyecto->{Proyectos_Model::$proyectoNombre} = $data[Proyectos_Model::$proyectoNombre];
        $proyecto->{Proyectos_Model::$cliente} = $data[Proyectos_Model::$cliente];
        $proyecto->{Proyectos_Model::$fechaInicial} = $data[Proyectos_Model::$fechaInicial];
        $proyecto->{Proyectos_Model::$fechaTermino} = $data[Proyectos_Model::$fechaTermino];
        $proyecto->{Proyectos_Model::$observaciones} = $data[Proyectos_Model::$observaciones];
        $proyecto->{Proyectos_Model::$tipo} = $data[Proyectos_Model::$tipo];
        $proyecto->{Proyectos_Model::$duracion} = $data[Proyectos_Model::$duracion];
        $proyecto->{Proyectos_Model::$gastobra} = $data[Proyectos_Model::$gastobra];
        $proyecto->{Proyectos_Model::$prioridad} = $data[Proyectos_Model::$prioridad];
        $proyecto->{Proyectos_Model::$montocontr} = $data[Proyectos_Model::$montocontr];
        $proyecto->{Proyectos_Model::$montoejercido} = $data[Proyectos_Model::$montoejercido];
        $proyecto->{Proyectos_Model::$ubicacion} = $data[Proyectos_Model::$ubicacion];
        $proyecto->{Proyectos_Model::$saldoEjercer}= $data[Proyectos_Model::$saldoEjercer];
        $proyecto->{Proyectos_Model::$estatus} = $data[Proyectos_Model::$estatus];

        if ($proyecto->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

         public function guardarActividads()
    {


        $data = request()->all();
        dd($data);
        if ($data[Actividad_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.indicadores.proyecto")) {
                return "No tienes permiso para agregar.";
            }

            $actividad = new Actividad_Model();
            $actividad->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.indicadores.proyecto")) {
                return "No tienes permiso para editar.";
            }

            $actividad = Actividad_Model::find($data[Actividad_Model::$id]);
            $actividad->id_usuario_edicion = Auth::id();

        }

        $actividad->{Actividad_Model::$tipoIndicador} = $data[Actividad_Model::$tipoIndicador];
        $actividad->{Actividad_Model::$porcentaje} = $data[Actividad_Model::$porcentaje];
        $actividad->{Actividad_Model::$programado} = $data[Actividad_Model::$programado];
        $actividad->{Actividad_Model::$real} = $data[Actividad_Model::$real];
        $actividad->{Actividad_Model::$idProyecto} = $data[Actividad_Model::$idProyecto];
        $actividad->{Actividad_Model::$semana} = $data[Actividad_Model::$semana];
        $actividad->{Actividad_Model::$area} = $data[Actividad_Model::$area];

        if ($actividad->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

/**
     * Metodo que sirve para obtener la informacion de un empleado
     *
     * @author Oscar Vargas
     * @param $id identificador unico del empleado
     * @return ficha del empleado
     */
    public function indicadores($id)
    {

        $proyectos = Proyectos_Model::select(
            Proyectos_Model::$id,
           Proyectos_Model::$proyectoNombre,
            Proyectos_Model::$cliente,
            Proyectos_Model::$fechaInicial,
            Proyectos_Model::$fechaTermino,
            Proyectos_Model::$observaciones,
            Proyectos_Model::$tipo,
            Proyectos_Model::$duracion,
            Proyectos_Model::$gastobra,
            Proyectos_Model::$prioridad,
            Proyectos_Model::$montocontr,
            Proyectos_Model::$montoejercido,
            Proyectos_Model::$saldoEjercer,
            Proyectos_Model::$ubicacion,
            Proyectos_Model::$estatus
           )
            ->where(Proyectos_Model::$id, $id)
            ->get();

             $arrayActividad = Actividad_Model::select(
            Actividad_Model::$id_actividad,
            Actividad_Model::$tipoIndicador,
            Actividad_Model::$porcentaje,
            Actividad_Model::$programado,
            Actividad_Model::$real,
            Actividad_Model::$idProyecto,
            Actividad_Model::$semana,
           Actividad_Model::$area) 
            ->where(Actividad_Model::$idProyecto, $id)
            ->orderby(Actividad_Model::$tipoIndicador)
            ->get();

        return view('Indicadores.Banco_Proyecto.indicadores', compact("proyectos","arrayActividad"));

    }

     public function eliminar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.indicadores.proyecto")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Proyectos_Model::$id];
        $proyecto = Proyectos_Model::find($id);

        if ($proyecto->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

        public function obtenerA(){

          //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.planeacion.registro_padron")) {
            return "Sin permiso para ver el historial";
        }
        $data = request()->all();

        $id = $data["id_proyecto"];

        $actividad = Actividad_Model::where(Actividad_Model::$idProyecto , "=" ,$id)
        ->where(Actividad_Model::$tipoIndicador, "=" , $data["indicador"])->get();

        return $actividad;
    }




}
