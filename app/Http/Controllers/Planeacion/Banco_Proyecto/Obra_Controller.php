<?php

namespace App\Http\Controllers\Planeacion\Banco_Proyecto;

use App\Models\Catalogos\Clasificacion_Obra_Model;
use App\Models\Catalogos\Cliente_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Planeacion\Banco_Proyecto\Obra_Model;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Obra_Controller extends Controller
{

    const PUNTO = ".";

    /**
     * Index del catalogo de banco de proyectos
     *
     * @author Oscar Vargas
     * @return Vista index de banco de proyectos
     */
    public function index()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.planeacion.banco_proyecto")) {
            return Redirect::to('/');
        }

        //Obtener lista de Banco de Proyectos
        $arrayObra = self::obtenerObrasParaBancoProyectos();

        //Obtener lista de Estados
        $arrayEstado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);

        //Obtener lista de Empresas
        $arrayEmpresa = Empresa_Model::lists(Empresa_Model::$razonSocial , Empresa_Model::$id);

        //Obtener lista de Clasificacion de Obra
        //$arrayClasificacion = Clasificacion_Obra_Model::lists(Empresa_Model::$razonSocial , Empresa_Model::$id);

        //Obtener lista de Bancos
        $arrayCliente = Cliente_Model::select(Cliente_Model::$id,
            DB::raw("CONCAT(".Cliente_Model::$nombre.",".Cliente_Model::$apellidoPaterno.",".Cliente_Model::$apellidoMaterno.") as cliente"))
        ->lists("cliente", Cliente_Model::$id);

        return view('Planeacion.Banco_Proyecto.index', compact('arrayCliente', 'arrayObra', 'arrayEstado', 'arrayEmpresa'));

    }

    public function obtenerObrasParaBancoProyectos(){

        return $array = Obra_Model::select(Obra_Model::$tabla.self::PUNTO.Obra_Model::$id,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$nombre,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$descripcion,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$prioridad,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$estatus,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$capitalMinimoContable,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$fechaInicio,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$fechaTermino,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$fechaLimiteBase,
            DB::raw("CONCAT(".Cliente_Model::$tabla.self::PUNTO.Cliente_Model::$nombre.",".Cliente_Model::$tabla.self::PUNTO.Cliente_Model::$apellidoPaterno.",".Cliente_Model::$tabla.self::PUNTO.Cliente_Model::$apellidoMaterno.") as nombreCliente"),
            DB::raw(Empresa_Model::$tabla.self::PUNTO.Empresa_Model::$razonSocial." as nombreEmpresa"))
            ->join(Cliente_Model::$tabla,
                Obra_Model::$tabla.self::PUNTO.Obra_Model::$idCliente,
                "=",
                Cliente_Model::$tabla.self::PUNTO.Cliente_Model::$id)
            ->leftjoin(Empresa_Model::$tabla,
                Obra_Model::$tabla.self::PUNTO.Obra_Model::$idEmpresa,
                "=",
                Empresa_Model::$tabla.self::PUNTO.Empresa_Model::$id)
            ->get();

    }

    public function obtenerAdicionalesDeObra(){

        return $array = Obra_Model::select(
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$localidad,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$observaciones,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$folio,
            DB::raw(Municipio_Model::$tabla.self::PUNTO.Municipio_Model::$nombre." as nombreMunicipio"),
            DB::raw(Estado_Model::$tabla.self::PUNTO.Estado_Model::$nombre." as nombreEstado"))
            ->join(Municipio_Model::$tabla,
                Obra_Model::$tabla.self::PUNTO.Obra_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla.self::PUNTO.Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla.self::PUNTO.Municipio_Model::$idEstado,
                "=",
                Estado_Model::$tabla.self::PUNTO.Estado_Model::$id)
            ->get();

    }

    /**
     * Metodo que sirve para guardar o editar una obra
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Altas de este Modulo
        if (!Helpers::get_permiso("alta.planeacion.banco_proyecto")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Obra_Model::$id] == "") {

            $obra = new Obra_Model();
            $obra->id_usuario_creacion = Auth::id();

        } else {

            $obra = Obra_Model::find($data[Obra_Model::$id]);
            $obra->id_usuario_edicion = Auth::id();

        }

        $obra->{Obra_Model::$nombre} = $data[Obra_Model::$nombre];
        $obra->{Obra_Model::$folio} = $data[Obra_Model::$folio];
        $obra->{Obra_Model::$idCliente} = $data[Obra_Model::$idCliente];
        $obra->{Obra_Model::$descripcion} = $data[Obra_Model::$descripcion];
        $obra->{Obra_Model::$prioridad} = $data[Obra_Model::$prioridad];
        $obra->{Obra_Model::$idMunicipio} = $data[Obra_Model::$idMunicipio];
        $obra->{Obra_Model::$localidad} = $data[Obra_Model::$localidad];
        $obra->{Obra_Model::$capitalMinimoContable} = $data[Obra_Model::$capitalMinimoContable];
        $obra->{Obra_Model::$fechaInicio} = $data[Obra_Model::$fechaInicio];
        $obra->{Obra_Model::$fechaTermino} = $data[Obra_Model::$fechaTermino];
        $obra->{Obra_Model::$fechaLimiteBase} = $data[Obra_Model::$fechaLimiteBase];
        $obra->{Obra_Model::$estatus} = "PENDIENTE";

        if ($obra->save()) {
            return "Se guardo correctamente el Proyecto";
        } else {
            return "No se guardo exitosamente el Proyecto";
        }

    }

    /**
     * Metodo que sirve para obtener una Obra
     *
     * @param POST id_obra identificador unico
     * @author Oscar Vargas
     * @return json con la obra solicitado
     */
    public function obtenerObra()
    {

        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("edicion.planeacion.banco_proyecto")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Obra_Model::$id];

        $obra = Obra_Model::select(Obra_Model::$id,
            Obra_Model::$descripcion,
            Obra_Model::$prioridad,
            Obra_Model::$capitalMinimoContable,
            Obra_Model::$fechaInicio,
            Obra_Model::$fechaTermino,
            Obra_Model::$fechaLimiteBase,
            DB::raw(Obra_Model::$tabla.self::PUNTO.Obra_Model::$nombre." as nombre"),
            Obra_Model::$idCliente,
            Obra_Model::$tabla.self::PUNTO.Obra_Model::$idMunicipio,
            Obra_Model::$folio,
            Obra_Model::$localidad,
            Obra_Model::$observaciones,
            Municipio_Model::$idEstado)
            ->join(Municipio_Model::$tabla,
                Obra_Model::$tabla . self::PUNTO . Obra_Model::$idMunicipio,
                "=",
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->where(Obra_Model::$id, $id)
                ->get();

        return $obra;

    }

    /**
     * Metodo que sirve para cancelar una Obra
     *
     * @param POST id_obra identificador unico
     * @author Oscar Vargas
     * @return mensaje de accion
     */
    public function cancelarObra()
    {
        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("baja.planeacion.banco_proyecto")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Obra_Model::$id];

        $obra = Obra_Model::find($id);

        $obra->{Obra_Model::$estatus} = 'CANCELADA';

        if($obra->save()){

            return "Se canceló la obra correctamente";
        }else{

            return "Ocurrió un error al cancelar la obra correctamente";
        }

    }

    /**
     * Metodo que sirve para autorizar una Obra
     *
     * @param POST id_obra identificador unico
     * @author Oscar Vargas
     * @return mensaje de accion
     */
    public function autorizarObra()
    {
        //Permiso de Cambios de este Modulo
        if (!Helpers::get_permiso("autorizar.planeacion.banco_proyecto")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Obra_Model::$id];

        $obra = Obra_Model::find($id);

        $obra->{Obra_Model::$idEmpresa} = $data[Obra_Model::$idEmpresa];
        $obra->{Obra_Model::$observaciones} = $data[Obra_Model::$observaciones];
        $obra->{Obra_Model::$estatus} = "PROGRAMADA";

        if($obra->save()){

            return "Se autorizo la obra correctamente";
        }else{

            return "Ocurrió un error al autorizar la obra correctamente";
        }

    }
}
