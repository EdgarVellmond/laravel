<?php

namespace App\Http\Controllers\Planeacion;

use App\Models\Planeacion\Registro_Padron_Model;
use App\Models\Catalogos\Cliente_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Empleado_Model;
use App\Models\Genericos\Documento_Model;
use App\Http\Controllers\Genericos\Documento_Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\Helpers;

class Registro_Padron_Controller extends Controller
{

    const PUNTO = '.';

    /**
     * Retorna la vista del index de Registro de Padrón
     * @author Oscar Vargas
     * @return view #Vista index de Registro de Padrón
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.planeacion.registro_padron")) {
            return Redirect::to('/');
        }

        $arrayRegistroPadron = Registro_Padron_Model::select(
            Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$id,
            Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$inicioVigencia,
            Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$finVigencia,
            Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$inscripcion,
            Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$comentario,
            Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$estatus,
            DB::raw(Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$nombre . " as nombreCliente"),
            DB::raw(Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$razonSocial . " as nombreEmpresa"),
            Documento_Model::$tabla . self::PUNTO . Documento_Model::$id,
            Documento_Model::$tabla . self::PUNTO . Documento_Model::$nombre,
            Documento_Model::$tabla . self::PUNTO . Documento_Model::$ruta,
            Documento_Model::$tabla . self::PUNTO . Documento_Model::$uuid,
            Documento_Model::$tabla . self::PUNTO . Documento_Model::$extension
            )
            ->leftjoin(Cliente_Model::$tabla,
                Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$idCliente,
                "=",
                Cliente_Model::$tabla . self::PUNTO . Cliente_Model::$id)
            ->join(Empresa_Model::$tabla,
                Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$idEmpresa,
                "=",
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$id)
            ->leftjoin(Documento_Model::$tabla,
                Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$id,
                "=",
                Documento_Model::$tabla . self::PUNTO . Documento_Model::$idPropietario)
            ->where(Registro_Padron_Model::$tabla . self::PUNTO . Registro_Padron_Model::$estatus, "Inscrito y vigente")
            ->orderBy(Registro_Padron_Model::$finVigencia)
            ->get();

        $cliente = Cliente_Model::lists(Cliente_Model::$nombre,Cliente_Model::$id)->toArray();

        $empresa = Empresa_Model::lists(Empresa_Model::$razonSocial,Empresa_Model::$id)->toArray();

        return view('Planeacion.Registro_Padron.index', compact('arrayRegistroPadron', 'cliente', 'empresa'));

    }

    /**
     * Metodo que sirve para guardar o editar una falta
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    /*public function guardar()
    {

        $data = request()->all();

        dd($data);

        if ($data[Registro_Padron_Model::$id] == "") {

            $registroPadron = new Registro_Padron_Model();
            $registroPadron->id_usuario_creacion = Auth::id();
            $registroPadron->{Registro_Padron_Model::$estatus} = 'ACTIVA';

        } else {

            $registroPadron = Registro_Padron_Model::find($data[Registro_Padron_Model::$id]);
            $registroPadron->id_usuario_edicion = Auth::id();

        }

        $registroPadron->{Registro_Padron_Model::$fechaInicio} = $data[Registro_Padron_Model::$fechaInicio];
        $registroPadron->{Registro_Padron_Model::$fechaFin} = $data[Registro_Padron_Model::$fechaFin];
        $registroPadron->{Registro_Padron_Model::$idEmpleado} = $data[Registro_Padron_Model::$idEmpleado];

        $fecha1 = $data[Registro_Padron_Model::$fechaInicio];
        $fecha2 = $data[Registro_Padron_Model::$fechaFin];


        if ($registroPadron->save()) {
            return "Se guardo correctamente la Falta";
        } else {
            return "No se guardo exitosamente la Falta";
        }

    }*/

    /**
     * Metodo que sirve para guardar o editar un Registro de Padrón
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar(Request $request)
    {

        $data = $request->all();

        if ($data[Registro_Padron_Model::$id] == "") {

            $auxiliar = Registro_Padron_Model::where(Registro_Padron_Model::$idEmpresa, $data[Registro_Padron_Model::$idEmpresa])
                                                ->where(Registro_Padron_Model::$idCliente, $data[Registro_Padron_Model::$idCliente])
                                                ->update([Registro_Padron_Model::$estatus => "No vigente"]);

            $registroPadron = new Registro_Padron_Model();
            $registroPadron->id_usuario_creacion = Auth::id();
            $registroPadron->estatus = "Inscrito y vigente";

        } else {

            $registroPadron = Registro_Padron_Model::find($data[Registro_Padron_Model::$id]);
            $registroPadron->id_usuario_edicion = Auth::id();

        }

        $registroPadron->{Registro_Padron_Model::$idCliente} = $data[Registro_Padron_Model::$idCliente];
        $registroPadron->{Registro_Padron_Model::$idEmpresa} = $data[Registro_Padron_Model::$idEmpresa];

        if(isset($data[Registro_Padron_Model::$comentario])){
            $registroPadron->{Registro_Padron_Model::$comentario} = $data[Registro_Padron_Model::$comentario];
        }

        $registroPadron->{Registro_Padron_Model::$inicioVigencia} = $data[Registro_Padron_Model::$inicioVigencia];
        $registroPadron->{Registro_Padron_Model::$finVigencia} = $data[Registro_Padron_Model::$finVigencia];
        $registroPadron->{Registro_Padron_Model::$inscripcion} = $data[Registro_Padron_Model::$inscripcion];

        if ($registroPadron->save()) {
            
            if ($request->hasFile(Documento_Model::$id)) {

                $instancia = new Documento_Model();

                $documentoInsertado = Documento_Controller::guardarDocumentoServidor($request->file(Documento_Model::$id), "registro_padron", "app\\Obra\\Planeacion\\Registro_Padron\\", $instancia, $data[Empleado_Model::$id] , "registro_padron","");

                if (!$documentoInsertado) {
                    return Redirect::back()->with("error" , "Error al guardar el documento");
                }

            }

            return Redirect::back()->with("message" , "Se guardó correctamente");

        } else {
            return Redirect::back()->with("error" , "Error al guardar el Registro de Padrón");
        }

    }

    /**
     * Metodo que sirve para guardar o editar un Registro de Padrón
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function actualizar(Request $request)
    {

        $data = $request->all();
        //dd($data);
        if (!$data[Registro_Padron_Model::$id] == "") {

            $registroPadronAnterior = Registro_Padron_Model::find($data[Registro_Padron_Model::$id]);

            $auxiliar = Registro_Padron_Model::where(Registro_Padron_Model::$idEmpresa, $registroPadronAnterior->{Registro_Padron_Model::$idEmpresa} )
                                            ->where(Registro_Padron_Model::$idCliente, $registroPadronAnterior->{Registro_Padron_Model::$idCliente} )
                                            ->update([Registro_Padron_Model::$estatus => "No vigente"]);

            $registroPadron = new Registro_Padron_Model();
            $registroPadron->id_usuario_creacion = Auth::id();
            $registroPadron->estatus = "Inscrito y vigente";

            $registroPadron->{Registro_Padron_Model::$idCliente} = $registroPadronAnterior->{Registro_Padron_Model::$idCliente};
            $registroPadron->{Registro_Padron_Model::$idEmpresa} = $registroPadronAnterior->{Registro_Padron_Model::$idEmpresa};
            $registroPadron->{Registro_Padron_Model::$inscripcion} = $registroPadronAnterior->{Registro_Padron_Model::$inscripcion};

            if(isset($data[Registro_Padron_Model::$comentario])){
                $registroPadron->{Registro_Padron_Model::$comentario} = $data[Registro_Padron_Model::$comentario];
            }

            $registroPadron->{Registro_Padron_Model::$inicioVigencia} = $data[Registro_Padron_Model::$inicioVigencia];
            $registroPadron->{Registro_Padron_Model::$finVigencia} = $data[Registro_Padron_Model::$finVigencia];

            if ($registroPadron->save()) {
            
                if ($request->hasFile(Documento_Model::$id)) {

                    $instancia = new Documento_Model();

                    $documentoInsertado = Documento_Controller::guardarDocumentoServidor($request->file(Documento_Model::$id), "registro_padron", "app\\Obra\\Planeacion\\Registro_Padron\\", $instancia, $data[Registro_Padron_Model::$id] , "registro_padron","");

                    if (!$documentoInsertado) {
                        return Redirect::back()->with("error" , "Error al guardar el documento");
                    }

                }

                return Redirect::back()->with("message" , "Se guardó correctamente");

            } else {
                return Redirect::back()->with("error" , "Error al guardar el Registro de Padrón");
            }
        }else{
            return Redirect::back()->with("error" , "Error al guardar el Registro de Padrón");
        }

    }

    /**
     * Metodo que sirve para obtener un Registro de Padrón
     *
     * @param POST id_falta identificador unico
     * @author Oscar Vargas
     * @return json 
     */
    public function obtenerRegistroPadronEditar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Registro_Padron_Model::$id];

        $registroPadron = Registro_Padron_Model::FindOrFail($id);

        return $registroPadron;

    }

    /**
     * Metodo que sirve para obtener un Registro de Padrón
     *
     * @param POST id 
     * @author Erik Villarreal
     * @return json 
     */
    public function obtenerHistorialRegistroPadron()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.planeacion.registro_padron")) {
            return "Sin permiso para ver el historial";
        }

        $data = request()->all();
        $id = $data[Registro_Padron_Model::$id];

        $registroPadron = Registro_Padron_Model::FindOrFail($id);

        $registroPadron = Registro_Padron_Model::select(
                                Registro_Padron_Model::$id,
                                Registro_Padron_Model::$inicioVigencia,
                                Registro_Padron_Model::$finVigencia,    
                                Registro_Padron_Model::$idEmpresa,
                                Registro_Padron_Model::$idCliente,
                                Registro_Padron_Model::$inscripcion,
                                Registro_Padron_Model::$comentario,
                                Registro_Padron_Model::$estatus
                        )
                        ->where(Registro_Padron_Model::$idCliente, $registroPadron->{Registro_Padron_Model::$idCliente})
                        ->where(Registro_Padron_Model::$idEmpresa, $registroPadron->{Registro_Padron_Model::$idEmpresa})
                        ->orderBy(Registro_Padron_Model::$createdAt, "desc")
                        ->get();

        return $registroPadron;

    }

    /**
     * Metodo que sirve para eliminar una falta
     *
     * @param POST id_falta identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Registro_Padron_Model::$id];
        $registroPadron = Registro_Padron_Model::find($id);

        if ($registroPadron->delete()) {
            return "Se eliminó correctamente la Falta";
        } else {
            return "No se eliminó exitosamente la Falta";
        }

    }

    


}
