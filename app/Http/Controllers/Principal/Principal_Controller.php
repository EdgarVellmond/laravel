<?php

namespace App\Http\Controllers\Principal;


use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Principal_Controller extends Controller
{
    /**
     * Muestra la Pantalla Principal
     * @author Luis Alfredo Chavez
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');

    }

    public function  logout()
    {
        Auth::logout();
        return Redirect::to('/')->with('message', 'La sesión cerró correctamente.');
    }
}
