<?php

namespace App\Http\Controllers\Recursos_Humanos;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Genericos\Archivo_Controller;
use App\Http\Controllers\Genericos\Solicitud_Controller;
use App\Http\Controllers\Genericos\Pdf_Controller;
use App\Http\Controllers\Genericos\Documento_Controller;
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Area_Model;
use App\Models\Recursos_Humanos\Expediente_Digital_Empleado_Model;
use App\Models\Catalogos\Expediente_Digital_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Genericos\Documento_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Prima_Riesgo_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;
use App\Models\Catalogos\Plantilla_Model;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Recursos_Humanos\Plantilla_Empleado_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Recursos_Humanos\Falta_Model;
use App\Models\Recursos_Humanos\Incapacidad_Model;
use App\Models\Recursos_Humanos\Permiso_Asistencia_Model;
use App\Models\Recursos_Humanos\Vacaciones_Model;
use App\Models\Sistema\Usuarios_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;


class Empleado_Controller extends Controller
{
    const EQUALS = '=';
    const PUNTO = '.';
    const PERMISO_VISTA = 'vista.recursos_humanos.empleado';
    const PERMISO_ALTA = 'alta.recursos_humanos.empleado';
    const PERMISO_EDICION = 'edicion.recursos_humanos.empleado';
    const PERMISO_BAJA = 'baja.recursos_humanos.empleado';
    const PERMISO_VISTA_ADICIONAL = 'vista_adicional.recursos_humanos.empleado';
    const PERMISO_VISTA_LICENCIA = 'vista_licencia.recursos_humanos.empleado';
    const PERMISO_VISTA_NOMINA = 'vista_nomina.recursos_humanos.empleado';
    const PERMISO_VISTA_DOMICILIO = 'vista_domicilio.recursos_humanos.empleado';
    const PERMISO_VISTA_SUCURSAL = 'vista_sucursal.recursos_humanos.empleado';
    const PERMISO_VISTA_FICHA = 'ficha.recursos_humanos.empleado';
    const PERMISO_VISTA_FOTOGRAFIA = 'fotografia.recursos_humanos.empleado';
    const PERMISO_ALTA_CONTRATO = 'alta_contrato.recursos_humanos.empleado';
    const PERMISO_EDICION_CONTRATO = 'edicion_contrato.recursos_humanos.empleado';
    const PERMISO_UPLOAD_EXPEDIENTE_DIGITAL = 'upload.expediente_digital.empleado';
    const PERMISO_EDICION_EXPEDIENTE_DIGITAL = 'edicion.expediente_digital.empleado';

    /**
     * Muestra el catálogo de Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA)) {
            return Redirect::to('/');
        }

        $idSucursal = Helpers::get_idSucursal();

        $empleados = Empleado_Model::select(
            Empleado_Model::$id,
            Empleado_Model::$nombre,
            Empleado_Model::$apellidoPaterno,
            Empleado_Model::$apellidoMaterno,
            Empleado_Model::$numeroEmpleado,
            Empleado_Model::$idSucursal,
            Empleado_Model::$idEmpleadoImagen
        )->where(Empleado_Model::$idSucursal, $idSucursal)
            ->get();

        $arrayEmpresa = Empresa_Model::where(Empresa_Model::$patron, 1)->lists(Empresa_Model::$razonSocial, Empresa_Model::$id);
        $estado = Estado_Model::lists(Estado_Model::$nombre, Estado_Model::$id);
        $sucursal = Sucursal_Model::lists(Sucursal_Model::$nombre, Sucursal_Model::$id);
        $puesto = Puesto_Model::lists(Puesto_Model::$nombre, Puesto_Model::$id);
        $area = Area_Model::lists(Area_Model::$nombre, Area_Model::$id);
        $primaRiesgo = Prima_Riesgo_Model::lists(Prima_Riesgo_Model::$nombre, Prima_Riesgo_Model::$id);

        $permiso = self::PERMISO_VISTA;

        $solicitud = Solicitud_Controller::obtenerSolicitudesRecursosHumanos();

        return view('Recursos_Humanos.Empleado.index', compact("empleados", "arrayEmpresa", "estado", "sucursal", "puesto", "area", "permiso", "primaRiesgo", "solicitud"));

    }

    /**
     * Metodo que sirve para obtener la informacion de un empleado
     *
     * @author Oscar Vargas
     * @param $id identificador unico del empleado
     * @return ficha del empleado
     */
    public function ficha($id)
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA_FICHA)) {
            return Redirect::to('/');
        }

        $ficha = Empleado_Model::select(
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroEmpleado,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$rfc,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$curp,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nss,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$telefono,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$correo,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroLicencia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$vigenciaLicencia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$salarioBaseDiario,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$salarioDiarioIntegrado,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$razonSocial,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$viaticos,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$calle,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$colonia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroExterior,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroInterior,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$cp,
            DB::raw(Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$nombre . " as nombreDepartamento"),
            DB::raw(Puesto_Model::$tabla . self::PUNTO . Puesto_Model::$nombre . " as nombrePuesto"),
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . " as nombreMunicipio"),
            DB::raw(Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$nombre . " as nombreSucursal"),
            DB::raw(Prima_Riesgo_Model::$tabla . self::PUNTO . Prima_Riesgo_Model::$nombre . " as nombrePrimaRiesgo"),
            DB::raw(Prima_Riesgo_Model::$tabla . self::PUNTO . Prima_Riesgo_Model::$valor . " as valorPrimaRiesgo"),
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$diasVacaciones,
            Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$ruta,
            Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$uuid,
            Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$extension,
            DB::raw(Area_Model::$tabla . self::PUNTO . Area_Model::$nombre . " as nombreArea"),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . " as nombreEstado")
        )
            ->leftjoin(Empleado_Imagen_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idEmpleadoImagen,
                self::EQUALS,
                Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$id)
            ->leftjoin(Departamento_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idDepartamento,
                self::EQUALS,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$id)
            ->leftjoin(Municipio_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idMunicipio,
                self::EQUALS,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->leftjoin(Puesto_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPuesto,
                self::EQUALS,
                Puesto_Model::$tabla . self::PUNTO . Puesto_Model::$id)
            ->leftjoin(Sucursal_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal,
                self::EQUALS,
                Sucursal_Model::$tabla . self::PUNTO . Sucursal_Model::$id)
            ->leftjoin(Prima_Riesgo_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPrimaRiesgo,
                self::EQUALS,
                Prima_Riesgo_Model::$tabla . self::PUNTO . Prima_Riesgo_Model::$id)
            ->leftjoin(Area_Model::$tabla,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$idArea,
                self::EQUALS,
                Area_Model::$tabla . self::PUNTO . Area_Model::$id)
            ->leftjoin(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                self::EQUALS,
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->leftjoin(Empresa_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPatron,
                self::EQUALS,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id, $id)
            ->get();

        $contrato = Plantilla_Empleado_Model::select(Plantilla_Empleado_Model::$id,
                                            Plantilla_Empleado_Model::$idEmpleado, 
                                            Plantilla_Empleado_Model::$observaciones, 
                                            Plantilla_Empleado_Model::$fechaInicio,
                                            Plantilla_Empleado_Model::$fechaFin,
                                            Plantilla_Empleado_Model::$vigencia,
                                            Plantilla_Empleado_Model::$createdAt)
            ->where(Plantilla_Empleado_Model::$idEmpleado, $id)
            ->orderBy(Plantilla_Empleado_Model::$createdAt, "DESC")
            ->get();

        $expediente = Expediente_Digital_Model::select(
                                    Expediente_Digital_Model::$tabla.self::PUNTO.Expediente_Digital_Model::$id,
                                    Expediente_Digital_Model::$tabla.self::PUNTO.Expediente_Digital_Model::$nombre,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$idUsuarioCreacion,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$idUsuarioEdicion,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$createdAt,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$updatedAt,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$id,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$idDocumento,
                                    Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$vigencia,
                                    DB::raw(Usuarios_Model::$tabla."_Edicion".self::PUNTO.Usuarios_Model::$nombre . " as " . Usuarios_Model::$id."_Edicion"),
                                    Documento_Model::$tabla.self::PUNTO.Documento_Model::$id,
                                    Documento_Model::$tabla.self::PUNTO.Documento_Model::$ruta,
                                    Documento_Model::$tabla.self::PUNTO.Documento_Model::$uuid,
                                    Documento_Model::$tabla.self::PUNTO.Documento_Model::$extension,
                                    DB::raw(Documento_Model::$tabla.self::PUNTO.Documento_Model::$nombre . " as " . Documento_Model::$nombre . "_documento"),
                                    DB::raw("(SELECT " . 
                                        Usuarios_Model::$tabla.self::PUNTO.Usuarios_Model::$nombre 
                                        . " FROM " . Usuarios_Model::$tabla 
                                        . " WHERE " . Usuarios_Model::$tabla.self::PUNTO.Usuarios_Model::$id . " = " . Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$idUsuarioCreacion 
                                        . ") as id_usuario")
                            )
                            ->where(Expediente_Digital_Model::$tabla.self::PUNTO.Expediente_Digital_Model::$idCategoriaExpedienteDigital, 1)
                            ->leftJoin(Expediente_Digital_Empleado_Model::$tabla, function($leftJoin){
                                $leftJoin->on(
                                        Expediente_Digital_Model::$tabla.self::PUNTO.Expediente_Digital_Model::$id,
                                        self::EQUALS,
                                        Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$idExpedienteDigital
                                    )
                                ->where(Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$vigencia, "=", 1);
                            })
                            ->leftJoin(Documento_Model::$tabla, function($leftJoin) use ($id){
                                $leftJoin->on(
                                        Expediente_Digital_Empleado_Model::$tabla.self::PUNTO.Expediente_Digital_Empleado_Model::$idDocumento,
                                        self::EQUALS,
                                        Documento_Model::$tabla.self::PUNTO.Documento_Model::$id
                                    )
                                ->where(Documento_Model::$tabla.self::PUNTO.Documento_Model::$idPropietario, "=", $id);
                            })
                            ->leftJoin(DB::raw(Usuarios_Model::$tabla . " as " . Usuarios_Model::$tabla . "_Edicion"), function($leftJoin){
                                    $leftJoin->on(
                                            Expediente_Digital_Model::$tabla.self::PUNTO.Expediente_Digital_Model::$idUsuarioEdicion,
                                            self::EQUALS,
                                            Usuarios_Model::$tabla . "_Edicion" .self::PUNTO.Usuarios_Model::$id
                                        );
                            })
                            ->get();

        $plantillas = Plantilla_Model::select(Plantilla_Model::$id, Plantilla_Model::$nombre, Plantilla_Model::$idTipoPlantilla)
            ->where(Plantilla_Model::$idTipoPlantilla, 3)
            ->get();

        $incidencias = self::obtenerHistorialIncidencias($id);


        return view('Recursos_Humanos.Empleado.ficha', compact("ficha", "contrato", "incidencias", "solicitud", "plantillas", "expediente"));

    }

    /**
     * Metodo que sirve para traer todas las incidencias de una empleado como faltas, permisos, vacaciones e incapacidades
     *
     * @author Oscar Vargas
     * @param $id identificador unico del empleado
     * @return objeto con todas las incidencias del empleado
     */
    public function obtenerHistorialIncidencias($id)
    {

        $permisos = Permiso_Asistencia_Model::select(Permiso_Asistencia_Model::$fechaInicio,
            Permiso_Asistencia_Model::$fechaFin,
            Permiso_Asistencia_Model::$dias,
            Permiso_Asistencia_Model::$comentarios,
            DB::raw(" 'PERMISO' " . " as tipo"),
            DB::raw("(CASE  WHEN goce_sueldo = 'SI' THEN 'CON GOCE DE SUELDO' WHEN goce_sueldo = 'NO' THEN 'SIN GOCE DE SUELDO' END)  as estatus"))
            ->where(Permiso_Asistencia_Model::$idEmpleado, $id);

        $incapacidad = Incapacidad_Model::select(Incapacidad_Model::$fechaInicio,
            Incapacidad_Model::$fechaFin,
            Incapacidad_Model::$dias,
            Incapacidad_Model::$comentarios,
            DB::raw(" 'INCAPACIDAD' " . " as tipo"),
            DB::raw(Incapacidad_Model::$tipoIncapacidad . " as estatus"))
            ->where(Incapacidad_Model::$idEmpleado, $id);

        $vacaciones = Vacaciones_Model::select(Vacaciones_Model::$fechaInicio,
            Vacaciones_Model::$fechaFin,
            Vacaciones_Model::$dias,
            Vacaciones_Model::$comentarios,
            DB::raw(" 'VACACIONES' " . " as tipo"),
            DB::raw(" 'N/A' " . " as estatus"))
            ->where(Vacaciones_Model::$idEmpleado, $id);

        return $falta = Falta_Model::select(Falta_Model::$fechaInicio,
            Falta_Model::$fechaFin,
            Falta_Model::$dias,
            Falta_Model::$comentarios,
            DB::raw(" 'FALTA' " . " as tipo"),
            DB::raw(Falta_Model::$estatus . " as estatus"))
            ->where(Falta_Model::$idEmpleado, $id)
            ->union($permisos)
            ->union($incapacidad)
            ->union($vacaciones)
            ->get();

    }

    /**
     * Función para obtener la información adicional personal del Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function obtenerAdicionalEmpleado()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA_ADICIONAL)) {
            return "Sin premiso";
        }

        $data = request()->all();

        return $direccion = Empleado_Model::select(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$rfc,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$curp,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nss,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$telefono,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$correo,
            DB::raw(Area_Model::$tabla . self::PUNTO . Area_Model::$nombre . ' as ' . Area_Model::$id),
            DB::raw(Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$nombre . ' as ' . Departamento_Model::$id),
            DB::raw(Puesto_Model::$tabla . self::PUNTO . Puesto_Model::$nombre . ' as ' . Puesto_Model::$id)
        )
            ->join(Departamento_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idDepartamento,
                self::EQUALS,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$id)
            ->join(Area_Model::$tabla,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$idArea,
                self::EQUALS,
                Area_Model::$tabla . self::PUNTO . Area_Model::$id)
            ->join(Puesto_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPuesto,
                self::EQUALS,
                Puesto_Model::$tabla . self::PUNTO . Puesto_Model::$id)
            ->where(Empleado_Model::$id, $data[Empleado_Model::$id])
            ->get();

    }

    /**
     * Función para obtener la información de la licencia del Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function obtenerLicenciaEmpleado()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA_LICENCIA)) {
            return "Sin premiso";
        }

        $data = request()->all();

        return $direccion = Empleado_Model::select(Empleado_Model::$numeroLicencia,
            Empleado_Model::$vigenciaLicencia)
            ->where(Empleado_Model::$id, $data[Empleado_Model::$id])
            ->get();

    }

    /**
     * Función para obtener la información de la licencia del Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function obtenerNominaEmpleado()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA_NOMINA)) {
            return "Sin premiso";
        }

        $data = request()->all();

        return $direccion = Empleado_Model::select(Empleado_Model::$salarioBaseDiario,
            Empleado_Model::$salarioDiarioIntegrado,
            Empleado_Model::$viaticos,
            DB::raw(Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$razonSocial . ' as ' . Empleado_Model::$idPatron)
        )
            ->join(Empresa_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPatron,
                self::EQUALS,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$id)
            ->where(Empleado_Model::$id, $data[Empleado_Model::$id])
            ->get();

    }

    /**
     * Función para obtener la dirección del Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function obtenerDireccionEmpleado()
    {
        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA_DOMICILIO)) {
            return "Sin premiso";
        }

        $data = request()->all();

        return $direccion = Empleado_Model::select(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$calle,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroExterior,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroInterior,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$colonia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$cp,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . ' as ' . Municipio_Model::$id),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . ' as ' . Estado_Model::$id))
            ->join(Municipio_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idMunicipio,
                self::EQUALS,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                self::EQUALS,
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id, $data[Empleado_Model::$id])
            ->get();

    }

    /**
     * Función para obtener la información de la imagen del Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function obtenerImagenEmpleado()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso(self::PERMISO_VISTA_FOTOGRAFIA)) {
            return "Sin premiso";
        }

        $data = request()->all();

        return $imagen = Empleado_Imagen_Model::select(Empleado_Imagen_Model::$ruta,
            Empleado_Imagen_Model::$uuid,
            Empleado_Imagen_Model::$extension)
            ->where(Empleado_Imagen_Model::$id, $data[Empleado_Imagen_Model::$id])
            ->get();

    }

    /**
     * Función para obtener la información del Empleado
     * @author Erik Villarreal
     * @return JSON #Información completa del empleado como objeto JSON
     */
    public function obtenerEmpleado()
    {
        $data = request()->all();

        return $direccion = Empleado_Model::select(
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroEmpleado,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$rfc,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$curp,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nss,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$telefono,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$correo,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroLicencia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$vigenciaLicencia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$salarioBaseDiario,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$salarioDiarioIntegrado,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPatron,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$viaticos,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$calle,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$colonia,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroExterior,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroInterior,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$cp,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idDepartamento,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPuesto,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idMunicipio,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPrimaRiesgo,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$diasVacaciones,
            Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$ruta,
            Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$uuid,
            Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$extension,
            Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$idArea,
            Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado
        )
            ->join(Empleado_Imagen_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idEmpleadoImagen,
                self::EQUALS,
                Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$id)
            ->join(Departamento_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idDepartamento,
                self::EQUALS,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$id)
            ->join(Municipio_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idMunicipio,
                self::EQUALS,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id, $data[Empleado_Model::$id])
            ->get();

    }

    /**
     * Función para guardar un registro en el Catálogo de Empleado
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        $data = $request->all();

        if ($data[Empleado_Model::$id] == "") {

            //Permiso de alta de este Modulo
            if (!Helpers::get_permiso(self::PERMISO_ALTA)) {
                return "Sin premiso";
            }

            $empleado = new Empleado_Model();
            $empleado->{Empleado_Model::$idUsuarioCreacion} = Auth::id();
        } else {

            //Permiso de edición de este Modulo
            if (!Helpers::get_permiso(self::PERMISO_EDICION)) {
                return "Sin premiso";
            }

            $empleado = Empleado_Model::find($data[Empleado_Model::$id]);
            $empleado->{Empleado_Model::$idUsuarioEdicion} = Auth::id();
        }

        if ($request->hasFile(Empleado_Model::$idEmpleadoImagen)) {

            $instancia = new Empleado_Imagen_Model();
            $imagenInsertada = Archivo_Controller::guardarArchivoServidor($request->file(Empleado_Model::$idEmpleadoImagen), "empleado", "app\\Recursos_Humanos\\Empleado\\", $instancia, "", "");

            if ($imagenInsertada) {

                $empleado->{Empleado_Model::$idEmpleadoImagen} = $imagenInsertada;

            } else {
                return Redirect::back()->with("error" , "No se pudo guardar la imagen");
            }

        }
        $empleado->{Empleado_Model::$nombre} = $data[Empleado_Model::$nombre];
        $empleado->{Empleado_Model::$apellidoPaterno} = $data[Empleado_Model::$apellidoPaterno];
        $empleado->{Empleado_Model::$apellidoMaterno} = $data[Empleado_Model::$apellidoMaterno];
        $empleado->{Empleado_Model::$numeroEmpleado} = $data[Empleado_Model::$numeroEmpleado];
        $empleado->{Empleado_Model::$rfc} = $data[Empleado_Model::$rfc];
        $empleado->{Empleado_Model::$curp} = $data[Empleado_Model::$curp];
        $empleado->{Empleado_Model::$nss} = $data[Empleado_Model::$nss];
        $empleado->{Empleado_Model::$telefono} = $data[Empleado_Model::$telefono];
        $empleado->{Empleado_Model::$correo} = $data[Empleado_Model::$correo];
        $empleado->{Empleado_Model::$numeroLicencia} = $data[Empleado_Model::$numeroLicencia];
        $empleado->{Empleado_Model::$vigenciaLicencia} = $data[Empleado_Model::$vigenciaLicencia];
        $empleado->{Empleado_Model::$salarioBaseDiario} = $data[Empleado_Model::$salarioBaseDiario];
        $empleado->{Empleado_Model::$salarioDiarioIntegrado} = $data[Empleado_Model::$salarioDiarioIntegrado];

        if (isset($data[Empleado_Model::$viaticos])) {
            if ($data[Empleado_Model::$viaticos] == "on") {
                $empleado->{Empleado_Model::$viaticos} = 1;
            } else {
                $empleado->{Empleado_Model::$viaticos} = 0;
            }
        }

        $empleado->{Empleado_Model::$calle} = $data[Empleado_Model::$calle];
        $empleado->{Empleado_Model::$colonia} = $data[Empleado_Model::$colonia];
        $empleado->{Empleado_Model::$numeroInterior} = $data[Empleado_Model::$numeroInterior];
        $empleado->{Empleado_Model::$numeroExterior} = $data[Empleado_Model::$numeroExterior];
        $empleado->{Empleado_Model::$cp} = $data[Empleado_Model::$cp];
        $empleado->{Empleado_Model::$idDepartamento} = $data[Empleado_Model::$idDepartamento];
        $empleado->{Empleado_Model::$idPuesto} = $data[Empleado_Model::$idPuesto];
        $empleado->{Empleado_Model::$idMunicipio} = $data[Empleado_Model::$idMunicipio];
        $empleado->{Empleado_Model::$idSucursal} = 1;
        $empleado->{Empleado_Model::$idPatron} = $data[Empleado_Model::$idPatron];
        $empleado->{Empleado_Model::$idPrimaRiesgo} = $data[Empleado_Model::$idPrimaRiesgo];
        $empleado->{Empleado_Model::$diasVacaciones} = $data[Empleado_Model::$diasVacaciones];

        if($empleado->save()){

            return Redirect::back()->with("success" , "Se guardo el empleado correctamente");

        }else{

            return Redirect::back()->with("error" , "No se guardo el empleado correctamente");

        }

    }

    /**
     * Función para guardar un contrato creado a partir de una plantilla 
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function guardarPlantillaEmpleado(Request $request)
    {
        $data = $request->all();

        $contenido = Pdf_Controller::generarContenidoPlantilla($data[Plantilla_Model::$id], $data[Empleado_Model::$id]);
            
        if($contenido){

            if ($data[Plantilla_Empleado_Model::$id] == "") {

                //Permiso de alta de contrato de este Módulo
                if (!Helpers::get_permiso(self::PERMISO_ALTA_CONTRATO)) {
                    return "Sin premiso";
                }

                $plantillaEmpleado = new Plantilla_Empleado_Model();
                $plantillaEmpleado->{Plantilla_Empleado_Model::$idUsuarioCreacion} = Auth::id();

            } else {

                //Permiso de edición de contrato de este Módulo
                if (!Helpers::get_permiso(self::PERMISO_EDICION_CONTRATO)) {
                    return "Sin premiso";
                }

                $plantillaEmpleado = Plantilla_Empleado_Model::find($data[Plantilla_Empleado_Model::$id]);
                $plantillaEmpleado->{Plantilla_Empleado_Model::$idUsuarioEdicion} = Auth::id();

            }

            $plantillaEmpleado->{Plantilla_Empleado_Model::$idEmpleado} = $data[Empleado_Model::$id];
            $plantillaEmpleado->{Plantilla_Empleado_Model::$idPlantilla} = $data[Plantilla_Model::$id];
            $plantillaEmpleado->{Plantilla_Empleado_Model::$contenido} = $contenido;
            $plantillaEmpleado->{Plantilla_Empleado_Model::$vigencia} = 1;
            $plantillaEmpleado->{Plantilla_Empleado_Model::$fechaInicio} = $data[Plantilla_Empleado_Model::$fechaInicio];
            $plantillaEmpleado->{Plantilla_Empleado_Model::$fechaFin} = $data[Plantilla_Empleado_Model::$fechaFin];

            if(isset($data[Plantilla_Empleado_Model::$observaciones])){
                $plantillaEmpleado->{Plantilla_Empleado_Model::$observaciones} = $data[Plantilla_Empleado_Model::$observaciones];
            }

            if($data[Plantilla_Empleado_Model::$id] == ""){
                $auxiliarVigencia = Plantilla_Empleado_Model::where(Plantilla_Empleado_Model::$idEmpleado, $data[Empleado_Model::$id])
                                                        ->update([Plantilla_Empleado_Model::$vigencia => 0]);
            }else{
                $auxiliarVigencia = true;
            }

            if($auxiliarVigencia){
               
                if ($plantillaEmpleado->save()) {

                    return Redirect::back()->with("success" , "Se guardo el empleado correctamente");

                } else {

                    return Redirect::back()->with("error" , "Problema al guardar el contrato");

                }

            }else{

                return Redirect::back()->with("error" , "Problema al guardar el contrato");

            }

        }else{

            return Redirect::back()->with("error" , "Problema al guardar el contrato");

        }
        
    }

    /**
     * Función para guardar un contrato creado a partir de una plantilla 
     * @author Erik Villarreal
     * @return \Illuminate\Http\Response
     */
    public function guardarDocumentoExpedienteDigitalEmpleado(Request $request)
    {
        $data = $request->all();
        
        if ($data[Expediente_Digital_Empleado_Model::$id] == "") {

            //Permiso de edición de contrato de este Módulo
            if (!Helpers::get_permiso(self::PERMISO_UPLOAD_EXPEDIENTE_DIGITAL)) {
                return "Sin premiso";
            }

            $expedienteDigitalEmpleado = new Expediente_Digital_Empleado_Model();
            $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$idUsuarioCreacion} = Auth::id();
        } else {

            //Permiso de edición de contrato de este Módulo
            if (!Helpers::get_permiso(self::PERMISO_EDICION_EXPEDIENTE_DIGITAL)) {
                return "Sin premiso";
            }

            $expedienteDigitalEmpleado = Expediente_Digital_Empleado_Model::find($data[Expediente_Digital_Empleado_Model::$id]);
            $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$idUsuarioEdicion} = Auth::id();
        }

        if ($request->hasFile(Documento_Model::$id)) {

            $instancia = new Documento_Model();

            $documentoInsertado = Documento_Controller::guardarDocumentoServidor($request->file(Documento_Model::$id), "expediente_digital_empleado", "app\\Expediente\\Empleado\\", $instancia, $data[Empleado_Model::$id] , "empleado","");

            if ($documentoInsertado) { 
                $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$idDocumento} = $documentoInsertado;
            }else{
                return Redirect::back()->with("error" , "Error al guardar el documento");
            }

        }

        $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$idEmpleado} = $data[Empleado_Model::$id];
        $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$idExpedienteDigital} = $data[Expediente_Digital_Model::$id];

        if(isset($data[Expediente_Digital_Empleado_Model::$observaciones])){
            $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$idEmpleado} = $data[Expediente_Digital_Empleado_Model::$observaciones];
        }
        
        $expedienteDigitalEmpleado->{Expediente_Digital_Empleado_Model::$vigencia} = 1;

        if($data[Expediente_Digital_Empleado_Model::$id] == ""){
            $auxiliarVigencia = Expediente_Digital_Empleado_Model::where(Expediente_Digital_Empleado_Model::$idEmpleado, $data[Expediente_Digital_Empleado_Model::$id])
                                                        ->update([Expediente_Digital_Empleado_Model::$vigencia => 0]);
        }else{
            $auxiliarVigencia = true;
        }

        if($expedienteDigitalEmpleado->save()){

            return Redirect::back()->with("success" , "Se guardo el empleado correctamente");

        }else{

            return Redirect::back()->with("error" , "No se guardo el empleado correctamente");

        }


        
    }

    /**
     * Función para borrar un registro en el Catálogo de Empleado (Borrado lógico)
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso de edición de contrato de este Módulo
        if (!Helpers::get_permiso(self::PERMISO_BAJA)) {
            return "Sin premiso";
        }

        $data = request()->all();

        $empleado = Empleado_Model::find($data[Empleado_Model::$id]);
        $archivo = Empleado_Imagen_Model::find($empleado->{Empleado_Model::$idEmpleadoImagen});

        if ($archivo) {
            if (Archivo_Controller::borrar('empleado', $archivo->{Empleado_Imagen_Model::$uuid}, $archivo->{Empleado_Imagen_Model::$extension})) {
                $archivo->delete();
            }
        }

        if ($empleado->delete()) {
            return "Borrado correctamente.";
        } else {
            return "Problemas al borrar.";
        }

    }

}
