<?php

namespace App\Http\Controllers\Recursos_Humanos;

use App\Models\Recursos_Humanos\Falta_Archivo_Model;
use App\Models\Recursos_Humanos\Falta_Model;
use App\Models\Recursos_Humanos\Empleado_Model;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Genericos\Archivo_Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\Helpers;

class Falta_Controller extends Controller
{

    const PUNTO = '.';

    /**
     * Retorna la vista del index de falta
     * @author Oscar Vargas
     * @return view #Vista index de falta
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $idSucursal = Helpers::get_idSucursal();

        $arrayFalta = Falta_Model::select(Falta_Model::$tabla . self::PUNTO . Falta_Model::$id,
            Falta_Model::$tabla . self::PUNTO . Falta_Model::$fechaInicio,
            Falta_Model::$tabla . self::PUNTO . Falta_Model::$fechaFin,
            Falta_Model::$tabla . self::PUNTO . Falta_Model::$dias,
            Falta_Model::$tabla . self::PUNTO . Falta_Model::$comentarios,
            Falta_Model::$tabla . self::PUNTO . Falta_Model::$estatus,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->join(Empleado_Model::$tabla,
                Falta_Model::$tabla . self::PUNTO . Falta_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal, $idSucursal)
            ->get();

        $arrayEmpleado = Empleado_Model::select(Empleado_Model::$id, DB::raw("CONCAT(" . Empleado_Model::$nombre . ",' '," .
            Empleado_Model::$apellidoPaterno . ",' '," .
            Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->where(Empleado_Model::$idSucursal, $idSucursal)
            ->lists("nombreEmpleado", Empleado_Model::$id);

        return view('Recursos_Humanos.Falta.index', compact('arrayFalta', "arrayEmpleado"));

    }

    /**
     * Metodo que sirve para guardar o editar una falta
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Falta_Model::$id] == "") {

            $falta = new Falta_Model();
            $falta->id_usuario_creacion = Auth::id();
            $falta->{Falta_Model::$estatus} = 'ACTIVA';

        } else {

            $falta = Falta_Model::find($data[Falta_Model::$id]);
            $falta->id_usuario_edicion = Auth::id();

        }

        $falta->{Falta_Model::$fechaInicio} = $data[Falta_Model::$fechaInicio];
        $falta->{Falta_Model::$fechaFin} = $data[Falta_Model::$fechaFin];
        $falta->{Falta_Model::$idEmpleado} = $data[Falta_Model::$idEmpleado];

        $fecha1 = $data[Falta_Model::$fechaInicio];
        $fecha2 = $data[Falta_Model::$fechaFin];
        $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
        $valor = abs($valor);
        $valor = floor($valor);
        $falta->{Falta_Model::$dias} = $valor + 1;


        if ($falta->save()) {
            return "Se guardo correctamente la Falta";
        } else {
            return "No se guardo exitosamente la Falta";
        }

    }

    /**
     * Metodo que sirve para obtener una falt
     *
     * @param POST id_falta identificador unico
     * @author Oscar Vargas
     * @return json con la falta solicitado
     */
    public function obtenerFalta()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Falta_Model::$id];

        $falta = Falta_Model::FindOrFail($id);

        return $falta;

    }

    /**
     * Metodo que sirve para eliminar una falta
     *
     * @param POST id_falta identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Falta_Model::$id];
        $falta = Falta_Model::find($id);

        if ($falta->delete()) {
            return "Se eliminó correctamente la Falta";
        } else {
            return "No se eliminó exitosamente la Falta";
        }

    }

    /**
     * Metodo que sirve para subir un archivo de falta
     * @author Oscar Vargas
     * @param Request $request
     * @return string
     */
    public function upload(Request $request)
    {

        $data = $request->all();
        $falta = Falta_Model::find($data[Falta_Model::$id]);

        if ($request->hasFile(Falta_Model::$idFaltaArchivo)) {

            $instancia = new Falta_Archivo_Model();
            $imagenInsertada = Archivo_Controller::guardarArchivoServidor($request->file(Falta_Model::$idFaltaArchivo), "falta", "app\\Recursos_Humanos\\Falta\\", $instancia, "", "");

            if ($imagenInsertada) {

                $falta->{Falta_Model::$idFaltaArchivo} = $imagenInsertada;

            } else {
                return "Error al guardar Imagen";
            }

            $falta->save();
            return Redirect::back();
        }

        return Redirect::back();
    }

    /**
     * Función para obtener la información del archivo de la falta
     * @author Oscar Vargas
     * @return \Illuminate\Http\Response
     */
    public function obtenerArchivoFalta()
    {
        $data = request()->all();

        return $imagen = Falta_Archivo_Model::select(Falta_Archivo_Model::$tabla . self::PUNTO . Falta_Archivo_Model::$ruta,
            Falta_Archivo_Model::$tabla . self::PUNTO . Falta_Archivo_Model::$uuid,
            Falta_Archivo_Model::$tabla . self::PUNTO . Falta_Archivo_Model::$extension)
            ->join(Falta_Model::$tabla,
                Falta_Archivo_Model::$tabla . self::PUNTO . Falta_Archivo_Model::$id,
                "=",
                Falta_Model::$tabla . self::PUNTO . Falta_Model::$idFaltaArchivo)
            ->where(Falta_Model::$tabla . self::PUNTO . Falta_Model::$id, $data[Falta_Model::$id])
            ->get();

    }

    /**
     * Metodo que sirve para justificar una falta
     *
     * @param POST id_falta identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function justificar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("justificar.recursos_humanos.falta")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Falta_Model::$id];
        $falta = Falta_Model::find($id);

        $falta->{Falta_Model::$estatus} = "JUSTIFICADA";
        $falta->{Falta_Model::$comentarios} = $data[Falta_Model::$comentarios];

        if ($falta->save()) {
            return "Se justificó correctamente la Falta";
        } else {
            return "No se justificó exitosamente la Falta";
        }

    }


}
