<?php

namespace App\Http\Controllers\Recursos_Humanos;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Recursos_Humanos\Incapacidad_Archivo_Model;
use App\Models\Recursos_Humanos\Incapacidad_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Genericos\Archivo_Controller;
use Illuminate\Support\Facades\Redirect;

class Incapacidad_Controller extends Controller
{

    const PUNTO = '.';

    /**
     * Retorna la vista del index de incapacidad
     * @author Oscar Vargas
     * @return view #Vista index de incapacidad
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.recursos_humanos.incapacidad")) {
            return Redirect::to('/');
        }

        $idSucursal = Helpers::get_idSucursal();

        $arrayIncapacidad = Incapacidad_Model::select(Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$id,
            Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$fechaInicio,
            Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$fechaFin,
            Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$dias,
            Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$comentarios,
            Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$tipoIncapacidad,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->join(Empleado_Model::$tabla,
                Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal, $idSucursal)
            ->get();

        $arrayEmpleado = Empleado_Model::select(Empleado_Model::$id, DB::raw("CONCAT(" . Empleado_Model::$nombre . ",' '," .
            Empleado_Model::$apellidoPaterno . ",' '," .
            Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->where(Empleado_Model::$idSucursal, $idSucursal)
            ->lists("nombreEmpleado", Empleado_Model::$id);

        return view('Recursos_Humanos.Incapacidad.index', compact('arrayIncapacidad', "arrayEmpleado"));

    }

    /**
     * Metodo que sirve para guardar o editar una incapacidad
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.recursos_humanos.incapacidad")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Incapacidad_Model::$id] == "") {

            $incapacidad = new Incapacidad_Model();
            $incapacidad->id_usuario_creacion = Auth::id();

        } else {

            $incapacidad = Incapacidad_Model::find($data[Incapacidad_Model::$id]);
            $incapacidad->id_usuario_edicion = Auth::id();

        }

        $incapacidad->{Incapacidad_Model::$fechaInicio} = $data[Incapacidad_Model::$fechaInicio];
        $incapacidad->{Incapacidad_Model::$fechaFin} = $data[Incapacidad_Model::$fechaFin];
        $incapacidad->{Incapacidad_Model::$comentarios} = $data[Incapacidad_Model::$comentarios];
        $incapacidad->{Incapacidad_Model::$idEmpleado} = $data[Incapacidad_Model::$idEmpleado];
        $incapacidad->{Incapacidad_Model::$tipoIncapacidad} = $data[Incapacidad_Model::$tipoIncapacidad];
        $fecha1 = $data[Incapacidad_Model::$fechaInicio];
        $fecha2 = $data[Incapacidad_Model::$fechaFin];
        $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
        $valor = abs($valor);
        $valor = floor($valor);
        $incapacidad->{Incapacidad_Model::$dias} = $valor + 1;


        if ($incapacidad->save()) {
            return "Se guardo correctamente la Incapacidad";
        } else {
            return "No se guardo exitosamente la Incapacidad";
        }

    }

    /**
     * Metodo que sirve para obtener una incapacidad
     *
     * @param POST id_incapacidad identificador unico
     * @author Oscar Vargas
     * @return json con la incapacidad solicitado
     */
    public function obtenerIncapacidad()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.recursos_humanos.incapacidad")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Incapacidad_Model::$id];

        $incapacidad = Incapacidad_Model::FindOrFail($id);

        return $incapacidad;

    }

    /**
     * Metodo que sirve para eliminar una incapacidad
     *
     * @param POST id_incapacidad identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.recursos_humanos.incapacidad")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Incapacidad_Model::$id];
        $incapacidad = Incapacidad_Model::find($id);

        if ($incapacidad->delete()) {
            return "Se eliminó correctamente la Incapacidad";
        } else {
            return "No se eliminó exitosamente la Incapacidad";
        }

    }

    public function upload(Request $request)
    {

        $data = $request->all();
        $incapacidad = Incapacidad_Model::find($data[Incapacidad_Model::$id]);

        if ($request->hasFile(Incapacidad_Model::$idIncapacidadArchivo)) {

            $instancia = new Incapacidad_Archivo_Model();
            $imagenInsertada = Archivo_Controller::guardarArchivoServidor($request->file(Incapacidad_Model::$idIncapacidadArchivo), "incapacidad", "app\\Recursos_Humanos\\Incapacidad\\", $instancia, "", "");

            if ($imagenInsertada) {

                $incapacidad->{Incapacidad_Model::$idIncapacidadArchivo} = $imagenInsertada;

            } else {
                return "Error al guardar Imagen";
            }

            $incapacidad->save();
            return Redirect::back();
        }

        return Redirect::back();
    }

    /**
     * Función para obtener la información del archivo de la incapacidad
     * @author Oscar Vargas
     * @return \Illuminate\Http\Response
     */
    public function obtenerArchivoIncapacidad()
    {
        $data = request()->all();

        return $imagen = Incapacidad_Archivo_Model::select(Incapacidad_Archivo_Model::$tabla . self::PUNTO . Incapacidad_Archivo_Model::$ruta,
            Incapacidad_Archivo_Model::$tabla . self::PUNTO . Incapacidad_Archivo_Model::$uuid,
            Incapacidad_Archivo_Model::$tabla . self::PUNTO . Incapacidad_Archivo_Model::$extension)
            ->join(Incapacidad_Model::$tabla,
                Incapacidad_Archivo_Model::$tabla . self::PUNTO . Incapacidad_Archivo_Model::$id,
                "=",
                Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$idIncapacidadArchivo)
            ->where(Incapacidad_Model::$tabla . self::PUNTO . Incapacidad_Model::$id, $data[Incapacidad_Model::$id])
            ->get();

    }

}
