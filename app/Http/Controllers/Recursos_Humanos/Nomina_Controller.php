<?php

namespace App\Http\Controllers\Recursos_Humanos;

use App\Models\Catalogos\Cuota_Obrero_Patronal_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Impuesto_Isr_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Salario_Minimo_Model;
use App\Models\Recursos_Humanos\Falta_Model;
use App\Models\Recursos_Humanos\Incapacidad_Model;
use App\Models\Recursos_Humanos\Nomina_Model;
use App\Models\Recursos_Humanos\Permiso_Asistencia_Model;
use Hamcrest\FeatureMatcher;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Support\Facades\Auth;


class Nomina_Controller extends Controller
{

    const PUNTO = '.';
    const ACTIVA = "ACTIVA";
    const MAYOR_IGUAL = ">=";
    const MENOR_IGUAL = "<=";
    const MAYOR = ">";
    const MENOR = "<";
    const NO = "NO";
    const SEMANAL = "SEMANAL";
    const DECENAL = "DECENAL";
    const CATORCENAL = "CATORCENAL";
    const QUINCENAL = "QUINCENAL";
    const MENSUAL = "MENSUAL";
    const IGUAL = "=";
    const EXCEDENTE = "Excedente";

    /**
     * Retorna la vista del index de nomina
     * @author Oscar Vargas
     * @return view #Vista index de nomina
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.recursos_humanos.nomina")) {
            return Redirect::to('/');
        }

        $arrayNomina = Nomina_Model::all();

        $arrayEmpresa = Empresa_Model::where(Empresa_Model::$patron, 1)
            ->lists(Empresa_Model::$razonSocial, Empresa_Model::$id);

        return view('Recursos_Humanos.Nomina.index', compact('arrayNomina', 'arrayEmpresa'));

    }

    /**
     * Metodo que sirve para generar la nomina
     *
     * @author Oscar Vargas
     * @return nomina por empleado
     */
    public function generarNomina()
    {

        $data = request()->all();

        $idEmpresa = $data[Nomina_Model::$idEmpresa];
        $fechaInicio = $data[Nomina_Model::$fechaInicio];
        $fechaFin = $data[Nomina_Model::$fechaFin];
        $arrayEmpresa = self::getEmpresa($idEmpresa);
        $tipoNomina = $arrayEmpresa[0]->{Empresa_Model::$tipoNomina};
        $diasNomina = self::definirDiasTipoNomina($arrayEmpresa[0]->{Empresa_Model::$tipoNomina});

        $array = [];
        $arrayValores = [];

        // Ciclo para obtener la informacion de la nomina por cada empleado
        $arrayEmpleado = self::getEmpleadosPorEmpresa($idEmpresa);

        foreach ($arrayEmpleado as $empleado) {

            $sueldoDiario = $empleado->{Empleado_Model::$salarioBaseDiario};
            $sueldoDiarioIntegrado = $empleado->{Empleado_Model::$salarioDiarioIntegrado};
            $faltas = self::getFaltas($fechaInicio, $fechaFin, $empleado->{Empleado_Model::$id});
            $incapacidads = self::getIncapacidades($fechaInicio, $fechaFin, $empleado->{Empleado_Model::$id});
            $permisos = self::getPermisosSinGoceSueldo($fechaInicio, $fechaFin, $empleado->{Empleado_Model::$id});
            $arrayValores["diasTotales"] = $diasNomina - ($incapacidads + $faltas + $permisos);
            $arrayValores["septimoDia"] = self::definirSeptimoDia($arrayValores["diasTotales"], $sueldoDiario);
            $numeroDomingos = (int)($arrayValores["diasTotales"] / 7);
            $arrayValores["sueldo"] = ($sueldoDiario * ($arrayValores["diasTotales"] - $numeroDomingos));
            $arrayValores["imss"] = self::calcularImss($arrayValores["diasTotales"], $sueldoDiarioIntegrado);
            $arrayValores["ispt"] = self::calcularIspt($sueldoDiario, $arrayValores["diasTotales"]);
            $array[$empleado->{Empleado_Model::$id}] = $arrayValores;

        }


        $pdf = \PDF::loadView('Recursos_Humanos.Nomina.pdf.nomina', compact('arrayEmpleado', 'arrayEmpresa', 'fechaInicio', 'fechaFin', 'array'))
            ->setPaper('Letter')
            ->setOption('footer-left', utf8_decode('Fecha de Impresión: ' . date("Y-m-d H:i:s")))
            ->setOption('footer-right', utf8_decode('Pagina: [page] de [topage]'))
            ->setOption('footer-font-size', '10');


        return $pdf->stream('Recibo de Nomina.pdf');
    }

    /**
     * Metodo que sirve para calcular el ispt por empleado
     *
     * @author Oscar Vargas
     * @param $sueldoDiario sueldo diario del empleado
     * @param $diasTotales dias totales trabajados
     * @return float valor del impuesto generado por el empleado
     */
    public function calcularIspt($sueldoDiario, $diasTotales)
    {

        $valor = $sueldoDiario * 30.4;

        $arrayIsr = Impuesto_Isr_Model::all();

        foreach ($arrayIsr as $isr) {

            if (($valor >= $isr->{Impuesto_Isr_Model::$limiteInferior}) && ($valor <= $isr->{Impuesto_Isr_Model::$limiteSuperior})) {

                $valor -= $isr->{Impuesto_Isr_Model::$limiteInferior};

                $valor = $valor * $isr->{Impuesto_Isr_Model::$porcentaje};
                $valor += $isr->{Impuesto_Isr_Model::$cuotaFija};

                $valor = ($valor / 30.4) * $diasTotales;

            }

        }
        return $valor;
    }

    /**
     * Metodo que sirve para calcular el imss de cada empleado
     *
     * @author Oscar Vargas
     * @param $diasTotales  total de dias pagados
     * @param $sueldoDiarioIntegrado  sueldo diario integrado del empleado
     * @return int retorna valor del imss por el empleado
     */
    public function calcularImss($diasTotales, $sueldoDiarioIntegrado)
    {

        $array = Cuota_Obrero_Patronal_Model::all();
        $salarioMinimo = self::getSalarioMinimo();

        if ($sueldoDiarioIntegrado > ($salarioMinimo * 3)) {
            $excedente = $diasTotales * ($sueldoDiarioIntegrado - ($salarioMinimo * 3));
        }

        $imss = 0;

        $sueldo = $diasTotales * $sueldoDiarioIntegrado;

        foreach ($array as $cuota) {

            if ($cuota->{Cuota_Obrero_Patronal_Model::$ramo} == self::EXCEDENTE) {

                if (isset($excedente)) {

                    $imss += ($cuota->{Cuota_Obrero_Patronal_Model::$valor} * $excedente);

                } else {
                    $imss += 0;
                }
            } else {
                $imss += $cuota->{Cuota_Obrero_Patronal_Model::$valor} * $sueldo;
            }

        }

        return $imss;
    }

    /**
     * Metodo que sirve para obtener el salario minimo actual
     *
     * @author Oscar Vargas
     * @return valor del salario minimo DF
     */
    public function getSalarioMinimo()
    {

        $salario = Salario_Minimo_Model::select(Salario_Minimo_Model::$valor)->get();

        return $salario[0]->{Salario_Minimo_Model::$valor};
    }

    /**
     * Metodo que sirve para obtener los permisos sin gose d sueldo de un empleado en un rango de fechas
     *
     * @author Oscar Vargas
     * @param $idEmpleado identificador unico del empleado
     * @return numero de permisos sin gose de sueldo
     */
    public function getPermisosSinGoceSueldo($fechaInicio, $fechaFin, $idEmpleado)
    {

        $valorDiasDentro = Permiso_Asistencia_Model::select(DB::raw("sum(dias) as dias"))
            ->where(Permiso_Asistencia_Model::$idEmpleado, $idEmpleado)
            ->where(Permiso_Asistencia_Model::$fechaInicio, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Permiso_Asistencia_Model::$fechaFin, self::MENOR_IGUAL, $fechaFin)
            ->where(Permiso_Asistencia_Model::$goceSueldo, self::NO)
            ->get();


        $temp = Permiso_Asistencia_Model::where(Permiso_Asistencia_Model::$idEmpleado, $idEmpleado)
            ->where(Permiso_Asistencia_Model::$fechaInicio, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Permiso_Asistencia_Model::$fechaInicio, self::MENOR_IGUAL, $fechaFin)
            ->where(Permiso_Asistencia_Model::$fechaFin, self::MAYOR, $fechaFin)
            ->where(Permiso_Asistencia_Model::$goceSueldo, self::NO)
            ->get();

        $diasSumar = 0;

        foreach ($temp as $valor) {

            $fecha1 = $valor->{Permiso_Asistencia_Model::$fechaInicio};
            $fecha2 = $fechaFin;
            $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
            $valor = abs($valor);
            $valor = floor($valor);
            $diasSumar = $diasSumar + ($valor + 1);

        }

        $temp2 = Permiso_Asistencia_Model::where(Permiso_Asistencia_Model::$idEmpleado, $idEmpleado)
            ->where(Permiso_Asistencia_Model::$fechaFin, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Permiso_Asistencia_Model::$fechaFin, self::MENOR_IGUAL, $fechaFin)
            ->where(Permiso_Asistencia_Model::$fechaInicio, self::MENOR, $fechaInicio)
            ->where(Permiso_Asistencia_Model::$goceSueldo, self::NO)
            ->get();

        foreach ($temp2 as $valor) {

            $fecha1 = $fechaInicio;
            $fecha2 = $valor->{Permiso_Asistencia_Model::$fechaFin};
            $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
            $valor = abs($valor);
            $valor = floor($valor);
            $diasSumar = $diasSumar + ($valor + 1);

        }

        return $valorDiasDentro[0]->{Permiso_Asistencia_Model::$dias} + $diasSumar;

    }


    /**
     * Metodo que sirve para obtener las incapacidades de un empleado en un rango de fechas
     *
     * @author Oscar Vargas
     * @param $idEmpleado identificador unico del empleado
     * @return numero de incapacidad
     */
    public function getIncapacidades($fechaInicio, $fechaFin, $idEmpleado)
    {

        $valorDiasDentro = Incapacidad_Model::select(DB::raw("sum(dias) as dias"))
            ->where(Incapacidad_Model::$idEmpleado, $idEmpleado)
            ->where(Incapacidad_Model::$fechaInicio, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Incapacidad_Model::$fechaFin, self::MENOR_IGUAL, $fechaFin)
            ->get();


        $temp = Incapacidad_Model::where(Incapacidad_Model::$idEmpleado, $idEmpleado)
            ->where(Incapacidad_Model::$fechaInicio, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Incapacidad_Model::$fechaInicio, self::MENOR_IGUAL, $fechaFin)
            ->where(Incapacidad_Model::$fechaFin, self::MAYOR, $fechaFin)
            ->get();

        $diasSumar = 0;

        foreach ($temp as $valor) {


            $fecha1 = $valor->{Incapacidad_Model::$fechaInicio};
            $fecha2 = $fechaFin;
            $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
            $valor = abs($valor);
            $valor = floor($valor);
            $diasSumar = $diasSumar + ($valor + 1);

        }

        $temp2 = Incapacidad_Model::where(Incapacidad_Model::$idEmpleado, $idEmpleado)
            ->where(Incapacidad_Model::$fechaFin, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Incapacidad_Model::$fechaFin, self::MENOR_IGUAL, $fechaFin)
            ->where(Incapacidad_Model::$fechaInicio, self::MENOR, $fechaInicio)
            ->get();


        foreach ($temp2 as $valor) {


            $fecha1 = $fechaInicio;
            $fecha2 = $valor->{Incapacidad_Model::$fechaFin};
            $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
            $valor = abs($valor);
            $valor = floor($valor);
            $diasSumar = $diasSumar + ($valor + 1);

        }

        return $valorDiasDentro[0]->{Incapacidad_Model::$dias} + $diasSumar;

    }

    /**
     * Metodo que sirve para obtener las faltas de un empleado en un rango de fechas
     *
     * @author Oscar Vargas
     * @param $idEmpleado identificador unico del empleado
     * @return numero de faltas
     */
    public function getFaltas($fechaInicio, $fechaFin, $idEmpleado)
    {

        $valorDiasDentro = Falta_Model::select(DB::raw("sum(dias) as dias"))
            ->where(Falta_Model::$estatus, self::ACTIVA)
            ->where(Falta_Model::$idEmpleado, $idEmpleado)
            ->where(Falta_Model::$fechaInicio, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Falta_Model::$fechaFin, self::MENOR_IGUAL, $fechaFin)
            ->get();

        $temp = Falta_Model::where(Falta_Model::$estatus, self::ACTIVA)
            ->where(Falta_Model::$idEmpleado, $idEmpleado)
            ->where(Falta_Model::$fechaInicio, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Falta_Model::$fechaInicio, self::MENOR_IGUAL, $fechaInicio)
            ->where(Falta_Model::$fechaFin, self::MAYOR, $fechaFin)
            ->get();

        $diasSumar = 0;

        foreach ($temp as $valor) {


            $fecha1 = $valor->{Falta_Model::$fechaInicio};
            $fecha2 = $fechaFin;
            $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
            $valor = abs($valor);
            $valor = floor($valor);
            $diasSumar = $diasSumar + ($valor + 1);

        }

        $temp2 = Falta_Model::where(Falta_Model::$idEmpleado, $idEmpleado)
            ->where(Falta_Model::$fechaFin, self::MAYOR_IGUAL, $fechaInicio)
            ->where(Falta_Model::$fechaFin, self::MENOR_IGUAL, $fechaFin)
            ->where(Falta_Model::$fechaInicio, self::MENOR, $fechaInicio)
            ->get();


        foreach ($temp2 as $valor) {


            $fecha1 = $fechaInicio;
            $fecha2 = $valor->{Falta_Model::$fechaFin};
            $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
            $valor = abs($valor);
            $valor = floor($valor);
            $diasSumar = $diasSumar + ($valor + 1);

        }

        return $valorDiasDentro[0]->{Falta_Model::$dias} + $diasSumar;

    }

    /**
     * Metodo que sirve para definir cuantos dias tiene el tipo de nomina que usa la empresa
     *
     * @author Oscar Vargas
     * @param $tipo tipo de nomina de la empresa
     * @return entero con los dias de la nomina
     */
    public function definirDiasTipoNomina($tipo)
    {
        $dias = 0;

        switch ($tipo) {
            case self::SEMANAL:
                $dias = 7;
                break;
            case self::DECENAL:
                $dias = 10;
                break;
            case self::CATORCENAL:
                $dias = 14;
                break;
            case self::QUINCENAL:
                $dias = 15;
                break;
            case self::MENSUAL:
                $dias = 30;
                break;
        }

        return $dias;
    }

    /**
     * Metodo que sirve para definir cuanto se le paga por el septimo dia al empleada
     *
     * @author Oscar Vargas
     * @param $tipo tipo de nomina de la empresa
     * @return entero con el numero de domingos de la nomina
     */
    public function definirSeptimoDia($dias, $sueldo)
    {
        $septimoDia = 0;
        if ($dias < 7) {
            $dias -= 1;
        }
        $valor = (int)($dias / 7);
        $septimoDia = $valor * $sueldo;

        if (($dias / 7) <> ((int)($dias / 7))) {
            $septimoDia += (($sueldo / 6) * ($dias - ($valor * 7)));
        }

        return $septimoDia;
    }

    /**
     * Metodo que sirve para obtener todos los empleados dados de alta con el patron especificado para el llenado de la nomina
     *
     * @author Oscar Vargas
     * @param $idEmpresa identificador unico de la empresa
     * @return objeto con la informacion de los empleados
     */
    public function getEmpleadosPorEmpresa($idEmpresa)
    {

        return Empleado_Model::select(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$numeroEmpleado,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$rfc,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$curp,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nss,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$inicioContrato,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$salarioBaseDiario,
            Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$salarioDiarioIntegrado,
            DB::raw(Puesto_Model::$tabla . self::PUNTO . Puesto_Model::$nombre . " as nombrePuesto"),
            DB::raw(Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$nombre . " as nombreDepartamento"))
            ->join(Puesto_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPuesto,
                self::IGUAL,
                Puesto_Model::$tabla . self::PUNTO . Puesto_Model::$id)
            ->join(Departamento_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idDepartamento,
                self::IGUAL,
                Departamento_Model::$tabla . self::PUNTO . Departamento_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idPatron, $idEmpresa)
            ->get();

    }

    /**
     * Metodo que sirve para obtener los datos de la empresa para el llenado de la nomina
     *
     * @author Oscar Vargas
     * @param $idEmpresa identificador unico de la empresa
     * @return objeto con la informacion de la empresa
     */
    public function getEmpresa($idEmpresa)
    {

        return Empresa_Model::select(Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$razonSocial,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$calle,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$numeroInterior,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$numeroExterior,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$cp,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$colonia,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$rfc,
            Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$tipoNomina,
            DB::raw(Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$nombre . " as nombreMunicipio"),
            DB::raw(Estado_Model::$tabla . self::PUNTO . Estado_Model::$nombre . " as nombreEstado"))
            ->join(Municipio_Model::$tabla,
                Empresa_Model::$tabla . self::PUNTO . Empresa_Model::$idMunicipio,
                self::IGUAL,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$id)
            ->join(Estado_Model::$tabla,
                Municipio_Model::$tabla . self::PUNTO . Municipio_Model::$idEstado,
                self::IGUAL,
                Estado_Model::$tabla . self::PUNTO . Estado_Model::$id)
            ->where(Empresa_Model::$id, $idEmpresa)
            ->get();

    }
}
