<?php

namespace App\Http\Controllers\Recursos_Humanos;

use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Recursos_Humanos\Permiso_Asistencia_Archivo_Model;
use App\Models\Recursos_Humanos\Permiso_Asistencia_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Genericos\Archivo_Controller;

class Permiso_Asistencia_Controller extends Controller
{

    const PUNTO = '.';

    /**
     * Retorna la vista del index de permiso asistencia
     * @author Oscar Vargas
     * @return view #Vista index de permiso asistencia
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.recursos_humanos.permiso_asistencia")) {
            return Redirect::to('/');
        }

        $idSucursal = Helpers::get_idSucursal();

        $arrayPermiso = Permiso_Asistencia_Model::select(Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$id,
            Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$fechaInicio,
            Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$fechaFin,
            Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$dias,
            Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$comentarios,
            Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$goceSueldo,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->join(Empleado_Model::$tabla,
                Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal, $idSucursal)
            ->get();

        $arrayEmpleado = Empleado_Model::select(Empleado_Model::$id, DB::raw("CONCAT(" . Empleado_Model::$nombre . ",' '," .
            Empleado_Model::$apellidoPaterno . ",' '," .
            Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->where(Empleado_Model::$idSucursal, $idSucursal)
            ->lists("nombreEmpleado", Empleado_Model::$id);

        return view('Recursos_Humanos.Permiso_Asistencia.index', compact('arrayPermiso', "arrayEmpleado"));

    }

    /**
     * Metodo que sirve para guardar o editar un permiso de asistencia
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.recursos_humanos.permiso_asistencia")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Permiso_Asistencia_Model::$id] == "") {

            $permiso = new Permiso_Asistencia_Model();
            $permiso->id_usuario_creacion = Auth::id();

        } else {

            $permiso = Permiso_Asistencia_Model::find($data[Permiso_Asistencia_Model::$id]);
            $permiso->id_usuario_edicion = Auth::id();

        }

        $permiso->{Permiso_Asistencia_Model::$fechaInicio} = $data[Permiso_Asistencia_Model::$fechaInicio];
        $permiso->{Permiso_Asistencia_Model::$fechaFin} = $data[Permiso_Asistencia_Model::$fechaFin];
        $permiso->{Permiso_Asistencia_Model::$comentarios} = $data[Permiso_Asistencia_Model::$comentarios];
        $permiso->{Permiso_Asistencia_Model::$idEmpleado} = $data[Permiso_Asistencia_Model::$idEmpleado];
        $permiso->{Permiso_Asistencia_Model::$goceSueldo} = $data[Permiso_Asistencia_Model::$goceSueldo];
        $fecha1 = $data[Permiso_Asistencia_Model::$fechaInicio];
        $fecha2 = $data[Permiso_Asistencia_Model::$fechaFin];
        $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
        $valor = abs($valor);
        $valor = floor($valor);
        $permiso->{Permiso_Asistencia_Model::$dias} = $valor + 1;


        if ($permiso->save()) {
            return "Se guardo correctamente el permiso de asistencia";
        } else {
            return "No se guardo exitosamente el permiso de asistencia";
        }

    }

    /**
     * Metodo que sirve para obtener un permiso de asistencia
     *
     * @param POST id_permiso_asistencia identificador unico
     * @author Oscar Vargas
     * @return json con el permiso de asistencia solicitado
     */
    public function obtenerPermisoAsistencia()
    {


        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.recursos_humanos.permiso_asistencia")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Permiso_Asistencia_Model::$id];

        $permiso = Permiso_Asistencia_Model::FindOrFail($id);

        return $permiso;

    }

    /**
     * Metodo que sirve para eliminar un permiso de asistencia
     *
     * @param POST id_permiso_asistencia identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.recursos_humanos.permiso_asistencia")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Permiso_Asistencia_Model::$id];
        $permiso = Permiso_Asistencia_Model::find($id);

        if ($permiso->delete()) {
            return "Se eliminó correctamente el permiso de asistencia";
        } else {
            return "No se eliminó exitosamente el permiso de asistencia";
        }

    }

    /**
     * Metodo que sirve para subir un archivo de un permiso de asistencia
     * @author Oscar Vargas
     * @param Request $request
     * @return string
     */
    public function upload(Request $request)
    {

        $data = $request->all();
        $permiso = Permiso_Asistencia_Model::find($data[Permiso_Asistencia_Model::$id]);

        if ($request->hasFile(Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo)) {

            $instancia = new Permiso_Asistencia_Archivo_Model();
            $imagenInsertada = Archivo_Controller::guardarArchivoServidor($request->file(Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo), "permiso_asistencia", "app\\Recursos_Humanos\\Permiso_Asistencia\\", $instancia, "", "");

            if ($imagenInsertada) {

                $permiso->{Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo} = $imagenInsertada;

            } else {
                return "Error al guardar Imagen";
            }

            $permiso->save();
            return Redirect::back();
        }

        return Redirect::back();
    }

    /**
     * Función para obtener la información del archivo del permiso
     * @author Oscar Vargas
     * @return \Illuminate\Http\Response
     */
    public function obtenerArchivoPermisoAsistencia()
    {
        $data = request()->all();

        $imagen = Permiso_Asistencia_Archivo_Model::select(Permiso_Asistencia_Archivo_Model::$tabla . self::PUNTO . Permiso_Asistencia_Archivo_Model::$ruta,
            Permiso_Asistencia_Archivo_Model::$tabla . self::PUNTO . Permiso_Asistencia_Archivo_Model::$uuid,
            Permiso_Asistencia_Archivo_Model::$tabla . self::PUNTO . Permiso_Asistencia_Archivo_Model::$extension)
            ->join(Permiso_Asistencia_Model::$tabla,
                Permiso_Asistencia_Archivo_Model::$tabla . self::PUNTO . Permiso_Asistencia_Archivo_Model::$id,
                "=",
                Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo)
            ->where(Permiso_Asistencia_Model::$tabla . self::PUNTO . Permiso_Asistencia_Model::$id, $data[Permiso_Asistencia_Model::$id])
            ->get();

        return $imagen;

    }

}
