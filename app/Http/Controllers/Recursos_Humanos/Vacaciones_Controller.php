<?php

namespace App\Http\Controllers\Recursos_Humanos;

use App\Models\Recursos_Humanos\Vacaciones_Archivo_Model;
use App\Models\Recursos_Humanos\Vacaciones_Model;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Genericos\Archivo_Controller;
use Illuminate\Support\Facades\Redirect;
use App\Models\Recursos_Humanos\Empleado_Model;


class Vacaciones_Controller extends Controller
{

    const PUNTO = '.';

    /**
     * Retorna la vista del index de vacaciones
     * @author Oscar Vargas
     * @return view #Vista index de vacaciones
     */
    public function index()
    {

        //Permiso de Despliegue de este Módulo
        if (!Helpers::get_permiso("vista.recursos_humanos.vacaciones")) {
            return Redirect::to('/');
        }

        $idSucursal = Helpers::get_idSucursal();

        $arrayVacaciones = Vacaciones_Model::select(Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$id,
            Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$fechaInicio,
            Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$fechaFin,
            Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$dias,
            Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$comentarios,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno . ",' '," .
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->join(Empleado_Model::$tabla,
                Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$idEmpleado,
                "=",
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
            ->where(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal, $idSucursal)
            ->get();

        $arrayEmpleado = Empleado_Model::select(Empleado_Model::$id, DB::raw("CONCAT(" . Empleado_Model::$nombre . ",' '," .
            Empleado_Model::$apellidoPaterno . ",' '," .
            Empleado_Model::$apellidoMaterno . ') as nombreEmpleado'))
            ->where(Empleado_Model::$idSucursal, $idSucursal)
            ->lists("nombreEmpleado", Empleado_Model::$id);

        return view('Recursos_Humanos.Vacaciones.index', compact('arrayVacaciones', "arrayEmpleado"));

    }

    /**
     * Metodo que sirve para guardar o editar unas vacaciones
     *
     * @param POST recibe el formulario
     * @author Oscar Vargas
     * @return mensaje
     */
    public function guardar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("alta.recursos_humanos.vacaciones")) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if ($data[Vacaciones_Model::$id] == "") {

            $vacaciones = new Vacaciones_Model();
            $vacaciones->id_usuario_creacion = Auth::id();

        } else {

            $vacaciones = Vacaciones_Model::find($data[Vacaciones_Model::$id]);
            $vacaciones->id_usuario_edicion = Auth::id();

        }

        $vacaciones->{Vacaciones_Model::$fechaInicio} = $data[Vacaciones_Model::$fechaInicio];
        $vacaciones->{Vacaciones_Model::$fechaFin} = $data[Vacaciones_Model::$fechaFin];
        $vacaciones->{Vacaciones_Model::$comentarios} = $data[Vacaciones_Model::$comentarios];
        $vacaciones->{Vacaciones_Model::$idEmpleado} = $data[Vacaciones_Model::$idEmpleado];
        $fecha1 = $data[Vacaciones_Model::$fechaInicio];
        $fecha2 = $data[Vacaciones_Model::$fechaFin];
        $valor = (strtotime($fecha1) - strtotime($fecha2)) / 86400;
        $valor = abs($valor);
        $valor = floor($valor);
        $vacaciones->{Vacaciones_Model::$dias} = $valor + 1;


        if ($vacaciones->save()) {
            return "Se guardo correctamente las vacaciones";
        } else {
            return "No se guardo exitosamente las vacaciones";
        }

    }

    /**
     * Metodo que sirve para obtener las vacaciones de un empleado
     *
     * @param POST id_vacaciones identificador unico
     * @author Oscar Vargas
     * @return json con la vacacion solicitado
     */
    public function obtenerVacaciones()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("edicion.recursos_humanos.vacaciones")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Vacaciones_Model::$id];

        $vacaciones = Vacaciones_Model::FindOrFail($id);

        return $vacaciones;

    }

    /**
     * Metodo que sirve para eliminar una vacacion
     *
     * @param POST id_vacaciones identificador unico
     * @author Oscar Vargas
     * @return respuesta del resultado
     */
    public function eliminar()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("baja.recursos_humanos.vacaciones")) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $id = $data[Vacaciones_Model::$id];
        $vacaciones = Vacaciones_Model::find($id);

        if ($vacaciones->delete()) {
            return "Se eliminó correctamente las Vacaciones";
        } else {
            return "No se eliminó exitosamente las Vacaciones";
        }

    }

    /**
     * Metodo que sirve para subir un archivo de vacaciones
     * @author Oscar Vargas
     * @param Request $request
     * @return string
     */
    public function upload(Request $request)
    {

        $data = $request->all();
        $vacaciones = Vacaciones_Model::find($data[Vacaciones_Model::$id]);

        if ($request->hasFile(Vacaciones_Model::$idVacacionesArchivo)) {

            $instancia = new Vacaciones_Archivo_Model();
            $imagenInsertada = Archivo_Controller::guardarArchivoServidor($request->file(Vacaciones_Model::$idVacacionesArchivo), "vacaciones", "app\\Recursos_Humanos\\Vacaciones\\", $instancia, "", "");

            if ($imagenInsertada) {

                $vacaciones->{Vacaciones_Model::$idVacacionesArchivo} = $imagenInsertada;

            } else {
                return "Error al guardar Imagen";
            }

            $vacaciones->save();
            return Redirect::back();
        }

        return Redirect::back();
    }

    /**
     * Función para obtener la información del archivo de las vacaciones
     * @author Oscar Vargas
     * @return \Illuminate\Http\Response
     */
    public function obtenerArchivoVacaciones()
    {
        $data = request()->all();

        return $imagen = Vacaciones_Archivo_Model::select(Vacaciones_Archivo_Model::$tabla . self::PUNTO . Vacaciones_Archivo_Model::$ruta,
            Vacaciones_Archivo_Model::$tabla . self::PUNTO . Vacaciones_Archivo_Model::$uuid,
            Vacaciones_Archivo_Model::$tabla . self::PUNTO . Vacaciones_Archivo_Model::$extension)
            ->join(Vacaciones_Model::$tabla,
                Vacaciones_Archivo_Model::$tabla . self::PUNTO . Vacaciones_Archivo_Model::$id,
                "=",
                Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$idVacacionesArchivo)
            ->where(Vacaciones_Model::$tabla . self::PUNTO . Vacaciones_Model::$id, $data[Vacaciones_Model::$id])
            ->get();

    }

}
