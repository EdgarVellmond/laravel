<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Sistema\Categoria_Variables_Sistema_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Categoria_Variables_Sistema_Controller extends Controller
{

    /**
     * Retorna la vista del index del catálogo de Categoría de Variables de Sistema
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Categoría de Variables de Sistema
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.sistema.categoria_variables_sistema")) {
            return Redirect::to('/');
        }

        $arrayCategoriaVariablesSistema = Categoria_Variables_Sistema_Model::select(Categoria_Variables_Sistema_Model::$id, Categoria_Variables_Sistema_Model::$nombre)->get();

        return view('Sistema.Categoria_Variables_Sistema.index', compact('arrayCategoriaVariablesSistema', 'permiso'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Categoría de Variables de Sistema
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();

        if ($data[Categoria_Variables_Sistema_Model::$id] == "") {

            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.sistema.categoria_variables_sistema")) {
                return "No tienes permiso para agregar.";
            }

            $categoria = new Categoria_Variables_Sistema_Model();
            $categoria->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.sistema.categoria_variables_sistema")) {
                return "No tienes permiso para editar.";
            }

            $categoria = Categoria_Variables_Sistema_Model::find($data[Categoria_Variables_Sistema_Model::$id]);
            $categoria->id_usuario_edicion = Auth::id();

        }

        $categoria->{Categoria_Variables_Sistema_Model::$nombre} = $data[Categoria_Variables_Sistema_Model::$nombre];

        if ($categoria->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener la Categoría
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerCategoriaVariablesSistemaEditar()
    {

        $data = request()->all();
        $id = $data[Categoria_Variables_Sistema_Model::$id];

        $categoria = Categoria_Variables_Sistema_Model::FindOrFail($id);

        return $categoria;

    }

    /**
     * Función para eliminar la Categoría
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.sistema.categoria_variables_sistema")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Categoria_Variables_Sistema_Model::$id];
        $categoria = Categoria_Variables_Sistema_Model::find($id);

        IF ($categoria->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}