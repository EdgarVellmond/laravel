<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Helpers\Helpers;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Sistema\Menu_Model;
use App\Models\Sistema\Permiso_Model;
use App\Models\Sistema\Usuario_Permiso_Model;
use App\Models\Sistema\Submenu_Model;
use App\Models\Sistema\Usuarios_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class Usuarios_Controller extends Controller
{
    const PUNTO = ".";

    /**
     * Metodo que sirve para retornar la vista principal de usuarios
     * @author Oscar Vargas
     * @return views/usuarios/index
     */
    public function index()
    {
        if (!Helpers::get_permiso('vista.sistema.control_usuarios')) {
            return Redirect::to('/');
        }
        $usuarios = Usuarios_Model::all();

        $empleados = Empleado_Model::select(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno . ",' '," . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno . " )" . ' as nombreEmpleado'))
            ->leftjoin(Usuarios_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id,
                "=",
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado)
            ->lists("nombreEmpleado",
                Empleado_Model::$id);

        return view('Sistema.Usuarios.index', compact('usuarios', 'empleados'));

    }

    /**
     * Este metodo sirve para mostrar los datos del usuario (ficha)
     * @author Oscar Vargas
     * @param $id Id principal del usuario
     * @return views/usuarios/display
     */
    public function fichaUsuario($id)
    {

        if (!Helpers::get_permiso('edicion.sistema.control_usuarios')) {
            return Redirect::to('/');
        }

        $usuario = Usuarios_Model::find($id);

        $empleados = Empleado_Model::select(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id,
            DB::raw("CONCAT(" . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$nombre . ",' '," . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoPaterno . ",' '," . Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$apellidoMaterno . " )" . ' as nombreEmpleado'))
            ->leftjoin(Usuarios_Model::$tabla,
                Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id,
                "=",
                Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado)
            ->lists("nombreEmpleado",
                Empleado_Model::$id);

        $permisos = Permiso_Model::all();

        $usuarioPermiso = Usuario_Permiso_Model::where(Usuario_Permiso_Model::$idUsuario, $id)
            ->lists(Usuario_Permiso_Model::$idPermiso, Usuario_Permiso_Model::$idPermiso);

        return view('Sistema.Usuarios.display', compact('usuario', 'empleados', 'permisos', 'usuarioPermiso'));

    }

    /**
     * Actualiza los datos del usuario
     * @author Oscar Vargas
     * @return te regresa a la ficha del usuario
     */
    public function actualizarDb()
    {

        if (!Helpers::get_permiso('edicion.sistema.control_usuarios')) {
            return Redirect::to('/');
        }

        $data = request()->all();
        $aData = Usuarios_Model::find($data[Usuarios_Model::$id]);

        if ($aData->validateforupdate($data, $data[Usuarios_Model::$id])) {
            $aData->setAttribute(Usuarios_Model::$nombre, $data[Usuarios_Model::$nombre]);
            $aData->setAttribute(Usuarios_Model::$usuario, $data[Usuarios_Model::$usuario]);
            $aData->setAttribute(Usuarios_Model::$correo, $data[Usuarios_Model::$correo]);
            $aData->setAttribute(Usuarios_Model::$idEmpleado, $data[Usuarios_Model::$idEmpleado]);
            if (isset($data[Usuarios_Model::$password])) {
                $aData->password = bcrypt($data[Usuarios_Model::$password]);
            }
            $aData->save();
            return Redirect::back()->with("message", "Se guardo el usuario correctamente");
        } else {
            return Redirect::back()->withInput()->withErrors($aData->errors());
        }

    }

    /**
     * Cambia de estatus al usuario
     * @author Oscar Vargas
     * @param $id Identificador del usuario
     * @param $Estatus El nuevo estatus del usuario
     * @return regresa a la vista de la ficha del usuario
     */
    public function estatusUsuario($id, $estatus)
    {

        if (!Helpers::get_permiso('edicion.sistema.control_usuarios')) {
            return Redirect::to('/');
        }

        $user = Usuarios_Model::find($id);
        $user->setAttribute(Usuarios_Model::$estatus, $estatus);
        $user->save();

        return Redirect::back()->with("message", "Se cambio el estatus del usuario correctamente");

    }

    /**
     * Se crean o se modifican los permisos del usuario para el menu
     * @author Luis Alfredo
     * @return regresa a la vista de la ficha del usuario
     */
    public function guardarPermisos()
    {

        if (!Helpers::get_permiso('edicion.sistema.control_usuarios')) {
            return Redirect::to('/');
        }

        $data = request()->all();

        if (!isset($data["opcion"])) {
            $permiso = Usuario_Permiso_Model::where(Usuario_Permiso_Model::$idPermiso, $data[Usuario_Permiso_Model::$idPermiso])
                ->where(Usuario_Permiso_Model::$idUsuario, $data["id_usuario"]);
            $permiso->forceDelete();

            return "danger";

        } else {

            $permiso = new Usuario_Permiso_Model();

            $permiso->{Usuario_Permiso_Model::$idPermiso} = $data[Usuario_Permiso_Model::$idPermiso];
            $permiso->{Usuario_Permiso_Model::$idUsuario} = $data["id_usuario"];
            $permiso->{Usuario_Permiso_Model::$idUsuarioCreacion} = Auth::id();

            $permiso->save();

            return "success";

        }

    }

    /**
     * Creacion de un Nuevo Usuario
     * @author Luis Alfredo
     * @return mixed
     */
    public function nuevoUsuario()
    {

        if (!Helpers::get_permiso('alta.sistema.control_usuarios')) {
            return Redirect::to('/');
        }

        $datos = request()->all();
        $usuario = new Usuarios_Model();
        if ($usuario->validate($datos)) {
            $usuario->setAttribute(Usuarios_Model::$nombre, $datos[Usuarios_Model::$nombre]);
            $usuario->setAttribute(Usuarios_Model::$usuario, $datos[Usuarios_Model::$usuario]);
            $usuario->setAttribute(Usuarios_Model::$correo, $datos[Usuarios_Model::$correo]);
            $usuario->setAttribute(Usuarios_Model::$password, bcrypt($datos[Usuarios_Model::$password]));
            $usuario->setAttribute(Usuarios_Model::$estatus, 1);
            $usuario->setAttribute(Usuarios_Model::$idEmpleado, $datos[Usuarios_Model::$idEmpleado]);
            $usuario->save();

            return Redirect::back()->with("message", "Se guardo el usuario correctamente");
        } else {
            return redirect()->back()->withInput()->withErrors($usuario->errors());
        }

    }

}
