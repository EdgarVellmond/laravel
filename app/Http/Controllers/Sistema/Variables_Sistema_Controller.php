<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\Sistema\Variables_Sistema_Model;
use App\Models\Sistema\Categoria_Variables_Sistema_Model;
use App\Models\Catalogos\Rol_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Variables_Sistema_Controller extends Controller
{

    const PUNTO = ".";
    const EQUALS = "=";

    /**
     * Retorna la vista del index del catálogo de Variables Contratos
     * @author Erik Villarreal
     * @return view #Vista index del catálogo de Variables Contratos
     */
    public function index()
    {

        //Permiso de Despliegue de este Modulo
        if (!Helpers::get_permiso("vista.sistema.variables_sistema")) {
            return Redirect::to('/');
        }

        $arrayVariablesSistema = Variables_Sistema_Model::select(
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$id, 
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$nombre,
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$tablaOrigen,
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$aliasTablaOrigen,
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$campo, 
                                                            DB::raw(Categoria_Variables_Sistema_Model::$tabla . self::PUNTO . Categoria_Variables_Sistema_Model::$nombre . ' as ' . Categoria_Variables_Sistema_Model::$id),
                                                            DB::raw(Rol_Model::$tabla . self::PUNTO . Rol_Model::$nombre . ' as ' . Rol_Model::$id)
                                                            )
                                                        ->join(Categoria_Variables_Sistema_Model::$tabla,
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$idCategoria,
                                                            self::EQUALS,
                                                            Categoria_Variables_Sistema_Model::$tabla.self::PUNTO.Categoria_Variables_Sistema_Model::$id
                                                            )
                                                        ->leftJoin(Rol_Model::$tabla,
                                                            Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$idRol,
                                                            self::EQUALS,
                                                            Rol_Model::$tabla.self::PUNTO.Rol_Model::$id
                                                            )
                                                        ->get();

        $categoriaVariablesSistema = Categoria_Variables_Sistema_Model::lists(Categoria_Variables_Sistema_Model::$nombre, Categoria_Variables_Sistema_Model::$id);

        $roles = Rol_Model::select(
                                    Rol_Model::$tabla . self::PUNTO . Rol_Model::$id,
                                    Rol_Model::$tabla . self::PUNTO . Rol_Model::$nombre
                                    )
                            ->leftJoin(Variables_Sistema_Model::$tabla,
                                        Rol_Model::$tabla.self::PUNTO.Rol_Model::$id,
                                        self::EQUALS,
                                        Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$id
                                        )
                            ->where(Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$idRol, "!=", 0)
                            ->lists(Rol_Model::$nombre, Rol_Model::$id);
        
        return view('Sistema.Variables_Sistema.index', compact('arrayVariablesSistema', 'categoriaVariablesSistema' , 'roles'));

    }

    /**
     * Función para crear un nuevo registro en el Catálogo de Variables de Contrato
     * @author Oscar Vargas
     * @return string #Mensaje de servidor
     */
    public function guardar()
    {

        $data = request()->all();
        
        if ($data[Variables_Sistema_Model::$id] == "") {
            
            //Permiso para crear nuevo registro
            if (!Helpers::get_permiso("alta.sistema.variables_sistema")) {
                return "No tienes permiso para agregar.";
            }

            $variables_sistema = new Variables_Sistema_Model();
            $variables_sistema->id_usuario_creacion = Auth::id();

        } else {

            //Permiso para editar registro
            if (!Helpers::get_permiso("edicion.sistema.variables_sistema")) {
                return "No tienes permiso para editar.";
            }

            $variables_sistema = Variables_Sistema_Model::find($data[Variables_Sistema_Model::$id]);
            $variables_sistema->id_usuario_edicion = Auth::id();

        }

        $variables_sistema->{Variables_Sistema_Model::$nombre} = $data[Variables_Sistema_Model::$nombre];
        $variables_sistema->{Variables_Sistema_Model::$idCategoria} = $data[Variables_Sistema_Model::$idCategoria];
        $variables_sistema->{Variables_Sistema_Model::$tablaOrigen} = $data[Variables_Sistema_Model::$tablaOrigen];
        $variables_sistema->{Variables_Sistema_Model::$aliasTablaOrigen} = $data[Variables_Sistema_Model::$aliasTablaOrigen];
        $variables_sistema->{Variables_Sistema_Model::$campo} = $data[Variables_Sistema_Model::$campo];
        $variables_sistema->{Variables_Sistema_Model::$idRol} = $data[Variables_Sistema_Model::$idRol];

        if ($variables_sistema->save()) {
            return "Se guardó correctamente";
        } else {
            return "Problemas al guardar";
        }

    }

    /**
     * Función para obtener la Variable de Contrato
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerVariablesSistemaEditar()
    {

        $data = request()->all();
        $id = $data[Variables_Sistema_Model::$id];

        $variables_sistema = Variables_Sistema_Model::FindOrFail($id);

        return $variables_sistema;

    }

    /**
     * Función para obtener la Variable de Contrato
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function obtenerVariablesSistema()
    {

        $data = request()->all();

        $variables_sistema = Variables_Sistema_Model::select(
                                                        Variables_Sistema_Model::$tabla . self::PUNTO . Variables_Sistema_Model::$id, 
                                                        Variables_Sistema_Model::$tabla . self::PUNTO . Variables_Sistema_Model::$nombre, 
                                                        Variables_Sistema_Model::$tabla . self::PUNTO . Variables_Sistema_Model::$tablaOrigen,
                                                        Variables_Sistema_Model::$tabla . self::PUNTO . Variables_Sistema_Model::$aliasTablaOrigen, 
                                                        Variables_Sistema_Model::$tabla . self::PUNTO . Variables_Sistema_Model::$campo,
                                                        DB::raw(Rol_Model::$tabla . self::PUNTO . Rol_Model::$nombre . ' as ' . Rol_Model::$id)
                                                        )
                                                    ->leftJoin(
                                                        Rol_Model::$tabla,
                                                        Variables_Sistema_Model::$tabla.self::PUNTO.Variables_Sistema_Model::$idRol,
                                                        self::EQUALS,
                                                        Rol_Model::$tabla.self::PUNTO.Rol_Model::$id
                                                        )                                
                                                    ->where(Variables_Sistema_Model::$aliasTablaOrigen, $data[Variables_Sistema_Model::$aliasTablaOrigen])
                                                    ->get();

        return $variables_sistema;

    }

    /**
     * Función para eliminar el Variables_sistema
     * @author Erik Villarreal
     * @return string #Mensaje de servidor
     */
    public function borrar()
    {

        //Permiso para borrar registro
        if (!Helpers::get_permiso("baja.sistema.variables_sistema")) {
            return "No tienes permiso para borrar.";
        }

        $data = request()->all();
        $id = $data[Variables_Sistema_Model::$id];
        $variables_sistema = Variables_Sistema_Model::find($id);

        IF ($variables_sistema->delete()) {
            return "Se eliminó correctamente ";
        } else {
            return "Problemas al eliminar";
        }

    }

}