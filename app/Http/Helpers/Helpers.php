<?php namespace App\Http\Helpers;

use App\Models\Catalogos\Cubeta_Model;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Models\Sistema\Historial_Model;
use App\Models\Sistema\Permiso_Model;
use App\Models\Sistema\Usuario_Permiso_Model;
use App\Models\Sistema\Usuarios_Model;
use Carbon\Carbon;
use \Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Collective\Html\FormFacade;
use Illuminate\Support\Facades\Redirect;


class Helpers
{

    const PUNTO = ".";
    const EQUALS = "=";

    /**
     * Metodo que genera un caracter de tipo UUID
     *
     * @author Erik Villarreal
     * @return string UUID
     */
    public static function generate_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Metodo que sirve para obtener el id_sucursal de algun usuario que tenga ligado un empleado
     *
     * @author Oscar Vargas
     * @return identificador unico de la sucursal
     */
    public static function get_idSucursal()
    {

        if (!is_null(Auth::user())) {

            $empleado = Usuarios_Model::select(Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idSucursal)
                ->join(Empleado_Model::$tabla,
                    Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado,
                    "=",
                    Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
                ->where(Usuarios_Model::$id, Auth::user()->getAttribute(Usuarios_Model::$id))
                ->get();

            return $empleado[0]->{Empleado_Model::$idSucursal};
        }
        return 0;

    }

    /**
     * Metodo que sirve para obtener el id_sucursal de algun usuario que tenga ligado un empleado
     *
     * @author Oscar Vargas
     * @return identificador unico de la sucursal
     */
    public static function get_imagenUsuario()
    {

        if (!is_null(Auth::user())) {

            $empleado = Usuarios_Model::select(Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$ruta,
                                                Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$uuid,
                                                Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$extension)
                ->join(Empleado_Model::$tabla,
                    Usuarios_Model::$tabla . self::PUNTO . Usuarios_Model::$idEmpleado,
                    "=",
                    Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$id)
                ->leftjoin(Empleado_Imagen_Model::$tabla,
                    Empleado_Model::$tabla . self::PUNTO . Empleado_Model::$idEmpleadoImagen,
                    self::EQUALS,
                    Empleado_Imagen_Model::$tabla . self::PUNTO . Empleado_Imagen_Model::$id)
                ->where(Usuarios_Model::$id, Auth::user()->getAttribute(Usuarios_Model::$id))
                ->get();

            return $empleado[0];
        }
        return 0;

    }

    /**
     * Metodo que sirve para agregar el contador de visitas de la pagina
     *
     * @author Oscar Vargas
     * @return string con el numero de visitas
     */
    public static function loginCount()
    {

        DB::table('log_visitas')->insert([
            'fecha_hora' => date('Y-m-d H:i:s'),
            'ip' => $_SERVER['REMOTE_ADDR'],
            'browser' => $_SERVER['HTTP_USER_AGENT']
        ]);
        $UltimaVisita = DB::table('log_visitas')->orderBy("id_log_visitas", "DESC")->first();
        return "No. de visitas:  $UltimaVisita->id_log_visitas";

    }

    /**
     * Metodo que sirve para saber si un usuario tiene algun permiso en especifico
     *
     * @author Oscar Vargas
     * @param $idUsuario identificador unico del usuario
     * @param $perfil nombre del permiso
     * @return boolean
     */
    public static function checkUsuarioPermiso($idUsuario, $perfil)
    {
        if ($idUsuario > 0) {
            $rowPermiso = Usuario_Permiso_Model::join(Permiso_Model::$tabla,
                Usuario_Permiso_Model::$tabla . self::PUNTO . Usuario_Permiso_Model::$idPermiso,
                "=",
                Permiso_Model::$tabla . self::PUNTO . Permiso_Model::$id)
                ->where(Usuario_Permiso_Model::$idUsuario, $idUsuario)
                ->where(Permiso_Model::$nombre, $perfil)
                ->get();

            if (count($rowPermiso) > 0) {

                return true;

            } else {

                return false;

            }

        }
    }

    /**
     * Metodo que obtiene los permisos de algun usuario que esta en sesion
     *
     * @author Oscar Vargas
     * @param $perfil nombre del permiso
     * @return boolean
     */
    public static function get_permiso($perfil)
    {

        if (!is_null(Auth::user())) {
            $rowPermiso = Usuario_Permiso_Model::join(Permiso_Model::$tabla,
                Usuario_Permiso_Model::$tabla . self::PUNTO . Usuario_Permiso_Model::$idPermiso,
                "=",
                Permiso_Model::$tabla . self::PUNTO . Permiso_Model::$id)
                ->where(Usuario_Permiso_Model::$idUsuario, Auth::user()->getAttribute(Usuarios_Model::$id))
                ->where(Permiso_Model::$nombre, $perfil)
                ->get();

            if (count($rowPermiso) > 0) {

                return true;

            } else {

                return false;

            }

        }

    }


}
