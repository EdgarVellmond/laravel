<?php

namespace App\Http\Middleware;

use App\Models\Sistema\Usuarios_Model;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LogLastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $expiresAt = Carbon::now()->addMinutes(5);
            Cache::put('usuario_sistema_online_' . Auth::user()->getAttribute(Usuarios_Model::$id), true, $expiresAt);
        }
        return $next($request);
    }
}
