<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/','Principal\Principal_Controller@index');
	Route::get('logout','Principal\Principal_Controller@logout');
        
    /**
     * Rutas del Sistema
     */

    Route::group(['prefix' => 'sistema'],function () {
        /**
        * Rutas get para los index de los catálogos
        */
        Route::get('variables_sistema','Sistema\Variables_Sistema_Controller@index');
        Route::get('categoria_variables_sistema','Sistema\Categoria_Variables_Sistema_Controller@index');

        /**
        * Rutas usadas por Variables_Sistema_Controller
        */
        Route::post('guardarVariablesSistema','Sistema\Variables_Sistema_Controller@guardar');
        Route::post('obtenerVariablesSistemaEditar','Sistema\Variables_Sistema_Controller@obtenerVariablesSistemaEditar');
        Route::post('eliminarVariablesSistema','Sistema\Variables_Sistema_Controller@borrar');

        /**
        * Rutas usadas por Categoria_Variables_Sistema_Controller
        */
        Route::post('guardarCategoriaVariablesSistema','Sistema\Categoria_Variables_Sistema_Controller@guardar');
        Route::post('obtenerCategoriaVariablesSistemaEditar','Sistema\Categoria_Variables_Sistema_Controller@obtenerCategoriaVariablesSistemaEditar');
        Route::post('eliminarCategoriaVariablesSistema','Sistema\Categoria_Variables_Sistema_Controller@borrar');
    });

    Route::group(['prefix' => 'usuarios'],function () {
        Route::get('/','Sistema\Usuarios_Controller@index');
        Route::post('nuevoUsuario','Sistema\Usuarios_Controller@nuevoUsuario');
        Route::get('fichaUsuario/{id}','Sistema\Usuarios_Controller@fichaUsuario');
        Route::post('fichaUsuario', 'Sistema\Usuarios_Controller@actualizarDb');
        Route::get('estatusUsuario/{id}/{Estatus}', 'Sistema\Usuarios_Controller@estatusUsuario');
        Route::post('fichaUsuario/movimientoPermiso', 'Sistema\Usuarios_Controller@guardarPermisos');

    });


    Route::group(['prefix' => 'catalogos'],function (){
        
        /**
        * Rutas get para los index de los catálogos
        */
        Route::get('empresa','Catalogos\Empresa_Controller@index');
        Route::get('puesto','Catalogos\Puesto_Controller@index');
        Route::get('rol','Catalogos\Rol_Controller@index');
        Route::get('area','Catalogos\Area_Controller@index');
        Route::get('departamento','Catalogos\Departamento_Controller@index');
        Route::get('tipo_plantilla','Catalogos\Tipo_Plantilla_Controller@index');
        Route::get('cuentas_nomina_sat','Catalogos\Cuentas_Nomina_Sat_Controller@index');
        Route::get('clave_giro_nomina','Catalogos\Clave_Giro_Nomina_Controller@index');
        Route::get('dia_festivo','Catalogos\Dia_Festivo_Controller@index');
        Route::get('salario_minimo','Catalogos\Salario_Minimo_Controller@index');
        Route::get('cuota_obrero_patronal','Catalogos\Cuota_Obrero_Patronal_Controller@index');
        Route::get('tipo_solicitud','Catalogos\Tipo_Solicitud_Controller@index');
        Route::get('categoria_expediente_digital','Catalogos\Categoria_Expediente_Digital_Controller@index');
        Route::get('expediente_digital','Catalogos\Expediente_Digital_Controller@index');
        Route::get('clasificacion_obra','Catalogos\Clasificacion_Obra_Controller@index');

        /**
        * Rutas de Plantilla_Controller
        */
        Route::get('plantilla','Catalogos\Plantilla_Controller@index');
        Route::get('nuevaPlantilla','Catalogos\Plantilla_Controller@nuevaPlantilla');
        Route::post('editarPlantilla','Catalogos\Plantilla_Controller@editarPlantilla');
        Route::get('vistaPrevia/{id}','Genericos\Pdf_Controller@generarVistaPreviaPdf');
        Route::get('vistaPreviaCompleta/{idPlantilla}/{idPropietario}','Genericos\Pdf_Controller@generarPlantillaPdf');


        /**
        * Rutas de index
        */
        Route::get('metodo_pago','Catalogos\Metodo_Pago_Controller@index');
        Route::get('prima_riesgo','Catalogos\Prima_Riesgo_Controller@index');
        Route::get('impuesto_isr','Catalogos\Impuesto_Isr_Controller@index');

        /**
        * Rutas usadas por Empresa_Controller
        */
        Route::post('obtenerDireccionEmpresa','Catalogos\Empresa_Controller@obtenerDireccionEmpresa');
        Route::post('obtenerMunicipios','Catalogos\Municipio_Controller@obtenerMunicipios');
        Route::post('guardarEmpresa','Catalogos\Empresa_Controller@guardar');
        Route::post('obtenerEmpresa','Catalogos\Empresa_Controller@obtenerEmpresa');
        Route::post('eliminarEmpresa','Catalogos\Empresa_Controller@borrar');

        /**
        * Rutas usadas por Puesto_Controller
        */
        Route::post('guardarPuesto','Catalogos\Puesto_Controller@guardar');
        Route::post('obtenerPuestoEditar','Catalogos\Puesto_Controller@obtenerPuestoEditar');
        Route::post('eliminarPuesto','Catalogos\Puesto_Controller@borrar');

        /**
        * Rutas usadas por Area_Controller
        */
        Route::post('guardarArea','Catalogos\Area_Controller@guardar');
        Route::post('obtenerAreaEditar','Catalogos\Area_Controller@obtenerAreaEditar');
        Route::post('eliminarArea','Catalogos\Area_Controller@borrar');
        Route::post('obtenerArea','Catalogos\Area_Controller@obtenerArea');

        /**
        * Rutas usadas por Departamento_Controller
        */
        Route::post('guardarDepartamento','Catalogos\Departamento_Controller@guardar');
        Route::post('obtenerDepartamentoEditar','Catalogos\Departamento_Controller@obtenerDepartamentoEditar');
        Route::post('eliminarDepartamento','Catalogos\Departamento_Controller@borrar');

        /**
        * Rutas usadas por Tipo_Plantilla_Controller
        */
        Route::post('guardarTipoPlantilla','Catalogos\Tipo_Plantilla_Controller@guardar');
        Route::post('obtenerTipoPlantillaEditar','Catalogos\Tipo_Plantilla_Controller@obtenerTipoPlantillaEditar');
        Route::post('eliminarTipoPlantilla','Catalogos\Tipo_Plantilla_Controller@borrar');

        /**
        * Rutas usadas por Cuentas_Nomina_Sat_Controller
        */
        Route::post('guardarCuentasNominaSat','Catalogos\Cuentas_Nomina_Sat_Controller@guardar');
        Route::post('obtenerCuentasNominaSatEditar','Catalogos\Cuentas_Nomina_Sat_Controller@obtenerCuentasNominaSatEditar');
        Route::post('eliminarCuentasNominaSat','Catalogos\Cuentas_Nomina_Sat_Controller@borrar');

        /**
        * Rutas usadas por Clave_Giro_Nomina_Controller
        */
        Route::post('guardarClaveGiroNomina','Catalogos\Clave_Giro_Nomina_Controller@guardar');
        Route::post('obtenerClaveGiroNominaEditar','Catalogos\Clave_Giro_Nomina_Controller@obtenerClaveGiroNominaEditar');
        Route::post('eliminarClaveGiroNomina','Catalogos\Clave_Giro_Nomina_Controller@borrar');

        /**
         * Rutas de metodo_pago
         */
        Route::post('guardarMetodoPago','Catalogos\Metodo_Pago_Controller@guardar');
        Route::post('eliminarMetodoPago','Catalogos\Metodo_Pago_Controller@eliminar');
        Route::post('obtenerMetodoPago','Catalogos\Metodo_Pago_Controller@obtenerMetodoPago');

        /**
         * Rutas de Prima Riesgo
         */
        Route::post('guardarPrimaRiesgo','Catalogos\Prima_Riesgo_Controller@guardar');
        Route::post('obtenerPrimaRiesgo','Catalogos\Prima_Riesgo_Controller@obtenerPrimaRiesgo');
        Route::post('eliminarPrimaRiesgo','Catalogos\Prima_Riesgo_Controller@eliminarPrimaRiesgo');

        /**
         * Rutas de Impuesto ISR
         */
        Route::post('guardarImpuestoIsr','Catalogos\Impuesto_Isr_Controller@guardar');
        Route::post('obtenerImpuestoIsr','Catalogos\Impuesto_Isr_Controller@obtenerImpuestoIsr');
        Route::post('eliminarImpuestoIsr','Catalogos\Impuesto_Isr_Controller@eliminarImpuestoIsr');

        /**
        * Rutas usadas por Dia_Festivo_Controller
        */
        Route::post('guardarDiaFestivo','Catalogos\Dia_Festivo_Controller@guardar');
        Route::post('obtenerDiaFestivoEditar','Catalogos\Dia_Festivo_Controller@obtenerDiaFestivoEditar');
        Route::post('eliminarDiaFestivo','Catalogos\Dia_Festivo_Controller@borrar');

        /**
         * Rutas de Salario Minimo
         */
        Route::post('guardarSalarioMinimo','Catalogos\Salario_Minimo_Controller@guardar');
        Route::post('obtenerSalarioMinimo','Catalogos\Salario_Minimo_Controller@obtenerSalarioMinimo');
        Route::post('eliminarSalarioMinimo','Catalogos\Salario_Minimo_Controller@eliminarSalarioMinimo');

        /**
         * Rutas de Cuota Obrero Patronal
         */
        Route::post('guardarCuotaObreroPatronal','Catalogos\Cuota_Obrero_Patronal_Controller@guardar');
        Route::post('obtenerCuotaObreroPatronal','Catalogos\Cuota_Obrero_Patronal_Controller@obtenerCuotaObreroPatronal');
        Route::post('eliminarCuotaObreroPatronal','Catalogos\Cuota_Obrero_Patronal_Controller@eliminarCuotaObreroPatronal');

        /**
        * Rutas usadas por Rol_Controller
        */
        Route::post('guardarRol','Catalogos\Rol_Controller@guardar');
        Route::post('obtenerRolEditar','Catalogos\Rol_Controller@obtenerRolEditar');
        Route::post('eliminarRol','Catalogos\Rol_Controller@borrar');

        /**
         * Rutas de Plantilla
         */
        Route::post('guardarPlantilla','Catalogos\Plantilla_Controller@guardar');
        Route::post('obtenerPlantillaEditar','Catalogos\Plantilla_Controller@obtenerPlantillaEditar');
        Route::post('obtenerPlantilla','Catalogos\Plantilla_Controller@obtenerPlantilla');
        Route::post('obtenerVariablesSistema','Sistema\Variables_Sistema_Controller@obtenerVariablesSistema');
        Route::post('eliminarPlantilla','Catalogos\Plantilla_Controller@borrar');

        /**
        * Rutas usadas por Tipo_Solicitud_Controller
        */
        Route::post('guardarTipoSolicitud','Catalogos\Tipo_Solicitud_Controller@guardar');
        Route::post('obtenerTipoSolicitudEditar','Catalogos\Tipo_Solicitud_Controller@obtenerTipoSolicitudEditar');
        Route::post('eliminarTipoSolicitud','Catalogos\Tipo_Solicitud_Controller@borrar');

        /**
        * Rutas usadas por Categoria_Expediente_Digital_Controller
        */
        Route::post('guardarCategoriaExpedienteDigital','Catalogos\Categoria_Expediente_Digital_Controller@guardar');
        Route::post('obtenerCategoriaExpedienteDigitalEditar','Catalogos\Categoria_Expediente_Digital_Controller@obtenerCategoriaExpedienteDigitalEditar');
        Route::post('eliminarCategoriaExpedienteDigital','Catalogos\Categoria_Expediente_Digital_Controller@borrar');
        Route::post('obtenerCategoriaExpedienteDigital','Catalogos\Categoria_Expediente_Digital_Controller@obtenerCategoriaExpedienteDigital');

        /**
        * Rutas usadas por Expediente_Digital_Controller
        */
        Route::post('guardarExpedienteDigital','Catalogos\Expediente_Digital_Controller@guardar');
        Route::post('obtenerExpedienteDigitalEditar','Catalogos\Expediente_Digital_Controller@obtenerExpedienteDigitalEditar');
        Route::post('eliminarExpedienteDigital','Catalogos\Expediente_Digital_Controller@borrar');

        /**
         * Rutas de cliente
         */
        Route::get('cliente','Catalogos\Cliente_Controller@index');
        Route::post('guardarCliente','Catalogos\Cliente_Controller@guardar');
        Route::post('obtenerDomicilioCliente','Catalogos\Cliente_Controller@obtenerDomicilioCliente');
        Route::post('obtenerCreditoCliente','Catalogos\Cliente_Controller@obtenerCreditoCliente');
        Route::post('eliminarCliente','Catalogos\Cliente_Controller@eliminarCliente');
        Route::post('obtenerCliente','Catalogos\Cliente_Controller@obtenerCliente');

        /*
        * Rutas usadas por Clasificacion_Obra_Controller
        */
        Route::post('guardarClasificacionObra','Catalogos\Clasificacion_Obra_Controller@guardar');
        Route::post('obtenerClasificacionObraEditar','Catalogos\Clasificacion_Obra_Controller@obtenerClasificacionObraEditar');
        Route::post('eliminarClasificacionObra','Catalogos\Clasificacion_Obra_Controller@borrar');

    });

    /**
     * Rutas de proveedor
     */
    Route::get('proveedor','Catalogos\Proveedor_Controller@index');
    Route::post('guardarProveedor','Catalogos\Proveedor_Controller@guardar');
    Route::post('obtenerDomicilioProveedor','Catalogos\Proveedor_Controller@obtenerDomicilioProveedor');
    Route::post('obtenerCreditoProveedor','Catalogos\Proveedor_Controller@obtenerCreditoProveedor');
    Route::post('eliminarProveedor','Catalogos\Proveedor_Controller@eliminarProveedor');
    Route::post('obtenerProveedor','Catalogos\Proveedor_Controller@obtenerProveedor');

    /**
     * Rutas de Municipio
     */
    Route::post('obtenerMunicipios','Catalogos\Municipio_Controller@obtenerMunicipios');


    /**
     * Rutas de Sucursal
     */
    Route::post('guardarSucursal','Catalogos\Sucursal_Controller@guardar');
    Route::get('sucursal','Catalogos\Sucursal_Controller@index');
    Route::post('eliminarSucursal','Catalogos\Sucursal_Controller@eliminarSucursal');
    Route::post('obtenerDomicilioSucursal','Catalogos\Sucursal_Controller@obtenerDomicilioSucursal');
    Route::post('obtenerSucursal','Catalogos\Sucursal_Controller@obtenerSucursal');

    /**
     * Rutas de Estado
     */
    Route::get('estado','Catalogos\Estado_Controller@index');
    Route::post('guardarEstado','Catalogos\Estado_Controller@guardar');
    Route::post('obtenerEstado','Catalogos\Estado_Controller@obtenerEstado');
    Route::post('eliminarEstado','Catalogos\Estado_Controller@eliminarEstado');

    /**
     * Rutas de Municipio
     */
    Route::get('municipio','Catalogos\Municipio_Controller@index');
    Route::post('guardarMunicipio','Catalogos\Municipio_Controller@guardar');
    Route::post('obtenerMunicipioEditar','Catalogos\Municipio_Controller@obtenerMunicipioEditar');
    Route::post('eliminarMunicipio','Catalogos\Municipio_Controller@eliminarMunicipio');


    Route::group(['prefix' => 'recursos_humanos'],function () {

        /**
         * Rutas de empleado
         */
        Route::get('empleado','Recursos_Humanos\Empleado_Controller@index');
        Route::get('fichaEmpleado/{id}','Recursos_Humanos\Empleado_Controller@ficha');
        Route::post('obtenerAdicionalEmpleado','Recursos_Humanos\Empleado_Controller@obtenerAdicionalEmpleado');
        Route::post('obtenerLicenciaEmpleado','Recursos_Humanos\Empleado_Controller@obtenerLicenciaEmpleado');
        Route::post('obtenerNominaEmpleado','Recursos_Humanos\Empleado_Controller@obtenerNominaEmpleado');
        Route::post('obtenerDireccionEmpleado','Recursos_Humanos\Empleado_Controller@obtenerDireccionEmpleado');
        Route::post('obtenerImagenEmpleado','Recursos_Humanos\Empleado_Controller@obtenerImagenEmpleado');
        Route::post('obtenerSucursal','Catalogos\Sucursal_Controller@obtenerSucursal');
        Route::post('obtenerDepartamentos','Catalogos\Departamento_Controller@obtenerDepartamentos');
        Route::post('obtenerMunicipios','Catalogos\Municipio_Controller@obtenerMunicipios');
        Route::post('guardarEmpleado','Recursos_Humanos\Empleado_Controller@guardar');
        Route::post('obtenerEmpleado','Recursos_Humanos\Empleado_Controller@obtenerEmpleado');
        Route::post('eliminarEmpleado','Recursos_Humanos\Empleado_Controller@borrar');
        Route::post('guardarPlantillaEmpleado','Recursos_Humanos\Empleado_Controller@guardarPlantillaEmpleado');
        Route::get('descargarDocumentos/{id}/{tabla}/{filesystem}','Genericos\Documento_Controller@descargarDocumentos');
        Route::get('obtenerDocumentos/{id}/{tabla}/{filesystem}','Genericos\Documento_Controller@obtenerDocumentos');
        Route::post('guardarDocumento','Recursos_Humanos\Empleado_Controller@guardarDocumentoExpedienteDigitalEmpleado');

        /**
         * Rutas de Incapacidad
         */
        Route::get('incapacidad','Recursos_Humanos\Incapacidad_Controller@index');
        Route::post('guardarIncapacidad','Recursos_Humanos\Incapacidad_Controller@guardar');
        Route::post('eliminarIncapacidad','Recursos_Humanos\Incapacidad_Controller@eliminar');
        Route::post('obtenerIncapacidad','Recursos_Humanos\Incapacidad_Controller@obtenerIncapacidad');
        Route::post('upload','Recursos_Humanos\Incapacidad_Controller@upload');
        Route::post('obtenerArchivoIncapacidad','Recursos_Humanos\Incapacidad_Controller@obtenerArchivoIncapacidad');
        
        /**
         * Rutas de Permiso Asistencia
         */
        Route::get('permiso_asistencia','Recursos_Humanos\Permiso_Asistencia_Controller@index');
        Route::post('guardarPermisoAsistencia','Recursos_Humanos\Permiso_Asistencia_Controller@guardar');
        Route::post('eliminarPermisoAsistencia','Recursos_Humanos\Permiso_Asistencia_Controller@eliminar');
        Route::post('obtenerPermisoAsistencia','Recursos_Humanos\Permiso_Asistencia_Controller@obtenerPermisoAsistencia');
        Route::post('uploadArchivoPermiso','Recursos_Humanos\Permiso_Asistencia_Controller@upload');
        Route::post('obtenerArchivoPermisoAsistencia','Recursos_Humanos\Permiso_Asistencia_Controller@obtenerArchivoPermisoAsistencia');

        /**
         * Rutas de Vacaciones
         */
        Route::get('vacaciones','Recursos_Humanos\Vacaciones_Controller@index');
        Route::post('guardarVacaciones','Recursos_Humanos\Vacaciones_Controller@guardar');
        Route::post('eliminarVacaciones','Recursos_Humanos\Vacaciones_Controller@eliminar');
        Route::post('obtenerVacaciones','Recursos_Humanos\Vacaciones_Controller@obtenerVacaciones');
        Route::post('uploadArchivoVacaciones','Recursos_Humanos\Vacaciones_Controller@upload');
        Route::post('obtenerArchivoVacaciones','Recursos_Humanos\Vacaciones_Controller@obtenerArchivoVacaciones');

        /**
         * Rutas de Faltas
         */
        Route::get('falta','Recursos_Humanos\Falta_Controller@index');
        Route::post('guardarFalta','Recursos_Humanos\Falta_Controller@guardar');
        Route::post('eliminarFalta','Recursos_Humanos\Falta_Controller@eliminar');
        Route::post('obtenerFalta','Recursos_Humanos\Falta_Controller@obtenerFalta');
        Route::post('uploadArchivoFalta','Recursos_Humanos\Falta_Controller@upload');
        Route::post('obtenerArchivoFalta','Recursos_Humanos\Falta_Controller@obtenerArchivoFalta');
        Route::post('justificarFalta','Recursos_Humanos\Falta_Controller@justificar');

        /**
         * Rutas de nómina
         */
        Route::get('nomina','Recursos_Humanos\Nomina_Controller@index');
        Route::post('generarNomina','Recursos_Humanos\Nomina_Controller@generarNomina');

        /**
         * Rutas de Solicitudes de recursos humanos
         */
        Route::post('cambioEstatusSolicitud','Genericos\Solicitud_Controller@cambiarEstatus');

    });

    Route::group(['prefix' => 'solicitud'],function () {

        Route::get('generarSolicitud','Genericos\Solicitud_Controller@index');
        Route::get('bitacoraSolicitud','Genericos\Solicitud_Controller@indexBitacora');
        Route::post('guardarSolicitud','Genericos\Solicitud_Controller@guardarSolicitud');
        Route::post('obtenerSolicitudEditar','Genericos\Solicitud_Controller@obtenerSolicitudEditar');

    });

    Route::group(['prefix' => 'planeacion'],function () {

        Route::get('bancoProyecto','Planeacion\Banco_Proyecto\Obra_Controller@index');
        Route::post('obtenerMunicipios','Catalogos\Municipio_Controller@obtenerMunicipios');
        Route::post('guardarProyecto','Planeacion\Banco_Proyecto\Obra_Controller@guardar');
        Route::post('obtenerObra','Planeacion\Banco_Proyecto\Obra_Controller@obtenerObra');
        Route::post('cancelarObra','Planeacion\Banco_Proyecto\Obra_Controller@cancelarObra');
        Route::post('obtenerObraAdicional','Planeacion\Banco_Proyecto\Obra_Controller@obtenerAdicionalesDeObra');
        Route::post('autorizarProyecto','Planeacion\Banco_Proyecto\Obra_Controller@autorizarObra');

        /*
        * Rutas usadas por Registro_Padron_Controller
        */
        
        Route::get('registroPadron','Planeacion\Registro_Padron_Controller@index');
        Route::post('guardarRegistroPadron','Planeacion\Registro_Padron_Controller@guardar');
        Route::post('actualizarRegistroPadron','Planeacion\Registro_Padron_Controller@actualizar');
        Route::post('obtenerRegistroPadronEditar','Planeacion\Registro_Padron_Controller@obtenerRegistroPadronEditar');
        Route::post('eliminarRegistroPadron','Planeacion\Registro_Padron_Controller@eliminar');
        Route::post('obtenerHistorialRegistroPadron','Planeacion\Registro_Padron_Controller@obtenerHistorialRegistroPadron');
        Route::get('descargarDocumentos/{id}/{tabla}/{filesystem}','Genericos\Documento_Controller@descargarDocumentos');
    });

 Route::group(['prefix' => 'indicadores'],function () {
    Route::get('bancoProyectos','indicadores\Proyectos_Controller@index');
    Route::post('obtenerProyectoEditar','indicadores\Proyectos_Controller@obtener');
    Route::post('ficha/obtenerProyectoEditar','indicadores\Proyectos_Controller@obtener');
    Route::post('ficha/guardarProyecto','indicadores\Proyectos_Controller@guardar');
    Route::post('guardarProyecto','indicadores\Proyectos_Controller@guardar');
    Route::post('eliminarProyecto','indicadores\Proyectos_Controller@eliminar');
    Route::get('ficha/{id}','indicadores\Proyectos_Controller@indicadores');
    Route::post('ficha/obtenerActividadEditar','indicadores\Proyectos_Controller@obtenerA');
   Route::post('ficha/guardarActividads','indicadores\Proyectos_Controller@guardarActividads');
    });



    Route::get("sistema/logo_actual",function (){
        $SisLogos = \Illuminate\Support\Facades\DB::table("sisLogos")->where("Activo",1)->get();
        return '<img alt="Logo" src="data:'.$SisLogos[0]->Mime.';base64,'.base64_encode($SisLogos[0]->Logo).'"/>';
    });
    Route::get("sistema/logo/{id}",function ($id){
        $SisLogos = \Illuminate\Support\Facades\DB::table("sisLogos")->find($id);
        return '<img alt="Logo" src="data:'.$SisLogos->Mime.';base64,'.base64_encode($SisLogos->Logo).'"/>';
    });
});