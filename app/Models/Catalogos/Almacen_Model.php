<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Almacen_Model extends Model
{

    protected $table = 'almacen';
    protected $primaryKey = 'id_almacen';

    public static $id = 'id_almacen';
    public static $tabla = 'almacen';
    public static $nombre = 'nombre';
    public static $numeroAlmacen = 'numero_almacen';
    public static $comentarios = 'comentarios';
    public static $idSucursal = 'id_sucursal';
    public static $estatus = 'estatus';

}
