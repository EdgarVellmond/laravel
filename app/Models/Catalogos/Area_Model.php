<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area_Model extends Model
{
    protected $table = 'area';
    public static $tabla = 'area';
    protected $primaryKey = 'id_area';

    public static $id = 'id_area';
    public static $nombre = 'nombre';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}