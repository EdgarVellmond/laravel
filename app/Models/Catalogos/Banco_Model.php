<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Banco_Model extends Model
{

    protected $table = 'banco';
    protected $primaryKey = 'id_banco';

    public static $id = 'id_banco';
    public static $tabla = 'banco';
    public static $nombre = 'nombre';
    public static $clave = 'clave';
    public static $razonSocial = 'razon_social';
    public static $estatus = 'estatus';

}
