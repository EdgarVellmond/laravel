<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria_Expediente_Digital_Model extends Model
{
    protected $table = 'categoria_expediente_digital';
    public static $tabla = 'categoria_expediente_digital';
    protected $primaryKey = 'id_categoria_expediente_digital';

    public static $id = 'id_categoria_expediente_digital';
    public static $nombre = 'nombre';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}