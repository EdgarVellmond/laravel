<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clasificacion_Obra_Model extends Model
{
    protected $table = 'clasificacion_obra';
    public static $tabla = 'clasificacion_obra';
    protected $primaryKey = 'id_clasificacion_obra';

    public static $id = 'id_clasificacion_obra';
    public static $nombre = 'nombre';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}