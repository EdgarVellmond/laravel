<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clave_Giro_Nomina_Model extends Model
{
    protected $table = 'clave_giro_nomina';
    public static $tabla = 'clave_giro_nomina';
    protected $primaryKey = 'id_clave_giro_nomina';

    public static $id = 'id_clave_giro_nomina';
    public static $cuenta = 'cuenta';
    public static $nombre = 'nombre';
    public static $tipoGiro = 'tipo_giro';
    public static $idEmpresa = 'id_empresa';
    public static $prestacion = 'prestacion';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}