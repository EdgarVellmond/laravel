<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente_Model extends Model
{

    use SoftDeletes;


    protected $table = 'cliente';
    protected $primaryKey = 'id_cliente';

    public static $id = 'id_cliente';
    public static $tabla = 'cliente';
    public static $nombre = 'nombre_cliente';
    public static $apellidoPaterno = 'apellido_paterno';
    public static $apellidoMaterno = 'apellido_materno';
    public static $rfc = 'rfc';
    public static $credito = 'credito';
    public static $diasCredito = 'dias_credito';
    public static $limiteCredito = 'limite_credito';
    public static $telefono = 'telefono';
    public static $correo = 'correo';
    public static $calle = 'calle';
    public static $colonia = 'colonia';
    public static $numeroInterior = 'numero_interior';
    public static $numeroExterior = 'numero_exterior';
    public static $cp = 'cp';
    public static $tipoPersona = 'tipo_persona';
    public static $idMetodoPago = 'id_metodo_pago';
    public static $idBanco = 'id_banco';
    public static $idMunicipio = 'id_municipio';

}
