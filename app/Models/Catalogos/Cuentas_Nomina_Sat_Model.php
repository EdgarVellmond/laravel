<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuentas_Nomina_Sat_Model extends Model
{
    protected $table = 'cuentas_nomina_sat';
    public static $tabla = 'cuentas_nomina_sat';
    protected $primaryKey = 'id_cuentas_nomina_sat';

    public static $id = 'id_cuentas_nomina_sat';
    public static $cuenta = 'cuenta';
    public static $nombre = 'nombre';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}