<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuota_Obrero_Patronal_Model extends Model
{

    protected $table = 'cuota_obrero_patronal';
    public static $tabla = 'cuota_obrero_patronal';
    protected $primaryKey = 'id_cuota_obrero_patronal';

    public static $id = 'id_cuota_obrero_patronal';
    public static $ramo = 'ramo';
    public static $valor = 'valor';
    public static $tipoCuota = 'tipo_cuota';

    use softDeletes;

}
