<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento_Model extends Model
{
    protected $table = 'departamento';
    public static $tabla = 'departamento';
    protected $primaryKey = 'id_departamento';

    public static $id = 'id_departamento';
    public static $nombre = 'nombre';
    public static $idArea = 'id_area';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}