<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dia_Festivo_Model extends Model
{
    protected $table = 'dia_festivo';
    public static $tabla = 'dia_festivo';
    protected $primaryKey = 'id_dia_festivo';

    public static $id = 'id_dia_festivo';
    public static $nombre = 'nombre';
    public static $fecha = 'fecha';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}