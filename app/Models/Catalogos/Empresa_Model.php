<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Catalogos\Empresa_Imagen_Model;

class Empresa_Model extends Model
{
    protected $table = 'empresa';
    public static $tabla = 'empresa';
    protected $primaryKey = 'id_empresa';

    public static $id = 'id_empresa';
    public static $razonSocial = 'razon_social';
    public static $clave = 'clave';
    public static $rfc = 'rfc';
    public static $correo = 'correo';
    public static $calle = 'calle';
    public static $numeroExterior = 'numero_exterior';
    public static $numeroInterior = 'numero_interior';
    public static $colonia = 'colonia';
    public static $cp = 'cp';
    public static $patron = 'patron';
    public static $operacion = 'operacion';
    public static $tipoNomina = 'tipo_nomina';
    public static $idEmpresaImagen = 'id_empresa_imagen';
    public static $idMunicipio = 'id_municipio';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

    /**
     * Relación de Empresa con su imagen en Empresa_Imagen
     * @author Erik Villarreal
     */
    public function relacionEmpresaImagen(){
        return $this->belongsTo(Empresa_Imagen_Model::class, self::$idEmpresaImagen);
    }
}
