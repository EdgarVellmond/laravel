<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estado_Model extends Model
{
    use SoftDeletes;

    protected $table = 'estado';
    protected $primaryKey = 'id_estado';

    public static $id = 'id_estado';
    public static $tabla = 'estado';
    public static $nombre = 'nombre';
}
