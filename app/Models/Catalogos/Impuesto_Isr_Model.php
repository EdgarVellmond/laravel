<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Impuesto_Isr_Model extends Model
{

    use SoftDeletes;

    protected $table = 'impuesto_isr';
    protected $primaryKey = 'id_impuesto_isr';

    public static $id = 'id_impuesto_isr';
    public static $tabla = 'impuesto_isr';
    public static $limiteInferior = 'limite_inferior';
    public static $limiteSuperior = 'limite_superior';
    public static $cuotaFija = 'cuota_fija';
    public static $porcentaje = 'porcentaje';
    public static $tipoNomina = 'tipo_nomina';

}
