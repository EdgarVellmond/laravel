<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Metodo_Pago_Model extends Model
{

    use SoftDeletes;
    protected $table = 'metodo_pago';
    protected $primaryKey = 'id_metodo_pago';

    public static $id = 'id_metodo_pago';
    public static $tabla = 'metodo_pago';
    public static $nombre = 'nombre';
    public static $tipoPago = 'tipo_pago';
    public static $formaPago = 'forma_pago';

}
