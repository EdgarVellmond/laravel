<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio_Model extends Model
{
    use SoftDeletes;

    protected $table = 'municipio';
    protected $primaryKey = 'id_municipio';

    public static $id = 'id_municipio';
    public static $tabla = 'municipio';
    public static $nombre = 'nombre';
    public static $idEstado = 'id_estado';
}
