<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plantilla_Model extends Model
{

    use softDeletes;

    protected $table = 'plantilla';
    public static $tabla = 'plantilla';
    protected $primaryKey = 'id_plantilla';

    public static $id = 'id_plantilla';
    public static $nombre = 'nombre';
    public static $idTipoPlantilla = 'id_tipo_plantilla';
    public static $contenido = 'contenido';

    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}
