<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prima_Riesgo_Model extends Model
{

    use SoftDeletes;

    protected $table = 'prima_riesgo';
    protected $primaryKey = 'id_prima_riesgo';

    public static $id = 'id_prima_riesgo';
    public static $tabla = 'prima_riesgo';
    public static $nombre = 'nombre';
    public static $valor = 'valor';

}
