<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salario_Minimo_Model extends Model
{

    use SoftDeletes;

    protected $table = 'salario_minimo';
    protected $primaryKey = 'id_salario_minimo';

    public static $id = 'id_salario_minimo';
    public static $tabla = 'salario_minimo';
    public static $valor = 'valor';
    public static $anio = 'anio';

}
