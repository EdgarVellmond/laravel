<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal_Model extends Model
{

    use SoftDeletes;

    protected $table = 'sucursal';
    protected $primaryKey = 'id_sucursal';

    public static $id = 'id_sucursal';
    public static $tabla = 'sucursal';
    public static $nombre = 'nombre';
    public static $clave = 'clave';
    public static $correo = 'correo';
    public static $telefono = 'telefono';
    public static $calle = 'calle';
    public static $numeroInterior = 'numero_interior';
    public static $numeroExterior = 'numero_exterior';
    public static $colonia = 'colonia';
    public static $cp = 'cp';
    public static $idEmpresa = 'id_empresa';
    public static $idMunicipio = 'id_municipio';



}
