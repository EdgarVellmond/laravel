<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo_Plantilla_Model extends Model
{
    protected $table = 'tipo_plantilla';
    public static $tabla = 'tipo_plantilla';
    protected $primaryKey = 'id_tipo_plantilla';

    public static $id = 'id_tipo_plantilla';
    public static $nombre = 'nombre';

    use softDeletes;

    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}