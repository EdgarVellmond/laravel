<?php

namespace App\Models\Catalogos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo_Solicitud_Model extends Model
{
    protected $table = 'tipo_solicitud';
    public static $tabla = 'tipo_solicitud';
    protected $primaryKey = 'id_tipo_solicitud';

    public static $id = 'id_tipo_solicitud';
    public static $nombre = 'nombre';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}