<?php

namespace App\Models\Genericos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento_Model extends Model
{
    protected $table = 'documento';
    public static $tabla = 'documento';
    protected $primaryKey = 'id_documento';

    public static $id = 'id_documento';
    public static $idPropietario = 'id_propietario';
    public static $tablaOrigen = 'tabla';
    public static $mime = 'mime';
    public static $ruta = 'ruta';
    public static $nombre = 'nombre';
    public static $peso = 'peso';
    public static $uuid = 'uuid';
    public static $extension = 'extension';
    public static $descripcion = 'descripcion';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}
