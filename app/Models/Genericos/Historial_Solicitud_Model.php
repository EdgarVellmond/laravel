<?php

namespace App\Models\Genericos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Historial_Solicitud_Model extends Model
{

    protected $table = 'historial_solicitud';
    public static $tabla = 'historial_solicitud';
    protected $primaryKey = 'id_historial_solicitud';

    public static $id = 'id_historial_solicitud';
    public static $accion = 'accion';
    public static $idSolicitud = 'id_solicitud';

    use softDeletes;

    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}
