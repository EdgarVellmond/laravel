<?php

namespace App\Models\Genericos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitud_Model extends Model
{
    protected $table = 'solicitud';
    public static $tabla = 'solicitud';
    protected $primaryKey = 'id_solicitud';

    public static $id = 'id_solicitud';
    public static $idArea = 'id_area';
    public static $idTipoSolicitud = 'id_tipo_solicitud';
    public static $asunto = 'asunto';
    public static $observaciones = 'observaciones';
    public static $estatus = 'estatus';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}
