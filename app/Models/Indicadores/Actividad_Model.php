<?php

namespace App\Models\Indicadores;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Actividad_Model extends Model
{
    //
     use softDeletes;

    protected $table = 'actividad';
    public static $tabla = 'actividad';
    protected $primaryKey = 'id_actividad';

    public static $id_actividad = 'id_actividad';
    public static $tipoIndicador = 'tipo_indicador';
    public static $porcentaje = 'porcentaje';
    public static $programado = 'programado';
    public static $real = 'real';
    public static $idProyecto = 'id_proyecto';
    public static $semana = 'semana';
    public static $area = 'area';

}
