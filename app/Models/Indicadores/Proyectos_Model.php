<?php

namespace App\Models\Indicadores;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyectos_Model extends Model
{
    //
    use softDeletes;

    protected $table = 'proyecto';
    public static $tabla = 'proyecto';
    protected $primaryKey = 'id_proyecto';

    public static $id = 'id_proyecto';
    public static $proyectoNombre = 'proyecto_nombre';
    public static $cliente = 'cliente';
    public static $ubicacion = 'ubicacion';
    public static $idEmpresa = 'id_empresa';
    public static $descripcion = 'descripcion';
    public static $prioridad = 'prioridad';
    public static $saldoEjercer = 'saldo_a_ejercer';
    public static $fechaInicial = 'fecha_inicial';
    public static $fechaTermino = 'fecha_termino';
    public static $observaciones = 'observaciones';
    public static $duracion = 'duracion';
    public static $gastobra = 'gasto_obra';
    public static $estatus = 'estatus';
    public static $tipo = 'tipo';
    public static $montocontr = 'monto_contrato';
    public static $montoejercido = 'monto_ejercido';
}

