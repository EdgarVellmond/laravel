<?php

namespace App\Models\Planeacion\Banco_Proyecto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Obra_Model extends Model
{

    use softDeletes;

    protected $table = 'obra';
    public static $tabla = 'obra';
    protected $primaryKey = 'id_obra';

    public static $id = 'id_obra';
    public static $nombre = 'nombre';
    public static $folio = 'folio';
    public static $idCliente = 'id_cliente';
    public static $idEmpresa = 'id_empresa';
    public static $descripcion = 'descripcion';
    public static $prioridad = 'prioridad';
    public static $idMunicipio = 'id_municipio';
    public static $localidad = 'localidad';
    public static $capitalMinimoContable = 'capital_minimo_contable';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaTermino = 'fecha_termino';
    public static $fechaLimiteBase = 'fecha_limite_base';
    public static $observaciones = 'observaciones';
    public static $estatus = 'estatus';

}
