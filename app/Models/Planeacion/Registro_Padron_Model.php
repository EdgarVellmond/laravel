<?php

namespace App\Models\Planeacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registro_Padron_Model extends Model
{

    use softDeletes;

    protected $table = 'registro_padron';
    public static $tabla = 'registro_padron';
    protected $primaryKey = 'id_registro_padron';

    public static $id = 'id_registro_padron';
    public static $idCliente = 'id_cliente';
    public static $idEmpresa = 'id_empresa';
    public static $comentario = 'comentario';
    public static $estatus = 'estatus';
    public static $inicioVigencia = 'inicio_vigencia';
    public static $finVigencia = 'fin_vigencia';
    public static $inscripcion = 'inscripcion';

    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';

}
