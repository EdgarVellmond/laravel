<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;

class Empleado_Model extends Model
{
    protected $table = 'empleado';
    public static $tabla = 'empleado';
    protected $primaryKey = 'id_empleado';

    public static $id = 'id_empleado';
    public static $nombre = 'nombre';
    public static $apellidoPaterno = 'apellido_paterno';
    public static $apellidoMaterno = 'apellido_materno';
    public static $numeroEmpleado = 'numero_empleado';
    public static $rfc = 'rfc';
    public static $curp = 'curp';
    public static $nss = 'nss';
    public static $telefono = 'telefono';
    public static $correo = 'correo';
    public static $numeroLicencia = 'numero_licencia';
    public static $vigenciaLicencia = 'vigencia_licencia';
    public static $salarioBaseDiario = 'salario_base_diario';
    public static $salarioDiarioIntegrado = 'salario_diario_integrado';
    public static $inicioContrato = 'inicio_contrato';
    public static $finContrato = 'fin_contrato';
    public static $viaticos = 'viaticos';
    public static $calle = 'calle';
    public static $colonia = 'colonia';
    public static $numeroInterior = 'numero_interior';
    public static $numeroExterior = 'numero_exterior';
    public static $cp = 'cp';
    public static $diasVacaciones = 'dias_vacaciones';
    public static $idEmpleadoImagen = 'id_empleado_imagen';
    public static $idDepartamento = 'id_departamento';
    public static $idPuesto = 'id_puesto';
    public static $idMunicipio = 'id_municipio';
    public static $idSucursal = 'id_sucursal';
    public static $idPatron = 'id_patron';
    public static $idPrimaRiesgo = 'id_prima_riesgo';


    use softDeletes;
    
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}
