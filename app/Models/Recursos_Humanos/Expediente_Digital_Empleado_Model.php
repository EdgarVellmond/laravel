<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expediente_Digital_Empleado_Model extends Model
{
    protected $table = 'expediente_digital_empleado';
    public static $tabla = 'expediente_digital_empleado';
    protected $primaryKey = 'id_expediente_digital_empleado';

    public static $id = 'id_expediente_digital_empleado';
    public static $idExpedienteDigital = 'id_expediente_digital';
    public static $idEmpleado = 'id_empleado';
    public static $idDocumento = 'id_documento';
    public static $observaciones = 'observaciones';
    public static $vigencia = 'vigencia';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}
