<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Falta_Model extends Model
{

    use softDeletes;

    protected $table = 'falta';
    public static $tabla = 'falta';
    protected $primaryKey = 'id_falta';

    public static $id = 'id_falta';
    public static $idEmpleado = 'id_empleado';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaFin = 'fecha_fin';
    public static $dias = 'dias';
    public static $comentarios = 'comentarios';
    public static $estatus = 'estatus';
    public static $idFaltaArchivo = 'id_falta_archivo';

}
