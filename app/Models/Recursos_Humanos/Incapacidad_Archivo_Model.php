<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incapacidad_Archivo_Model extends Model
{

    protected $table = 'incapacidad_archivo';
    public static $tabla = 'incapacidad_archivo';
    protected $primaryKey = 'id_incapacidad_archivo';

    public static $id = 'id_incapacidad_archivo';
    public static $mime = 'mime';
    public static $ruta = 'ruta';
    public static $nombre = 'nombre';
    public static $peso = 'peso';
    public static $uuid = 'uuid';
    public static $extension = 'extension';

    use softDeletes;

}
