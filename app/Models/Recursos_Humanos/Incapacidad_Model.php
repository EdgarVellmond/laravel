<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incapacidad_Model extends Model
{

    use softDeletes;

    protected $table = 'incapacidad';
    public static $tabla = 'incapacidad';
    protected $primaryKey = 'id_incapacidad';

    public static $id = 'id_incapacidad';
    public static $idEmpleado = 'id_empleado';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaFin = 'fecha_fin';
    public static $dias = 'dias';
    public static $comentarios = 'comentarios';
    public static $idIncapacidadArchivo = 'id_incapacidad_archivo';
    public static $tipoIncapacidad = 'tipo_incapacidad';

}
