<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nomina_Model extends Model
{

    use softDeletes;

    protected $table = 'nomina';
    public static $tabla = 'nomina';
    protected $primaryKey = 'id_nomina';

    public static $id = 'id_nomina';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaFin = 'fecha_fin';
    public static $periodo = 'periodo';
    public static $idEmpresa = 'id_empresa';
    public static $totalPagado = 'total_pagado';

}
