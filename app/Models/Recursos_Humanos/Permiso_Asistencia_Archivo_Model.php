<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso_Asistencia_Archivo_Model extends Model
{

    protected $table = 'permiso_asistencia_archivo';
    public static $tabla = 'permiso_asistencia_archivo';
    protected $primaryKey = 'id_permiso_asistencia_archivo';

    public static $id = 'id_permiso_asistencia_archivo';
    public static $mime = 'mime';
    public static $ruta = 'ruta';
    public static $nombre = 'nombre';
    public static $peso = 'peso';
    public static $uuid = 'uuid';
    public static $extension = 'extension';

    use softDeletes;

}
