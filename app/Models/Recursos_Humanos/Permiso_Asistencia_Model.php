<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso_Asistencia_Model extends Model
{

    use softDeletes;

    protected $table = 'permiso_asistencia';
    public static $tabla = 'permiso_asistencia';
    protected $primaryKey = 'id_permiso_asistencia';

    public static $id = 'id_permiso_asistencia';
    public static $idEmpleado = 'id_empleado';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaFin = 'fecha_fin';
    public static $dias = 'dias';
    public static $comentarios = 'comentarios';
    public static $idPermisoAsistenciaArchivo = 'id_permiso_asistencia_archivo';
    public static $goceSueldo = 'goce_sueldo';


}
