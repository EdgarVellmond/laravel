<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plantilla_Empleado_Model extends Model
{

    use softDeletes;

    protected $table = 'plantilla_empleado';
    public static $tabla = 'plantilla_empleado';
    protected $primaryKey = 'id_plantilla_empleado';

    public static $id = 'id_plantilla_empleado';
    public static $idEmpleado = 'id_empleado';
    public static $idPlantilla = 'id_plantilla';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaFin = 'fecha_fin';
    public static $observaciones = 'observaciones';
    public static $contenido = 'contenido';
    public static $vigencia = 'vigencia';

    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $deletedAt = 'deleted_at';

}
