<?php

namespace App\Models\Recursos_Humanos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacaciones_Model extends Model
{

    use softDeletes;

    protected $table = 'vacaciones';
    public static $tabla = 'vacaciones';
    protected $primaryKey = 'id_vacaciones';

    public static $id = 'id_vacaciones';
    public static $idEmpleado = 'id_empleado';
    public static $fechaInicio = 'fecha_inicio';
    public static $fechaFin = 'fecha_fin';
    public static $dias = 'dias';
    public static $comentarios = 'comentarios';
    public static $idVacacionesArchivo = 'id_vacaciones_archivo';

}
