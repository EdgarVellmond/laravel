<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria_Variables_Sistema_Model extends Model
{
    protected $table = 'categoria_variables_sistema';
    public static $tabla = 'categoria_variables_sistema';
    protected $primaryKey = 'id_categoria_variables_sistema';

    public static $id = 'id_categoria_variables_sistema';
    public static $nombre = 'nombre';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}