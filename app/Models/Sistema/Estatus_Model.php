<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class Estatus extends Model
{
    protected $table = "sistema_estatus";
    protected $primaryKey = "id_estatus";
}
