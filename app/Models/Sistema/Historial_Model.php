<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class Historial_Model extends Model
{
    protected $table = "sistema_historial";
    protected $primaryKey = "id_historial";

    public static $id = 'id_historial';
    public static $tabla = 'tabla';
    public static $idCambiado = 'id_cambiado';
    public static $datosNuevo = 'datos_nuevo';
    public static $datosAnterior = 'datos_anterior';
    public static $idUsuario = 'id_usuario';
    public static $servidor = 'servidor';
    public static $ipCliente = 'ip_cliente';
    public static $browser = 'browser';

    public function relacionUsuario(){
        return $this->belongsTo(Usuarios_Model::class,self::$idUsuario);
    }
}
