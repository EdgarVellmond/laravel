<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class Permiso_Model extends Model
{

    protected $table = 'permiso';
    public static $tabla = 'permiso';
    protected $primaryKey = 'id_permiso';

    public static $id = 'id_permiso';
    public static $nombre = 'nombre';
    public static $modulo = 'modulo';
    public static $descripcion = 'descripcion';


}
