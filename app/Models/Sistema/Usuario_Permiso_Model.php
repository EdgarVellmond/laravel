<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Usuario_Permiso_Model extends Model
{

    protected $table = 'usuario_permiso';
    public static $tabla = 'usuario_permiso';
    protected $primaryKey = 'id_usuario_permiso';

    public static $id = 'id_usuario_permiso';
    public static $idUsuario = 'id_usuario';
    public static $idPermiso = 'id_permiso';

    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

    use softDeletes;

}
