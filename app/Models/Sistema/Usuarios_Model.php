<?php

namespace App\Models\Sistema;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class Usuarios_Model extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * Variables Principales del Modelo
     * @author Luis Chavez
     */
    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';

    public static $id = 'id_usuario';
    public static $tabla = 'usuarios';
    public static $estatus = 'estatus';
    public static $usuario = 'usuario';
    public static $password = 'password';
    public static $nombre = 'nombre';
    public static $correo = 'correo';
    public static $idEmpleado = "id_empleado";
    
    protected $fillable = ['nombre','usuario','correo', 'password'];
    protected $hidden = ['password', 'remember_token'];

    /**
     * @author Luis Chavez
     * @var mensajes del model de usuarios
     */
    private $messages = array(
            'nombre.required'         => 'El nombre de usuario es obligatorio.',
            'usuario.required'       => 'El nombre de usuario es obligatorio!',
            'usuario.unique'         => 'El nombre de usuario ya existe, elige otro!',
            'password.required'       => 'El largo del password debe ser minimo de 6!',
            'correo.required'          => 'El correo electrónico es obligatorio !',
            'correo.unique'            => 'El correo elctrónico ya exise, elige otro!' 

        );

    /**
     * Funcion para saber si el usuario esta en linea
     * @author Luis Chavez
     * @return boleano
     */
    public function isOnline(){
        return Cache::has('usuario_sistema_online_' . $this->getAttribute(self::$id));
    }

    /**
     * Funcion para Validar Nuevos Usuarios en el Sistema
     * @author Luis Chavez
     * @param $data ##Datos para la validacion
     * @return bool ##Resultado de la Validacion
     */
    public function validate($data){
        $rules = array(
            'nombre'   => 'required',
            'usuario' => 'required|unique:usuarios,usuario',
            'password' => 'required|min:4',
            'correo'    => 'required'
        );
        $validator = Validator::make($data, $rules, $this->messages);
        if ($validator->fails()){
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }
    /**
     * Funcion para Validar cambios en el Usuario
     * @author Luis Chavez
     * @param $data ##Datos para la validacion
     * @return bool ##Resultado de la Validacion
     */
    public function validateforupdate($data,$id) {
        if(isset($data['password'])){
            $rules = [
                'nombre'   => 'required',
                'usuario' => 'required|unique:usuarios,usuario,'.$id.','.self::$id,
                'correo'    => 'required',
                'password' => 'required|min:4'
            ];
        }else{
            $rules = [
                'nombre'   => 'required',
                'usuario' => 'required|unique:usuarios,usuario,'.$id.','.self::$id,
                'correo'    => 'required'
            ];
        }
        $messages = array(
            'nombre.required'         => 'El Nombre del Usuario es Obligatorio',
            'usuario.required'       => 'El Usuario es Obligatorio',
            'usuario.unique'         => 'El Usuario ya existe, Elige otro Usuario',
            'password.required'       => 'El Largo de la Contraseña debe ser minimo de 4 Caracteres',
            'correo.required'          => 'El correo electrónico es Obligatorio',

        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()){
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }
    public function errors(){
        return $this->errors;
    }
}
