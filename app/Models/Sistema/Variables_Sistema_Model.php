<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variables_Sistema_Model extends Model
{
    protected $table = 'variables_sistema';
    public static $tabla = 'variables_sistema';
    protected $primaryKey = 'id_variables_sistema';

    public static $id = 'id_variables_sistema';
    public static $idCategoria = 'id_categoria_variables_sistema';
    public static $nombre = 'nombre';
    public static $tablaOrigen = 'tabla';
    public static $aliasTablaOrigen = 'alias_tabla';
    public static $campo = 'campo';
    public static $idRol = 'id_rol';

    use softDeletes;
    
    public static $createdAt = 'created_at';
    public static $updatedAt = 'updated_at';
    public static $idUsuarioCreacion = 'id_usuario_creacion';
    public static $idUsuarioEdicion = 'id_usuario_edicion';

}