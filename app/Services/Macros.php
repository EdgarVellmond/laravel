<?php namespace App\Services;

use Collective\Html\FormBuilder;

class Macros extends FormBuilder
{
    public function selectState($name, $selected = null, $options = array())
    {
        $list = [
            '' => 'Select One...',
            'AL' => 'Alabama',
            'AK' => 'Alaska',
        ];

        return $this->select($name, $list, $selected, $options);
    }
}