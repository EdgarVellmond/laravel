<?php

return [

    /*
     *  Corresponde a tabla metodo_pago
     */
    'tipo_pago' => [
        'ABONO' => 'Abono',
        'CARGO' => 'Cargo',
    ],

    'forma_pago' => [
        'CONTADO' => 'Contado',
        'CREDITO' => 'Credito',
    ],

    /*
     * Corresponde a tabla proveedor
     */
    'tipo_persona' => [
        'FISICA' => 'Física',
        'MORAL' => 'Moral',
    ],

    /*
     * Corresponde a tabla impuesto_isr
     */
    'tipo_nomina' => [
        'SEMANAL' => 'Semanal',
        'DECENAL' => 'Decenal',
        'QUINCENAL' => 'Quincenal',
        'MENSUAL' => 'Mensual',
    ],

    /*
     * Corresponde a tabla incapacidad
     */
    'tipo_incapacidad' => [
        'MATERNIDAD' => 'Maternidad',
        'ENFERMEDAD_GENERAL' => 'Enfermedad General',
        'RIESGO_TRABAJO' => 'Riesgo de Trabajo',
    ],

    /*
     * Corresponde a tabla permiso_asistencia
     */
    'goce_sueldo' => [
        'SI' => 'Si',
        'NO' => 'No',
    ],

    /*
     * Corresponde a tabla falta
     */
    'estatus' => [
        'ACTIVA' => 'ACTIVA',
        'JUSTIFICADA' => 'JUSTIFICADA',
    ],

    /*
     * Corresponde a tabla cuota_obrero_patronal
     */
    'tipo_cuota' => [
        'TRABAJADOR' => 'Trabajador',
        'PATRON' => 'Patron',
    ],

    /*
     * Corresponde a tabla clave_giro_nomina
     */
    'tipo_giro' => [
        'PERCEPCION' => 'Percepcion',
        'DEDUCCION' => 'Deduccion',
    ],

    /*
     * Corresponde a tabla obra
     */
    'prioridad' => [
        'ALTA' => 'Alta',
        'MEDIA' => 'Media',
        'BAJA' => 'Baja',
    ],

];

?>