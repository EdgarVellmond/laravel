<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_permiso', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id_usuario_permiso');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->integer('id_permiso')->unsigned()->nullable();
            $table->integer('id_usuario_edicion')->unsigned()->nullable();
            $table->integer('id_usuario_creacion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_permiso')->references('id_permiso')->on('id_permiso')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permiso', function (Blueprint $table) {
            $table->dropForeign('permiso_id_usuario_foreign');
            $table->dropForeign('permiso_id_permiso_foreign');
        });
        Schema::drop('permiso');
    }
}
