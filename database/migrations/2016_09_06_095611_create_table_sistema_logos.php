<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSistemaLogos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_logos', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('activo');
            $table->string('periodo');
            $table->string('nombre');
            $table->binary('logo');
            $table->string('mime');
            $table->string('extension');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sistema_logos');
    }
}
