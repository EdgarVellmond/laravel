<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSistemaHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_historial', function (Blueprint $table) {
            $table->increments('id_historial');
            $table->string('tabla');
            $table->bigInteger('id_cambiado');
            $table->text('datos_nuevo');
            $table->text('datos_anterior');
            $table->bigInteger('id_usuario');
            $table->string('servidor');
            $table->string('ip_cliente');
            $table->string('browser');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sistema_historial');
    }
}
