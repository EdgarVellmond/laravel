<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipio', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_municipio');
            $table->string('nombre',100);
            $table->Integer('id_estado')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_estado')->references('id_estado')->on('estado')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('municipio', function (Blueprint $table) {
        $table->dropForeign('municipio_id_estado_foreign');
        });
        Schema::drop('municipio');
    }
}
