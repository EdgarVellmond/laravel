<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpresaImagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_imagen', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_empresa_imagen');
            $table->binary('archivo');
            $table->string('mime',45);
            $table->string('tipo',45);
            $table->string('nombre',100);
            $table->string('peso',45);
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresa_imagen');
    }
}
