<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_empresa');
            $table->string('razon_social',100);
            $table->string('clave',45);
            $table->string('rfc',15);
            $table->string('correo',70);
            $table->string('calle',100);
            $table->string('numero_interior',45);
            $table->string('numero_exterior',45);
            $table->string('colonia',100);
            $table->string('cp',5);
            $table->tinyInteger('patron');
            $table->tinyInteger('operacion');
            $table->enum('tipo_nomina', ['SEMANAL', 'DECENAL', 'QUINCENAL', 'MENSUAL']);
            $table->Integer('id_empresa_imagen')->unsigned()->nullable();
            $table->Integer('id_municipio')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empresa_imagen')->references('id_empresa_imagen')->on('empresa_imagen')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_municipio')->references('id_municipio')->on('municipio')->onDelete('set null')->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa', function (Blueprint $table) {
            $table->dropForeign('empresa_id_empresa_imagen_foreign');
            $table->dropForeign('empresa_id_municipio_foreign');
        });
        Schema::drop('empresa');
    }
}
