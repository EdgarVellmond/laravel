<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSucursal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_sucursal');
            $table->string('nombre',100);
            $table->string('clave',45);
            $table->string('correo',70);
            $table->string('telefono',70);
            $table->string('calle',100);
            $table->string('numero_interior',45);
            $table->string('numero_exterior',45);
            $table->string('colonia',100);
            $table->string('cp',5);
            $table->Integer('id_empresa')->unsigned()->nullable();
            $table->Integer('id_municipio')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empresa')->references('id_empresa')->on('empresa')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_municipio')->references('id_municipio')->on('municipio')->onDelete('set null')->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sucursal', function (Blueprint $table) {
            $table->dropForeign('sucursal_id_empresa_foreign');
            $table->dropForeign('sucursal_id_municipio_foreign');
        });
        Schema::drop('sucursal');
    }
}
