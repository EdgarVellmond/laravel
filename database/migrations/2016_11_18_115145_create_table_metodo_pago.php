<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMetodoPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metodo_pago', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_metodo_pago');
            $table->string('nombre',100);
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->enum('tipo_pago', ['ABONO', 'CARGO']);
            $table->enum('forma_pago', ['CREDITO', 'CONTADO']);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('metodo_pago');
    }
}
