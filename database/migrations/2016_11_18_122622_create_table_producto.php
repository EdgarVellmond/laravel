<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_producto');
            $table->string('nombre',100);
            $table->string('descripcion',200);
            $table->tinyInteger('activo_fijo');
            $table->integer('cantidad_presentacion');
            $table->string('sku',100);
            $table->enum('tipo_producto', ['ARTICULO', 'SERVICIO']);
            $table->Integer('id_sistema')->unsigned()->nullable();
            $table->enum('unidad_medida', ['PIEZA', 'CAJA','KILO'.'LITRO','ONZA','PAQUETE']);
            $table->Integer('id_sucursal')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_sistema')->references('id_sistema')->on('sistema')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_sucursal')->references('id_sucursal')->on('sucursal')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto', function (Blueprint $table) {

            $table->dropForeign('producto_id_sistema_foreign');
            $table->dropForeign('producto_id_sucursal_foreign');
        });
        Schema::drop('producto');
    }
}
