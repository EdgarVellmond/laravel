<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_empleado');
            $table->string('nombre',100);
            $table->string('apellido_paterno',100);
            $table->string('apellido_materno',100);
            $table->string('numero_empleado',45);
            $table->string('rfc',45);
            $table->string('curp',45);
            $table->string('nss',45);
            $table->string('telefono',45);
            $table->string('correo',100);
            $table->string('numero_licencia',45);
            $table->date('vigencia_licencia');
            $table->double('salario_base_diario');
            $table->double('salario_diario_integrado');
            $table->tinyInteger('viaticos');
            $table->string('calle',100);
            $table->string('colonia',100);
            $table->string('numero_interior',45);
            $table->string('numero_exterior',45);
            $table->string('cp',5);
            $table->Integer('dias_vacaciones')->unsigned()->nullable();
            $table->Integer('id_empleado_imagen')->unsigned()->nullable();
            $table->Integer('id_departamento')->unsigned()->nullable();
            $table->Integer('id_puesto')->unsigned()->nullable();
            $table->Integer('id_municipio')->unsigned()->nullable();
            $table->Integer('id_sucursal')->unsigned()->nullable();

            $table->Integer('id_patron')->unsigned()->nullable();
            $table->Integer('id_prima_riesgo')->unsigned()->nullable();

            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empleado_imagen')->references('id_empleado_imagen')->on('empleado_imagen')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_departamento')->references('id_departamento')->on('departamento')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_puesto')->references('id_puesto')->on('puesto')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_municipio')->references('id_municipio')->on('municipio')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_sucursal')->references('id_sucursal')->on('sucursal')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_patron')->references('id_empresa')->on('empresa')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_prima_riesgo')->references('id_prima_riesgo')->on('prima_riesgo')->onDelete('set null')->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empleado', function (Blueprint $table) {
            $table->dropForeign('empleado_id_empleado_imagen_foreign');
            $table->dropForeign('empleado_id_departamento_foreign');
            $table->dropForeign('empleado_id_puesto_foreign');
            $table->dropForeign('empleado_id_municipio_foreign');
            $table->dropForeign('empleado_id_sucursal_foreign');
            $table->dropForeign('empleado_id_patron_foreign');
            $table->dropForeign('empleado_id_prima_riesgo_foreign');
        });
        Schema::drop('empleado');
    }
}
