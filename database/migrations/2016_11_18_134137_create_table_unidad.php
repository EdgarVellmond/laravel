<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUnidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidad', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_unidad');
            $table->string('numero_economico',100);
            $table->string('marca',45);
            $table->string('modelo',45);
            $table->string('kilometraje',45);
            $table->enum('combustible', ['GASOLINA', 'DIESEL','GAS L.P.','ELECTRICO']);
            $table->Integer('id_sucursal')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_sucursal')->references('id_sucursal')->on('sucursal')->onDelete('set null')->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidad', function (Blueprint $table) {

            $table->dropForeign('unidad_id_sucursal_foreign');
        });
        Schema::drop('unidad');
    }
}
