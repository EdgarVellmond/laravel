<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlmacen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almacen', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_almacen');
            $table->string('nombre',100);
            $table->string('numero_almacen',100);
            $table->string('comentarios',200);
            $table->Integer('id_sucursal')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_sucursal')->references('id_sucursal')->on('sucursal')->onDelete('set null')->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('almacen', function (Blueprint $table) {
            $table->dropForeign('almacen_id_sucursal_foreign');
        });
        Schema::drop('almacen');
    }
}
