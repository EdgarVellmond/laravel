<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresaImagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresa_imagen', function (Blueprint $table) {
            $table->longText('tipo')->change();
            $table->renameColumn('tipo','ruta');
            $table->string("uuid");
            $table->string("extension");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa_imagen', function (Blueprint $table) {
            //
        });
    }
}
