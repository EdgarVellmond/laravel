<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoPlantilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_plantilla', function (Blueprint $table) {
            $table->bigIncrements('id_tipo_plantilla');
            $table->string("nombre");
            $table->bigInteger("id_usuario_creacion");
            $table->bigInteger("id_usuario_edicion");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_plantilla');
    }
}
