<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCuentasNominaSat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_nomina_sat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("cuenta");
            $table->string("nombre");
            $table->bigInteger("id_usuario_creacion");
            $table->bigInteger("id_usuario_edicion");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuentas_nomina_sat');
    }
}
