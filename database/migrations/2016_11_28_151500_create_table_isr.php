<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIsr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impuesto_isr', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_impuesto_isr');
            $table->double('limite_inferior');
            $table->double('limite_superior');
            $table->double('cuota_fija');
            $table->double('porcentaje');
            $table->enum('tipo_nomina', ['SEMANAL', 'DECENAL', 'QUINCENAL']);
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('impuesto_isr');
    }
}
