<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClaveGiroNomina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clave_giro_nomina', function (Blueprint $table) {
            $table->bigIncrements('id_clave_giro_nomina');
            $table->string("cuenta");
            $table->string("nombre");
            $table->enum('tipo_giro', ['PERCEPCION', 'DEDUCCION']);
            $table->tinyInteger('prestacion');
            $table->Integer("id_empresa")->unsigned()->nullable();
            $table->bigInteger("id_usuario_creacion");
            $table->bigInteger("id_usuario_edicion");
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empresa')->references('id_empresa')->on('empresa')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clave_giro_nomina', function (Blueprint $table) {
            $table->dropForeign('clave_giro_nomina_id_empresa_foreign');
        });
        Schema::drop('clave_giro_nomina');
    }
}
