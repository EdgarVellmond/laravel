<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlantilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantilla', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigIncrements('id_plantilla');
            $table->string('nombre');
            $table->bigInteger('id_tipo_plantilla')->unsigned()->nullable();
            $table->longText('contenido');
            $table->bigInteger('id_usuario_creacion');
            $table->bigInteger('id_usuario_edicion');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_tipo_plantilla')->references('id_tipo_plantilla')->on('tipo_plantilla')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plantilla', function (Blueprint $table) {
            $table->dropForeign('plantilla_id_tipo_plantilla_foreign');
        });
        Schema::drop('plantilla');
    }
}
