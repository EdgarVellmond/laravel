<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIncapacidadArchivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incapacidad_archivo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_incapacidad_archivo');
            $table->string('mime',45);
            $table->longText('ruta');
            $table->string('nombre',100);
            $table->string('uuid');
            $table->string('extension');
            $table->string('peso',45);
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incapacidad_archivo');
    }
}
