<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIncapacidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incapacidad', function (Blueprint $table) {
            $table->bigIncrements('id_incapacidad');
            $table->Integer("id_empleado")->unsigned()->nullable();
            $table->date("fecha_inicio");
            $table->date("fecha_fin");
            $table->integer("dias");
            $table->string("comentarios");
            $table->Integer("id_incapacidad_archivo")->unsigned()->nullable();
            $table->enum('tipo_incapacidad', ['MATERNIDAD', 'ENFERMEDAD_GENERAL', 'RIESGO_TRABAJO']);
            $table->bigInteger("id_usuario_creacion")->unsigned()->nullable();
            $table->bigInteger("id_usuario_edicion")->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empleado')->references('id_empleado')->on('empleado')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_incapacidad_archivo')->references('id_incapacidad_archivo')->on('incapacidad_archivo')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incapacidad', function (Blueprint $table) {
            $table->dropForeign('incapacidad_id_empleado_foreign');
            $table->dropForeign('incapacidad_id_incapacidad_archivo_foreign');
        });
        Schema::drop('incapacidad');
    }
}
