<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermisoAsistencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('permiso_asistencia', function (Blueprint $table) {
            $table->bigIncrements('id_permiso_asistencia');
            $table->Integer("id_empleado")->unsigned()->nullable();
            $table->date("fecha_inicio");
            $table->date("fecha_fin");
            $table->integer("dias");
            $table->string("comentarios");
            $table->Integer("id_permiso_asistencia_archivo")->unsigned()->nullable();
            $table->enum('goce_sueldo', ['SI', 'NO']);
            $table->bigInteger("id_usuario_creacion")->unsigned()->nullable();
            $table->bigInteger("id_usuario_edicion")->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empleado')->references('id_empleado')->on('empleado')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_permiso_asistencia_archivo')->references('id_permiso_asistencia_archivo')->on('permiso_asistencia_archivo')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permiso_asistencia', function (Blueprint $table) {
            $table->dropForeign('permiso_asistencia_id_empleado_foreign');
            $table->dropForeign('permiso_asistencia_id_permiso_asistencia_archivo_foreign');
        });
        Schema::drop('permiso_asistencia');
    }
}
