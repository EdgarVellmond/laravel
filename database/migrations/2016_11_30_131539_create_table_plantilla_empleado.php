<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContratoEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantilla_empleado', function (Blueprint $table) {
            $table->bigIncrements('id_plantilla_empleado');
            $table->integer('id_empleado')->unsigned()->nullable();
            $table->bigInteger('id_plantilla')->unsigned()->nullable();
            $table->longText('contenido');
            $table->datetime('fechaInicio');
            $table->datatime('fechaFin');
            $table->text('observaciones');
            $table->bigInteger('id_usuario_creacion');
            $table->bigInteger('id_usuario_edicion');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empleado')->references('id_empleado')->on('empleado')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_plantilla')->references('id_plantilla')->on('contrato')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plantilla_empleado', function (Blueprint $table) {
            $table->dropForeign('plantilla_empleado_id_empleado_foreign');
            $table->dropForeign('plantilla_empleado_id_plantilla_foreign');
        });
        Schema::drop('plantilla_empleado');
    }
}
