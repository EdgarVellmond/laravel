<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVacaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacaciones', function (Blueprint $table) {
            $table->bigIncrements('id_vacaciones');
            $table->Integer("id_empleado")->unsigned()->nullable();
            $table->date("fecha_inicio");
            $table->date("fecha_fin");
            $table->integer("dias");
            $table->string("comentarios");
            $table->Integer("id_vacaciones_archivo")->unsigned()->nullable();
            $table->bigInteger("id_usuario_creacion")->unsigned()->nullable();
            $table->bigInteger("id_usuario_edicion")->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empleado')->references('id_empleado')->on('empleado')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_vacaciones_archivo')->references('id_vacaciones_archivo')->on('vacaciones_archivo')->onDelete('set null')->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacaciones', function (Blueprint $table) {
            $table->dropForeign('vacaciones_id_empleado_foreign');
            $table->dropForeign('vacaciones_id_vacaciones_archivo_foreign');
        });
        Schema::drop('vacaciones');
    }
}
