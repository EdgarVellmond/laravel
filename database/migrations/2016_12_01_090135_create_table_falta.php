<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFalta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('falta', function (Blueprint $table) {

            $table->bigIncrements('id_falta');
            $table->Integer("id_empleado")->unsigned()->nullable();
            $table->date("fecha_inicio");
            $table->date("fecha_fin");
            $table->integer("dias");
            $table->string("comentarios");
            $table->Integer("id_falta_archivo")->unsigned()->nullable();
            $table->enum('estatus', ['ACTIVA', 'JUSTIFICADA']);
            $table->bigInteger("id_usuario_creacion")->unsigned()->nullable();
            $table->bigInteger("id_usuario_edicion")->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empleado')->references('id_empleado')->on('empleado')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_falta_archivo')->references('id_falta_archivo')->on('falta_archivo')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('falta', function (Blueprint $table) {
            $table->dropForeign('falta_id_empleado_foreign');
            $table->dropForeign('falta_id_falta_archivo_foreign');
        });
        Schema::drop('falta');
    }
}
