<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCuotaObreroPatronal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuota_obrero_patronal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_cuota_obrero_patronal');
            $table->string('ramo');
            $table->double('valor');
            $table->enum('tipo_cuota', ['EMPLEADO', 'PATRON']);
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuota_obrero_patronal');
    }
}
