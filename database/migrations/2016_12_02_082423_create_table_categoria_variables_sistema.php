<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoriaVariablesSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_variables_sistema', function (Blueprint $table) {
            $table->increments('id_categoria_variables_sistema');
            $table->string('nombre');
            $table->bigInteger('id_usuario_creacion');
            $table->bigInteger('id_usuario_edicion');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categoria_variables_sistema');
    }
}
