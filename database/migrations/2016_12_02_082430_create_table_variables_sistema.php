<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVariablesSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables_sistema', function (Blueprint $table) {
            $table->increments('id_variables_sistema');
            $table->integer('id_categoria_variables_sistema')->unsigned()->nullable();
            $table->string('nombre');
            $table->string('tabla');
            $table->string('alias_tabla');
            $table->string('campo');
            $table->bigInteger('id_usuario_creacion');
            $table->bigInteger('id_usuario_edicion');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_categoria_variables_sistema')->references('id_categoria_variables_sistema')->on('categoria_variables_sistema')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variables_sistema', function (Blueprint $table) {
            $table->dropForeign('variables_sistema_id_categoria_variables_sistema_foreign');
        });
        Schema::drop('variables_sistema');
    }
}
