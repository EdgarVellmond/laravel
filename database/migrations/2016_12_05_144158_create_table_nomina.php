<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNomina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomina', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_nomina');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->integer('periodo');
            $table->double('total_pagado');
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empresa')->references('id_empresa')->on('empresa')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nomina', function (Blueprint $table) {
            $table->dropForeign('nomina_id_empresa_foreign');
        });
        Schema::drop('nomina');
    }
}
