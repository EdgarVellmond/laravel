<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_documento');
            $table->string('tabla');
            $table->integer('id_propietario');
            $table->string('mime',45);
            $table->string('ruta');
            $table->string('nombre',100);
            $table->string('peso',45);
            $table->string('uuid');
            $table->string('extension');
            $table->string('descripcion');
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documento');
    }
}
