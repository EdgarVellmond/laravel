<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_tipo_solicitud');
            $table->string('nombre');
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_solicitud');
    }
}
