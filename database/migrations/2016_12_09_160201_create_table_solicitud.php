<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_solicitud');
            $table->integer('id_tipo_solicitud');
            $table->integer('asunto');
            $table->string('id_area');
            $table->string('observaciones');
            $table->string('estatus');
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_tipo_solicitud')->references('id_tipo_solicitud')->on('tipo_solicitud')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud', function (Blueprint $table) {
            $table->dropForeign('solicitud_id_tipo_solicitud_foreign');
        });
        Schema::drop('solicitud');
    }
}
