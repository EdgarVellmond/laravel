<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistorialSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_solicitud', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_historial_solicitud');
            $table->integer('id_solicitud');
            $table->string('accion');
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_solicitud')->references('id_solicitud')->on('solicitud')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historial_solicitud', function (Blueprint $table) {
            $table->dropForeign('historial_solicitud_id_solicitud_foreign');
        });
        Schema::drop('historial_solicitud');
    }
}
