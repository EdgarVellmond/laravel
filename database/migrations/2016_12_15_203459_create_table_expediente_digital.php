<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExpedienteDigital extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expediente_digital', function (Blueprint $table) {
            $table->increments('id_expediente_digital');
            $table->integer('id_categoria_expediente_digital');
            $table->string('nombre');
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_categoria_expediente_digital')->references('id_categoria_expediente_digital')->on('categoria_expediente_digital')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expediente_digital', function (Blueprint $table) {
            $table->dropForeign('expediente_digital_id_categoria_expediente_digital_foreign');
        });
        Schema::drop('expediente_digital');
    }
}
