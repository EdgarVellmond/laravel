<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExpedienteDigitalEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expediente_digital_empleado', function (Blueprint $table) {
            $table->increments('id_expediente_digital_empleado');
            $table->integer('id_expediente_digital');
            $table->integer('id_empleado');
            $table->integer('id_documento');
            $table->text('observaciones');
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_expediente_digital')->references('id_expediente_digital')->on('expediente_digital')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_empleado')->references('id_empleado')->on('empleado')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_documento')->references('id_documento')->on('documento')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expediente_digital_empleado', function (Blueprint $table) {
            $table->dropForeign('expediente_digital_empleado_id_expediente_digital_foreign');
            $table->dropForeign('expediente_digital_empleado_id_empleado_foreign');
            $table->dropForeign('expediente_digital_empleado_id_documento_foreign');
        });
        Schema::drop('expediente_digital_empleado');
    }
}
