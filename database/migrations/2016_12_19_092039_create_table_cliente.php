<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_cliente');
            $table->string('nombre_cliente',100);
            $table->string('apellido_paterno',100);
            $table->string('apellido_materno',100);
            $table->string('rfc',15);
            $table->tinyInteger('credito');
            $table->integer('dias_credito');
            $table->double('limite_credito');
            $table->string('telefono',45);
            $table->string('correo',100);
            $table->string('calle',100);
            $table->string('colonia',100);
            $table->string('numero_interior',45);
            $table->string('numero_exterior',45);
            $table->string('cp',5);
            $table->enum('tipo_persona', ['FISICA', 'MORAL']);
            $table->Integer('id_metodo_pago')->unsigned()->nullable();
            $table->Integer('id_banco')->unsigned()->nullable();
            $table->Integer('id_municipio')->unsigned()->nullable();
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_metodo_pago')->references('id_metodo_pago')->on('metodo_pago')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_banco')->references('id_banco')->on('banco')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_municipio')->references('id_municipio')->on('municipio')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cliente', function (Blueprint $table) {
            $table->dropForeign('cliente_id_metodo_pago_foreign');
            $table->dropForeign('cliente_id_banco_foreign');
            $table->dropForeign('cliente_id_municipio_foreign');
        });
        Schema::drop('cliente');
    }
}
