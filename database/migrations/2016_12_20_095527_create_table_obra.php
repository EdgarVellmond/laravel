<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableObra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obra', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_obra');
            $table->string('nombre',100);
            $table->string('folio',100);
            $table->integer('id_cliente')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->string('descripcion');
            $table->enum('prioridad', ['ALTA', 'MEDIA', 'BAJA']);
            $table->integer('id_municipio')->unsigned()->nullable();
            $table->string('localidad');
            $table->double('capital_minimo_contable');
            $table->date('fecha_inicio');
            $table->date('fecha_termino');
            $table->dateTime('fecha_limite_base');
            $table->string('observaciones');
            $table->enum('estatus', ['PENDIENTE', 'PROGRAMADA' , 'CANCELADA' , 'EN PROCESO', 'TERMINADA']);
            $table->bigInteger('id_usuario_creacion')->unsigned()->nullable();
            $table->bigInteger('id_usuario_edicion')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_cliente')->references('id_cliente')->on('cliente')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_empresa')->references('id_empresa')->on('empresa')->onDelete('set null')->onUpdate('set null');
            $table->foreign('id_municipio')->references('id_municipio')->on('municipio')->onDelete('set null')->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obra', function (Blueprint $table) {
            $table->dropForeign('obra_id_cliente_foreign');
            $table->dropForeign('obra_id_empresa_foreign');
            $table->dropForeign('obra_id_municipio_foreign');
        });
        Schema::drop('obra');
    }
}
