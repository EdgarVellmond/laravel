<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'nombre' => 'Administrador del Sistema',
            'usuario' => 'administrador',
            'correo' => 'administrador@localhost.com',
            'password' => bcrypt('1234'),
            'estatus' => 1
        ]);

        DB::table('permiso')->insert([
            'id_permiso' => 1,
            'nombre' => 'vista.sistema',
            'modulo' => 'Sistema',
            'descripcion' => 'Vista del menu del modulo de Sistema'
        ]);

        DB::table('permiso')->insert([
            'id_permiso' => 2,
            'nombre' => 'activar.sistema.control_usuarios',
            'modulo' => 'Sistema',
            'descripcion' => 'Permiso para activar usuarios en la vista de control de usuarios en el módulo de Sistemas'
        ]);

        DB::table('permiso')->insert([
            'id_permiso' => 3,
            'nombre' => 'alta.sistema.control_usuarios',
            'modulo' => 'Sistema',
            'descripcion' => 'Permiso para dar de alta en el modulo de sistemas, seccion de control de usuarios'
        ]);

        DB::table('permiso')->insert([
            'id_permiso' => 4,
            'nombre' => 'desactivar.sistema.control_usuarios',
            'modulo' => 'Sistema',
            'descripcion' => 'Permiso para desactivar usuarios en la vista de control de usuarios en el módulo de Sistemas'
        ]);

        DB::table('permiso')->insert([
            'id_permiso' => 5,
            'nombre' => 'edicion.sistema.control_usuarios',
            'modulo' => 'Sistema',
            'descripcion' => 'Permiso para editar un usuario en el modulo de Sistema en la seccion de control de usuarios'
        ]);

        DB::table('permiso')->insert([
            'id_permiso' => 6,
            'nombre' => 'vista.sistema.control_usuarios',
            'modulo' => 'Sistema',
            'descripcion' => 'Permiso de despliegue de la vista de control de usuarios en el modulo de Sistemas'
        ]);

        DB::table('usuario_permiso')->insert([
            'id_usuario_permiso' => 1,
            'id_permiso' => 1,
            'id_usuario' => 1
        ]);

        DB::table('usuario_permiso')->insert([
            'id_usuario_permiso' => 2,
            'id_permiso' => 2,
            'id_usuario' => 1
        ]);

        DB::table('usuario_permiso')->insert([
            'id_usuario_permiso' => 3,
            'id_permiso' => 3,
            'id_usuario' => 1
        ]);

        DB::table('usuario_permiso')->insert([
            'id_usuario_permiso' => 4,
            'id_permiso' => 4,
            'id_usuario' => 1
        ]);

        DB::table('usuario_permiso')->insert([
            'id_usuario_permiso' => 5,
            'id_permiso' => 5,
            'id_usuario' => 1
        ]);

        DB::table('usuario_permiso')->insert([
            'id_usuario_permiso' => 6,
            'id_permiso' => 6,
            'id_usuario' => 1
        ]);
    }
}
