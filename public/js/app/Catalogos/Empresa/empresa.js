$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();
    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    /**
     *Evento onShow para consultar la dirección de la Empresa y mostrar
     *en el modal ver-direccion
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver-direccion").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerDireccionEmpresa',
            data: {'id_empresa': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                $("#" + calle).val(response.calle);
                $("#" + numeroExterior).val(response.numero_exterior);
                $("#" + numeroInterior).val(response.numero_interior);
                $("#" + colonia).val(response.colonia);
                $("#" + cp).val(response.cp);
                $("#" + idMunicipio).val(response.id_municipio);
                $("#" + idEstado).val(response.id_estado);

            }
        });

    });

    /**
     *Evento onShow para mostrar la imagen presentada en la tabla en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver-imagen").on('show.bs.modal', function (e) {

        var src = $('#' + $(e.relatedTarget).data('id')).attr('src');
        $('#modal-' + idEmpresaImagen).attr('src', src);

    });

    /**
     *Evento onShow para el modal-form que permite agregar o editar la Empresa
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-form").on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0) {
            obtenerEmpresa(id);
        }

        $('#modal-form-' + idEmpresa).val(id);

    });

    /**
     *Evento onHide para reiniciar el modal-form
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-form").on('hide.bs.modal', function (e) {
        reiniciarModalForm();
    });

    /**
     *Evento change para consultar los municipios del estado seleccionado
     *@author Erik Villarreal
     *@return void
     */
    $("#modal-form-" + idEstado).change(function () {
        llenarSelectMunicipios($("#modal-form-" + idMunicipio + "_temporal").val());
    });

    /**
     *Evento change para mandar id de la empresa a borrar al modal
     *@author Erik Villarreal
     *@return void
     */
    $("#modal-eliminar").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        $('#modal-eliminar-' + idEmpresa).val(id);

    });

    //Agregar campo default
    agregarDefaultEstado("modal-form-" + idEstado);
    agregarDefaultEstado("modal-form-" + idMunicipio);
    agregarDefaultEstado("modal-form-" + tipoNomina);

});

/**
 * Metodo que sirve para agregar un Option default al Combo de los Estados
 * @author Oscar Vargas
 * @return void
 */
function agregarDefaultEstado(combo) {
    var select = document.getElementById(combo);
    var opcion;

    opcion = document.createElement("OPTION");
    opcion.text = "Seleccionar";
    opcion.value = 0;
    select.add(opcion);

    $('#' + combo).val(0).change();
}

/**
 * Metodo que sirve para llenar el select2 de municipios
 * @author Oscar Vargas
 * @return void
 */
function llenarSelectMunicipios(id) {

    $('#modal-form-' + idMunicipio).empty().trigger('change');

    $.ajax({
        url: 'obtenerMunicipios',
        data: {id_estado: $('#modal-form-' + idEstado).val(), '_token': token},
        type: 'POST',
        dataType: 'json',
        success: function (json) {

            var select = document.getElementById("modal-form-" + idMunicipio);
            var opcion;

            $.each(json, function (key) {
                opcion = document.createElement("OPTION");
                opcion.text = json[key];
                opcion.value = key;
                select.add(opcion);
            });

            if (id != "") {
                $("#modal-form-" + idMunicipio).val(id).change();
            }
        }
    });

}

/**
 * Metodo que sirve para obtener la información de la empresa
 * @author Oscar Vargas
 * @return void
 */
function obtenerEmpresa(id) {

    $.ajax({
        url: 'obtenerEmpresa',
        data: {id_empresa: id, '_token': token},
        type: 'POST',
        dataType: 'json',
        success: function (json) {

            response = json[0];
            $("#modal-form-" + clave).val(response.clave);
            $("#modal-form-" + rfc).val(response.rfc);
            $("#modal-form-" + correo).val(response.correo);
            $("#modal-form-" + razonSocial).val(response.razon_social);
            $("#modal-form-" + calle).val(response.calle);
            $("#modal-form-" + numeroExterior).val(response.numero_exterior);
            $("#modal-form-" + numeroInterior).val(response.numero_interior);
            $("#modal-form-" + colonia).val(response.colonia);
            $("#modal-form-" + cp).val(response.cp);
            $("#modal-form-" + idMunicipio + "_temporal").val(response.id_municipio).change();
            $("#modal-form-" + idEstado).val(response.id_estado).change();
            $("#modal-form-" + tipoNomina).val(response.tipo_nomina).change();
            if (response.patron == 1) {
                document.getElementById("modal-form-" + patron).checked = true;
            } else {
                document.getElementById("modal-form-" + patron).checked = false;
            }
            if (response.operacion == 1) {
                document.getElementById("modal-form-" + operacion).checked = true;
            } else {
                document.getElementById("modal-form-" + operacion).checked = false;
            }
            var src = urlBase + response.ruta + response.uuid + '.' + response.extension;
            $('#modal-form-' + idEmpresaImagen).attr('src', src);

        }
    });

}

/**
 *Función que reinicia el modal-form
 *@author Erik Villarreal
 *@return void
 */
function reiniciarModalForm() {
    $("#modal-form-" + clave).val("");
    $("#modal-form-" + rfc).val("");
    $("#modal-form-" + correo).val("");
    $("#modal-form-" + razonSocial).val("");
    $("#modal-form-" + calle).val("");
    $("#modal-form-" + numeroExterior).val("");
    $("#modal-form-" + numeroInterior).val("");
    $("#modal-form-" + colonia).val("");
    $("#modal-form-" + cp).val("");
    $("#modal-form-" + idEstado).val(0).change();
    $("#modal-form-" + tipoNomina).val(0).change();
    document.getElementById("modal-form-" + patron).checked = false;
    document.getElementById("modal-form-" + operacion).checked = false;
    var src = "";
    $('#modal-form-' + idEmpresaImagen).attr('src', "");
}

/**
 *Evento change para el Preview de la imagen
 *@author Erik Villarreal
 *@param  input #elemento HTML
 *@return void
 */
function verImagenAnetsSubir(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#modal-form-' + idEmpresaImagen).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/**
 * Metodo que sirve para eliminar un proveedor por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarEmpresa() {

    $.ajax({
        url: 'eliminarEmpresa',
        data: {
            id_empresa: $('#modal-eliminar-' + idEmpresa).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para validar que no sea 0 el valor de los Combos y nula la imagen
 * @author Erik Villarreal
 * @returns {boolean}
 */
function validarCombos() {

    if (document.getElementById("modal-form-" + patron).checked == false && document.getElementById("modal-form-" + operacion).checked == false) {
        alert("Debe elegir si es Patrón y/o Operación");
        return false;
    }

    if ($("#modal-form-" + idEstado).val() == "" || $("#modal-form-" + idEstado).val() == 0 ||
        $("#modal-form-" + idMunicipio).val() == "" || $("#modal-form-" + idMunicipio).val() == 0) {

        alert("No has seleccionado un Estado o Municipio");

    } else if ($("#modal-file-" + idEmpresaImagen).val() == "" && ($('#modal-form-' + idEmpresa).val() == "" || $('#modal-form-' + idEmpresa).val() == 0)) {

        alert("No has seleccionado una Imagen");

    } else {
        return true;
    }
    return false;
}
