/**
 * Created by Oscar Vargas on 24/11/2016.
 */



$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    inicializarSelect2();

    llenarSelectMunicipios("");

    /**
     * Funcion que se ejecuta cuando cambia el select de estado
     */
    $('#id_estado').change(function () {

        llenarSelectMunicipios("");

    });

    /**
     * Metodo que sirve para llenar el select2 de municipios
     * @author Oscar Vargas
     * @return void
     */
    function llenarSelectMunicipios(a) {

        $('#model-nuevo_proveedor .modal-body #id_municipio').empty().trigger('change');

        $.ajax({
            url: 'obtenerMunicipios',
            data: {id_estado: $('#id_estado').val(), '_token': token},
            type: 'POST',
            // el tipo de información que se espera de respuesta
            dataType: 'json',
            success: function (json) {

                var select = document.getElementById('id_municipio');
                var opcion;

                $.each(json, function (key) {
                    opcion = document.createElement("OPTION");
                    opcion.text = json[key];
                    opcion.value = key;
                    select.add(opcion);
                });

                if (a != "") {

                    $('#model-nuevo_proveedor .modal-body #id_municipio').val(a).change();
                }
            }
        });
    }


    /**
     * Metodo que sirve para iniciar el select2
     * @author Oscar Vargas
     * @return void
     */
    function inicializarSelect2() {

        $('.select2').select2({
            width: '100%',
            class: 'form-control'
        });

    }

    /**
     * Metodo que se ejecuta al abrirse el modal de ver_domicilio para cargar su informacion
     * @author Oscar Vargas
     * @return void
     */
    $('#model-ver_direccion').on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerDomicilioProveedor',
            data: {id_proveedor: $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            // el tipo de información que se espera de respuesta
            //dataType : 'json',
            success: function (json) {
                $('#model-ver_direccion .modal-body #calle').val(json[0].calle);
                $('#model-ver_direccion .modal-body #colonia').val(json[0].colonia);
                $('#model-ver_direccion .modal-body #numero_exterior').val(json[0].numero_exterior);
                $('#model-ver_direccion .modal-body #numero_interior').val(json[0].numero_interior);
                $('#model-ver_direccion .modal-body #cp').val(json[0].cp);
                $('#model-ver_direccion .modal-body #id_municipio').val(json[0].nombreMunicipio);
                $('#model-ver_direccion .modal-body #id_estado').val(json[0].nombreEstado);


            }
        });

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de ver_credito para cargar su informacion
     * @author Oscar Vargas
     * @return void
     */
    $('#model-ver_credito').on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerCreditoProveedor',
            data: {
                id_proveedor: $(e.relatedTarget).data('id_proveedor_credito'),
                '_token': token
            },
            type: 'POST',
            success: function (json) {

                if (json[0].credito == 0) {
                    $('#model-ver_credito .modal-body #credito').val("No");
                } else {
                    $('#model-ver_credito .modal-body #credito').val("Si");
                }

                $('#model-ver_credito .modal-body #dias_credito').val(json[0].dias_credito);
                $('#model-ver_credito .modal-body #limite_credito').val(json[0].limite_credito);
                $('#model-ver_credito .modal-body #id_metodo_pago').val(json[0].nombreMetodo);
                $('#model-ver_credito .modal-body #id_banco').val(json[0].nombreBanco);

            }
        });

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar_proveedor para poner el id principal del proveedor
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar_proveedor').on('show.bs.modal', function (e) {

        $('#model-eliminar_proveedor .modal-body #id_proveedor').val($(e.relatedTarget).data('id_eliminar_proveedor'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar proveedor para poner el id principal del proveedor
     * @author Oscar Vargas
     * @return void
     */
    $('#model-nuevo_proveedor').on('show.bs.modal', function (e) {

        $('#model-nuevo_proveedor .modal-body #id_proveedor').val($(e.relatedTarget).data('id_editar_proveedor'));

        if ($('#model-nuevo_proveedor .modal-body #id_proveedor').val() != "") {

            $.ajax({
                url: 'obtenerProveedor',
                data: {
                    id_proveedor: $('#model-nuevo_proveedor .modal-body #id_proveedor').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-nuevo_proveedor .modal-body #nombre_proveedor').val(json[0].nombre_proveedor);
                    $('#model-nuevo_proveedor .modal-body #apellido_paterno').val(json[0].apellido_paterno);
                    $('#model-nuevo_proveedor .modal-body #apellido_materno').val(json[0].apellido_materno);
                    $('#model-nuevo_proveedor .modal-body #rfc').val(json[0].rfc);
                    $('#model-nuevo_proveedor .modal-body #tipo_persona').val(json[0].tipo_persona).change();
                    $('#model-nuevo_proveedor .modal-body #telefono').val(json[0].telefono);
                    $('#model-nuevo_proveedor .modal-body #correo').val(json[0].correo);
                    $('#model-nuevo_proveedor .modal-body #calle').val(json[0].calle);
                    $('#model-nuevo_proveedor .modal-body #colonia').val(json[0].colonia);
                    $('#model-nuevo_proveedor .modal-body #numero_interior').val(json[0].numero_interior);
                    $('#model-nuevo_proveedor .modal-body #numero_exterior').val(json[0].numero_exterior);
                    $('#model-nuevo_proveedor .modal-body #cp').val(json[0].cp);
                    $('#model-nuevo_proveedor .modal-body #credito').val(json[0].credito);
                    $('#model-nuevo_proveedor .modal-body #dias_credito').val(json[0].dias_credito);
                    $('#model-nuevo_proveedor .modal-body #limite_credito').val(json[0].limite_credito);
                    $('#model-nuevo_proveedor .modal-body #id_banco').val(json[0].id_banco).change();
                    $('#model-nuevo_proveedor .modal-body #id_metodo_pago').val(json[0].id_metodo_pago).change();
                    $('#model-nuevo_proveedor .modal-body #id_estado').val(json[0].id_estado).change();
                    llenarSelectMunicipios(json[0].id_municipio);

                }
            });

        }

    });


});// final de document.ready

/**
 * Metodo que sirve para guardar un proveedor por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarProveedor() {

    $.ajax({
        url: 'guardarProveedor',
        data: $('#add_form').serialize(),
        type: 'POST',
        // el tipo de información que se espera de respuesta
        //dataType : 'json',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un proveedor por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarProveedor() {

    $.ajax({
        url: 'eliminarProveedor',
        data: {
            id_proveedor: $('#model-eliminar_proveedor .modal-body #id_proveedor').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}