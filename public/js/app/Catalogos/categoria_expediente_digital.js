/**
 * Created by Erik Villarreal at 15/12/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     *Evento onShow para el modal-form que permite agregar o editar una categoria de expediente digital
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerCategoriaExpedienteDigital(id);
        }

        $('#modal-form-' + idCategoriaExpedienteDigital).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del categoria de expediente digital
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idCategoriaExpedienteDigital).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreCategoriaExpedienteDigital).val("");
        $('#modal-form-' + idCategoriaExpedienteDigital).val("");

    });

});// final document.ready

/**
 * Metodo que sirve para obtener la categoria de expediente digital
 * @author Erik Villarreal
 * @return void
 */
function obtenerCategoriaExpedienteDigital(id) {
    $.ajax({
        url: 'obtenerCategoriaExpedienteDigitalEditar',
        data: {
            id_categoria_expediente_digital: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreCategoriaExpedienteDigital).val(response.nombre);

        }
    });
}

/**
 * Metodo que sirve para guardar una categoria de expediente digital por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarCategoriaExpedienteDigital() {

    $.ajax({
        url: 'guardarCategoriaExpedienteDigital',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una categoria de expediente digital por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarCategoriaExpedienteDigital() {

    $.ajax({
        url: 'eliminarCategoriaExpedienteDigital',
        data: {
            id_categoria_expediente_digital: $('#modal-eliminar-' + idCategoriaExpedienteDigital).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}