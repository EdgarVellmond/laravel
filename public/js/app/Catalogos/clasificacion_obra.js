/**
 * Created by Erik Villarreal at 19/12/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     *Evento onShow para el modal-form que permite agregar o editar una clasificación de obra
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerClasificacionObra(id);
        }

        $('#modal-form-' + idClasificacionObra).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la clasificación de obra
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idClasificacionObra).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreClasificacionObra).val("");
        $('#modal-form-' + idClasificacionObra).val("");

    });

});// final document.ready

/**
 * Metodo que sirve para obtener la clasificación de obra
 * @author Erik Villarreal
 * @return void
 */
function obtenerClasificacionObra(id) {
    $.ajax({
        url: 'obtenerClasificacionObraEditar',
        data: {
            id_clasificacion_obra: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreClasificacionObra).val(response.nombre);

        }
    });
}

/**
 * Metodo que sirve para guardar una clasificación de obra por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarClasificacionObra() {

    $.ajax({
        url: 'guardarClasificacionObra',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una clasificación de obra por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarClasificacionObra() {

    $.ajax({
        url: 'eliminarClasificacionObra',
        data: {
            id_clasificacion_obra: $('#modal-eliminar-' + idClasificacionObra).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}