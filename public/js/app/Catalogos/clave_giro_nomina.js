/**
 * Created by Erik Villarreal at 28/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     *Evento onShow para el modal-form que permite agregar o editar una clave giro nómina
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerClaveGiroNomina(id);
        }

        $('#modal-form-' + idClaveGiroNomina).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la clave giro nómina
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idClaveGiroNomina).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + cuentaClaveGiroNomina).val("");
        $('#modal-form-' + nombreClaveGiroNomina).val("");
        $('#modal-form-' + idClaveGiroNomina).val("");
        $('#modal-form-' + empresaClaveGiroNomina).val(0);

    });

});// final document.ready

/**
 * Metodo que sirve para obtener la clave giro nómina
 * @author Erik Villarreal
 * @return void
 */
function obtenerClaveGiroNomina(id) {
    $.ajax({
        url: 'obtenerClaveGiroNominaEditar',
        data: {
            id_clave_giro_nomina: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + cuentaClaveGiroNomina).val(response.cuenta);
            $('#modal-form-' + nombreClaveGiroNomina).val(response.nombre);
            $('#modal-form-' + tipoClaveGiroNomina).val(response.tipo_giro);
            $('#modal-form-' + empresaClaveGiroNomina).val(response.id_empresa);
            if (response.prestacion == 1) {
                document.getElementById("modal-form-" + prestacionClaveGiroNomina).checked = true;
            } else {
                document.getElementById("modal-form-" + prestacionClaveGiroNomina).checked = false;
            }


        }
    });
}

/**
 * Metodo que sirve para guardar una clave giro nómina por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarClaveGiroNomina() {

    $.ajax({
        url: 'guardarClaveGiroNomina',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una clave giro nómina por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarClaveGiroNomina() {

    $.ajax({
        url: 'eliminarClaveGiroNomina',
        data: {
            id_clave_giro_nomina: $('#modal-eliminar-' + idClaveGiroNomina).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}