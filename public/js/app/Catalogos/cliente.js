/**
 * Created by Oscar Vargas on 24/11/2016.
 */



$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    inicializarSelect2();

    llenarSelectMunicipios("");

    /**
     * Funcion que se ejecuta cuando cambia el select de estado
     */
    $('#id_estado').change(function () {

        llenarSelectMunicipios("");

    });

    /**
     * Metodo que sirve para llenar el select2 de municipios
     * @author Oscar Vargas
     * @return void
     */
    function llenarSelectMunicipios(a) {

        $('#model-nuevo_cliente .modal-body #id_municipio').empty().trigger('change');

        $.ajax({
            url: 'obtenerMunicipios',
            data: {id_estado: $('#id_estado').val(), '_token': token},
            type: 'POST',
            // el tipo de información que se espera de respuesta
            dataType: 'json',
            success: function (json) {

                var select = document.getElementById('id_municipio');
                var opcion;

                $.each(json, function (key) {
                    opcion = document.createElement("OPTION");
                    opcion.text = json[key];
                    opcion.value = key;
                    select.add(opcion);
                });

                if (a != "") {

                    $('#model-nuevo_cliente .modal-body #id_municipio').val(a).change();
                }
            }
        });
    }


    /**
     * Metodo que sirve para iniciar el select2
     * @author Oscar Vargas
     * @return void
     */
    function inicializarSelect2() {

        $('.select2').select2({
            width: '100%',
            class: 'form-control'
        });

    }

    /**
     * Metodo que se ejecuta al abrirse el modal de ver_domicilio para cargar su informacion
     * @author Oscar Vargas
     * @return void
     */
    $('#model-ver_direccion').on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerDomicilioCliente',
            data: {id_cliente: $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            // el tipo de información que se espera de respuesta
            //dataType : 'json',
            success: function (json) {
                $('#model-ver_direccion .modal-body #calle').val(json[0].calle);
                $('#model-ver_direccion .modal-body #colonia').val(json[0].colonia);
                $('#model-ver_direccion .modal-body #numero_exterior').val(json[0].numero_exterior);
                $('#model-ver_direccion .modal-body #numero_interior').val(json[0].numero_interior);
                $('#model-ver_direccion .modal-body #cp').val(json[0].cp);
                $('#model-ver_direccion .modal-body #id_municipio').val(json[0].nombreMunicipio);
                $('#model-ver_direccion .modal-body #id_estado').val(json[0].nombreEstado);


            }
        });

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de ver_credito para cargar su informacion
     * @author Oscar Vargas
     * @return void
     */
    $('#model-ver_credito').on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerCreditoCliente',
            data: {
                id_cliente: $(e.relatedTarget).data('id_cliente_credito'),
                '_token': token
            },
            type: 'POST',
            success: function (json) {

                if (json[0].credito == 0) {
                    $('#model-ver_credito .modal-body #credito').val("No");
                } else {
                    $('#model-ver_credito .modal-body #credito').val("Si");
                }

                $('#model-ver_credito .modal-body #dias_credito').val(json[0].dias_credito);
                $('#model-ver_credito .modal-body #limite_credito').val(json[0].limite_credito);
                $('#model-ver_credito .modal-body #id_metodo_pago').val(json[0].nombreMetodo);
                $('#model-ver_credito .modal-body #id_banco').val(json[0].nombreBanco);

            }
        });

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar_proveedor para poner el id principal del cliente
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar_cliente').on('show.bs.modal', function (e) {

        $('#model-eliminar_cliente .modal-body #id_cliente').val($(e.relatedTarget).data('id_eliminar_cliente'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar cliente para poner el id principal del cliente
     * @author Oscar Vargas
     * @return void
     */
    $('#model-nuevo_cliente').on('show.bs.modal', function (e) {

        $('#model-nuevo_cliente .modal-body #id_cliente').val($(e.relatedTarget).data('id_editar_cliente'));

        if ($('#model-nuevo_cliente .modal-body #id_cliente').val() != "") {

            $.ajax({
                url: 'obtenerCliente',
                data: {
                    id_cliente: $('#model-nuevo_cliente .modal-body #id_cliente').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-nuevo_cliente .modal-body #nombre_proveedor').val(json[0].nombre_proveedor);
                    $('#model-nuevo_cliente .modal-body #apellido_paterno').val(json[0].apellido_paterno);
                    $('#model-nuevo_cliente .modal-body #apellido_materno').val(json[0].apellido_materno);
                    $('#model-nuevo_cliente .modal-body #rfc').val(json[0].rfc);
                    $('#model-nuevo_cliente .modal-body #tipo_persona').val(json[0].tipo_persona).change();
                    $('#model-nuevo_cliente .modal-body #telefono').val(json[0].telefono);
                    $('#model-nuevo_cliente .modal-body #correo').val(json[0].correo);
                    $('#model-nuevo_cliente .modal-body #calle').val(json[0].calle);
                    $('#model-nuevo_cliente .modal-body #colonia').val(json[0].colonia);
                    $('#model-nuevo_cliente .modal-body #numero_interior').val(json[0].numero_interior);
                    $('#model-nuevo_cliente .modal-body #numero_exterior').val(json[0].numero_exterior);
                    $('#model-nuevo_cliente .modal-body #cp').val(json[0].cp);
                    $('#model-nuevo_cliente .modal-body #credito').val(json[0].credito);
                    $('#model-nuevo_cliente .modal-body #dias_credito').val(json[0].dias_credito);
                    $('#model-nuevo_cliente .modal-body #limite_credito').val(json[0].limite_credito);
                    $('#model-nuevo_cliente .modal-body #id_banco').val(json[0].id_banco).change();
                    $('#model-nuevo_cliente .modal-body #id_metodo_pago').val(json[0].id_metodo_pago).change();
                    $('#model-nuevo_cliente .modal-body #id_estado').val(json[0].id_estado).change();
                    llenarSelectMunicipios(json[0].id_municipio);

                }
            });

        }

    });


});// final de document.ready

/**
 * Metodo que sirve para guardar un cliente por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarCliente() {

    $.ajax({
        url: 'guardarCliente',
        data: $('#add_form').serialize(),
        type: 'POST',
        // el tipo de información que se espera de respuesta
        //dataType : 'json',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un cliente por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarCliente() {

    $.ajax({
        url: 'eliminarCliente',
        data: {
            id_cliente: $('#model-eliminar_cliente .modal-body #id_cliente').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}