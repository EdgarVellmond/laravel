/**
 * Created by Erik Villarreal at 28/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     *Evento onShow para el modal-form que permite agregar o editar una cuenta nómina sat
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerCuentasNominaSat(id);
        }

        $('#modal-form-' + idCuentasNominaSat).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la cuenta nómina sat
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idCuentasNominaSat).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + cuentaCuentasNominaSat).val("");
        $('#modal-form-' + nombreCuentasNominaSat).val("");
        $('#modal-form-' + idCuentasNominaSat).val("");

    });

});// final document.ready

/**
 * Metodo que sirve para obtener la cuenta nómina sat
 * @author Erik Villarreal
 * @return void
 */
function obtenerCuentasNominaSat(id) {
    $.ajax({
        url: 'obtenerCuentasNominaSatEditar',
        data: {
            id_cuentas_nomina_sat: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + cuentaCuentasNominaSat).val(response.cuenta);
            $('#modal-form-' + nombreCuentasNominaSat).val(response.nombre);

        }
    });
}

/**
 * Metodo que sirve para guardar una cuenta nómina sat por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarCuentasNominaSat() {

    $.ajax({
        url: 'guardarCuentasNominaSat',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una cuenta nómina sat por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarCuentasNominaSat() {

    $.ajax({
        url: 'eliminarCuentasNominaSat',
        data: {
            id_cuentas_nomina_sat: $('#modal-eliminar-' + idCuentasNominaSat).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}