/**
 * Created by Oscar Vargas on 01/12/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     * Metodo que se ejecuta al abrirse el modal de guardar cuota para poner el id principal de la cuota
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_cuota_obrero_patronal').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_cuota_obrero_patronal').val() != "") {
            $.ajax({
                url: 'obtenerCuotaObreroPatronal',
                data: {
                    id_cuota_obrero_patronal: $('#model-guardar .modal-body #id_cuota_obrero_patronal').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #ramo').val(json.ramo);
                    $('#model-guardar .modal-body #valor').val(json.valor);
                    $('#model-guardar .modal-body #tipo_cuota').val(json.tipo_cuota);

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la cuota
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_cuota_obrero_patronal').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_cuota_obrero_patronal').val("");
        $('#model-guardar .modal-body #ramo').val("");
        $('#model-guardar .modal-body #valor').val("");

    });

});// final document.ready


/**
 * Metodo que sirve para guardar una cuota por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarCuotaObreroPatronal() {

    $.ajax({
        url: 'guardarCuotaObreroPatronal',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una cuota por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarCuotaObreroPatronal() {

    $.ajax({
        url: 'eliminarCuotaObreroPatronal',
        data: {
            id_cuota_obrero_patronal: $('#model-eliminar .modal-body #id_cuota_obrero_patronal').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}