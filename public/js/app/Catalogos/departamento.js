/**
 * Created by Erik Villarreal at 27/11/2016.
 */


$(document).ready(function () {

    $(".select2").select2({
        width: "100%"
    });

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     *Evento onShow para el modal-form que permite agregar o editar un Departamento
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerDepartamento(id);
        }

        $('#modal-form-' + idDepartamento).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del Departamento
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idDepartamento).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreDepartamento).val("");
        $('#modal-form-' + idDepartamento).val("");
        $('#modal-form-' + idArea).val(0).change();

    });

    agregarDefaultEstado("modal-form-" + idArea);

});// final document.ready

/**
 * Metodo que sirve para agregar un Option default al Combo de los Estados
 * @author Oscar Vargas
 * @return void
 */
function agregarDefaultEstado(combo) {
    var select = document.getElementById(combo);
    var opcion;

    opcion = document.createElement("OPTION");
    opcion.text = "Seleccionar";
    opcion.value = 0;
    select.add(opcion);

    $('#' + combo).val(0).change();
}

/**
 * Metodo que sirve para obtener el Departamento
 * @author Erik Villarreal
 * @return void
 */
function obtenerDepartamento(id) {
    $.ajax({
        url: 'obtenerDepartamentoEditar',
        data: {
            id_departamento: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreDepartamento).val(response.nombre);
            $('#modal-form-' + idArea).val(response.id_area).change();

        }
    });
}

/**
 * Metodo que sirve para guardar un Departamento por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarDepartamento() {

    if ($('#modal-form-' + idArea).val() != 0 && $('#modal-form-' + idArea).val() != "") {

        $.ajax({
            url: 'guardarDepartamento',
            data: $('#add_edit_form').serialize(),
            type: 'POST',
            success: function (message) {
                $('#modal-alert-message-success').modal('show');
                $("#success-message").html(message);
                location.reload();
            }, error: function (message) {
                $('#modal-alert-message-error').modal('show');
                $("#error-message").html(message);
            }
        });

    } else {
        alert("No has seleccionado el Área");
    }
    return false;
}

/**
 * Metodo que sirve para eliminar un Departamento por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarDepartamento() {

    $.ajax({
        url: 'eliminarDepartamento',
        data: {
            id_departamento: $('#modal-eliminar-' + idDepartamento).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}