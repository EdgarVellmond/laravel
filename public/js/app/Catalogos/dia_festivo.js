/**
 * Created by Erik Villarreal at 30/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    /**
    *Evento onShow para el modal-form que permite agregar o editar un dia festivo
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if(id != "" && id != 0 && id != undefined){
            obtenerDiaFestivo(id);
        }

         $('#modal-form-' + idDiaFestivo).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del dia festivo
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idDiaFestivo).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreDiaFestivo).val("");
        $('#modal-form-' + idDiaFestivo).val("");
        $('#modal-form-' + fechaDiaFestivo).val("");

    });

});// final document.ready

/**
* Metodo que sirve para obtener el dia festivo
* @author Erik Villarreal
* @return void
*/
function obtenerDiaFestivo(id){
    $.ajax({
        url: 'obtenerDiaFestivoEditar',
        data: {
            id_dia_festivo: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreDiaFestivo).val(response.nombre);
            $('#modal-form-' + fechaDiaFestivo).val(response.fecha);

        }
    });
}

/**
 * Metodo que sirve para guardar un dia festivo por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarDiaFestivo() {

    $.ajax({
        url: 'guardarDiaFestivo',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un dia festivo por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarDiaFestivo() {

    $.ajax({
        url: 'eliminarDiaFestivo',
        data: {
            id_dia_festivo: $('#modal-eliminar-' + idDiaFestivo).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}