/**
 * Created by Oscar Vargas on 25/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     * Metodo que se ejecuta al abrirse el modal de guardar estado para poner el id principal de la estado
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_estado').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_estado').val() != "") {
            $.ajax({
                url: 'obtenerEstado',
                data: {
                    id_estado: $('#model-guardar .modal-body #id_estado').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #nombre').val(json.nombre);

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del estado
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_estado').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_estado').val("");
        $('#model-guardar .modal-body #nombre').val("");

    });

});// final document.ready


/**
 * Metodo que sirve para guardar un estado por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarEstado() {

    $.ajax({
        url: 'guardarEstado',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un estado por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarEstado() {

    $.ajax({
        url: 'eliminarEstado',
        data: {
            id_estado: $('#model-eliminar .modal-body #id_estado').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}