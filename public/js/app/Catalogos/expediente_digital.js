/**
 * Created by Erik Villarreal at 15/12/2016.
 */


$(document).ready(function () {

    $(".select2").select2({
        width: "100%"
    });

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     *Evento onShow para el modal-form que permite agregar o editar un Expediente Digital
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerExpedienteDigital(id);
        }

        $('#modal-form-' + idExpedienteDigital).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del Expediente Digital
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idExpedienteDigital).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreExpedienteDigital).val("");
        $('#modal-form-' + idExpedienteDigital).val("");
        $('#modal-form-' + idCategoriaExpedienteDigital).val(0).change();

    });

    agregarDefaultCategoria("modal-form-" + idCategoriaExpedienteDigital);

});// final document.ready

/**
 * Metodo que sirve para agregar un Option default al Combo de las Categorías
 * @author Oscar Vargas
 * @return void
 */
function agregarDefaultCategoria(combo) {
    var select = document.getElementById(combo);
    var opcion;

    opcion = document.createElement("OPTION");
    opcion.text = "Seleccionar";
    opcion.value = 0;
    select.add(opcion);

    $('#' + combo).val(0).change();
}

/**
 * Metodo que sirve para obtener el Expediente Digital
 * @author Erik Villarreal
 * @return void
 */
function obtenerExpedienteDigital(id) {
    $.ajax({
        url: 'obtenerExpedienteDigitalEditar',
        data: {
            id_expediente_digital: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreExpedienteDigital).val(response.nombre);
            $('#modal-form-' + idCategoriaExpedienteDigital).val(response.id_categoria_expediente_digital).change();

        }
    });
}

/**
 * Metodo que sirve para guardar un Expediente Digital por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarExpedienteDigital() {

    if ($('#modal-form-' + idCategoriaExpedienteDigital).val() != 0 && $('#modal-form-' + idCategoriaExpedienteDigital).val() != "") {

        $.ajax({
            url: 'guardarExpedienteDigital',
            data: $('#add_edit_form').serialize(),
            type: 'POST',
            success: function (message) {
                $('#modal-alert-message-success').modal('show');
                $("#success-message").html(message);
                location.reload();
            }, error: function (message) {
                $('#modal-alert-message-error').modal('show');
                $("#error-message").html(message);
            }
        });

    } else {
        alert("No has seleccionado la Categoría");
    }
    return false;
}

/**
 * Metodo que sirve para eliminar un Expediente Digital por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarExpedienteDigital() {

    $.ajax({
        url: 'eliminarExpedienteDigital',
        data: {
            id_expediente_digital: $('#modal-eliminar-' + idExpedienteDigital).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}