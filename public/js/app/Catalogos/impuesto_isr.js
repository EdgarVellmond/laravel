/**
 * Created by Oscar Vargas on 28/11/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar impuesto isr para poner el id principal del impuesto isr
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_impuesto_isr').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_impuesto_isr').val() != "") {
            $.ajax({
                url: 'obtenerImpuestoIsr',
                data: {
                    id_impuesto_isr: $('#model-guardar .modal-body #id_impuesto_isr').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #limite_inferior').val(json.limite_inferior);
                    $('#model-guardar .modal-body #limite_superior').val(json.limite_superior);
                    $('#model-guardar .modal-body #cuota_fija').val(json.cuota_fija);
                    $('#model-guardar .modal-body #porcentaje').val(json.porcentaje);
                    $('#model-guardar .modal-body #tipo_nomina').val(json.tipo_nomina);

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del impuesto isr
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_impuesto_isr').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_impuesto_isr').val("");
        $('#model-guardar .modal-body #limite_inferior').val("");
        $('#model-guardar .modal-body #limite_superior').val("");
        $('#model-guardar .modal-body #porcentaje').val("");
        $('#model-guardar .modal-body #cuota_fija').val("");

    });

});// final document.ready


/**
 * Metodo que sirve para guardar un impuesto isr por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarImpuestoIsr() {

    $.ajax({
        url: 'guardarImpuestoIsr',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un impuesto isr por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarImpuestoIsr() {

    $.ajax({
        url: 'eliminarImpuestoIsr',
        data: {
            id_impuesto_isr: $('#model-eliminar .modal-body #id_impuesto_isr').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}