/**
 * Created by Oscar Vargas on 28/11/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar metodo_pago para poner el id principal del metodo pago
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_metodo_pago').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_metodo_pago').val() != "") {
            $.ajax({
                url: 'obtenerMetodoPago',
                data: {
                    id_metodo_pago: $('#model-guardar .modal-body #id_metodo_pago').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #nombre').val(json.nombre);
                    $('#model-guardar .modal-body #tipo_pago').val(json.tipo_pago);
                    $('#model-guardar .modal-body #forma_pago').val(json.forma_pago);

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_metodo_pago').val("");
        $('#model-guardar .modal-body #nombre').val("");

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del metodo pago
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_metodo_pago').val($(e.relatedTarget).data('id_eliminar'));

    });

}); // fin document.ready

/**
 * Metodo que sirve para guardar un metodoPago por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarMetodoPago() {

    $.ajax({
        url: 'guardarMetodoPago',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un metodo pago por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarMetodoPago() {

    $.ajax({
        url: 'eliminarMetodoPago',
        data: {
            id_metodo_pago: $('#model-eliminar .modal-body #id_metodo_pago').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}