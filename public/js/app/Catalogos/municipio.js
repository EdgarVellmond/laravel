/**
 * Created by Oscar Vargas on 25/11/2016.
 */

$(document).ready(function () {

    inicializarSelect2();

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     * Metodo que sirve para iniciar el select2
     * @author Oscar Vargas
     * @return void
     */
    function inicializarSelect2() {

        $('.select2').select2({
            width: '100%',
            class: 'form-control'
        });
    }

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar municipio para poner el id principal de municipio
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_municipio').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_municipio').val() != "") {
            $.ajax({
                url: 'obtenerMunicipioEditar',
                data: {
                    id_municipio: $('#model-guardar .modal-body #id_municipio').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #nombre').val(json.nombre);
                    $('#model-guardar .modal-body #id_estado').val(json.id_estado).change();

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_estado').val(1).change();
        $('#model-guardar .modal-body #id_municipio').val("");
        $('#model-guardar .modal-body #nombre').val("");

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del municipio
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_municipio').val($(e.relatedTarget).data('id_eliminar'));

    });

});// fin de document.ready


/**
 * Metodo que sirve para guardar un municipio por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarMunicipio() {

    $.ajax({
        url: 'guardarMunicipio',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un municipio por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarMunicipio() {

    $.ajax({
        url: 'eliminarMunicipio',
        data: {
            id_municipio: $('#model-eliminar .modal-body #id_municipio').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}