/**
 * Created by Erik Villarreal at 26/11/2016.
 */


$(document).ready(function () {

                $( "#vp" ).click(function( event ) {
                    event.preventDefault();
                });

    $('.select2').select2({
                width: '100%',
                class: 'form-control'
            });

    $('#summernote').summernote({
            lang: 'es-ES',
            height: null,                 // set editor height
            minHeight: 500,             // set minimum height of editor
            maxHeight: 650,             // set maximum height of editor
            focus: true              // set focus to editable area after initializing summernote
        });

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    tablaVariables = $('#t_list_variables').DataTable({
        "dom": "<'row'<'col-lg-12'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        "info":     false,
        "paging":   false,
        "ordering": false,
        "columns": [
            {
                "orderable":  false,
                "target": -1,
                "data": "agregar",
                "render": function ( data, type, full, meta ) {
                     return '<button onclick="agregarVariable(this)" class="btn btn-success btn-xs glyphicon glyphicon-plus" value="'+data+'"></button>';
                }
            },
            { "data": "nombre" }  
        ]
    });

    /**
    *Evento onShow para el modal-form que permite agregar o editar un plantilla
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if(id != "" && id != 0 && id != undefined){
            obtenerPlantillaEditar(id);
        }

         $('#modal-form-' + idPlantilla).val(id);
         $('#modal-form-editar-' + idPlantilla).val(id);
         
         if(idPlantillaEditar != 0){
            $('#modal-form-' + idPlantilla).val(idPlantillaEditar);
         }

    });

    /**
    *Evento onShow para el modal-vista_previa que permite agregar el src para mostrar la vista
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-vista_previa').on('show.bs.modal', function (e) {

        var src = $(e.relatedTarget).data('src');

        $('#modal-vista_previa_iFrame').attr('src', src);
        $('#modal-vista_previa_blank').attr('href', src);

    });

    /**
    *Evento onHide para el modal-vista_previa que permite reiniciar el src del iFrame
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-vista_previa').on('hide.bs.modal', function (e) {

        $('#modal-vista_previa_iFrame').attr('src', "");
        $('#modal-vista_previa_blank').attr('href', "");

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la plantilla
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idPlantilla).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        if(idPlantillaEditar == 0){
            $('#modal-form-' + nombrePlantilla).val("");
            $('#modal-form-' + idPlantilla).val("");
            $('#modal-form-' + idTipoPlantilla).val(0).change();
        }

    });

    //Agregar campo default a combos
    agregarDefault("modal-form-" + idTipoPlantilla);
    obtenerVariablesSistema();
    
    if(idPlantillaEditar != 0){
        obtenerPlantilla(idPlantillaEditar);
    }

});// final document.ready

/**
* Metodo que sirve para agregar Variable al Summernote
* @author Erik Villarreal
* @return void
*/
function agregarVariable(boton){
    var tamaño = boton.value.split(".");
    //Si la variable es especial, siempre traerá una longitud de 3
    if(tamaño.length == 3){
        var variable = "$e{" + boton.value + "}";
    }else{
        var variable = "$p{" + boton.value + "}";
    }

    $('#summernote').summernote('insertText', variable);
}

/**
* Metodo que sirve para agregar un Option default a un Combo
* @author Erik Villarreal
* @return void
*/
function agregarDefault(combo){
    var select = document.getElementById(combo);
    var opcion;

    console.log(document.getElementById(combo));

    opcion = document.createElement("OPTION");
    opcion.text = "Seleccionar";
    opcion.value = 0;
    select.add(opcion);

    $('#' + combo).val(0).change();
}

/**
* Metodo que sirve para obtener la plantilla
* @author Erik Villarreal
* @return void
*/
function obtenerPlantillaEditar(id){
    $.ajax({
        url: 'obtenerPlantillaEditar',
        data: {
            id_plantilla: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombrePlantilla).val(response[0].nombre);
            $('#modal-form-' + idTipoPlantilla).val(response[0].id_tipo_plantilla).change();

        }
    });
}

/**
* Metodo que sirve para obtener la plantilla
* @author Erik Villarreal
* @return void
*/
function obtenerPlantilla(id){
    $.ajax({
        url: 'obtenerPlantilla',
        data: {
            id_plantilla: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombrePlantilla).val(response[0].nombre);
            $('#modal-form-' + idTipoPlantilla).val(response[0].id_tipo_plantilla).change();
            $('#summernote').summernote('code', response[0].contenido);

        }
    });
}

/**
* Metodo que sirve para obtener las variables a utilizar en la plantilla
* @author Erik Villarreal
* @return void
*/
function obtenerVariablesSistema(){

    var tabla = $('#' + tablaOrigen).val();

    if(tabla != 0 && tabla != undefined && tabla != ""){

        $.ajax({
            url: 'obtenerVariablesSistema',
            data: {
                "_token": token,
                "alias_tabla": tabla   
            },
            type: 'POST',
            success: function (response) {

                $.each(response,function(index){

                    if(response[index].id_rol != null){
                        tablaVariables.row.add({
                            "agregar":  response[index].tabla + "." + response[index].campo + "." + response[index].id_rol,
                            "nombre":   response[index].nombre
                        });
                    }else{
                        tablaVariables.row.add({
                            "agregar":  response[index].tabla + "." + response[index].campo,
                            "nombre":   response[index].nombre
                        });
                    }

                });
                tablaVariables.draw();

            }
        });

    }
    
}

/**
 * Metodo que sirve para guardar una plantilla por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarPlantilla() {

    var id = $("#modal-form-" + idPlantilla).val();

    if(id != 0 && id != "" && id != undefined){

        $.ajax({
            url: 'guardarPlantilla',
            data: $('#add_edit_form').serialize(),
            type: 'POST',
            success: function (message) {
                $('#modal-alert-message-success').modal('show');
                $("#success-message").html(message);
                location.reload();
            }, error: function (message) {
                $('#modal-alert-message-error').modal('show');
                $("#error-message").html(message);
            }
        });

    }else{

        $.ajax({
            url: 'guardarPlantilla',
            data: {
                "_token": token,
                id_plantilla: "",
                nombre: $("#modal-form-" + nombrePlantilla).val(),
                id_tipo_plantilla: $("#modal-form-" + idTipoPlantilla).val(),
                contenido: $('#summernote').summernote('code')
            },
            type: 'POST',
            success: function (message) {
                $('#modal-alert-message-success').modal('show');
                $("#success-message").html(message);
                location.reload();
            }, error: function (message) {
                $('#modal-alert-message-error').modal('show');
                $("#error-message").html(message);
            }
        });

    }

    return false;
}

/**
 * Metodo que sirve para eliminar una plantilla por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarPlantilla() {

    $.ajax({
        url: 'eliminarPlantilla',
        data: {
            id_plantilla: $('#modal-eliminar-' + idPlantilla).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}