/**
 * Created by Erik Villarreal at 6/12/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     *Evento onShow para el modal-form que permite agregar o editar un rol
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerRol(id);
        }

        $('#modal-form-' + idRol).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del rol
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idRol).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreRol).val("");
        $('#modal-form-' + idRol).val("");

    });

});// final document.ready

/**
 * Metodo que sirve para obtener el rol
 * @author Erik Villarreal
 * @return void
 */
function obtenerRol(id) {
    $.ajax({
        url: 'obtenerRolEditar',
        data: {
            id_rol: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreRol).val(response.nombre);

        }
    });
}

/**
 * Metodo que sirve para guardar un rol por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarRol() {

    $.ajax({
        url: 'guardarRol',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un rol por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarRol() {

    $.ajax({
        url: 'eliminarRol',
        data: {
            id_rol: $('#modal-eliminar-' + idRol).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}