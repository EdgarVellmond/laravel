/**
 * Created by Oscar Vargas on 01/12/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


    /**
     * Metodo que se ejecuta al abrirse el modal de guardar salario minimo para poner el id principal del salario minimo
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_salario_minimo').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_salario_minimo').val() != "") {
            $.ajax({
                url: 'obtenerSalarioMinimo',
                data: {
                    id_salario_minimo: $('#model-guardar .modal-body #id_salario_minimo').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #anio').val(json.anio);
                    $('#model-guardar .modal-body #valor').val(json.valor);

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del salario minimo
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_salario_minimo').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_salario_minimo').val("");
        $('#model-guardar .modal-body #anio').val("");
        $('#model-guardar .modal-body #valor').val("");

    });

});// final document.ready


/**
 * Metodo que sirve para guardar un salario minimo por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarSalarioMinimo() {

    $.ajax({
        url: 'guardarSalarioMinimo',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un salario minimo por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarSalarioMinimo() {

    $.ajax({
        url: 'eliminarSalarioMinimo',
        data: {
            id_salario_minimo: $('#model-eliminar .modal-body #id_salario_minimo').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}