/**
 * Created by Oscar Vargas on 24/11/2016.
 */

$(document).ready(function () {

    inicializarSelect2();
    llenarSelectMunicipios("");


    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    $('.btn').tooltip();

    /**
     * Funcion que se ejecuta cuando cambia el select de estado
     */
    $('#id_estado').change(function () {

        llenarSelectMunicipios("");

    });

    /**
     * Metodo que sirve para iniciar el select2
     * @author Oscar Vargas
     * @return void
     */
    function inicializarSelect2() {

        $('.select2').select2({
            width: '100%',
            class: 'form-control'
        });
    }

    /**
     * Metodo que sirve para llenar el select2 de municipios
     * @author Oscar Vargas
     * @return void
     */
    function llenarSelectMunicipios(a) {

        $('#model-guardar .modal-body #id_municipio').empty().trigger('change');

        $.ajax({
            url: 'obtenerMunicipios',
            data: {id_estado: $('#id_estado').val(), '_token': token},
            type: 'POST',
            // el tipo de información que se espera de respuesta
            dataType: 'json',
            success: function (json) {

                var select = document.getElementById('id_municipio');
                var opcion;

                $.each(json, function (key) {
                    opcion = document.createElement("OPTION");
                    opcion.text = json[key];
                    opcion.value = key;
                    select.add(opcion);
                });

                if (a != "") {

                    $('#model-guardar .modal-body #id_municipio').val(a).change();
                }
            }
        });
    }

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar sucursal para poner el id principal de la sucursal
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_sucursal').val($(e.relatedTarget).data('id_editar_sucursal'));

        if ($('#model-guardar .modal-body #id_sucursal').val() != "") {
            $.ajax({
                url: 'obtenerSucursal',
                data: {
                    id_sucursal: $('#model-guardar .modal-body #id_sucursal').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #nombre').val(json[0].nombre);
                    $('#model-guardar .modal-body #clave').val(json[0].clave);
                    $('#model-guardar .modal-body #correo').val(json[0].correo);
                    $('#model-guardar .modal-body #telefono').val(json[0].telefono);
                    $('#model-guardar .modal-body #id_empresa').val(json[0].id_empresa).change();
                    $('#model-guardar .modal-body #calle').val(json[0].calle);
                    $('#model-guardar .modal-body #colonia').val(json[0].colonia);
                    $('#model-guardar .modal-body #numero_interior').val(json[0].numero_interior);
                    $('#model-guardar .modal-body #numero_exterior').val(json[0].numero_exterior);
                    $('#model-guardar .modal-body #cp').val(json[0].cp);
                    ;
                    $('#model-guardar .modal-body #id_estado').val(json[0].id_estado).change();
                    llenarSelectMunicipios(json[0].id_municipio);

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la sucursal
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_sucursal').val($(e.relatedTarget).data('id_eliminar_sucursal'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de ver_domicilio para cargar su informacion
     * @author Oscar Vargas
     * @return void
     */
    $('#model-ver_direccion').on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerDomicilioSucursal',
            data: {id_sucursal: $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',

            success: function (json) {
                $('#model-ver_direccion .modal-body #calle').val(json[0].calle);
                $('#model-ver_direccion .modal-body #colonia').val(json[0].colonia);
                $('#model-ver_direccion .modal-body #numero_exterior').val(json[0].numero_exterior);
                $('#model-ver_direccion .modal-body #numero_interior').val(json[0].numero_interior);
                $('#model-ver_direccion .modal-body #cp').val(json[0].cp);
                $('#model-ver_direccion .modal-body #id_municipio').val(json[0].nombreMunicipio);
                $('#model-ver_direccion .modal-body #id_estado').val(json[0].nombreEstado);


            }
        });

    });


});// fin document.ready

/**
 * Metodo que sirve para guardar una sucursal por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarSucursal() {

    $.ajax({
        url: 'guardarSucursal',
        data: $('#add_form_sucursal').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una sucursal por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarSucursal() {

    $.ajax({
        url: 'eliminarSucursal',
        data: {
            id_sucursal: $('#model-eliminar .modal-body #id_sucursal').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}