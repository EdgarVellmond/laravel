/**
 * Created by Erik Villarreal at 28/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     *Evento onShow para el modal-form que permite agregar o editar un tipo de plantilla
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerTipoPlantilla(id);
        }

        $('#modal-form-' + idTipoPlantilla).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del tipo de plantilla
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idTipoPlantilla).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreTipoPlantilla).val("");
        $('#modal-form-' + idTipoPlantilla).val("");

    });

});// final document.ready

/**
 * Metodo que sirve para obtener el tipo de plantilla
 * @author Erik Villarreal
 * @return void
 */
function obtenerTipoPlantilla(id) {
    $.ajax({
        url: 'obtenerTipoPlantillaEditar',
        data: {
            id_tipo_plantilla: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreTipoPlantilla).val(response.nombre);

        }
    });
}

/**
 * Metodo que sirve para guardar un tipo de plantilla por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarTipoPlantilla() {

    $.ajax({
        url: 'guardarTipoPlantilla',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un tipo de plantilla por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarTipoPlantilla() {

    $.ajax({
        url: 'eliminarTipoPlantilla',
        data: {
            id_tipo_plantilla: $('#modal-eliminar-' + idTipoPlantilla).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}