/**
 * Created by Oscar Vargas on 12/12/2016.
 */

$(document).ready(function () {

    $(".select2").select2();

    $(".btn").tooltip();

    oTableSolicitud = $('#t_list_solicitud').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    oTableBitacora = $('#t_list_bitacora').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    oTableSolicitudCancelado = $('#t_list_solicitud_cancelado').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    oTableSolicitudFinalizado = $('#t_list_solicitud_finalizado').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    /**
     *Evento change para mandar id de la solicitud al modal de comenzar
     *@author Oscar Vargas
     *@return void
     */
    $("#modal-comenzar").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        $('#modal-comenzar-id_solicitud').val(id);

    });

    /**
     *Evento change para mandar id de la solicitud al modal de finalizar
     *@author Oscar Vargas
     *@return void
     */
    $("#modal-finalizar").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        $('#modal-finalizar-id_solicitud').val(id);

    });

    /**
     *Evento change para mandar id de la solicitud al modal de rechazar
     *@author Oscar Vargas
     *@return void
     */
    $("#modal-rechazar").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        $('#modal-rechazar-id_solicitud').val(id);

    });

    /**
     *Evento change para mandar id de la solicitud al modal de cancelar
     *@author Oscar Vargas
     *@return void
     */
    $("#modal-cancelar").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        var estatus = $(e.relatedTarget).data('estatus');
        
        $('#modal-cancelar-id_solicitud').val(id);

        if(estatus != null && estatus != "" && estatus != undefined){
            $('#modal-cancelar-estatus').val(estatus);
        }

    });

    /**
     *Evento change para obtener la información de la Solicitud
     *@author Oscar Vargas
     *@return void
     */
    $("#modal-form").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        $('#modal-form-id_solicitud').val(id);

        if(id != 0 && id != null && id != undefined && id != ""){
            obtenerSolicitudEditar(id);
        }

    });

    /**
     *Evento change para limpiar el modal form
     *@author Oscar Vargas
     *@return void
     */
    $("#modal-form").on('hide.bs.modal', function (e) {

       $('#modal-form-asunto').val("");
       $('#modal-form-id_solicitud').val("");
       $('#modal-form-id_area').val(0).change();
       $('#modal-form-id_tipo_solicitud').val(0).change();

    });

});

/**
 * Metodo que sirve para obtener la información de la Solicitud 
 * @author Erik Villarreal
 * @return void
 */
function obtenerSolicitudEditar(id) {
    $.ajax({
        url: 'obtenerSolicitudEditar',
        data: {
            id_solicitud: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $.each(response,function(index){
                
                if(index == 0){
                    $('#modal-form-asunto').val(response[index].asunto);
                    $('#modal-form-id_area').val(response[index].id_area).change();
                    $('#modal-form-id_tipo_solicitud').val(response[index].id_tipo_solicitud).change();
                }

            });
            
        }
    });
}