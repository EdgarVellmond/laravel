
$(document).ready(function () {

oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();

    /**
     *Evento onShow para el modal-form que permite agregar o editar un proyecto
     *@author Edgar Bautista
     *@param  e #event JQuery
     *@return void
     */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0 && id != undefined) {
            obtenerProyecto(id);
        }
        $('#id_proyecto').val(id);

 });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal del área
     * @author Edgar Bautista
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idProyecto).val($(e.relatedTarget).data('id'));

    });

    

    $('#modal-forms').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        var indicador = $(e.relatedTarget).data('indicador');
        var idActividad = $(e.relatedTarget).data('actividad')
        if (id != "" && id != 0 && id != undefined) {
            
                obtenerActividad(id,indicador,idActividad);
             
        }

        $('#id_proyecto2').val(id);

    });


    

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Edgar Bautista
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {
        $('#modal-form-' + nombreProyecto).val("");
        $('#modal-form-' + idProyecto).val("");
        $('#modal-form-' + cliente).val("");
        $('#modal-form-' + fechaInicial).val("");
        $('#modal-form-' + fechaTermino).val("");
        $('#modal-form-' + prioridad).val("Selecciona");
        $('#modal-form-' + estatus).val("Selecciona");
        $('#modal-form-' + ubicacion).val("");
        $('#modal-form-' + tipo).val("Selecciona");
        $('#modal-form-' + duracion).val("");
        //$('#modal-form-' + observaciones).val("");
        $('#modal-form-' + gastobra).val("0");
        $('#modal-form-' + montocontr).val("0");
        $('#modal-form-' + montoejercido).val("0");
        $('#modal-form-' + saldoEjercer).val("0");
    });
        
        // $('#modal-forms').on('hide.bs.modal', function (e) {
        //     $.each(response , function(key){
        //     $('#modal-form-tipo_indicador-'+(key+1)).val("Selecciona");
        //     $('#modal-form-area-'+(key+1)).val("Selecciona");
        //     $('#modal-form-programado-'+(key+1)).val("0");
        //     $('#modal-form-real-'+(key+1)).val("0");
        //         } });

    
});//final document.ready

/**
 * Metodo que sirve para obtener el proyecto
 * @author Edgar Bautista
 * @return void
 */
function obtenerProyecto(id) {
    $.ajax({
        url: 'obtenerProyectoEditar',
        data: {
            id_proyecto: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {
            $('#modal-form-proyecto_nombre').val(response.proyecto_nombre);
            $('#modal-form-cliente').val(response.cliente);
            $('#modal-form-fecha_inicial').val(response.fecha_inicial);
            $('#modal-form-fecha_termino').val(response.fecha_termino);
            $('#modal-form-observaciones').val(response.observaciones);
            $('#modal-form-tipo').val(response.tipo);
            $('#modal-form-gasto_obra').val(response.gasto_obra);
            $('#modal-form-prioridad').val(response.prioridad);
            $('#modal-form-estatus').val(response.estatus);
            $('#modal-form-duracion').val(response.duracion);
            $('#modal-form-monto_ejercido').val(response.monto_ejercido);
            $('#modal-form-monto_contrato').val(response.monto_contrato);
            $('#modal-form-saldo_a_ejercer').val(response.saldo_a_ejercer);
            $('#modal-form-ubicacion').val(response.ubicacion);
        }
    });
}

/**
 * Metodo que sirve para obtener el proyecto
 * @author Edgar Bautista
 * @return void
 */
function obtenerActividad(id,indicador,idActividad) {
    $.ajax({
        url: 'obtenerActividadEditar',
        data: {
            id_proyecto: id,
            indicador: indicador,
            idActividades: idActividad,
            '_token': token
        },
        type: 'POST',
        success: function (response) {
                $.each(response , function(key){
            $('#modal-form-tipo_indicador-'+(key+1)).val(response[key].tipo_indicador);
            $('#modal-form-area-'+(key+1)).val(response[key].area);
            $('#modal-form-programado-'+(key+1)).val(response[key].programado);
            $('#modal-form-real-'+(key+1)).val(response[key].real);
                } );
            
           
        }
    });
}



function guardarProyecto() {

    $.ajax({
        url: 'guardarProyecto',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

function guardarActividads() {

    $.ajax({
        url: 'guardarActividads',
        data: $('#add_edit_forms').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            //location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un área por servicio Ajax
 * @author Edgar Bautista
 * @returns {boolean}
 */
function eliminarProyecto() {

    $.ajax({
        url: 'eliminarProyecto',
        data: {
            id_proyecto: $('#modal-eliminar-' + idProyecto).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}