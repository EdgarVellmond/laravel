/**
 * Created by Oscar Vargas on 20/12/2016.
 */



$(document).ready(function () {

    $('.btn').tooltip();

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    oTableTerminada = $('#t_list_terminada').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    oTableProceso = $('#t_list_proceso').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    oTableCanceladas = $('#t_list_canceladas').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            },
            {"targets": [1, 2, 3], 'className': ' sinwarp'}
        ]
    });

    inicializarSelect2();

    llenarSelectMunicipios("");

    /**
     * Funcion que se ejecuta cuando cambia el select de estado
     */
    $('#id_estado').change(function () {

        llenarSelectMunicipios("");

    });

    /**
     * Metodo que sirve para llenar el select2 de municipios
     * @author Oscar Vargas
     * @return void
     */
    function llenarSelectMunicipios(a) {

        $('#model-nuevo_proyecto .modal-body #id_municipio').empty().trigger('change');

        $.ajax({
            url: 'obtenerMunicipios',
            data: {id_estado: $('#id_estado').val(), '_token': token},
            type: 'POST',
            // el tipo de información que se espera de respuesta
            dataType: 'json',
            success: function (json) {

                var select = document.getElementById('id_municipio');
                var opcion;

                $.each(json, function (key) {
                    opcion = document.createElement("OPTION");
                    opcion.text = json[key];
                    opcion.value = key;
                    select.add(opcion);
                });

                if (a != "") {

                    $('#model-nuevo_proyecto .modal-body #id_municipio').val(a).change();
                }
            }
        });
    }


    /**
     * Metodo que sirve para iniciar el select2
     * @author Oscar Vargas
     * @return void
     */
    function inicializarSelect2() {

        $('.select2').select2({
            width: '100%',
            class: 'form-control'
        });

    }





    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar_proveedor para poner el id principal de ka ivra
     * @author Oscar Vargas
     * @return void
     */
    $('#model-cancelar_proyecto').on('show.bs.modal', function (e) {

        $('#model-cancelar_proyecto .modal-body #id_obra').val($(e.relatedTarget).data('id_eliminar_proyecto'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de autorizar para poner el id principal de la obra
     * @author Oscar Vargas
     * @return void
     */
    $('#model-autorizar').on('show.bs.modal', function (e) {

        $('#model-autorizar .modal-body #id_obra').val($(e.relatedTarget).data('id_autorizar'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar proyecto para borrar todo el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-nuevo_proyecto').on('hide.bs.modal', function (e) {

        $('#model-nuevo_proyecto .modal-body #nombre').val("");
        $('#model-nuevo_proyecto .modal-body #id_obra').val("");
        $('#model-nuevo_proyecto .modal-body #folio').val("");
        $('#model-nuevo_proyecto .modal-body #descripcion').val("");
        $('#model-nuevo_proyecto .modal-body #capital_minimo_contable').val("");
        $('#model-nuevo_proyecto .modal-body #id_cliente').val(1).change();
        $('#model-nuevo_proyecto .modal-body #prioridad').val();
        $('#model-nuevo_proyecto .modal-body #id_estado').val(1).change();
        $('#model-nuevo_proyecto .modal-body #localidad').val("");
        $('#model-nuevo_proyecto .modal-body #fecha_inicio').val("");
        $('#model-nuevo_proyecto .modal-body #fecha_termino').val("");
        $('#model-nuevo_proyecto .modal-body #fecha_limite_base').val("");

        llenarSelectMunicipios("");


    });

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar proveedor para poner el id principal del proveedor
     * @author Oscar Vargas
     * @return void
     */
    $('#model-nuevo_proyecto').on('show.bs.modal', function (e) {

        $('#model-nuevo_proyecto .modal-body #id_obra').val($(e.relatedTarget).data('id_editar_proyecto'));

        if ($('#model-nuevo_proyecto .modal-body #id_obra').val() != "") {

            $.ajax({
                url: 'obtenerObra',
                data: {
                    id_obra: $('#model-nuevo_proyecto .modal-body #id_obra').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {


                    $('#model-nuevo_proyecto .modal-body #nombre').val(json[0].nombre);
                    $('#model-nuevo_proyecto .modal-body #folio').val(json[0].folio);
                    $('#model-nuevo_proyecto .modal-body #descripcion').val(json[0].descripcion);
                    $('#model-nuevo_proyecto .modal-body #capital_minimo_contable').val(json[0].capital_minimo_contable);
                    $('#model-nuevo_proyecto .modal-body #id_cliente').val(json[0].id_cliente).change();
                    $('#model-nuevo_proyecto .modal-body #prioridad').val(json[0].prioridad);
                    $('#model-nuevo_proyecto .modal-body #id_estado').val(json[0].id_estado).change();
                    $('#model-nuevo_proyecto .modal-body #id_municipio').val(json[0].id_municipio).change();
                    $('#model-nuevo_proyecto .modal-body #localidad').val(json[0].localidad);
                    $('#model-nuevo_proyecto .modal-body #fecha_inicio').val(json[0].fecha_inicio);
                    $('#model-nuevo_proyecto .modal-body #fecha_termino').val(json[0].fecha_termino);
                    $('#model-nuevo_proyecto .modal-body #fecha_limite_base').val(json[0].fecha_limite_base);

                    llenarSelectMunicipios(json[0].id_municipio);

                }
            });

        }

    });


    /**
     * Metodo que se ejecuta al abrirse el modal de ver informacion adicionales
     * @author Oscar Vargas
     * @return void
     */
    $('#model-ver_adicional').on('show.bs.modal', function (e) {

            $.ajax({
                url: 'obtenerObraAdicional',
                data: {
                    id_obra: $(e.relatedTarget).data('id_ver_adicional'),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {


                    $('#model-ver_adicional .modal-body #localidad').val(json[0].localidad);
                    $('#model-ver_adicional .modal-body #id_estado').val(json[0].nombreEstado);
                    $('#model-ver_adicional .modal-body #id_municipio').val(json[0].nombreMunicipio);
                    $('#model-ver_adicional .modal-body #folio').val(json[0].folio);
                    $('#model-ver_adicional .modal-body #observaciones').val(json[0].observaciones);

                }

            });

    });


});// final de document.ready

/**
 * Metodo que sirve para guardar un proyecto por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarProyecto() {

    $.ajax({
        url: 'guardarProyecto',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para autrizar un proyecto por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function autorizarProyecto() {

    $.ajax({
        url: 'autorizarProyecto',
        data: $('#add_form_autorizar').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar un proyecto por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function cancelarProyecto() {

    $.ajax({
        url: 'cancelarObra',
        data: {
            id_obra: $('#model-cancelar_proyecto .modal-body #id_obra').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}