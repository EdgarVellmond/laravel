/**
 * Created by Erik Villarreal on 21/12/2016.
 */

$(document).ready(function () {

        oTable =  $('#t_list').dataTable({
                        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
                        language: {
                            'url': urlTable
                        },
                        order: [1],
                        columnDefs: [
                        {
                            "targets": [0],
                            'searchable': false,
                            "orderable": false,
                            'className': 'text-center ',
                            'width': '5%'
                        }
                        ]
                    });

    tablaVariables = $('#historial_registro_padron').DataTable({
        "dom": "<'row'<'col-lg-12'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        "info":     false,
        "paging":   false,
        "ordering": false,
        "columnDefs": [
                        {
                            "targets": [0,1,2,3,4],
                            'className': 'text-center '
                        }
                    ],
        "columns": [
            {
                "orderable":  false,
                "target": -1,
                "data": "id",
                "render": function ( data, type, full, meta ) {

                    return '<a href="' + urlBase + '/descargarDocumentos/' + data + '/registro_padron/registro_padron" class="btn btn-xs btn-success" target="_blank" title="Descargar documento"> <span class="glyphicon glyphicon-download-alt"></span></a>';

                }
            },
            { "data": "local" },
            { "data": "vigencia" },
            { "data": "comentario" },
            { "data": "estatus" }
        ],
        "fnRowCallback": function( nRow, data) {
                var clase;

                if ( data["estatus"] == "Inscrito y vigente" )
                    clase = "success";
                else
                    clase = "";

                $(nRow).addClass( clase );
                    return nRow;
        }
    });

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });

    $('.btn').tooltip();

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar falta para poner el id principal de falta
     * @author Erik Villarreal
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_registro_padron').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_registro_padron').val() != "") {
            $.ajax({
                url: 'obtenerRegistroPadronEditar',
                data: {
                    id_registro_padron: $(e.relatedTarget).data('id_editar'),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #modal-form-inicio_vigencia').val(json.inicio_vigencia);
                    $('#model-guardar .modal-body #modal-form-fin_vigencia').val(json.fin_vigencia);
                    $('#model-guardar .modal-body #modal-form-id_cliente').val(json.id_cliente).change();
                    $('#model-guardar .modal-body #modal-form-inscripcion').val(json.inscripcion);
                    $('#model-guardar .modal-body #modal-form-comentario').val(json.comentario);
                    $('#model-guardar .modal-body #modal-form-id_empresa').val(json.id_empresa).change();

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar falta para poner el id principal de falta
     * @author Erik Villarreal
     * @return void
     */
    $('#modal-update').on('show.bs.modal', function (e) {

        $('#modal-update .modal-body #id_registro_padron').val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la incapacidad
     * @author Erik Villarreal
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_registro_padron').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la incapacidad
     * @author Erik Villarreal
     * @return void
     */
    $('#modal-historial').on('show.bs.modal', function (e) {

        if($(e.relatedTarget).data('id') != 0){
            obtenerHistorialRegistroPadron($(e.relatedTarget).data('id'));
        }

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Erik Villarreal
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #modal-form-inicio_vigencia').val("");
        $('#model-guardar .modal-body #modal-form-fin_vigencia').val("");
        $('#model-guardar .modal-body #modal-form-id_cliente').val(1).change();
        $('#model-guardar .modal-body #modal-form-inscripcion').val("");
        $('#model-guardar .modal-body #modal-form-comentario').val("");
        $('#model-guardar .modal-body #modal-file').val("");
        $('#model-guardar .modal-body #modal-form-id_empresa').val(1).change();

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de actualizar para reiniciar el formulario
     * @author Erik Villarreal
     * @return void
     */
    $('#modal-update').on('hide.bs.modal', function (e) {

        $('#modal-update .modal-body #modal-update-inicio_vigencia').val("");
        $('#modal-update .modal-body #modal-update-fin_vigencia').val("");
        $('#modal-update .modal-body #modal-update-comentario').val("");
        $('#modal-update .modal-body #modal-file-update-').val("");

    });

}); // fin document.ready


/**
* Metodo que sirve para obtener las variables a utilizar en la plantilla
* @author Erik Villarreal
* @return void
*/
function obtenerHistorialRegistroPadron(id){

    tablaVariables.clear();

    if(id != 0 && id != undefined && id != ""){

        $.ajax({
            url: 'obtenerHistorialRegistroPadron',
            data: {
                "_token": token,
                "id_registro_padron": id   
            },
            type: 'POST',
            success: function (response) {

                $.each(response,function(index){

                    tablaVariables.row.add({
                        "id":  response[index].id_registro_padron,
                        "local":  response[index].id_registro_padron,
                        "vigencia":  response[index].inicio_vigencia + " a " + response[index].fin_vigencia,
                        "comentario": response[index].comentario,
                        "estatus":  response[index].estatus
                    });

                });

                tablaVariables.draw();

            }
        });

    }
    
}


/**
 * Metodo que sirve para eliminar una falta por servicio Ajax
 * @author Erik Villarreal
 * @returns {boolean}
 */
function eliminarFalta() {

    $.ajax({
        url: 'eliminarFalta',
        data: {
            id_falta: $('#model-eliminar .modal-body #id_falta').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}
