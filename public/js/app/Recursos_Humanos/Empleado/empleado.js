$(document).ready(function () {

    $('a').tooltip();

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });


    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });


    /**
     *Evento onShow para consultar la información adicional del Empleado y mostrar
     *en el modal ver-adicional
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver-adicional").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerAdicionalEmpleado',
            data: {'id_empleado': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                $("#modal-ver-adicional-" + rfc).val(response.rfc);
                $("#modal-ver-adicional-" + curp).val(response.curp);
                $("#modal-ver-adicional-" + nss).val(response.nss);
                $("#modal-ver-adicional-" + telefono).val(response.telefono);
                $("#modal-ver-adicional-" + correo).val(response.correo);
                $("#modal-ver-adicional-" + idArea).val(response.id_area);
                $("#modal-ver-adicional-" + idDepartamento).val(response.id_departamento);
                $("#modal-ver-adicional-" + idPuesto).val(response.id_puesto);

            }
        });

    });

    /**
     *Evento onShow para consultar la información de la licencia del Empleado y mostrar
     *en el modal ver-licencia
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver-licencia").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerLicenciaEmpleado',
            data: {'id_empleado': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                $("#modal-ver-licencia-" + numeroLicencia).val(response.numero_licencia);
                $("#modal-ver-licencia-" + vigenciaLicencia).val(response.vigencia_licencia);

            }
        });

    });

    /**
     *Evento onShow para consultar la información de la nómina del Empleado y mostrar
     *en el modal ver_nomina
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver_nomina").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerNominaEmpleado',
            data: {'id_empleado': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                $("#modal-ver_nomina_" + salarioBaseDiario).val(response.salario_base_diario);
                $("#modal-ver_nomina_" + salarioDiarioIntegrado).val(response.salario_diario_integrado);
                $("#modal-ver_nomina_" + idPatron).val(response.id_patron);
                if (response.viaticos == 1) {
                    $("#modal-ver_nomina_" + viaticos).val("Sí");
                } else {
                    $("#modal-ver_nomina_" + viaticos).val("No");
                }


            }
        });

    });

    /**
     *Evento onShow para consultar la dirección del Empleado y mostrar
     *en el modal ver-direccion
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver-direccion").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerDireccionEmpleado',
            data: {'id_empleado': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                $("#" + calle).val(response.calle);
                $("#" + numeroExterior).val(response.numero_exterior);
                $("#" + numeroInterior).val(response.numero_interior);
                $("#" + colonia).val(response.colonia);
                $("#" + cp).val(response.cp);
                $("#" + idMunicipio).val(response.id_municipio);
                $("#" + idEstado).val(response.id_estado);

            }
        });

    });

    /**
     *Evento onShow para consultar la sucursal del Empleado y mostrar
     *en el modal ver_sucursal
     _           *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver_sucursal").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerSucursal',
            data: {'id_sucursal': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                $("#modal-ver_sucursal_" + nombreSucursal).val(response.nombre);
                $("#modal-ver_sucursal_" + calleSucursal).val(response.calle);
                $("#modal-ver_sucursal_" + clave).val(response.clave);
                $("#modal-ver_sucursal_" + coloniaSucursal).val(response.colonia);
                $("#modal-ver_sucursal_" + correoSucursal).val(response.correo);
                $("#modal-ver_sucursal_" + numeroInteriorSucursal).val(response.numero_interior);
                $("#modal-ver_sucursal_" + numeroExteriorSucursal).val(response.numero_exterior);
                $("#modal-ver_sucursal_" + telefonoSucursal).val(response.telefono);
                $("#modal-ver_sucursal_" + idEmpresa).val(response.id_empresa).change();
                $("#modal-ver_sucursal_" + cpSucursal).val(response.cp);
                $("#modal-ver_sucursal_" + idEstado).val(response.nombreEstado);
                $("#modal-ver_sucursal_" + idMunicipio).val(response.nombreMunicipio);

            }
        });

    });

    /**
     *Evento onShow para consultar la imagen del Empleado y mostrar
     *en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-ver-imagen").on('show.bs.modal', function (e) {

        $.ajax({
            url: 'obtenerImagenEmpleado',
            data: {'id_empleado_imagen': $(e.relatedTarget).data('id'), '_token': token},
            type: 'POST',
            success: function (json) {

                response = json[0];
                var src = urlBase + response.ruta + response.uuid + "." + response.extension;

                $('#modal-ver-imagen-' + idEmpleadoImagen).attr('src', src);

            }
        });

    });

    /**
     *Evento onShow para el modal-form que permite agregar o editar la Empleado
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-form").on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');

        if (id != "" && id != 0) {
            obtenerEmpleado(id);
        }

        $('#modal-form-' + idEmpleado).val(id);

    });

    /**
     *Evento onHide para reiniciar el modal-form
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#modal-form").on('hide.bs.modal', function (e) {
        reiniciarModalForm();
    });

    /**
     *Evento change para consultar los municipios del estado seleccionado
     *@author Erik Villarreal
     *@return void
     */
    $("#modal-form-" + idEstado).change(function () {
        llenarSelectMunicipios($("#modal-form-" + idMunicipio + "_temporal").val(), "#modal-form-" + idMunicipio, "#modal-form-" + idEstado);
    });

    /**
     *Evento change para consultar los departamentos del área seleccionada
     *@author Erik Villarreal
     *@return void
     */
    $("#modal-form-" + idArea).change(function () {
        llenarSelectDepartamentos($("#modal-form-" + idDepartamento + "_temporal").val(), "#modal-form-" + idDepartamento, "#modal-form-" + idArea);
    });

    /**
     *Evento change para mandar id de la empresa a borrar al modal
     *@author Erik Villarreal
     *@return void
     */
    $("#modal-eliminar").on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        $('#modal-eliminar-' + idEmpleado).val(id);

    });

    //Agregar campo default
    agregarDefaultEstado("modal-form-" + idEstado);
    agregarDefaultEstado("modal-form-" + idArea);
    agregarDefaultEstado("modal-form-" + idPrimaRiesgo);
});

/**
 * Metodo que sirve para agregar un Option default al Combo de los Estados
 * @author Oscar Vargas
 * @return void
 */
function agregarDefaultEstado(combo) {
    var select = document.getElementById(combo);
    var opcion;

    console.log(document.getElementById(combo));

    opcion = document.createElement("OPTION");
    opcion.text = "Seleccionar";
    opcion.value = 0;
    select.add(opcion);

    $('#' + combo).val(0).change();
}

/**
 * Metodo que sirve para llenar el select2 de municipios
 * @author Oscar Vargas
 * @param id #id al que cambiará el idComboMunicipio
 * @param idComboMunicipio #id HTML del combo Municipio
 * @param idComboEstado #id HTML del combo Estado
 * @return void
 */
function llenarSelectMunicipios(id, idComboMunicipio, idComboEstado) {

    $(idComboMunicipio).empty().trigger('change');

    $.ajax({
        url: 'obtenerMunicipios',
        data: {id_estado: $(idComboEstado).val(), '_token': token},
        type: 'POST',
        dataType: 'json',
        success: function (json) {

            var select = document.getElementById(idComboMunicipio.substring(1));
            var opcion;

            $.each(json, function (key) {
                opcion = document.createElement("OPTION");
                opcion.text = json[key];
                opcion.value = key;
                select.add(opcion);
            });

            if (id != "") {
                $(idComboMunicipio).val(id).change();
            }
        }
    });

}

/**
 * Metodo que sirve para llenar el select2 de municipios
 * @author Oscar Vargas
 * @param id #id al que cambiará el idComboDepartamento
 * @param idComboDepartamento #id HTML del combo Departamento
 * @param idComboArea #id HTML del combo Area
 * @return void
 */
function llenarSelectDepartamentos(id, idComboDepartamento, idComboArea) {

    $(idComboDepartamento).empty().trigger('change');

    $.ajax({
        url: 'obtenerDepartamentos',
        data: {id_area: $(idComboArea).val(), '_token': token},
        type: 'POST',
        dataType: 'json',
        success: function (json) {

            var select = document.getElementById(idComboDepartamento.substring(1));
            var opcion;

            $.each(json, function (key) {
                opcion = document.createElement("OPTION");
                opcion.text = json[key];
                opcion.value = key;
                select.add(opcion);
            });

            if (id != "") {
                $(idComboDepartamento).val(id).change();
            }
        }
    });

}

/**
 * Metodo que sirve para obtener la información de la empresa
 * @author Oscar Vargas
 * @return void
 */
function obtenerEmpleado(id) {

    $.ajax({
        url: 'obtenerEmpleado',
        data: {id_empleado: id, '_token': token},
        type: 'POST',
        dataType: 'json',
        success: function (json) {

            response = json[0];
            $("#modal-form-" + nombre).val(response.nombre);
            $("#modal-form-" + apellidoPaterno).val(response.apellido_paterno);
            $("#modal-form-" + apellidoMaterno).val(response.apellido_materno);
            $("#modal-form-" + numeroEmpleado).val(response.numero_empleado);
            $("#modal-form-" + rfc).val(response.rfc);
            $("#modal-form-" + curp).val(response.curp);
            $("#modal-form-" + nss).val(response.nss);
            $("#modal-form-" + telefono).val(response.telefono);
            $("#modal-form-" + correo).val(response.correo);
            $("#modal-form-" + idDepartamento + "_temporal").val(response.id_departemento);
            $("#modal-form-" + idArea).val(response.id_area).change();
            var src = urlBase + response.ruta + response.uuid + '.' + response.extension;
            $('#modal-form-' + idEmpleadoImagen).attr('src', src);

            $("#modal-form-" + calle).val(response.calle);
            $("#modal-form-" + numeroInterior).val(response.numero_interior);
            $("#modal-form-" + numeroExterior).val(response.numero_exterior);
            $("#modal-form-" + colonia).val(response.colonia);
            $("#modal-form-" + cp).val(response.cp);
            $("#modal-form-" + idMunicipio + "_temporal").val(response.id_municipio);
            $("#modal-form-" + idEstado).val(response.id_estado).change();

            $("#modal-form-" + numeroLicencia).val(response.numero_licencia);
            $("#modal-form-" + vigenciaLicencia).val(response.vigencia_licencia);

            $("#modal-form-" + salarioBaseDiario).val(response.salario_base_diario);
            $("#modal-form-" + salarioDiarioIntegrado).val(response.salario_diario_integrado);
            $("#modal-form-" + idPatron).val(response.id_patron).change();
            $("#modal-form-" + idPrimaRiesgo).val(response.id_prima_riesgo).change();
            if (response.viaticos == 1) {
                document.getElementById("modal-form-" + viaticos).checked = true;
            } else {
                document.getElementById("modal-form-" + viaticos).checked = true;
            }
            $("#modal-form-" + diasVacaciones).val(response.dias_vacaciones);

        }
    });

}

/**
 *Función que reinicia el modal-form
 *@author Erik Villarreal
 *@return void
 */
function reiniciarModalForm() {
    $("#modal-form-" + nombre).val("");
    $("#modal-form-" + apellidoPaterno).val("");
    $("#modal-form-" + apellidoMaterno).val("");
    $("#modal-form-" + numeroEmpleado).val("");
    $("#modal-form-" + rfc).val("");
    $("#modal-form-" + curp).val("");
    $("#modal-form-" + nss).val("");
    $("#modal-form-" + telefono).val("");
    $("#modal-form-" + correo).val("");
    $("#modal-form-" + idDepartamento + "_temporal").val("");
    $("#modal-form-" + idArea).val(0).change();
    $('#modal-form-' + idEmpleadoImagen).attr('src', "");

    $("#modal-form-" + calle).val("");
    $("#modal-form-" + numeroInterior).val("");
    $("#modal-form-" + numeroExterior).val("");
    $("#modal-form-" + colonia).val("");
    $("#modal-form-" + cp).val("");
    $("#modal-form-" + idMunicipio + "_temporal").val("");
    $("#modal-form-" + idEstado).val(0).change();

    $("#modal-form-" + numeroLicencia).val("");
    $("#modal-form-" + vigenciaLicencia).val("");

    $("#modal-form-" + salarioBaseDiario).val("");
    $("#modal-form-" + salarioDiarioIntegrado).val("");
    $("#modal-form-" + idPatron).val("").change();
    $("#modal-form-" + idPrimaRiesgo).val(0).change();
    document.getElementById("modal-form-" + viaticos).checked = false;
    $("#modal-form-" + diasVacaciones).val("");
}

/**
 *Evento change para el Preview de la imagen
 *@author Erik Villarreal
 *@param  input #elemento HTML
 *@return void
 */
function verImagenAnetsSubir(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#modal-form-' + idEmpleadoImagen).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/**
 * Metodo que sirve para eliminar un proveedor por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarEmpleado() {

    $.ajax({
        url: 'eliminarEmpleado',
        data: {
            id_empleado: $('#modal-eliminar-' + idEmpleado).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para validar que no sea 0 el valor de los Combos
 * @author Erik Villarreal
 * @returns {boolean}
 */
function validarCombos() {

    if ($("#modal-form-" + idEstado).val() == "" || $("#modal-form-" + idEstado).val() == 0 ||
        $("#modal-form-" + idMunicipio).val() == "" || $("#modal-form-" + idMunicipio).val() == 0) {

        alert("No has seleccionado un Estado o Municipio");

    } else if ($("#modal-file-" + idEmpleadoImagen).val() == "" && ($('#modal-form-' + idEmpleado).val() == "" || $('#modal-form-' + idEmpleado).val() == 0)) {

        alert("No has seleccionado una Imagen");

    } else {
        return true;
    }
    return false;
}

