/**
 * Created by Oscar Vargas on 08/12/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    oTableIncidencias = $('#t_list_incidencias').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [0],
        columnDefs: [
            {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center', 'width': '10%'}
        ]
    });

    oTablePlantilla = $('#t_list_plantilla').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [1],
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    oTableExpediente = $('#t_list_expediente').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlLanguage
        },
        order: [1],
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    /**
    * Metodo que sirve para validar si el ID de la plantilla no va vacío o en 0
    * @author Erik Villarreal
    * @return void
    */
    $( "#form-generar_contrato-save" ).submit(function() {
            if($("#hiddenId").val() == null || $("#hiddenId").val() == "" || 
                $("#hiddenId").val() == undefined || $("#hiddenId").val() == 0){
                return false;
            }
    });

    /**
    *Evento onShow para el modal-vista_previa que permite agregar el src para mostrar la vista
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-vista_previa').on('show.bs.modal', function (e) {

        var src = $(e.relatedTarget).data('src');

        $('#modal-vista_previa_iFrame').attr('src', src);
        $('#modal-vista_previa_blank').attr('href', src);

    });

    /**
    *Evento onHide para el modal-vista_previa que permite reiniciar el src del iFrame
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-vista_previa').on('hide.bs.modal', function (e) {

        $('#modal-vista_previa_iFrame').attr('src', "");
        $('#modal-vista_previa_blank').attr('href', "");

    });

    /**
    *Evento onShow para el modal-subir que permite agregar valores a los hidden del formulario
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-subir').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');
        var idEmp = $(e.relatedTarget).data('id_empleado');
        var idExpDigital = $(e.relatedTarget).data('id_expediente_digital');

        $('#modal-subir-' + idExpedienteDigitalEmpleado).val(id);
        $('#modal-subir-' + idEmpleado).val(idEmp);
        $('#modal-subir-' + idExpedienteDigital).val(idExpDigital);

    });

    /**
    *Evento onHide para el modal-subir que permite reiniciar valores
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-subir').on('hide.bs.modal', function (e) {

        $('#modal-subir-' + idExpedienteDigitalEmpleado).val("");
        $('#modal-subir-' + idEmpleado).val("");
        $('#modal-subir-' + idExpedienteDigital).val("");

    });

});

/**
 *Evento change para el Preview de la imagen
 *@author Erik Villarreal
 *@param  input #elemento HTML
 *@return void
 */
function verImagenAntesSubir(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#modal-subir-vista_previa').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/**
* Metodo que sirve para colocar el id de la Plantilla elegida en un input hidden dentro del form
* @author Erik Villarreal
* @return void
*/
function guardarId(e){
    $("#hiddenId").val(e.id);
}
