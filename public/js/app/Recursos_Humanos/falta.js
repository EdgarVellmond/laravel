/**
 * Created by Oscar Vargas on 01/12/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        order: [1],
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });

    $('.btn').tooltip();

    /**
     * Metodo que se ejecuta al abrirse el modal de guardar falta para poner el id principal de falta
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_falta').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_falta').val() != "") {
            $.ajax({
                url: 'obtenerFalta',
                data: {
                    id_falta: $('#model-guardar .modal-body #id_falta').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #fecha_inicio').val(json.fecha_inicio);
                    $('#model-guardar .modal-body #fecha_fin').val(json.fecha_fin);
                    $('#model-guardar .modal-body #id_empleado').val(json.id_empleado).change();

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la incapacidad
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_falta').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de upload para poner el id principal de la falta
     * @author Oscar Vargas
     * @return void
     */
    $('#model-upload').on('show.bs.modal', function (e) {

        $('#model-upload .modal-body #id_falta').val($(e.relatedTarget).data('id_upload'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de justificar para poner el id principal de la falta
     * @author Oscar Vargas
     * @return void
     */
    $('#model-justificar').on('show.bs.modal', function (e) {

        $('#model-justificar .modal-body #id_falta').val($(e.relatedTarget).data('id_justificar'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de justificar para poner todo el modal resetiado
     * @author Oscar Vargas
     * @return void
     */
    $('#model-justificar').on('hide.bs.modal', function (e) {

        $('#model-justificar .modal-body #id_falta').val("");
        $('#model-justificar .modal-body #comentarios').val("");

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_falta').val("");
        $('#model-guardar .modal-body #fecha_inicio').val("");
        $('#model-guardar .modal-body #fecha_fin').val("");
        $('#model-guardar .modal-body #id_empleado').val(1).change();

    });

    /**
     *Evento onShow para consultar la imagen de la falta y mostrar
     *en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#model-upload").on('show.bs.modal', function (e) {
        console.log($(e.relatedTarget).data('id_upload'));
        $.ajax({
            url: 'obtenerArchivoFalta',
            data: {'id_falta': $(e.relatedTarget).data('id_upload'), '_token': token },
            type: 'POST',
            success : function(json) {

                response = json[0];
                var src = urlBase + response.ruta + response.uuid + "." + response.extension;

                $('#model-upload .modal-body #modal-form-id_falta_archivo').attr('src', src);

            }
        });

    });

    /**
     *Funciona para limpiar el modal de la imagen
     *@author Oscar Vargas
     *@param  e #event JQuery
     *@return void
     */
    $("#model-upload").on('hide.bs.modal', function (e) {

        $('#model-upload .modal-body #modal-form-id_falta_archivo').attr('src', "");

    });

}); // fin document.ready

/**
 * Metodo que sirve para guardar una falta por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarFalta() {

    $.ajax({
        url: 'guardarFalta',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para justificar una falta por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function justificarFalta() {

    $.ajax({
        url: 'justificarFalta',
        data: $('#add_form_justificar').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una falta por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarFalta() {

    $.ajax({
        url: 'eliminarFalta',
        data: {
            id_falta: $('#model-eliminar .modal-body #id_falta').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 *Evento change para el Preview de la imagen
 *@author Oscar Vargas
 *@param  input #elemento HTML
 *@return void
 */
function verImagenAntesSubir(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#modal-form-id_falta_archivo').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}