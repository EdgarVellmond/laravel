/**
 * Created by Oscar Vargas on 29/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        order: [1],
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });

    $('.btn').tooltip();


    /**
     * Metodo que se ejecuta al abrirse el modal de guardar incapacidad para poner el id principal de incapacidad
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_incapacidad').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_incapacidad').val() != "") {
            $.ajax({
                url: 'obtenerIncapacidad',
                data: {
                    id_incapacidad: $('#model-guardar .modal-body #id_incapacidad').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #fecha_inicio').val(json.fecha_inicio);
                    $('#model-guardar .modal-body #fecha_fin').val(json.fecha_fin);
                    $('#model-guardar .modal-body #comentarios').val(json.comentarios);
                    $('#model-guardar .modal-body #tipo_incapacidad').val(json.tipo_incapacidad);
                    $('#model-guardar .modal-body #id_empleado').val(json.id_empleado).change();

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la incapacidad
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_incapacidad').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la incapacidad
     * @author Oscar Vargas
     * @return void
     */
    $('#model-upload').on('show.bs.modal', function (e) {

        $('#model-upload .modal-body #id_incapacidad').val($(e.relatedTarget).data('id_upload'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_incapacidad').val("");
        $('#model-guardar .modal-body #fecha_inicio').val("");
        $('#model-guardar .modal-body #fecha_fin').val("");
        $('#model-guardar .modal-body #comentarios').val("");
        $('#model-guardar .modal-body #id_empleado').val(1).change();

    });

    /**
     *Evento onShow para consultar la imagen de la incapacidad y mostrar
     *en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#model-upload").on('show.bs.modal', function (e) {
        console.log($(e.relatedTarget).data('id_upload'));
        $.ajax({
            url: 'obtenerArchivoIncapacidad',
            data: {'id_incapacidad': $(e.relatedTarget).data('id_upload'), '_token': token },
            type: 'POST',
            success : function(json) {

                response = json[0];
                var src = urlBase + response.ruta + response.uuid + "." + response.extension;

                $('#model-upload .modal-body #modal-form-id_incapacidad_archivo').attr('src', src);

            }
        });

    });

    /**
     *Evento onShow para consultar la imagen de la incapacidad y mostrar
     *en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#model-upload").on('hide.bs.modal', function (e) {

        $('#model-upload .modal-body #modal-form-id_incapacidad_archivo').attr('src', "");

    });

}); // fin document.ready

/**
 * Metodo que sirve para guardar una incapacidad por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarIncapacidad() {

    $.ajax({
        url: 'guardarIncapacidad',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una incapacidad por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarIncapacidad() {

    $.ajax({
        url: 'eliminarIncapacidad',
        data: {
            id_incapacidad: $('#model-eliminar .modal-body #id_incapacidad').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 *Evento change para el Preview de la imagen
 *@author Oscar Vargas
 *@param  input #elemento HTML
 *@return void
 */
function verImagenAntesSubir(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#modal-form-id_incapacidad_archivo').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}