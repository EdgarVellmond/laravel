/**
 * Created by Oscar Vargas on 05/12/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        order: [1],
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.btn').tooltip();


});


 function generarNomina() {

     $.ajax({
         url: 'generarNomina',
         data: $('#add_form').serialize(),
         type: 'POST',
         success: function (message) {

             window.open(message, '_blank');

         }, error: function (message) {
             $('#modal-alert-message-error').modal('show');
             $("#error-message").html(message);
             location.reload();
         }
     });
     return false;
}