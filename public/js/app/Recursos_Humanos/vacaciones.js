/**
 * Created by Oscar Vargas on 30/11/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        order: [1],
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });

    $('.btn').tooltip();


    /**
     * Metodo que se ejecuta al abrirse el modal de guardar vacaciones para poner el id principal de vacaciones
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('show.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_vacaciones').val($(e.relatedTarget).data('id_editar'));

        if ($('#model-guardar .modal-body #id_vacaciones').val() != "") {
            $.ajax({
                url: 'obtenerVacaciones',
                data: {
                    id_vacaciones: $('#model-guardar .modal-body #id_vacaciones').val(),
                    '_token': token
                },
                type: 'POST',
                success: function (json) {

                    $('#model-guardar .modal-body #fecha_inicio').val(json.fecha_inicio);
                    $('#model-guardar .modal-body #fecha_fin').val(json.fecha_fin);
                    $('#model-guardar .modal-body #comentarios').val(json.comentarios);
                    $('#model-guardar .modal-body #id_empleado').val(json.id_empleado).change();

                }
            });
        }

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de vacaciones
     * @author Oscar Vargas
     * @return void
     */
    $('#model-eliminar').on('show.bs.modal', function (e) {

        $('#model-eliminar .modal-body #id_vacaciones').val($(e.relatedTarget).data('id_eliminar'));

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de vacaciones
     * @author Oscar Vargas
     * @return void
     */
    $('#model-upload').on('show.bs.modal', function (e) {

        $('#model-upload .modal-body #id_vacaciones').val($(e.relatedTarget).data('id_upload'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#model-guardar').on('hide.bs.modal', function (e) {

        $('#model-guardar .modal-body #id_vacaciones').val("");
        $('#model-guardar .modal-body #fecha_inicio').val("");
        $('#model-guardar .modal-body #fecha_fin').val("");
        $('#model-guardar .modal-body #comentarios').val("");
        $('#model-guardar .modal-body #id_empleado').val(1).change();

    });

    /**
     *Evento onShow para consultar la imagen de vacaciones y mostrar
     *en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#model-upload").on('show.bs.modal', function (e) {
        console.log($(e.relatedTarget).data('id_upload'));
        $.ajax({
            url: 'obtenerArchivoVacaciones',
            data: {'id_vacaciones': $(e.relatedTarget).data('id_upload'), '_token': token },
            type: 'POST',
            success : function(json) {

                response = json[0];
                var src = urlBase + response.ruta + response.uuid + "." + response.extension;

                $('#model-upload .modal-body #modal-form-id_vacaciones_archivo').attr('src', src);

            }
        });

    });

    /**
     *Evento onShow para consultar la imagen de vacaciones y mostrar
     *en el modal ver-imagen
     *@author Erik Villarreal
     *@param  e #event JQuery
     *@return void
     */
    $("#model-upload").on('hide.bs.modal', function (e) {

        $('#model-upload .modal-body #modal-form-id_vacaciones_archivo').attr('src', "");

    });

}); // fin document.ready

/**
 * Metodo que sirve para guardar una vacaciones por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarVacaciones() {

    $.ajax({
        url: 'guardarVacaciones',
        data: $('#add_form').serialize(),
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una vacaciones por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarVacaciones() {

    $.ajax({
        url: 'eliminarVacaciones',
        data: {
            id_vacaciones: $('#model-eliminar .modal-body #id_vacaciones').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $('#modal-alert-message-success').modal('show');
            $("#success-message").html(message);
            location.reload();
        }, error: function (message) {
            $('#modal-alert-message-error').modal('show');
            $("#error-message").html(message);
        }
    });
    return false;
}

/**
 *Evento change para el Preview de la imagen
 *@author Oscar Vargas
 *@param  input #elemento HTML
 *@return void
 */
function verImagenAntesSubir(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#modal-form-id_vacaciones_archivo').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}