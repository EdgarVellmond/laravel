/**
 * Created by Erik Villarreal at 02/12/2016.
 */


$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    /**
    *Evento onShow para el modal-form que permite agregar o editar una Variable de Sistema
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if(id != "" && id != 0 && id != undefined){
            obtenerCategoriaVariablesSistema(id);
        }

         $('#modal-form-' + idCategoriaVariablesSistema).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de la Categoría de Variables de Sistema
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idCategoriaVariablesSistema).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {

        $('#modal-form-' + nombreCategoriaVariablesSistema).val("");
        $('#modal-form-' + idCategoriaVariablesSistema).val("");

    });

});// final document.ready

/**
* Metodo que sirve para obtener la Categoría de Variables de Sistema
* @author Erik Villarreal
* @return void
*/
function obtenerCategoriaVariablesSistema(id){
    $.ajax({
        url: 'obtenerCategoriaVariablesSistemaEditar',
        data: {
            id_categoria_variables_sistema: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreCategoriaVariablesSistema).val(response.nombre);

        }
    });
}

/**
 * Metodo que sirve para guardar una Categoría Variables de Sistema por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarCategoriaVariablesSistema() {

    $.ajax({
        url: 'guardarCategoriaVariablesSistema',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            alert(message);
            location.reload();
        }, error: function (message) {
            alert(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una Categoría de Variables de Sistema por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarCategoriaVariablesSistema() {

    $.ajax({
        url: 'eliminarCategoriaVariablesSistema',
        data: {
            id_categoria_variables_sistema: $('#modal-eliminar-' + idCategoriaVariablesSistema).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            alert(message);
            location.reload();
        }, error: function (message) {
            alert(message);
        }
    });
    return false;
}