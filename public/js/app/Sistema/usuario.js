/**
 * Created by Oscar Vargas on 14/12/2016.
 */

$(document).ready(function () {

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });



});

function check(e){

    var opcion;

    if( e.checked ) {
        opcion = 1;

    }else{
        opcion: 0;
    }

    $.ajax({
        url: 'movimientoPermiso',
        data: {
            opcion: opcion,
            id_permiso: e.id,
            id_usuario: $('#id_usuario_permiso').val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            $("#"+e.id).removeClass($("#"+e.id).attr("class"));
            $("#"+e.id).addClass(message);


        }, error: function (message) {

        }
    });
}