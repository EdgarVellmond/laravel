/**
 * Created by Erik Villarreal at 26/11/2016.
 */


$(document).ready(function () {

    $('.select2').select2({
        width: '100%',
        class: 'form-control'
    });

    oTable = $('#t_list').dataTable({
        "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
        language: {
            'url': urlTable
        },
        columnDefs: [
            {
                "targets": [0],
                'searchable': false,
                "orderable": false,
                'className': 'text-center ',
                'width': '5%'
            }
        ]
    });

    /**
    *Evento onShow para el modal-form que permite agregar o editar una Variable de Sistema
    *@author Erik Villarreal
    *@param  e #event JQuery
    *@return void
    */
    $('#modal-form').on('show.bs.modal', function (e) {

        var id = $(e.relatedTarget).data('id');

        if(id != "" && id != 0 && id != undefined){
            obtenerVariablesSistema(id);
        }

         $('#modal-form-' + idVariablesSistema).val(id);

    });

    /**
     * Metodo que se ejecuta al abrirse el modal de eliminar para poner el id principal de las Variables de Sistema
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-eliminar').on('show.bs.modal', function (e) {

        $('#modal-eliminar-' + idVariablesSistema).val($(e.relatedTarget).data('id'));

    });

    /**
     * Metodo que se ejecuta al cerrarse el modal de guardar para reiniciar el formulario
     * @author Oscar Vargas
     * @return void
     */
    $('#modal-form').on('hide.bs.modal', function (e) {
        $('#modal-form-' + nombreVariablesSistema).val("");
        $('#modal-form-' + idVariablesSistema).val("");
        $('#modal-form-' + tablaVariablesSistema).val("");
        $('#modal-form-' + campoVariablesSistema).val("");
        $('#modal-form-' + idRolVariablesSistema).val("");
        $('#modal-form-' + idCategoriaVariablesSistema).val(0).change();

    });

});// final document.ready

/**
* Metodo que sirve para obtener la Variable de Sistema
* @author Erik Villarreal
* @return void
*/
function obtenerVariablesSistema(id){
    $.ajax({
        url: 'obtenerVariablesSistemaEditar',
        data: {
            id_variables_sistema: id,
            '_token': token
        },
        type: 'POST',
        success: function (response) {

            $('#modal-form-' + nombreVariablesSistema).val(response.nombre);
            $('#modal-form-' + idCategoriaVariablesSistema).val(response.id_categoria_variables_sistema).change();
            $('#modal-form-' + tablaVariablesSistema).val(response.tabla);
            $('#modal-form-' + aliasTablaVariablesSistema).val(response.alias_tabla);
            $('#modal-form-' + campoVariablesSistema).val(response.campo);
            $('#modal-form-' + idRolVariablesSistema).val(response.id_rol).change();

        }
    });
}

/**
 * Metodo que sirve para guardar una Variable de Sistema por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function guardarVariablesSistema() {

    $.ajax({
        url: 'guardarVariablesSistema',
        data: $('#add_edit_form').serialize(),
        type: 'POST',
        success: function (message) {
            alert(message);
            location.reload();
        }, error: function (message) {
            alert(message);
        }
    });
    return false;
}

/**
 * Metodo que sirve para eliminar una Variable de Sistema por servicio Ajax
 * @author Oscar Vargas
 * @returns {boolean}
 */
function eliminarVariablesSistema() {

    $.ajax({
        url: 'eliminarVariablesSistema',
        data: {
            id_variables_sistema: $('#modal-eliminar-' + idVariablesSistema).val(),
            '_token': token
        },
        type: 'POST',
        success: function (message) {
            alert(message);
            location.reload();
        }, error: function (message) {
            alert(message);
        }
    });
    return false;
}