# Proyecto Seed
Proyecto base para la Generacion de Nuevos Sistemas Basados en Laravel 5.2 con skin solamente para SIOP

- Control de Usuarios
    - Control de Permisos
    - Usuario Principal: administrador
    - Contraseña Principal: 1234
- Control de Menus
    - Pestaña Sistema (Control de Usuarios, Control de Menus, Control de Catalogos)
    
###Requisitos para hacer un nuevo proyecto
- Tener instalado Git
- Base de Datos Normalizada segun el estandar
    - Para el sistema de Cubeta de Catalogos se necesita tener en la tabla los siguientes campos
        - estatus
        - created_at
        - id_usuario_creacion
        - updated_at
        - id_usuario_edicion
        
###Instrucciones Iniciales para un Nuevo Proyecto
- git clone del proyecto a la carpeta destino
- entrar a la carpeta destino y borrar la carpeta ".git"
- abrir consola de comandos dentro de la carpeta del nuevo proyecto
- usar el comando "composer update" y esperar a que se descargue todo
- con base al archivo ".env.example" copiarlo y nombrarlo solo como ".env" (usar el editor para hacer el archivo)
- en el archivo ".env" cambiar la variable "NOMBRE_SISTEMA" al nombre del proyecto nuevo
- cambiar el nombre completo del sistema en el archivo "resources\views\auth\login.blade.php" linea 116 
- cambiar la descripcion del sistema en el archivo "resources\views\auth\login.blade.php" linea 118
- generar la base de datos en mysql
- asignarle las siguientes variables al archivo ".env" 
    - DB_DATABASE=Base_de_Datos_del_Proyecto
    - DB_USERNAME=Usuario_de_Base_de_Datos
    - DB_PASSWORD=Contraseña_de_Base_de_Datos
- en la carpeta del proyecto usar el comando:
    - php artisan key:generate 
- Generar las tablas del sistema usando el comando: 
    - php artisan migrate --seed
- crear el repositorio en bitbucket con el nombre del proyecto nuevo
- cambiar el archivo readme.md para cambiar la informacion principal del proyecto en bitbucket
- en la carpeta del proyecto usar los siguientes comandos en la terminal:
    - git init
    - git add .
    - git commit -m "Commit Inicial"
    - git remote add origin URL_del_repositorio_nuevo_en_bitbucket
    - git push origin master
    - git checkout -b develop
 

###Proyecto Instalado Completamente
- usar el comando "php artisan serve"
- en el navegador accesar a la direccion "http://localhost:8000"
- logearse con las credenciales principales

###Desarrollo del Sistema
- Cada cambio nuevo importante al sistema usar el comando
    - git add .
    - git commit -m "Comentarios del Cambio"
    - git push origin Nombre_del_Branch (principalmente develop)    
- Para generar tablas por medio de migrates usa la funcion (esto ayuda a poder descargar el proyecto y trabajar con lo mas nuevo junto a la base de datos)
    - php artisan make:migration Nombre_del_Archivo_Migrate --create=Nombre_de_la_Tabla
- Si se necesita generar una modificacion de una tabla ya creada generar un migrate de alteracion de tablas con la funcion:
    - php artisan make:migration Nombre_del_Archivo_Migrate --table=Nombre_de_la_Tabla

##Notas
- Si generas tablas de base de datos por medio de migrates al tener la funcion:
    - $table->timestamps();
    //Esto genera los campos
        - created_at
        - updated_at
- Recomendacion: Usar PHPStorm, el IDE te reconoce mucho del sistema interno de laravel
    - se recomienda instalar el plugin "Bootstrap 3" y "Snippet Laravel"