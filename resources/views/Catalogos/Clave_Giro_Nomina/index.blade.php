<?php
use App\Models\Catalogos\Clave_Giro_Nomina_Model;
use App\Http\Helpers\Helpers;

?>

@extends('layouts.listado')

@section('title')
    Clave de Giro de Nómina
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/clave_giro_nomina.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var oTable;

        var idClaveGiroNomina = '{{Clave_Giro_Nomina_Model::$id}}';
        var cuentaClaveGiroNomina = '{{Clave_Giro_Nomina_Model::$cuenta}}';
        var nombreClaveGiroNomina = '{{Clave_Giro_Nomina_Model::$nombre}}';
        var tipoClaveGiroNomina = '{{Clave_Giro_Nomina_Model::$tipoGiro}}';
        var empresaClaveGiroNomina = '{{Clave_Giro_Nomina_Model::$idEmpresa}}';
        var prestacionClaveGiroNomina = '{{Clave_Giro_Nomina_Model::$prestacion}}';
    </script>



@endsection

@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Clave de Giro de Nómina
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.clave_giro_nomina"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Nuevo" data-toggle="modal"
                               data-target="#modal-form"><span class="glyphicon glyphicon-plus"
                                                               aria-hidden="true"></span>Nuevo</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Clave de Giro de Nómina</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acción</th>
                            <th>Cuenta</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Empresa</th>
                            <th>Prestación</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayClaveGiroNomina as $clave_giro_nomina)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.catalogos.clave_giro_nomina"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#modal-form"
                                           data-id="{{ $clave_giro_nomina->{Clave_Giro_Nomina_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-pencil"></span></a>
                                    @endif
                                        @if(Helpers::get_permiso("baja.catalogos.clave_giro_nomina"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#modal-eliminar"
                                           data-id="{{ $clave_giro_nomina->{Clave_Giro_Nomina_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-remove"></span></a>
                                    @endif

                                </td>
                                <td>{{ $clave_giro_nomina->{ Clave_Giro_Nomina_Model::$cuenta } }}</td>
                                <td>{{ $clave_giro_nomina->{ Clave_Giro_Nomina_Model::$nombre } }}</td>
                                <td>{{ $clave_giro_nomina->{ Clave_Giro_Nomina_Model::$tipoGiro } }}</td>
                                <td>{{ $clave_giro_nomina->nombreEmpresa }}</td>
                                <td>{{ $clave_giro_nomina->{ Clave_Giro_Nomina_Model::$prestacion } }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('Catalogos.Clave_Giro_Nomina.modal.form')
@include('Catalogos.Clave_Giro_Nomina.modal.eliminar')