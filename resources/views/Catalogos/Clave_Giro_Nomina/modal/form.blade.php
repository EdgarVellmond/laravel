<?php
use App\Models\Catalogos\Clave_Giro_Nomina_Model;

?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Clave de Giro de Nómina</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarClaveGiroNomina()']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm max300">
                    <span class="input-group-addon">Cuenta</span>
                    <input type="text" name="{{ Clave_Giro_Nomina_Model::$cuenta }}"
                           id="modal-form-{{ Clave_Giro_Nomina_Model::$cuenta }}" class="form-control"
                           placeholder="Cuenta" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300">
                    <span class="input-group-addon">Nombre</span>
                    <input type="text" name="{{ Clave_Giro_Nomina_Model::$nombre }}"
                           id="modal-form-{{ Clave_Giro_Nomina_Model::$nombre }}" class="form-control"
                           placeholder="Nombre" required>
                </div>
                <br>
                <div class="form-input-group input-group-sm">
                    <span class="input-group-addon">Tipo:</span>
                    {!! Form::select( Clave_Giro_Nomina_Model::$tipoGiro , Config::get('enums.tipo_giro'), 0, ['id' => Clave_Giro_Nomina_Model::$tipoGiro, 'class' => "form-control"]) !!}
                </div>
                <br>
                <div class="form-input-group input-group-sm">
                    <span class="input-group-addon">Patron:</span>
                    {!! Form::select( Clave_Giro_Nomina_Model::$idEmpresa , $arrayEmpresa, 0, ['id' => Clave_Giro_Nomina_Model::$idEmpresa, 'class' => "form-control"]) !!}
                </div>
                <br>
                <div class="form-group">
                    <label>Prestación:</label>
                    <input type="checkbox" name="{{ Clave_Giro_Nomina_Model::$prestacion }}"
                           id="modal-form-{{ Clave_Giro_Nomina_Model::$prestacion }}"
                           >
                </div>

                <input type="hidden" name="{{ Clave_Giro_Nomina_Model::$id }}"
                       id="modal-form-{{ Clave_Giro_Nomina_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>