<?php
use App\Models\Catalogos\Cliente_Model;

?>

<div class="modal fade" id="model-ver_credito">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ver Información de Créditos y Pagos</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm">
                    <span class="input-group-addon">¿Tiene Crédito?:</span>
                    <input type="text" name="{{ Cliente_Model::$credito }}" id="{{ Cliente_Model::$credito }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Dias de Crédito:</span>
                    <input type="text" name="{{ Cliente_Model::$diasCredito }}"
                           id="{{ Cliente_Model::$diasCredito }}" class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Límite de Crédito:</span>
                    <input type="text" name="{{ Cliente_Model::$limiteCredito }}"
                           id="{{ Cliente_Model::$limiteCredito }}" class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Banco:</span>
                    <input type="text" name="{{ Cliente_Model::$idBanco }}" id="{{ Cliente_Model::$idBanco }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Forma de Pago:</span>
                    <input type="text" name="{{ Cliente_Model::$idMetodoPago }}"
                           id="{{ Cliente_Model::$idMetodoPago }}" class="form-control"
                           disabled style="background-color: white">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>