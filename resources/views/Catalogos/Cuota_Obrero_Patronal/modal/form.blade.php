<?php
use App\Models\Catalogos\Cuota_Obrero_Patronal_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nueva Cuota Obrero Patronal</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarCuotaObreroPatronal()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="{{ Cuota_Obrero_Patronal_Model::$ramo }}"
                           id="{{ Cuota_Obrero_Patronal_Model::$ramo }}" class="form-control" placeholder="Ramo"
                           required>
                </div>
                <div class="form-group">
                    <label for="nombre">Valor:</label>
                    <input type="text" name="{{ Cuota_Obrero_Patronal_Model::$valor }}"
                           id="{{ Cuota_Obrero_Patronal_Model::$valor }}" class="form-control" placeholder="Valor"
                           required>
                </div>
                <div class="form-group">
                    <label for="nombre">Tipo:</label>
                    {!! Form::select( Cuota_Obrero_Patronal_Model::$tipoCuota , Config::get('enums.tipo_cuota'), 0, ['id' => Cuota_Obrero_Patronal_Model::$tipoCuota, 'class' => "form-control"]) !!}
                </div>

                <input type="hidden" name="{{ Cuota_Obrero_Patronal_Model::$id }}"
                       id="{{ Cuota_Obrero_Patronal_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>