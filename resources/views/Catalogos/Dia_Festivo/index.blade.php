<?php
use App\Models\Catalogos\Dia_Festivo_Model;
use App\Http\Helpers\Helpers;
?>

@extends('layouts.listado')

@section('title')
    Día Festivo
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/dia_festivo.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var oTable;

        var idDiaFestivo = '{{Dia_Festivo_Model::$id}}';
        var nombreDiaFestivo = '{{Dia_Festivo_Model::$nombre}}';
        var fechaDiaFestivo = '{{Dia_Festivo_Model::$fecha}}';
    </script>



@endsection

@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Día Festivo
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.dia_festivo"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Nuevo" data-toggle="modal"
                               data-target="#modal-form"><span class="glyphicon glyphicon-plus"
                                                                  aria-hidden="true"></span>Nuevo</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Días festivos</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acción</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayDiaFestivo as $dia_festivo)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.catalogos.dia_festivo"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#modal-form"
                                           data-id="{{ $dia_festivo->{Dia_Festivo_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-pencil" ></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.catalogos.dia_festivo"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#modal-eliminar"
                                           data-id="{{ $dia_festivo->{Dia_Festivo_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-remove" ></span></a>
                                    @endif

                                </td>

                                <td>{{ $dia_festivo->{ Dia_Festivo_Model::$nombre } }}</td>
                                <td>{{ $dia_festivo->{ Dia_Festivo_Model::$fecha } }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('Catalogos.Dia_Festivo.modal.form')
@include('Catalogos.Dia_Festivo.modal.eliminar')