<?php
use App\Models\Catalogos\Dia_Festivo_Model;
?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Puesto</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarDiaFestivo()']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Nombre</span>
                    <input type="text" name="{{ Dia_Festivo_Model::$nombre }}" id="modal-form-{{ Dia_Festivo_Model::$nombre }}" class="form-control" placeholder="Nombre" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Fecha</span>
                    <input type="date" name="{{ Dia_Festivo_Model::$fecha }}" id="modal-form-{{ Dia_Festivo_Model::$fecha }}" class="form-control" placeholder="Nombre" required>
                </div>

                <input type="hidden" name="{{ Dia_Festivo_Model::$id }}" id="modal-form-{{ Dia_Festivo_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>