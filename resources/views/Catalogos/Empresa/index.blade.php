<?php
use App\Models\Catalogos\Cubeta_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Empresa_Imagen_Model;
use App\Http\Helpers\Helpers;

?>
@extends('layouts.listado')

@section('title')
    Catálogo Empresa
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/Empresa/empresa.js') !!}

    <script type="text/javascript">
        var oTable;
        var token = '{{ csrf_token() }}';
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";

        var idEmpresa = "{{Empresa_Model::$id}}";
        var calle = "{{Empresa_Model::$calle}}";
        var numeroExterior = "{{Empresa_Model::$numeroExterior}}";
        var numeroInterior = "{{Empresa_Model::$numeroInterior}}";
        var colonia = "{{Empresa_Model::$colonia}}";
        var cp = "{{Empresa_Model::$cp}}";
        var idMunicipio = "{{Empresa_Model::$idMunicipio}}";
        var idEstado = "{{Municipio_Model::$idEstado}}";
        var idEmpresaImagen = "{{Empresa_Model::$idEmpresaImagen}}";
        var clave = "{{Empresa_Model::$clave}}";
        var rfc = "{{Empresa_Model::$rfc}}";
        var correo = "{{Empresa_Model::$correo}}";
        var razonSocial = "{{Empresa_Model::$razonSocial}}";
        var patron = "{{Empresa_Model::$patron}}";
        var operacion = "{{Empresa_Model::$operacion}}";
        var tipoNomina = "{{Empresa_Model::$tipoNomina}}";
        var urlBase = "{{url('/')}}/";
    </script>

@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Empresa
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.empresa"))
                            <a href="#" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-id=""
                               data-target="#modal-form">
                                <span class="glyphicon glyphicon-plus"></span>
                                Nuevo
                            </a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Empresa</h3>
                </div>
                <div class="panel-body">

                    <table class="table table-hover table-bordered small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>id</th>
                            <th>Clave</th>
                            <th>RFC</th>
                            <th>Correo</th>
                            <th>Razón social</th>
                            <th>Dirección</th>
                            <th>Patrón</th>
                            <th>Operación</th>
                            <th>Tipo de Nómina</th>
                            <th>Imagen</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($empresas as $row)
                            <tr>
                                <td>
                                    @if(Helpers::get_permiso("edicion.catalogos.empresa"))
                                        <a href="#" class="btn btn-xs btn-success" data-toggle="modal"
                                           data-target="#modal-form" data-id="{{$row->{Empresa_Model::$id} }}">
                                            <span class="glyphicon glyphicon-pencil"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.catalogos.empresa"))
                                        <a href="#" class="btn btn-xs btn-danger" data-toggle="modal"
                                           data-target="#modal-eliminar" data-id="{{$row->{Empresa_Model::$id} }}">
                                            <span class="glyphicon glyphicon-remove"></span></a>
                                    @endif
                                </td>
                                <td>{{$row->{Empresa_Model::$id} }}</td>
                                <td>{{$row->{Empresa_Model::$clave} }}</td>
                                <td>{{$row->{Empresa_Model::$rfc} }}</td>
                                <td>{{$row->{Empresa_Model::$correo} }}</td>
                                <td>{{$row->{Empresa_Model::$razonSocial} }}</td>
                                <td>
                                        <a href="#" class="btn btn-xs btn-info" data-toggle="modal"
                                           data-target="#modal-ver-direccion" data-id="{{$row->{Empresa_Model::$id} }}">
                                            <span class="glyphicon glyphicon-home"></span></a>
                                </td>
                                <td>
                                    @if($row->{Empresa_Model::$patron} == 1)
                                        <input type="checkbox" checked disabled>
                                    @else
                                        <input type="checkbox" disabled>
                                    @endif
                                </td>
                                <td>
                                    @if($row->{Empresa_Model::$operacion} == 1)
                                        <input type="checkbox" checked disabled>
                                    @else
                                        <input type="checkbox" disabled>
                                    @endif
                                </td>
                                <td>{{$row->{Empresa_Model::$tipoNomina} }}</td>
                                <td>
                                        @if( url($row->{Empresa_Imagen_Model::$ruta}) != "")
                                            <div class="thumbnail"
                                                 data-id="{{$row->{Empresa_Model::$idEmpresaImagen} }}"
                                                 data-toggle="modal" style="cursor: pointer;"
                                                 data-target="#modal-ver-imagen">
                                                <img id="{{$row->{Empresa_Model::$idEmpresaImagen} }}"
                                                     src="{{ url($row->{Empresa_Imagen_Model::$ruta} . $row->{Empresa_Imagen_Model::$uuid} . '.' .$row->{Empresa_Imagen_Model::$extension} )}} "
                                                     alt="" class="img-responsive " alt="" width="50" height="50"/>
                                            </div>
                                        @else
                                            No se ha agregado imagen
                                        @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    @include("Catalogos.Empresa.modal.form", ["estado", $estado])
    @include("Catalogos.Empresa.modal.ver_direccion")
    @include("Catalogos.Empresa.modal.ver_imagen")
    @include("Catalogos.Empresa.modal.eliminar")

@endsection