<?php
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Municipio_Model;

?>
<style type="text/css">
    .max300 {
        max-width: 300px;
    }
</style>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Empresa</h4>
            </div>
            {!! Form::open(['id'=>'add_edit_form', 'url' => url('catalogos/guardarEmpresa'),'role' => 'form', 'class' => 'form', 'enctype'=>'multipart/form-data', 'file'=>true, 'onSubmit' => 'return validarCombos()']) !!}
            <div class="modal-body">

                <table class="tabla">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Imagen</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <table class="tabla" id="t_list">
                                <thead>
                                <tr>
                                    <th>General</th>
                                    <th></th>
                                    <th>Dirección</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">Clave</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$clave}}"
                                                   name="{{Empresa_Model::$clave}}" style="background-color: white"
                                                   required>
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">Calle</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$calle}}"
                                                   name="{{Empresa_Model::$calle}}" style="background-color: white"
                                                   required>
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">RFC</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$rfc}}"
                                                   name="{{Empresa_Model::$rfc}}" style="background-color: white"
                                                   required>
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">N° Exterior</span>
                                            <input type="number" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$numeroExterior}}"
                                                   name="{{Empresa_Model::$numeroExterior}}"
                                                   style="background-color: white" required>
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">Correo</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$correo}}"
                                                   name="{{Empresa_Model::$correo}}" style="background-color: white"
                                                   required>
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">N° Interior</span>
                                            <input type="number" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$numeroInterior}}"
                                                   name="{{Empresa_Model::$numeroInterior}}"
                                                   style="background-color: white" >
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">Razón Social</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$razonSocial}}"
                                                   name="{{Empresa_Model::$razonSocial}}"
                                                   style="background-color: white" required>
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">Colonia</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$colonia}}"
                                                   name="{{Empresa_Model::$colonia}}" style="background-color: white"
                                                   required>
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">¿Es patrón?</span>
                                            <span class="input-group-addon">
                                                            <input type="checkbox"
                                                                   id="modal-form-{{Empresa_Model::$patron}}"
                                                                   name="{{Empresa_Model::$patron}}" class=""><br>
                                                        </span>
                                            <input type="text" style="background-color: white;" class="form-control"
                                                   disabled name="">
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">C.P.</span>
                                            <input type="text" value="" placeholder="" class="form-control"
                                                   id="modal-form-{{Empresa_Model::$cp}}" name="{{Empresa_Model::$cp}}"
                                                   style="background-color: white" required>
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon">¿Tiene operación?</span>
                                            <span class="input-group-addon">
                                                            <input type="checkbox"
                                                                   id="modal-form-{{Empresa_Model::$operacion}}"
                                                                   name="{{Empresa_Model::$operacion}}" class=""><br>
                                                        </span>
                                            <input type="text" style="background-color: white;" class="form-control"
                                                   disabled name="">
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">Estado</span>
                                            {!! Form::select( Estado_Model::$id , $estado, 0, ['id' => "modal-form-" . Estado_Model::$id, 'class' => "form-control select2"]) !!}
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">Tipo Nómina</span>
                                            {!! Form::select( Empresa_Model::$tipoNomina , Config::get('enums.tipo_nomina'), 0, ['id' => "modal-form-" . Empresa_Model::$tipoNomina, 'class' => "form-control"]) !!}
                                        </div>
                                        <br>
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="input-group input-group-sm max300">
                                            <span class="input-group-addon">Municipio</span>
                                            <select id="modal-form-{{Municipio_Model::$id}}"
                                                    name="{{Municipio_Model::$id}}" class="form-control select2"
                                                    required>
                                                <option value="0">Seleccionar municipio</option>
                                            </select>
                                            <input type="hidden" id="modal-form-{{Municipio_Model::$id}}_temporal">
                                        </div>
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            <div class="thumbnail" style="min-width: 250px; max-width: 300px;">
                                <img id="modal-form-{{Empresa_Model::$idEmpresaImagen}}" src="" alt=""
                                     class="img-responsive " id='imagen' alt=""/>
                            </div>
                            <div class="input-group input-group-sm" style="max-width: 250px;">
                                <span class="input-group-addon">Imagen</span>
                                <input type="file" class="form-control" placeholder=""
                                       id="modal-file-{{Empresa_Model::$idEmpresaImagen}}"
                                       name="{{Empresa_Model::$idEmpresaImagen}}" onchange='verImagenAnetsSubir(this);'
                                       style="background-color: white;">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <input type="hidden" name="{{Empresa_Model::$id}}" id="modal-form-{{Empresa_Model::$id}}">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>