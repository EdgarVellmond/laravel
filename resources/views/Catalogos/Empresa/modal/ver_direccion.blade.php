<?php
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;

?>

<style type="text/css">
    .tabla {
        width: 100%;
        text-align: center;
    }

    .encabezado {
        text-align: left;
    }
</style>

<div class="modal fade" id="modal-ver-direccion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dirección de la Empresa</h4>
            </div>
            <div class="modal-body">

                <table class="tabla" id="t_list">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Calle</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Empresa_Model::$calle}}" disabled style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">C.P.</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Empresa_Model::$cp}}" disabled style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">N° Exterior</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Empresa_Model::$numeroExterior}}" disabled style="background-color: white"
                                       required>
                            </div>
                            <br>
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Municipio</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Empresa_Model::$idMunicipio}}" disabled style="background-color: white"
                                       required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">N° Interior</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Empresa_Model::$numeroInterior}}" disabled style="background-color: white"
                                       required>
                            </div>
                            <br>
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Estado</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Municipio_Model::$idEstado}}" disabled style="background-color: white"
                                       required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Colonia</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="{{Empresa_Model::$colonia}}" disabled style="background-color: white"
                                       required>
                            </div>
                            <br>
                        </td>
                        <td>&nbsp;&nbsp;</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->