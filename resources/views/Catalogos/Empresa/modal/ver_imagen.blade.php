<?php
use App\Models\Catalogos\Empresa_Model;

?>

<style type="text/css">
    .tabla {
        width: 100%;
        text-align: center;
    }

    .encabezado {
        text-align: left;
    }
</style>

<div class="modal fade" id="modal-ver-imagen">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dirección de la Empresa</h4>
            </div>
            <div class="modal-body">
                <img id="modal-{{Empresa_Model::$idEmpresaImagen}}" src="" style="width: 100%; height: auto;">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>