<?php
use App\Models\Catalogos\Expediente_Digital_Model;

?>

<div class="modal fade" id="modal-eliminar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Eliminar Expediente Digital</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form', 'onSubmit' => 'return eliminarExpedienteDigital()']) !!}
            <div class="modal-body">

                <label>¿ Estás seguro que deseas eliminar este Expediente ?</label>
                <input type="hidden" name="{{ Expediente_Digital_Model::$id  }}"
                       id="modal-eliminar-{{ Expediente_Digital_Model::$id  }}">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>