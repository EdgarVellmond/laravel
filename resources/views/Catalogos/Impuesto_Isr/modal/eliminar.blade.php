<?php
use App\Models\Catalogos\Impuesto_Isr_Model;

?>

<div class="modal fade" id="model-eliminar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Eliminar Impuesto ISR</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form', 'onSubmit' => 'return eliminarImpuestoIsr()']) !!}
            <div class="modal-body">

                <label>¿ Estás seguro que deseas eliminar este Impuesto ISR ?</label>
                <input type="hidden" name="{{ Impuesto_Isr_Model::$id  }}" id="{{ Impuesto_Isr_Model::$id  }}">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>