<?php
use App\Models\Catalogos\Impuesto_Isr_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Impuesto ISR</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarImpuestoIsr()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Límite Inferior:</label>
                    <input type="text" name="{{ Impuesto_Isr_Model::$limiteInferior }}"
                           id="{{ Impuesto_Isr_Model::$limiteInferior }}" class="form-control"
                           placeholder="Impuesto Inferior" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Límite Superior:</label>
                    <input type="text" name="{{ Impuesto_Isr_Model::$limiteSuperior }}"
                           id="{{ Impuesto_Isr_Model::$limiteSuperior }}" class="form-control"
                           placeholder="Impuesto Superior" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Porcentaje:</label>
                    <input type="text" name="{{ Impuesto_Isr_Model::$porcentaje }}"
                           id="{{ Impuesto_Isr_Model::$porcentaje }}" class="form-control" placeholder="Porcentaje"
                           required>
                </div>
                <div class="form-group">
                    <label for="nombre">Cuota Fija:</label>
                    <input type="text" name="{{ Impuesto_Isr_Model::$cuotaFija }}"
                           id="{{ Impuesto_Isr_Model::$cuotaFija }}" class="form-control" placeholder="Cuota Fija"
                           required>
                </div>
                <div class="form-group">
                    <label for="nombre">Tipo Nómina:</label>
                    {!! Form::select( Impuesto_Isr_Model::$tipoNomina , Config::get('enums.tipo_nomina'), 0, ['id' => Impuesto_Isr_Model::$tipoNomina, 'class' => "form-control"]) !!}
                </div>


                <input type="hidden" name="{{ Impuesto_Isr_Model::$id }}" id="{{ Impuesto_Isr_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>