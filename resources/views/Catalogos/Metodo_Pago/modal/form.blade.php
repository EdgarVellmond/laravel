<?php
use App\Models\Catalogos\Metodo_Pago_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Método de Pago</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarMetodoPago()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="{{ Metodo_Pago_Model::$nombre }}" id="{{ Metodo_Pago_Model::$nombre }}"
                           class="form-control" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Tipo Pago:</label>
                    {!! Form::select( Metodo_Pago_Model::$tipoPago , Config::get('enums.tipo_pago'), 0, ['id' => Metodo_Pago_Model::$tipoPago, 'class' => "form-control"]) !!}
                </div>
                <div class="form-group">
                    <label for="nombre">Forma Pago:</label>
                    {!! Form::select( Metodo_Pago_Model::$formaPago , Config::get('enums.forma_pago'), 0, ['id' => Metodo_Pago_Model::$formaPago, 'class' => "form-control"]) !!}
                </div>

                <input type="hidden" name="{{ Metodo_Pago_Model::$id }}" id="{{ Metodo_Pago_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>