<?php
use App\Models\Catalogos\Municipio_Model;

?>

<div class="modal fade" id="model-eliminar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Eliminar Estado</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form', 'onSubmit' => 'return eliminarMunicipio()']) !!}
            <div class="modal-body">

                <label>¿ Estás seguro que deseas eliminar este Municipio ?</label>
                <input type="hidden" name="{{ Municipio_Model::$id  }}" id="{{ Municipio_Model::$id  }}">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>