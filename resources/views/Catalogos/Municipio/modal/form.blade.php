<?php
use App\Models\Catalogos\Municipio_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Municipio</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarMunicipio()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="{{ Municipio_Model::$nombre }}" id="{{ Municipio_Model::$nombre }}"
                           class="form-control" placeholder="Nombre" required>
                </div>
                <div class="form-group">
                    <label>Estado:</label>
                    {!! Form::select( "id_estado" , $arrayEstado, 0, ['id' => 'id_estado', 'class' => "form-control select2"]) !!}
                </div>

                <input type="hidden" name="{{ Municipio_Model::$id }}" id="{{ Municipio_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>