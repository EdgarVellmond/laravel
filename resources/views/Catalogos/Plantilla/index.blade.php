<?php
use App\Models\Catalogos\Plantilla_Model;
use App\Models\Sistema\Variables_Sistema_Model;
use App\Http\Helpers\Helpers;
?>
@extends('layouts.listado')

@section('title')
    Catálogo de Plantilla
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
    <style type="text/css">
        td,th{
            text-align: center;
        }
    </style>
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/plantilla.js') !!}

    <script type="text/javascript">
        var oTable;
        var tablaVariables;
        var token = '{{ csrf_token() }}';
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";

        @if(isset($idPlantilla))
            var idPlantillaEditar = {{ $idPlantilla }};
        @else
            var idPlantillaEditar = 0;
        @endif
        
        var idPlantilla = "{{ Plantilla_Model::$id }}";
        var nombrePlantilla = "{{ Plantilla_Model::$nombre }}";
        var tablaOrigen = "{{ Variables_Sistema_Model::$tablaOrigen }}";
        var idTipoPlantilla = "{{ Plantilla_Model::$idTipoPlantilla }}";
        var contenidoPlantilla = "{{ Plantilla_Model::$contenido }}";

        var urlBase = "{{url('/')}}/";
    </script>

@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Plantillas
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.plantilla"))
                            <a href="nuevaPlantilla" type="button" class="btn btn-success btn-xs">
                                <span class="glyphicon glyphicon-plus"></span> 
                                Nuevo
                            </a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Plantilla</h3>
                </div>
                <div class="panel-body">
                    
                    <table class="table table-hover table-bordered small" id="t_list">
                        <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>id</th>
                                <th>Nombre</th>
                                <th>Tipo de Plantilla</th>
                                <th>Vista previa</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($plantilla as $row)
                            <tr>
                                <td>
                                    @if(Helpers::get_permiso("edicion.catalogos.plantilla"))
                                        <a href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modal-form" data-id="{{$row->{Plantilla_Model::$id} }}">
                                        <span class="glyphicon glyphicon-pencil"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.catalogos.plantilla"))
                                        <a href="#" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-eliminar" data-id="{{$row->{Plantilla_Model::$id} }}">
                                        <span class="glyphicon glyphicon-remove"></span></a>
                                    @endif
                                </td>
                                <td>{{$row->{Plantilla_Model::$id} }}</td>
                                <td>{{$row->{Plantilla_Model::$nombre} }}</td>
                                <td>{{$row->{Plantilla_Model::$idTipoPlantilla} }}</td>
                                <td>
                                        <a href="#" data-src = "vistaPrevia/{{$row->{Plantilla_Model::$id} }}" data-toggle="modal" class="btn btn-xs btn-info" data-target="#modal-vista_previa">
                                        <span class="glyphicon glyphicon-eye-open"></span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@include("Catalogos.Plantilla.modal.form")
@include("Catalogos.Plantilla.modal.vista_previa")
@include("Catalogos.Plantilla.modal.eliminar")
    
@endsection