<?php
use App\Models\Catalogos\Plantilla_Model;
?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Plantilla</h4>
            </div>

            <div class="modal-body">
            @if(!isset($idPlantilla))
            {!! Form::open(['url' => 'catalogos/editarPlantilla','class'=>'form', 'method'=> 'POST']) !!}
            
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Contenido</span>
                    <button type="submit" class="btn " style="height: 30px; border: 2px solid lightgray; background-color: white; width: 100%; color: black">
                            <span class="glyphicon glyphicon-plus"></span> 
                            Editar Contenido
                    </button>
                </div>

                <input type="hidden" name="{{ Plantilla_Model::$id }}" id="modal-form-editar-{{ Plantilla_Model::$id }}" value="0">

            {!!Form::close()!!}
            @endif

            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarPlantilla()']) !!}
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Nombre</span>
                    <input type="text" name="{{ Plantilla_Model::$nombre }}" id="modal-form-{{ Plantilla_Model::$nombre }}" class="form-control" placeholder="Nombre" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Tipo de plantilla</span>
                    {!! Form::select( Plantilla_Model::$idTipoPlantilla , $tipoPlantilla, 0, ['id' => "modal-form-" . Plantilla_Model::$idTipoPlantilla, 'class' => "form-control select2"]) !!}
                </div>
                
                
                <input type="hidden" name="{{ Plantilla_Model::$id }}" id="modal-form-{{ Plantilla_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>