<?php
use App\Models\Catalogos\Plantilla_Model;
use App\Models\Sistema\Variables_Sistema_Model;
use App\Http\Helpers\Helpers;
?>
@extends('layouts.listado')

@section('title')
    Catálogo Plantilla
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
    <style type="text/css">
        table{
            max-height: 500px;
            scroll-behavior: auto;
        }

        td,th{
            text-align: center;
        }
        td.details-control {
            background: url('../images/details_open.png') no-repeat center center;
            cursor: pointer;
        }
    </style>
    
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/plantilla.js') !!}

    <script type="text/javascript">
        var oTable;
        var tablaVariables;
        var token = '{{ csrf_token() }}';
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";

        @if(isset($idPlantilla))
            var idPlantillaEditar = {{ $idPlantilla }};
        @else
            var idPlantillaEditar = 0;
        @endif
        
        var idPlantilla = "{{ Plantilla_Model::$id }}";
        var nombrePlantilla = "{{ Plantilla_Model::$nombre }}";
        var tablaOrigen = "{{ Variables_Sistema_Model::$tablaOrigen }}";
        var idTipoPlantilla = "{{ Plantilla_Model::$idTipoPlantilla }}";
        var contenidoPlantilla = "{{ Plantilla_Model::$contenido }}";

        var urlBase = "{{url('/')}}/";
        var urlVariablesSistema = "{{url('/catalogos/obtenerVariablesSistema')}}/";

        
    </script>

@endsection
@section('content')
        
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="position: relative;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.plantilla"))
                            <a href="#" type="button" data-toggle="modal" data-target="#modal-form" class="btn btn-success btn-xs">
                                <span class="glyphicon glyphicon-plus"></span> 
                                Guardar
                            </a>
                        @endif
                    </div>
                    <h3 class="panel-title">Nueva plantilla</h3>
                </div>
                <div class="panel-body">
                    
                    <div class="input-group input-group-sm max300" >
                        <span class="input-group-addon">Categoría</span>
                        {!! Form::select( Variables_Sistema_Model::$tablaOrigen , $arrayTablas, 0, ['id' => Variables_Sistema_Model::$tablaOrigen, 'class' => "form-control select2"]) !!}
                    </div>
                    <br>
                    <table class="table table-hover table-bordered small" id="t_list_variables" focusable="false">
                        <thead>
                            <tr>
                                <th>Agregar</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div id="summernote" focusable="false"></div>
        </div>

    </div>

@include("Catalogos.Plantilla.modal.form")
    
@endsection