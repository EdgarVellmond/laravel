<?php
use App\Models\Catalogos\Prima_Riesgo_Model;
use App\Http\Helpers\Helpers;

?>

@extends('layouts.listado')

@section('title')
    Prima Riesgo
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/prima_riesgo.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
        var oTable;
    </script>



@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}">Principal</a></li>
        <li class="active">Catálogo de Prima Riesgo</li>
    </ol>
@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Prima Riesgo
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.prima_riesgo"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar" data-toggle="modal"
                               data-target="#model-guardar"><span class="glyphicon glyphicon-plus"
                                                                  aria-hidden="true"></span> Nueva Prima Riesgo</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Prima Riesgo</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>Nombre</th>
                            <th>Valor</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayPrima as $prima)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.catalogos.prima_riesgo"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#model-guardar"
                                           data-id_editar="{{ $prima->getAttribute(Prima_Riesgo_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.catalogos.prima_riesgo"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#model-eliminar"
                                           data-id_eliminar="{{ $prima->getAttribute(Prima_Riesgo_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    @endif

                                </td>

                                <td>{{ $prima->{ Prima_Riesgo_Model::$nombre } }}</td>
                                <td>{{ $prima->{ Prima_Riesgo_Model::$valor } }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('Catalogos.Prima_Riesgo.modal.form')
@include('Catalogos.Prima_Riesgo.modal.eliminar')