<?php
use App\Models\Catalogos\Proveedor_Model;
use App\Http\Helpers\Helpers;

?>

@extends('layouts.listado')

@section('title')
    Proveedores
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Catalogos/Proveedor/proveedor.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
        var oTable;
    </script>

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}">Principal</a></li>
        <li class="active">Catálogo de Proveedor</li>
    </ol>
@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Proveedor
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.catalogos.proveedor"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar" data-toggle="modal"
                               data-target="#model-nuevo_proveedor"><span class="glyphicon glyphicon-plus"
                                                                          aria-hidden="true"></span> Nuevo Proveedor</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Proveedor</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>No. Proveedor</th>
                            <th>Nombre</th>
                            <th>RFC</th>
                            <th>Teléfono</th>
                            <th>Correo</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayProveedor as $proveedor)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.catalogos.proveedor"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#model-nuevo_proveedor"
                                           data-id_editar_proveedor="{{ $proveedor->getAttribute(Proveedor_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.catalogos.proveedor"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#model-eliminar_proveedor"
                                           data-id_eliminar_proveedor="{{ $proveedor->getAttribute(Proveedor_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    @endif
                                    <a href="#" type="button" class="btn btn-info btn-xs" title="Ver Domicilio"
                                       data-toggle="modal" data-target="#model-ver_direccion"
                                       data-id="{{ $proveedor->{Proveedor_Model::$id} }}"><span
                                                class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
                                        @if(Helpers::get_permiso("credito.catalogos.proveedor"))
                                    <a href="#" type="button" class="btn btn-warning btn-xs" title="Pagos y Crédito"
                                       data-toggle="modal" data-target="#model-ver_credito"
                                       data-id_proveedor_credito="{{ $proveedor->{Proveedor_Model::$id} }}"><span
                                                class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                        @endif
                                </td>
                                <td>{{ $proveedor->{ Proveedor_Model::$numeroProveedor } }}</td>
                                <td>{{ $proveedor->{ Proveedor_Model::$nombre }." ".$proveedor->{Proveedor_Model::$apellidoPaterno}." ".$proveedor->{Proveedor_Model::$apellidoMaterno}  }}</td>
                                <td>{{ $proveedor->{ Proveedor_Model::$rfc } }}</td>
                                <td>{{ $proveedor->{ Proveedor_Model::$telefono } }}</td>
                                <td>{{ $proveedor->{ Proveedor_Model::$correo } }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include("Catalogos.Proveedor.modal.nuevo")
    @include("Catalogos.Proveedor.modal.ver_domicilio")
    @include("Catalogos.Proveedor.modal.ver_credito")
    @include("Catalogos.Proveedor.modal.eliminar")

@endsection