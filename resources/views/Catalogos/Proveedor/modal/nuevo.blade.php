<?php
use App\Models\Catalogos\Proveedor_Model;

?>
<style type="text/css">

    .tabla {
        width: 100%;
    }

    .tabla th {
        text-align: center;
    }

</style>
<div class="modal fade" id="model-nuevo_proveedor">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Proveedor</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarProveedor()']) !!}
            <div class="modal-body">
                <table class="tabla">
                    <thead>
                    <tr>
                        <th><h4>Datos Personales</h4></th>
                        <th></th>
                        <th><h4>Dirección</h4></th>
                        <th></th>
                        <th><h4>Crédito y Pagos</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" name="{{ Proveedor_Model::$nombre }}"
                                       id="{{ Proveedor_Model::$nombre }}" class="form-control" placeholder="Nombre"
                                       required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Calle:</label>
                                <input type="text" name="{{ Proveedor_Model::$calle }}"
                                       id="{{ Proveedor_Model::$calle }}" class="form-control" placeholder="Calle"
                                       required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Crédito:</label>
                                <input type="checkbox" name="{{ Proveedor_Model::$credito }}"
                                       id="{{ Proveedor_Model::$credito }}">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Apellido Paterno:</label>
                                <input type="text" name="{{ Proveedor_Model::$apellidoPaterno }}"
                                       id="{{ Proveedor_Model::$apellidoPaterno }}" class="form-control"
                                       placeholder="Apellido Paterno">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Colonia:</label>
                                <input type="text" name="{{ Proveedor_Model::$colonia }}"
                                       id="{{ Proveedor_Model::$colonia }}" class="form-control" placeholder="Colonia"
                                       required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Días de Crédito:</label>
                                <input type="number" name="{{ Proveedor_Model::$diasCredito }}"
                                       id="{{ Proveedor_Model::$diasCredito }}" class="form-control"
                                       placeholder="Días de crédito">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Apellido Materno:</label>
                                <input type="text" name="{{ Proveedor_Model::$apellidoMaterno }}"
                                       id="{{ Proveedor_Model::$apellidoMaterno }}" class="form-control"
                                       placeholder="Apellido Materno">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Número Interior:</label>
                                <input type="text" name="{{ Proveedor_Model::$numeroInterior }}"
                                       id="{{ Proveedor_Model::$numeroInterior }}" class="form-control"
                                       placeholder="Número interior">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Límite de Crédito:</label>
                                <input type="number" name="{{ Proveedor_Model::$limiteCredito }}"
                                       id="{{ Proveedor_Model::$limiteCredito }}" class="form-control"
                                       placeholder="Límite de crédito">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>RFC:</label>
                                <input type="text" name="{{ Proveedor_Model::$rfc }}" id="{{ Proveedor_Model::$rfc }}"
                                       class="form-control" placeholder="RFC" required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Número exterior:</label>
                                <input type="text" name="{{ Proveedor_Model::$numeroExterior }}"
                                       id="{{ Proveedor_Model::$numeroExterior }}" class="form-control"
                                       placeholder="Número exterior" required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Método Pago:</label>
                                {!! Form::select( Proveedor_Model::$idMetodoPago , $arrayMetodoPago, 0, ['id' => Proveedor_Model::$idMetodoPago, 'class' => "form-control select2"]) !!}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Tipo Persona:</label>
                                {!! Form::select( Proveedor_Model::$tipoPersona , Config::get('enums.tipo_persona'), 0, ['id' => Proveedor_Model::$tipoPersona, 'class' => "form-control"]) !!}
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Código Postal:</label>
                                <input type="number" name="{{ Proveedor_Model::$cp }}" id="{{ Proveedor_Model::$cp }}"
                                       class="form-control" placeholder="Código Postal" required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Banco:</label>
                                {!! Form::select( Proveedor_Model::$idBanco , $arrayBanco, 0, ['id' => Proveedor_Model::$idBanco, 'class' => "form-control select2"]) !!}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Teléfono:</label>
                                <input type="text" name="{{ Proveedor_Model::$telefono }}"
                                       id="{{ Proveedor_Model::$telefono }}" class="form-control"
                                       placeholder="Teléfono">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Estado:</label>
                                {!! Form::select( "id_estado" , $arrayEstado, 0, ['id' => 'id_estado', 'class' => "form-control select2"]) !!}
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Correo:</label>
                                <input type="email" name="{{ Proveedor_Model::$correo }}"
                                       id="{{ Proveedor_Model::$correo }}" class="form-control" placeholder="Correo">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Municipio:</label>
                                <select class="form-control select2" id="{{ Proveedor_Model::$idMunicipio }}"
                                        name="{{ Proveedor_Model::$idMunicipio }}">
                                    <option value="0">N/D</option>
                                </select>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td></td>
                    </tr>
                    <input type="hidden" name="id_proveedor" id="id_proveedor" value="0">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>