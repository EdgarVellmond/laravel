<?php
use App\Models\Catalogos\Salario_Minimo_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Salario Mínimo</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarSalarioMinimo()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Año:</label>
                    <input type="text" name="{{ Salario_Minimo_Model::$anio }}" id="{{ Salario_Minimo_Model::$anio }}"
                           class="form-control" placeholder="Año" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Valor:</label>
                    <input type="text" name="{{ Salario_Minimo_Model::$valor }}" id="{{ Salario_Minimo_Model::$valor }}"
                           class="form-control" placeholder="Valor actual" required>
                </div>

                <input type="hidden" name="{{ Salario_Minimo_Model::$id }}" id="{{ Salario_Minimo_Model::$id }}"
                       value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>