<?php
use App\Models\Catalogos\Sucursal_Model;

?>
<style type="text/css">

    .tabla {
        width: 100%;
    }

    .tabla th {
        text-align: center;
    }

</style>
<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nueva Sucursal</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form_sucursal','onSubmit' => 'return guardarSucursal()']) !!}
            <div class="modal-body">
                <table class="tabla">
                    <thead>
                    <tr>
                        <th><h4>Datos Personales</h4></th>
                        <th></th>
                        <th><h4>Dirección</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" name="{{ Sucursal_Model::$nombre }}"
                                       id="{{ Sucursal_Model::$nombre }}" class="form-control" placeholder="Nombre"
                                       required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Calle:</label>
                                <input type="text" name="{{ Sucursal_Model::$calle }}" id="{{ Sucursal_Model::$calle }}"
                                       class="form-control" placeholder="Calle" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Clave:</label>
                                <input type="text" name="{{ Sucursal_Model::$clave }}" id="{{ Sucursal_Model::$clave }}"
                                       class="form-control" placeholder="Clave">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Colonia:</label>
                                <input type="text" name="{{ Sucursal_Model::$colonia }}"
                                       id="{{ Sucursal_Model::$colonia }}" class="form-control" placeholder="Colonia"
                                       required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Correo:</label>
                                <input type="text" name="{{ Sucursal_Model::$correo }}"
                                       id="{{ Sucursal_Model::$correo }}" class="form-control"
                                       placeholder="Apellido Materno">
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Número Interior:</label>
                                <input type="text" name="{{ Sucursal_Model::$numeroInterior }}"
                                       id="{{ Sucursal_Model::$numeroInterior }}" class="form-control"
                                       placeholder="Número interior">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Teléfono:</label>
                                <input type="text" name="{{ Sucursal_Model::$telefono }}"
                                       id="{{ Sucursal_Model::$telefono }}" class="form-control" placeholder="RFC"
                                       required>
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Número exterior:</label>
                                <input type="text" name="{{ Sucursal_Model::$numeroExterior }}"
                                       id="{{ Sucursal_Model::$numeroExterior }}" class="form-control"
                                       placeholder="Número exterior" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Empresa:</label>
                                {!! Form::select( Sucursal_Model::$idEmpresa , $arrayEmpresa, 0, ['id' => Sucursal_Model::$idEmpresa, 'class' => "form-control"]) !!}
                            </div>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Código Postal:</label>
                                <input type="number" name="{{ Sucursal_Model::$cp }}" id="{{ Sucursal_Model::$cp }}"
                                       class="form-control" placeholder="Código Postal" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Estado:</label>
                                {!! Form::select( "id_estado" , $arrayEstado, 0, ['id' => 'id_estado', 'class' => "form-control select2"]) !!}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="form-group">
                                <label>Municipio:</label>
                                <select class="form-control select2" id="{{ Sucursal_Model::$idMunicipio }}"
                                        name="{{ Sucursal_Model::$idMunicipio }}">
                                    <option value="0">N/D</option>
                                </select>
                            </div>
                        </td>

                    </tr>
                    <input type="hidden" name="{{ Sucursal_Model::$id }}" id="{{ Sucursal_Model::$id }}" value="0">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>