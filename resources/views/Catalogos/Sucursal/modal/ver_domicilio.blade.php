<?php
use App\Models\Catalogos\Sucursal_Model;

?>

<div class="modal fade" id="model-ver_direccion">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ver Dirección</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Calle:</span>
                    <input type="text" name="{{ Sucursal_Model::$calle }}" id="{{ Sucursal_Model::$calle }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Colonia:</span>
                    <input type="text" name="{{ Sucursal_Model::$colonia }}" id="{{ Sucursal_Model::$colonia }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Número Exterior:</span>
                    <input type="text" name="{{ Sucursal_Model::$numeroExterior }}"
                           id="{{ Sucursal_Model::$numeroExterior }}" class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Número Interior:</span>
                    <input type="text" name="{{ Sucursal_Model::$numeroInterior }}"
                           id="{{ Sucursal_Model::$numeroInterior }}" class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">C.P:</span>
                    <input type="text" name="{{ Sucursal_Model::$cp }}" id="{{ Sucursal_Model::$cp }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Municipio:</span>
                    <input type="text" name="{{ Sucursal_Model::$idMunicipio }}" id="{{ Sucursal_Model::$idMunicipio }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Estado:</span>
                    <input type="text" name="id_estado" id="id_estado" class="form-control"
                           disabled style="background-color: white">
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>