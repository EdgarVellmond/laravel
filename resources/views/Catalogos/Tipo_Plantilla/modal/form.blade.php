<?php
use App\Models\Catalogos\Tipo_Plantilla_Model;

?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tipo de Plantilla</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarTipoPlantilla()']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm max300">
                    <span class="input-group-addon">Nombre</span>
                    <input type="text" name="{{ Tipo_Plantilla_Model::$nombre }}"
                           id="modal-form-{{ Tipo_Plantilla_Model::$nombre }}" class="form-control" placeholder="Nombre"
                           required>
                </div>

                <input type="hidden" name="{{ Tipo_Plantilla_Model::$id }}"
                       id="modal-form-{{ Tipo_Plantilla_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>