<div class="modal fade" id="modal-alert-message-error">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-danger">
                    <strong>Aviso!</strong>
                    <p id="error-message"></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">

    $(document).ready(function () {

        $('#modal-alert-message-error').on('show.bs.modal',function (e) {
            setTimeout(function(){
                $('#modal-alert-message-error').modal('hide');
            }, 3000);
        });
    });
</script>