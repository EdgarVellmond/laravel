<div class="modal fade" id="modal-alert-message-success">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-success">
                    <strong>Aviso!</strong>
                    <p id="success-message"></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#modal-alert-message-success').on('shown.bs.modal',function (e) {
            setTimeout(function(){
                $('#modal-alert-message-success').modal('hide');
            }, 3000);
        });
    });
</script>