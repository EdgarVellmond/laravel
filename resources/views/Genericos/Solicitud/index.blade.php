<?php
use App\Models\Catalogos\Puesto_Model;
use App\Http\Helpers\Helpers;

$ruta = "recursos_humanos/cambioEstatusSolicitud";
$rutaSolicita = "solicitud/guardarSolicitud";

?>

@extends('layouts.listado')

@section('title')
    Solicitudes
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Genericos/solicitud.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var oTableSolicitud;
        var oTable;

        var idPuesto = '{{Puesto_Model::$id}}';
        var nombrePuesto = '{{Puesto_Model::$nombre}}';
    </script>

@endsection

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Solicitudes</a></li>
        <li><a data-toggle="tab" href="#menu1">Solicitudes Canceladas</a></li>
        <li><a data-toggle="tab" href="#menu2">Solicitudes Finalizadas</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">

            <div class="row">
                <div class="page-header">
                    <h1>Solicitudes
                        <small></small>
                    </h1>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">
                                @if(Helpers::get_permiso("alta.generico.solicitud"))
                                    <a href="#" type="button" class="btn btn-success btn-xs" title="Nuevo"
                                       data-toggle="modal"
                                       data-target="#modal-form"><span class="glyphicon glyphicon-plus"
                                                                       aria-hidden="true"></span>Nuevo</a>
                                @endif
                            </div>
                            <h3 class="panel-title">Control de Solicitudes</h3>
                        </div>
                        <div class="panel-body">

                            @include("Genericos.Solicitud.table_solicita")

                        </div>
                    </div>
                </div>

            </div>


        </div><!-- FIN DEL PRIMER TAB-->

        <div id="menu1" class="tab-pane fade">
            <div class="row">
                <div class="page-header">
                    <h1>Solicitudes Canceladas
                        <small></small>
                    </h1>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">
                            </div>
                            <h3 class="panel-title">Control de Solicitudes</h3>
                        </div>
                        <div class="panel-body">

                            @include("Genericos.Solicitud.table_cancelado")

                        </div>
                    </div>
                </div>


            </div>
        </div><!-- FIN DEL SEGUNDO TAB-->

        <div id="menu2" class="tab-pane fade">
            <div class="row">
                <div class="page-header">
                    <h1>Solicitudes Finalizadas
                        <small></small>
                    </h1>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">
                            </div>
                            <h3 class="panel-title">Control de Solicitudes</h3>
                        </div>
                        <div class="panel-body">

                            @include("Genericos.Solicitud.table_finalizado")

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    @include("Genericos.Solicitud.modal.cancelar")
    @include("Genericos.Solicitud.modal.form")
@endsection
