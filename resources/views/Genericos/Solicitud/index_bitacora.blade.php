<?php
use App\Models\Catalogos\Puesto_Model;
use App\Http\Helpers\Helpers;
?>

@extends('layouts.listado')

@section('title')
    Bitacora
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Genericos/solicitud.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var oTableBitacora;
    </script>

@endsection

@section('content')

            <div class="row">
                <div class="page-header">
                    <h1>Bitacora
                        <small></small>
                    </h1>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">
                            </div>
                            <h3 class="panel-title">Control de Solicitudes</h3>
                        </div>
                        <div class="panel-body">

                            @include("Genericos.Solicitud.table_bitacora")

                        </div>
                    </div>
                </div>
            </div>

@endsection
