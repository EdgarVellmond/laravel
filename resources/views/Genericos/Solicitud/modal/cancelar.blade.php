<?php
use App\Models\Genericos\Solicitud_Model;

?>

<div class="modal fade" id="modal-cancelar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Cancelar Solicitud</h4>
            </div>
            {!! Form::open(['url' => url($ruta),'class'=>'form','id'=>'delete-form']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label>Comentarios:</label>
                    <input type="text" name="{{ Solicitud_Model::$observaciones }}"
                           id="{{ Solicitud_Model::$observaciones }}" class="form-control" required>
                </div>
                <input type="hidden" name="{{ Solicitud_Model::$id  }}"
                       id="modal-cancelar-{{ Solicitud_Model::$id  }}">
                <input type="hidden" name="{{ Solicitud_Model::$estatus }}" value="Cancelado">
                <input type="hidden" id="modal-cancelar-{{ Solicitud_Model::$estatus }}" name="estatusAnterior" value="En proceso" >
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Cancelada</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>