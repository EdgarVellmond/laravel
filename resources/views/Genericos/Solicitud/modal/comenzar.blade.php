<?php
use App\Models\Genericos\Solicitud_Model;

?>

<div class="modal fade" id="modal-comenzar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Comenzar Solicitud</h4>
            </div>
            {!! Form::open(['url' => url($ruta),'class'=>'form','id'=>'delete-form']) !!}
            <div class="modal-body">
                <label>¿ Estás seguro que deseas comenzar esta solicitud ?</label>
                <input type="hidden" name="{{ Solicitud_Model::$id  }}" id="modal-comenzar-{{ Solicitud_Model::$id  }}">
                <input type="hidden" name="{{ Solicitud_Model::$estatus }}" value="En proceso" >
                <input type="hidden" name="estatusAnterior" value="Pendiente" >
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Comenzar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>