<?php
use App\Models\Genericos\Solicitud_Model;

?>

<div class="modal fade" id="modal-finalizar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Finalizar Solicitud</h4>
            </div>
            {!! Form::open(['url' => url($ruta),'class'=>'form','id'=>'delete-form']) !!}
            <div class="modal-body">

                <label>¿ Estás seguro que deseas finalizar esta solicitud ?</label>
                <input type="hidden" name="{{ Solicitud_Model::$id  }}"
                       id="modal-finalizar-{{ Solicitud_Model::$id  }}">
                <input type="hidden" name="{{ Solicitud_Model::$estatus }}" value="Finalizada">
                <input type="hidden" name="estatusAnterior" value="En proceso" >
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Comenzar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>