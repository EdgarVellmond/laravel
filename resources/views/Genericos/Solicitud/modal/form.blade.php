<?php
use App\Models\Genericos\Solicitud_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Genericos\Documento_Model;

?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Solicitud</h4>
            </div>
            {!! Form::open(['id'=>'add_edit_form', 'url' => url($rutaSolicita),'role' => 'form', 'class' => 'form', 'enctype'=>'multipart/form-data', 'file'=>true]) !!}
            <div class="modal-body">

                            <div class="input-group input-group-sm ">
                                <span class="input-group-addon">Asunto</span>
                                    <input type="text" value="" placeholder="" class="form-control"
                                           id="modal-form-{{Solicitud_Model::$asunto}}" name="{{Solicitud_Model::$asunto}}"
                                           style="background-color: white" required>
                            </div>
                            <br>
                            <div class="input-group input-group-sm " style="width: 100%;">
                                <span class="input-group-addon">Tipo de Solicitud</span>
                                {!! Form::select( Tipo_Solicitud_Model::$id , $tipos_solicitud, 0, ['id' => "modal-form-" . Tipo_Solicitud_Model::$id, 'class' => "form-control select2", "style" => "width: 100%;" ]) !!}
                            </div>
                            <br>
                            <div class="input-group input-group-sm " style="width: 100%;">
                                <span class="input-group-addon">Área</span>
                                {!! Form::select( Area_Model::$id , $areas, 0, ['id' => "modal-form-" . Area_Model::$id, 'class' => "form-control select2" , "style" => "width: 100%;"]) !!}
                            </div>
                            <br>
                            <div class="input-group input-group-sm" >
                                <span class="input-group-addon">Archivos</span>
                                <input type="file" multiple="true" class="form-control" placeholder=""
                                       id="modal-file-{{ Documento_Model::$id  }}"
                                       name="{{ Documento_Model::$id  }}[]" onchange='verImagenAnetsSubir(this);'
                                       style="background-color: white;">
                            </div>

                    </tbody>
                </table>

                <input type="hidden" name="{{ Solicitud_Model::$id  }}" id="modal-form-{{ Solicitud_Model::$id  }}">
                <input type="hidden" name="{{ Solicitud_Model::$estatus  }}" id="modal-form-{{ Solicitud_Model::$estatus  }}" value="Pendiente">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>