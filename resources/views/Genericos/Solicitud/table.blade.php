<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Http\Helpers\Helpers;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;

?>


<table class="table table-hover table-bordered small" id="t_list_solicitud">
    <thead>
    <tr>
        <th>Acciones</th>
        <th>Folio</th>
        <th>Descripcion</th>
        <th>Tipo</th>
        <th>Empleado solicita</th>
        <th>Estatus</th>
        <th>Observaciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($solicitud as $row)

        @if($row->{"estatusSolicitud"} != "Cancelado" && $row->{"estatusSolicitud"} != "Finalizada")

            @if($row->{"estatusSolicitud"} == "Pendiente")
                <tr class="warning">
            @endif
            @if($row->{"estatusSolicitud"} == "En proceso")
                <tr class="info">
            @endif
            @if($row->{"estatusSolicitud"} == "Rechazada")
                <tr class="danger">
                    @endif

                    <td>
                        @if(Helpers::get_permiso("download.generico.solicitud"))
                            <a href="{{ url('/recursos_humanos').'/descargarDocumentos/'.$row->{Solicitud_Model::$id}.'/solicitud/solicitud'}}"
                               class="btn btn-xs btn-success" data-toggle="modal"
                               target="_blank" title="Descargar documentación"
                               data-id="{{$row->{Solicitud_Model::$id} }}">
                                <span class="glyphicon glyphicon-download-alt"></span></a>
                        @endif
                        @if(Helpers::get_permiso("comenzar.generico.solicitud"))
                            @if($row->{"estatusSolicitud"} == "Pendiente" )
                                <a href="#" class="btn btn-xs btn-success" data-toggle="modal"
                                   data-target="#modal-comenzar" title="Comenzar"
                                   data-id="{{$row->{Solicitud_Model::$id} }}">
                                    <span class="glyphicon glyphicon-ok"></span></a>
                            @endif
                        @endif
                        @if(Helpers::get_permiso("finalizar.generico.solicitud"))
                            @if($row->{"estatusSolicitud"} == "En proceso" )
                                <a href="#" class="btn btn-xs btn-warning" data-toggle="modal"
                                   data-target="#modal-finalizar" title="Finalizar"
                                   data-id="{{$row->{Solicitud_Model::$id} }}">
                                    <span class="glyphicon glyphicon-pencil"></span></a>
                            @endif
                        @endif
                        @if(Helpers::get_permiso("rechazar.generico.solicitud"))
                            @if($row->{"estatusSolicitud"} == "Pendiente")
                                <a href="#" class="btn btn-xs btn-danger" data-toggle="modal"
                                   data-target="#modal-rechazar" title="Rechazar"
                                   data-id="{{$row->{Solicitud_Model::$id} }}">
                                    <span class="glyphicon glyphicon-remove"></span></a>
                            @endif
                        @endif

                        @if(Helpers::get_permiso("cancelar.generico.solicitud"))
                            @if($row->{"estatusSolicitud"} == "En proceso" )
                                <a href="#" class="btn btn-xs btn-danger" data-toggle="modal"
                                   data-target="#modal-cancelar" title="Cancelar"
                                   data-id="{{$row->{Solicitud_Model::$id} }}">
                                    <span class="glyphicon glyphicon-warning-sign"></span></a>
                            @endif
                        @endif
                    </td>
                    <td>{{$row->{Solicitud_Model::$id} }}</td>
                    <td>{{$row->{Solicitud_Model::$asunto} }}</td>
                    <td>{{$row->{Tipo_Solicitud_Model::$nombre} }}</td>
                    <td>{{$row->{"nombreEmpleado"} }} </td>
                    <td>{{$row->{"estatusSolicitud"} }}</td>
                    <td>{{$row->{ Solicitud_Model::$observaciones} }}</td>

                </tr>

            @endif
            @endforeach

    </tbody>
</table>

