<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Http\Helpers\Helpers;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;

?>

<table class="table table-hover table-bordered small" id="t_list_bitacora">
    <thead>
    <tr>
        <th>Acciones</th>
        <th>Fecha</th>
        <th>Descripcion</th>
        <th>Tipo</th>
        <th>Empleado solicita</th>
        <th>Estatus</th>
        <th>Observaciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($solicitud as $row)

        @if($row->{"estatusSolicitud"} == "Pendiente")
            <tr class="warning">
        @endif
        @if($row->{"estatusSolicitud"} == "En proceso")
            <tr class="info">
        @endif
        @if($row->{"estatusSolicitud"} == "Rechazada")
            <tr class="danger">
        @endif
        @if($row->{"estatusSolicitud"} == "Cancelado")
            <tr class="danger">
        @endif
        @if($row->{"estatusSolicitud"} == "Finalizada")
            <tr class="success">
                @endif

                <td>
                    @if(Helpers::get_permiso("download.generico.solicitud"))
                        <a href="{{ url('/recursos_humanos').'/descargarDocumentos/'.$row->{Solicitud_Model::$id}.'/solicitud/solicitud'}}"
                           class="btn btn-xs btn-success" data-toggle="modal"
                           target="_blank" title="Descargar documentación"
                           data-id="{{$row->{Solicitud_Model::$id} }}">
                            <span class="glyphicon glyphicon-download-alt"></span></a>
                    @endif
                    @if(Helpers::get_permiso("detalle.generico.bitacora_solicitud"))
                        <a href="#" class="btn btn-xs btn-info" data-toggle="modal"
                           data-target="#modal-comenzar" title="Ver detalle"
                           data-id="{{$row->{Solicitud_Model::$id} }}">
                            <span class="glyphicon glyphicon-eye-open"></span></a>
                    @endif
                </td>
                <td>{{$row->{Solicitud_Model::$createdAt} }}</td>
                <td>{{$row->{Solicitud_Model::$asunto} }}</td>
                <td>{{$row->{Tipo_Solicitud_Model::$nombre} }}</td>
                <td>{{$row->{"nombreEmpleado"} }} </td>
                <td>{{$row->{"estatusSolicitud"} }}</td>
                <td>{{$row->{ Solicitud_Model::$observaciones} }}</td>

            </tr>

            @endforeach

    </tbody>
</table>
