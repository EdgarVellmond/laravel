<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Http\Helpers\Helpers;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;

?>


<table class="table table-hover table-bordered small" id="t_list_solicitud_cancelado">
    <thead>
    <tr>
        <th>Folio</th>
        <th>Descripcion</th>
        <th>Tipo</th>
        <th>Empleado solicita</th>
        <th>Estatus</th>
        <th>Observaciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($solicitud as $row)

        @if($row->{"estatusSolicitud"} == "Cancelado")

            <tr class="danger">

                <td>{{$row->{Solicitud_Model::$id} }}</td>
                <td>{{$row->{Solicitud_Model::$asunto} }}</td>
                <td>{{$row->{Tipo_Solicitud_Model::$nombre} }}</td>
                <td>{{$row->{"nombreEmpleado"} }} </td>
                <td>{{$row->{"estatusSolicitud"} }}</td>
                <td>{{$row->{ Solicitud_Model::$observaciones} }}</td>

            </tr>

        @endif
    @endforeach
    </tbody>
</table>

