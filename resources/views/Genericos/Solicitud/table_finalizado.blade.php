<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Http\Helpers\Helpers;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;

?>


<table class="table table-hover table-bordered small" id="t_list_solicitud_finalizado">
    <thead>
    <tr>
        <th>Acciones</th>
        <th>Folio</th>
        <th>Descripcion</th>
        <th>Tipo</th>
        <th>Empleado solicita</th>
        <th>Estatus</th>
        <th>Observaciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($solicitud as $row)

        @if($row->{"estatusSolicitud"} == "Finalizada")

            <tr class="success">

                <td>
                    @if(Helpers::get_permiso("download.generico.solicitud"))
                        <a href="{{ url('/recursos_humanos').'/descargarDocumentos/'.$row->{Solicitud_Model::$id}.'/solicitud/solicitud'}}"
                           class="btn btn-xs btn-success" data-toggle="modal"
                           target="_blank" title="Descargar documentación"
                           data-id="{{$row->{Solicitud_Model::$id} }}">
                            <span class="glyphicon glyphicon-download-alt"></span></a>
                    @endif

                </td>
                <td>{{$row->{Solicitud_Model::$id} }}</td>
                <td>{{$row->{Solicitud_Model::$asunto} }}</td>
                <td>{{$row->{Tipo_Solicitud_Model::$nombre} }}</td>
                <td>{{$row->{"nombreEmpleado"} }} </td>
                <td>{{$row->{"estatusSolicitud"} }}</td>
                <td>{{$row->{ Solicitud_Model::$observaciones} }}</td>

            </tr>

        @endif
    @endforeach
    </tbody>
</table>

