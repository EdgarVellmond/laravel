<?php
use App\Models\Indicadores\Actividad_Model;
use App\Models\Indicadores\Proyectos_Model;

?>
<style type="text/css">
    .divider {
position: relative;
border-bottom: 1px solid #f0f0f0;
margin-bottom: 10px;
margin-top: 10px; }

.hidden {
  display: none;
  visibility: hidden;
  margin-bottom: -10px;
  margin-top: -10px; 
} 

</style>

<div class="modal fade" id="modal-forms">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar un nuevo indicador</h4>
            </div>
           {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_forms','onSubmit' => 'return guardarActividads()']) !!}
            <?php 
                        $cont=0;
                        $compara[]='0';
                        $compara2[]='0';
                         $contador=1;
                        ?>
                        <div class="modal-body">
                        @foreach($arrayActividad as $ok)
                        @for ($contador; $contador <= $array->{ Proyectos_Model::$duracion }; $contador++)
                        @if($cont == 0)
                        <div class="input-group">
                                <span class="input-group-addon">Area Responsable</span>
                                <select name="{{ Actividad_Model::$area }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$area }}-{{$contador}}" aria-controls="t_list" class="form-control input-sm">
                                 <option default>Selecciona</option>
                                 <option value="Contabilidad">Contabilidad</option>
                                 <option value="Finanzas">Finanzas</option>
                                 <option value="Construccion">Construccion</option>
                                 <option value="Logistica">Logistica</option>
                                 <option value="Desarrollo">Desarrollo</option>
                                 <option value="Consultoria">Consultoria</option>
                                 <option value="Otros">Otros</option>
                             </select>
                             <span class="input-group-addon">Indicador</span>
                             <select name="{{ Actividad_Model::$tipoIndicador }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$tipoIndicador }}-{{$contador}}"  aria-controls="t_list" class="form-control input-sm">
                                <option default>Selecciona</option>
                                <option value="Tiempo">Tiempo</option>
                                <option value="Avance">Avance</option>
                                <option value="Gasto">Gasto</option>
                                <option value="Estimado">Estimado</option>
                                <option value="Cobro">Cobro</option>
                                <option value="Anticipo">Anticipo</option>
                            </select>
                        </div> 
                        <br>
                        @endif
                        @if($cont >= 1)
                        <div class="input-group hidden">
                                <span class="input-group-addon">Area Responsable</span>
                                <select name="{{ Actividad_Model::$area }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$area }}-{{$contador}}" aria-controls="t_list" class="form-control input-sm">
                                 <option default>Selecciona</option>
                                 <option value="Contabilidad">Contabilidad</option>
                                 <option value="Finanzas">Finanzas</option>
                                 <option value="Construccion">Construccion</option>
                                 <option value="Logistica">Logistica</option>
                                 <option value="Desarrollo">Desarrollo</option>
                                 <option value="Consultoria">Consultoria</option>
                                 <option value="Otros">Otros</option>
                             </select>
                             <span class="input-group-addon">Indicador</span>
                             <select name="{{ Actividad_Model::$tipoIndicador }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$tipoIndicador }}-{{$contador}}"  aria-controls="t_list" class="form-control input-sm">
                                <option default>Selecciona</option>
                                <option value="Tiempo">Tiempo</option>
                                <option value="Avance">Avance</option>
                                <option value="Gasto">Gasto</option>
                                <option value="Estimado">Estimado</option>
                                <option value="Cobro">Cobro</option>
                                <option value="Anticipo">Anticipo</option>
                            </select>
                        </div> 
                        <br>
                        @endif
                        <?php
                        $cont++;
                        ?>
                         <div>
                             {{$contador }}    {{ $array->{ Proyectos_Model::$tipo } }} 
                             <input type="hidden" name="{{ Actividad_Model::$semana }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$semana }}-{{$contador}}" class="form-control input-sm" placeholder="Ingrese numero" value="{{$contador}}" required>                    
                            </div>
                               <div class="divider"></div>
                        <div>
                            <div class="input-group">

                            <span class="input-group-addon">Monto Programado:</span>
                            <input type="number" name="{{ Actividad_Model::$programado }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$programado }}-{{$contador}}" class="form-control input-sm" placeholder="Ingrese numero" required>
                            <span class="input-group-addon">Tipo</span>
                            <input type="number" name="{{ Actividad_Model::$real }}-{{$contador}}" id="modal-form-{{ Actividad_Model::$real }}-{{$contador}}" class="form-control input-sm" placeholder="Ingrese numero" required>
                        </div>
                    </div>
                    <br>
                        @endfor
                        @endforeach
                    </div>  
                <input type="hidden" name="id_proyecto" id="id_proyecto2" value="0">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
