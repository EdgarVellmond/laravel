<?php
use App\Models\Indicadores\Proyectos_Model;
?>

<div class="modal fade" id="#model-ver_adicional">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar un nuevo proyecto</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarProyecto()']) !!}
            <div class="modal-body">
                <div class="input-group input-group-sm max300">
                    <span class="input-group-addon">Nombre de la obra</span>
                    <input type="text" name="{{ Proyectos_Model::$proyectoNombre }}" id="modal-forms-{{ Proyectos_Model::$proyectoNombre }}" class="form-control" placeholder="Nombre del proyecto" required>
                </div>
                
                <input type="hidden" name="id_proyecto" id="id_proyecto" value="0">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
