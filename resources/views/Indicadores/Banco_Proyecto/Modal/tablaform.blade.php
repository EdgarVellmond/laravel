<?php
use App\Models\Indicadores\Proyectos_Model;

?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar un nuevo proyecto</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarProyecto()']) !!}
            <div class="modal-body">
                <div class="input-group">
                    <span class="input-group-addon">Nombre de la obra</span>
                    <input type="text" name="{{ Proyectos_Model::$proyectoNombre }}" id="modal-form-{{ Proyectos_Model::$proyectoNombre }}" class="form-control" placeholder="Nombre del proyecto" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Cliente</span>
                    <input type="text" name="{{ Proyectos_Model::$cliente}}" id="modal-form-{{Proyectos_Model::$cliente }}" class="form-control" placeholder="Cliente" required>
                    <span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="true"></span>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Fecha Inicial</span>
                    <input type="date" name="{{ Proyectos_Model::$fechaInicial }}" id="modal-form-{{ Proyectos_Model::$fechaInicial }}" class="form-control" placeholder="Fecha inicial" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Fecha de termino</span>
                    <input type="date" name="{{ Proyectos_Model::$fechaTermino }}" id="modal-form-{{ Proyectos_Model::$fechaTermino }}" class="form-control" size="4" placeholder="Fecha de termino" required>

                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Duracion</span>
                    <input type="number" name="{{ Proyectos_Model::$duracion }}" id="modal-form-{{ Proyectos_Model::$duracion }}" class="form-control input-sm" placeholder="Ingrese numero" required>
                    <span class="input-group-addon">Tipo</span>
                    <select name="{{ Proyectos_Model::$tipo }}" id="modal-form-{{ Proyectos_Model::$tipo }}" aria-controls="t_list" class="form-control input-sm">
                        <option default>Selecciona</option>s
                        <option value="Dias">Dias</option>
                        <option value="Semanas">Semanas</option>
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Ubicacion</span>
                    <input type="text" name="{{ Proyectos_Model::$ubicacion }}" id="modal-form-{{ Proyectos_Model::$ubicacion }}" class="form-control" placeholder="Ubicacion" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Gasto de obra</span>
                    <input type="number" name="{{ Proyectos_Model::$gastobra }}" id="modal-form-{{ Proyectos_Model::$gastobra }}" class="form-control" placeholder="Ingrese cantidad" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Monto de contrato</span>
                    <input type="number" name="{{ Proyectos_Model::$montocontr }}" id="modal-form-{{ Proyectos_Model::$montocontr }}" class="form-control" placeholder="Ingrese cantidad" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Monto Ejercido</span>
                    <input type="number" name="{{ Proyectos_Model::$montoejercido }}" id="modal-form-{{ Proyectos_Model::$montoejercido }}" class="form-control" placeholder="Ingrese cantidad" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Saldo por ejercer</span>
                    <input type="number" name="{{ Proyectos_Model::$saldoEjercer }}" id="modal-form-{{ Proyectos_Model::$saldoEjercer }}" class="form-control" placeholder="Ingrese cantidad" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">Estatus</span>
                    <select name="{{ Proyectos_Model::$estatus }}" id="modal-form-{{ Proyectos_Model::$estatus }}" aria-controls="t_list" class="form-control input-sm">
                        <option default>Selecciona</option>
                        <option value="Activo">Activo</option>
                        <option value="En proceso">En proceso</option>
                        <option value="En pausa">En pausa</option>
                        <option value="Terminado">Terminado</option>
                        <option value="Pendiente">Pendiente</option>
                        <option value="Rechazada">Rechazada</option>
                    </select>
                    <span class="input-group-addon">Prioridad</span>
                    <select name="{{ Proyectos_Model::$prioridad }}" id="modal-form-{{ Proyectos_Model::$prioridad }}"  aria-controls="t_list" class="form-control input-sm">
                        <option default>Selecciona</option>
                        <option value="Alta">Alta</option>
                        <option value="Moderada">Moderada</option>
                        <option value="Baja">Baja</option>
                    </select>
                </div>
                <br>
                <div class="input-group input-group">
                    <span class="input-group-addon">Observaciones</span>
                    <input type="text" name="{{ Proyectos_Model::$observaciones}}" rows="5" cols="20" id="modal-form-{{Proyectos_Model::$observaciones }}" class="form-control" placeholder="Ingrese aqui sus observaciones" >
                </div>
                <input type="hidden" name="id_proyecto" id="id_proyecto" value="0">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
