<?php
use App\Models\Indicadores\Proyectos_Model;
use App\Http\Helpers\Helpers;
?>
@extends('layouts.listado')

@section('title')
Banco de Proyectos
@endsection
@section('links')
{!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
{!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
{!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
{!! Html::script('js/app/Indicadores/proyecto.js') !!}
<script type="text/javascript">
    var token = '{{ csrf_token() }}';
    var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
    var oTable;
    var idProyecto = '{{Proyectos_Model::$id}}';
    var nombreProyecto = '{{Proyectos_Model::$proyectoNombre}}';
    var cliente = '{{Proyectos_Model::$cliente}}';
    var fechaInicial = '{{Proyectos_Model::$fechaInicial}}';
    var fechaTermino = '{{Proyectos_Model::$fechaTermino}}';
    var prioridad = '{{Proyectos_Model::$prioridad}}';
    var estatus = '{{Proyectos_Model::$estatus}}';
    var ubicacion = '{{Proyectos_Model::$ubicacion}}';
    var tipo = '{{Proyectos_Model::$tipo}}';
    var duracion = '{{Proyectos_Model::$duracion}}';
    var gastobra = '{{Proyectos_Model::$gastobra}}';
    var montocontr = '{{Proyectos_Model::$montocontr}}';
    var montoejercido = '{{Proyectos_Model::$montoejercido}}';
    var saldoEjercer = '{{Proyectos_Model::$saldoEjercer}}';
</script>
@endsection
@section('content')
<div class="array">
    <div class="page-header">
        <h1>Modulo de proyectos
            <small></small>
        </h1>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                    @if(Helpers::get_permiso("alta.indicadores.proyecto"))
                    <a href="#" type="button" class="btn btn-success btn-xs" title="Nuevo" data-toggle="modal"
                    data-target="#modal-form"><span class="glyphicon glyphicon-plus"
                    aria-hidden="true"></span>Nuevo Proyecto</a>
                    @endif
                </div>
                <h3 class="panel-title">Control de proyectos</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover small" id="t_list">
                    <thead>
                        <tr>
                            <th>Acción</th>
                            <th width="15%">Nombre proyecto</th>
                            <th width="15%">Cliente</th>
                            <th width="5%" >Fecha inicial</th>
                            <th width="5%">Fecha de termino</th>
                            <th >Monto contratado</th>
                            <th>Monto ejercido</th>
                            <th>Saldo a ejercer</th>
                            <th>Observaciones</th>
                            <th width="5%">Prioridad</th>
                            <th WIDTH="5%">Estatus</th>
                        </tr>
                    </thead>
                    <tbody>

                       @foreach($arrayProyecto as $array)
                       @if($array->{"estatus"} != "Cancelado" && $array->{"estatus"} != "Finalizada")

                       @if($array->{"estatus"} == "En proceso")
                       <tr class="warning">
                        @endif
                        @if($array->{"estatus"} == "Activo")
                        <tr class="info">
                            @endif
                            @if($array->{"estatus"} == "Rechazada")
                            <tr class="danger">
                                @endif
                                @if($array->{"estatus"} == "Terminado")
                                <tr class="success">
                                    @endif
                                    @if($array->{"estatus"} == "Cancelado")
                                    <tr class="danger">
                                        @endif
                   
                        <td>
                            @if(Helpers::get_permiso("edicion.indicadores.proyecto"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                            data-toggle="modal" data-target="#modal-form"
                            data-id="{{ $array->getAttribute(Proyectos_Model::$id) }}"><span
                            class="glyphicon glyphicon-pencil"></span></a>
                            @endif
                            @if(Helpers::get_permiso("baja.indicadores.proyecto"))
                            <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                            data-toggle="modal" data-target="#modal-eliminar"
                            data-id="{{ $array->{Proyectos_Model::$id} }}"><span
                            class="glyphicon glyphicon-remove"></span></a>
                            @endif
                            @if(Helpers::get_permiso("edicion.indicadores.proyecto"))
                            <a href="ficha/{{$array->getAttribute(Proyectos_Model::$id) }}" title="Ficha" class="btn btn-xs btn-info"><span
                            class="glyphicon glyphicon-eye-open"></span></a>
                            @endif
                        </td>
                        <td>{{ $array->{ Proyectos_Model::$proyectoNombre } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$cliente } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$fechaInicial } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$fechaTermino } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$montocontr } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$montoejercido } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$saldoEjercer } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$observaciones } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$prioridad } }}</td>
                        <td>{{ $array->{ Proyectos_Model::$estatus } }}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
</div>

@endsection

@include("Indicadores.Banco_Proyecto.Modal.form")
@include("Indicadores.Banco_Proyecto.Modal.eliminar")