<?php
use App\Models\Indicadores\Proyectos_Model;
use App\Models\Indicadores\Actividad_Model;
use App\Http\Helpers\Helpers;
?>
@extends('layouts.listado')
@section('title')
Banco de Proyectos
@endsection
@section('links')
{!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
{!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
{!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
{!! Html::script('js/app/Indicadores/proyecto.js') !!}
<script type="text/javascript">
    var token = '{{ csrf_token() }}';
    var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
    var oTable;
    // var idProye = '{{Proyectos_Model::$id}}';
    var nombreProyecto = '{{Proyectos_Model::$proyectoNombre}}';
    var cliente = '{{Proyectos_Model::$cliente}}';
    var fechaInicial = '{{Proyectos_Model::$fechaInicial}}';
    var fechaTermino = '{{Proyectos_Model::$fechaTermino}}';
    var prioridad = '{{Proyectos_Model::$prioridad}}';
    var estatus = '{{Proyectos_Model::$estatus}}';
    var ubicacion = '{{Proyectos_Model::$ubicacion}}';
    var tipo = '{{Proyectos_Model::$tipo}}';
    var duracion = '{{Proyectos_Model::$duracion}}';
    var gastobra = '{{Proyectos_Model::$gastobra}}';
    var montocontr = '{{Proyectos_Model::$montocontr}}';
    var montoejercido = '{{Proyectos_Model::$montoejercido}}';
    var saldoEjercer = '{{Proyectos_Model::$saldoEjercer}}';
    //variables de actividad
    var id_actividad = '{{Actividad_Model::$id_actividad}}'; 
    var tipoIndicador = '{{Actividad_Model::$tipoIndicador}}'; 
    var porcentaje = '{{Actividad_Model::$porcentaje}}'; 
    var programado = '{{Actividad_Model::$programado}}';
    var real = '{{Actividad_Model::$real}}'; 
    var area = '{{Actividad_Model::$area}}'
    var idProyecto = '{{Actividad_Model::$idProyecto}}';
</script>
@endsection
@section('content')
  <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Principal</a></li>
        <li><a href="{{ url('/indicadores/bancoProyectos') }}">Modulo de proyectos</a></li>
        <li class="active">Ficha</li>
    </ol>
    <div class="page-header">
        <h1>Modulo de proyectos
            <small></small>
        </h1>
    </div>
    <style>
    .tdInformacionIndicador {
        width: 100px!important;
    }
    .tdcontenedor{
        width: 600px!important;
    }
</style>

<div class="panel panel-primary tdcontenedor">
    <div class="panel-heading  ">
        <div class="btn-group pull-right">
        @if(Helpers::get_permiso("edicion.indicadores.proyecto"))
            @if($proyectos[0]->{Proyectos_Model::$estatus} == "Pendiente" || $proyectos[0]->{Proyectos_Model::$estatus} == "Rechazada"  || $proyectos[0]->{Proyectos_Model::$estatus} == "Activo" || $proyectos[0]->{Proyectos_Model::$estatus} == "En proceso" || $proyectos[0]->{Proyectos_Model::$estatus} == "En pausa")
            <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
            data-toggle="modal" data-target="#modal-form"
            data-id="{{ $proyectos[0]->getAttribute(Proyectos_Model::$id) }}"><span
            class="glyphicon glyphicon-pencil"></span>Editar</a>
               @endif
        @endif
        </div>
        <h3 class="panel-title">Información Comisión</h3>
    </div>
    <div class="panel-body" style="overflow-x: auto;">
        @foreach($proyectos as $array)

        <table id="informacionComision" class="table">
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Nombre de la obra :</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$proyectoNombre} }}
                </td>
            </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Cliente :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$cliente} }}
                </td>
            </tr>
             <tr>
                <td class="tdInformacionIndicador">
                    <b>Fecha Inicial :</b>
                </td>
                <td >
                    {{ $array->{Proyectos_Model::$fechaInicial} }}
                </td>
                <td class="tdInformacionIndicador">
                    <b>Fecha de termino :</b>
                </td>
                <td >
                    {{ $array->{Proyectos_Model::$fechaTermino} }}
                </td>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Duracion :</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$duracion} }} {{ $array->{Proyectos_Model::$tipo} }}
                </td>
                
            </tr>
                   <tr>
                <td class="tdInformacionIndicador">
                    <b>Monto de contrato :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$montocontr} }}
                </td>
                 <td class="tdInformacionIndicador">
                    <b>Monto Ejercido :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$montoejercido} }}
                </td>
            </tr>
            <tr>
               
            </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Prioridad :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$prioridad} }}
                </td>
                 <td class="tdInformacionIndicador">
                    <b>Estatus :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$estatus} }}
                </td>
            </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Saldo a ejercer:</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$saldoEjercer} }}
                </td>
               </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Observaciones:</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$observaciones} }}
                </td>
               </tr>
        </table>

            @endforeach

    </div>
</div>
    <a href="#"></a>
    <div class="col-xs-12 col-sm-12 col-md-24 col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="btn-group pull-right">
                @if(Helpers::get_permiso("alta.indicadores.proyecto"))
                    @if($proyectos[0]->{Proyectos_Model::$estatus} == "Pendiente" || $proyectos[0]->{Proyectos_Model::$estatus} == "Rechazada"  || $proyectos[0]->{Proyectos_Model::$estatus} == "Activo" || $proyectos[0]->{Proyectos_Model::$estatus} == "En proceso" || $proyectos[0]->{Proyectos_Model::$estatus} == "En pausa")
                    <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar"
                            data-toggle="modal" data-target="#modal-forms"><span
                            class="glyphicon glyphicon-plus"></span>Agregar</a>
                    @endif
                @endif
                </div>
                <h3 class="panel-title">Control de indicadores</h3>
            </div>
            <div class="panel-body" style="overflow-x: auto;">
                <table class="table  table-bordered table-hover small" id="t_list">
                    <thead>
                        <tr>
                            <th rowspan="2" WIDTH="8%">Acciones</th>
                            <th rowspan="2" WIDTH="8%">Area responsable</th>
                            <th rowspan="2" WIDTH="6%">Indicador</th>
                            @for ($i = 1; $i <= $array->{ Proyectos_Model::$duracion }; $i++)
                            <th colspan="2">{{ $i }} {{ $array->{ Proyectos_Model::$tipo } }} </th> 
                            @endfor
                            <tr>
                                @for ($i = 1; $i <= $array->{ Proyectos_Model::$duracion }; $i++)
                                <th>Programado</th>
                                <th>Real</th>
                                @endfor
                            </tr> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cont=0;
                        $compara[]='0';
                        $compara2[]='0';
                        
                        ?>
                       @foreach($arrayActividad as $arrays)
                       <?php $contador=1;?>
                        @if($arrays->{Actividad_Model::$tipoIndicador}  !=  $compara[$cont] && $arrays->{Actividad_Model::$area} != $compara2[$cont] ) 
                       <tr>
                        <td> 
                            @if($proyectos[0]->{Proyectos_Model::$estatus} == "Pendiente" || $proyectos[0]->{Proyectos_Model::$estatus} == "Rechazada"  || $proyectos[0]->{Proyectos_Model::$estatus} == "Activo" || $proyectos[0]->{Proyectos_Model::$estatus} == "En proceso" || $proyectos[0]->{Proyectos_Model::$estatus} == "En pausa")
                             <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                            data-toggle="modal" data-target="#modal-forms" data-id="{{ $array->getAttribute(Proyectos_Model::$id) }}" data-indicador="{{ $arrays->getAttribute(Actividad_Model::$tipoIndicador) }}" data-actividad="{{ $arrays->{Actividad_Model::$id_actividad} }}"><span class="glyphicon glyphicon-pencil"></span></a>
                              @endif
                        </td>
                        <td>{{ $arrays->{Actividad_Model::$area} }}</td>
                        <td>{{ $arrays->{Actividad_Model::$tipoIndicador} }}</td>
                        @foreach($arrayActividad as $ok)
                         @if( $arrays->{Actividad_Model::$tipoIndicador}  ==  $ok->{Actividad_Model::$tipoIndicador} && $arrays->{Actividad_Model::$area} == $ok->{Actividad_Model::$area} ) 
                           <td>{{ $ok ->{Actividad_Model::$programado} }} {{$ok ->{Actividad_Model::$semana} }}</td>
                           <td>{{ $ok ->{Actividad_Model::$real} }} {{$ok ->{Actividad_Model::$semana} }}</td>
                           <?php 
                           $contador++;
                           ?>
                         @endif              
                        @endforeach
                        @if($contador != 1 )
                         @for ($contador; $contador <= $array->{ Proyectos_Model::$duracion }; $contador++)
                                <td></td>
                                <td></td>
                        @endfor
                        @endif
                    </tr>
                    @endif
                     <?php
                        $compara[]=$arrays->{Actividad_Model::$tipoIndicador};
                        $compara2[]=$arrays->{Actividad_Model::$area};
                        $cont++;
                        ?> 
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>





<a href="#">.</a>

@endsection
@include("Indicadores.Banco_Proyecto.Modal.form")
@include("Indicadores.Banco_Proyecto.Modal.actividad")
