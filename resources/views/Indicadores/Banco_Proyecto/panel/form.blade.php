<?php
use App\Models\Indicadores\Proyectos_Model;
use App\Http\Helpers\Helpers;

?>
    <style>
    .tdInformacionIndicador {
        width: 100px!important;
    }
    .tdcontenedor{
        width: 600px!important;
    }
</style>

<div class="panel panel-primary tdcontenedor">
    <div class="panel-heading  ">
        <div class="btn-group pull-right">
            @if($proyectos[0]->{Proyectos_Model::$estatus} == "PENDIENTE" || $proyectos[0]->{Proyectos_Model::$estatus} == "RECHAZADA")
            <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                data-toggle="modal" data-target="#modal-form"
                data-id="{{ $proyectos[0]->{Proyectos_Model::$id} }}"><span
                class="glyphicon glyphicon-pencil"></span>Editar</a>
                @endif
        </div>
        <h3 class="panel-title">Información Comisión</h3>
    </div>
    <div class="panel-body" style="overflow-x: auto;">
        @foreach($proyectos as $array)

        <table id="informacionComision" class="table">
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Nombre de la obra :</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$proyectoNombre} }}
                </td>
            </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Cliente :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$cliente} }}
                </td>
            </tr>
             <tr>
                <td class="tdInformacionIndicador">
                    <b>Fecha Inicial :</b>
                </td>
                <td >
                    {{ $array->{Proyectos_Model::$fechaInicial} }}
                </td>
                <td class="tdInformacionIndicador">
                    <b>Fecha de termino :</b>
                </td>
                <td >
                    {{ $array->{Proyectos_Model::$fechaTermino} }}
                </td>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Duracion :</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$duracion} }} {{ $array->{Proyectos_Model::$tipo} }}
                </td>
                
            </tr>
                   <tr>
                <td class="tdInformacionIndicador">
                    <b>Monto de contrato :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$montocontr} }}
                </td>
                 <td class="tdInformacionIndicador">
                    <b>Monto Ejercido :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$montoejercido} }}
                </td>
            </tr>
            <tr>
               
            </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Prioridad :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$prioridad} }}
                </td>
                 <td class="tdInformacionIndicador">
                    <b>Estatus :</b>
                </td>
                <td>
                    {{ $array->{Proyectos_Model::$estatus} }}
                </td>
            </tr>
            <tr>
                <td class="tdInformacionIndicador">
                    <b>Observaciones:</b>
                </td>
                <td colspan="4">
                    {{ $array->{Proyectos_Model::$observaciones} }}
                </td>
               </tr>
        </table>

            @endforeach

    </div>
</div>