<?php
use App\Models\Planeacion\Banco_Proyecto\Obra_Model;
use App\Http\Helpers\Helpers;

?>

@extends('layouts.listado')

@section('title')
    Banco de Proyectos
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Planeacion/banco_proyecto.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
        var oTable;
        var oTableCanceladas;
        var oTableProceso;
        var oTableTerminada;
    </script>

@endsection

@section('content')

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Obras Pendientes y en Concurso</a></li>
        <li><a data-toggle="tab" href="#menu1">Obras Canceladas</a></li>
        <li><a data-toggle="tab" href="#menu2">Obras en Proceso</a></li>
        <li><a data-toggle="tab" href="#menu3">Obras Terminadas</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active ">

            <div class="row">
                <div class="page-header">
                    <h1>Banco de Proyectos
                        <small></small>
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">
                                @if(Helpers::get_permiso("alta.planeacion.banco_proyecto"))
                                    <a href="#" type="button" class="btn btn-success btn-xs" title="Nuevo"
                                       data-toggle="modal"
                                       data-target="#model-nuevo_proyecto"><span class="glyphicon glyphicon-plus"
                                                                                 aria-hidden="true"></span> Nuevo
                                        Proyecto</a>
                                @endif
                            </div>
                            <h3 class="panel-title">Control de Proyectos</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover small" id="t_list">
                                <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Nombre</th>
                                    <th>Cliente</th>
                                    <th>Empresa Participante</th>
                                    <th>Descripción</th>
                                    <th>Prioridad</th>
                                    <th>Capital</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Término</th>
                                    <th>Fecha Limite Base</th>
                                    <th>Estatus</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($arrayObra as $obra)
                                    @if($obra->{ Obra_Model::$estatus } == 'PENDIENTE' || $obra->{ Obra_Model::$estatus }== 'PROGRAMADA'  )
                                        @if($obra->{ Obra_Model::$estatus } == 'PENDIENTE')
                                            <tr class="warning">
                                        @endif
                                        @if($obra->{ Obra_Model::$estatus } == 'PROGRAMADA')
                                            <tr>
                                                @endif
                                                <td class="text-center">
                                                    @if(Helpers::get_permiso("edicion.planeacion.banco_proyecto"))
                                                        @if($obra->{ Obra_Model::$estatus } == 'PENDIENTE' )
                                                            <a href="#" type="button" class="btn btn-success btn-xs"
                                                               title="Editar"
                                                               data-toggle="modal" data-target="#model-nuevo_proyecto"
                                                               data-id_editar_proyecto="{{ $obra->getAttribute(Obra_Model::$id) }}"><span
                                                                        class="glyphicon glyphicon-pencil"
                                                                        aria-hidden="true"></span></a>
                                                        @endif
                                                    @endif
                                                    @if(Helpers::get_permiso("baja.planeacion.banco_proyecto"))
                                                        @if($obra->{ Obra_Model::$estatus } == 'PENDIENTE' || $obra->{ Obra_Model::$estatus }== 'PROGRAMADA'  )
                                                            <a href="#" type="button" class="btn btn-danger btn-xs"
                                                               title="Cancelar"
                                                               data-toggle="modal"
                                                               data-target="#model-cancelar_proyecto"
                                                               data-id_eliminar_proyecto="{{ $obra->getAttribute(Obra_Model::$id) }}"><span
                                                                        class="glyphicon glyphicon-remove"
                                                                        aria-hidden="true"></span></a>
                                                        @endif
                                                    @endif
                                                    @if(Helpers::get_permiso("autorizar.planeacion.banco_proyecto"))
                                                        @if($obra->{ Obra_Model::$estatus } == 'PENDIENTE' )
                                                            <a href="#" type="button" class="btn btn-info btn-xs"
                                                               title="Autorizar"
                                                               data-toggle="modal"
                                                               data-target="#model-autorizar"
                                                               data-id_autorizar="{{ $obra->getAttribute(Obra_Model::$id) }}"><span
                                                                        class="glyphicon glyphicon-ok"
                                                                        aria-hidden="true"></span></a>
                                                        @endif
                                                    @endif

                                                        <a href="#" type="button" class="btn btn-warning btn-xs"
                                                           title="Ver Información Adicional"
                                                           data-toggle="modal"
                                                           data-target="#model-ver_adicional"
                                                           data-id_ver_adicional="{{ $obra->getAttribute(Obra_Model::$id) }}"><span
                                                                    class="glyphicon glyphicon-eye-open"
                                                                    aria-hidden="true"></span></a>

                                                </td>
                                                <td>{{ $obra->{ Obra_Model::$nombre } }}</td>
                                                <td>{{ $obra->{ "nombreCliente" } }}</td>
                                                <td>{{ $obra->{ "nombreEmpresa" } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$descripcion } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$prioridad } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$capitalMinimoContable } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$fechaInicio } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$fechaTermino } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$fechaLimiteBase } }}</td>
                                                <td>{{ $obra->{ Obra_Model::$estatus } }}</td>

                                            </tr>
                                        @endif
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- fin del tab 1 -->

        <div id="menu1" class="tab-pane fade">

            <div class="row">
                <div class="page-header">
                    <h1>Banco de Proyectos Cancelados
                        <small></small>
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">

                            </div>
                            <h3 class="panel-title">Control de Proyectos Cancelados</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover small" id="t_list_canceladas">
                                <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Nombre</th>
                                    <th>Cliente</th>
                                    <th>Empresa Participante</th>
                                    <th>Descripción</th>
                                    <th>Prioridad</th>
                                    <th>Capital</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Término</th>
                                    <th>Fecha Limite Base</th>
                                    <th>Estatus</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($arrayObra as $obra)
                                    @if($obra->{ Obra_Model::$estatus } == 'CANCELADA' )
                                        <tr class="danger">

                                            <td>
                                                <a href="#" type="button" class="btn btn-warning btn-xs"
                                                   title="Ver Información Adicional"
                                                   data-toggle="modal"
                                                   data-target="#model-ver_adicional"
                                                   data-id_ver_adicional="{{ $obra->getAttribute(Obra_Model::$id) }}"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"></span></a>
                                            </td>
                                            <td>{{ $obra->{ Obra_Model::$nombre } }}</td>
                                            <td>{{ $obra->{ "nombreCliente" } }}</td>
                                            <td>{{ $obra->{ "nombreEmpresa" } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$descripcion } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$prioridad } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$capitalMinimoContable } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaInicio } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaTermino } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaLimiteBase } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$estatus } }}</td>

                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- fin tab 2 -->

        <div id="menu2" class="tab-pane fade">

            <div class="row">
                <div class="page-header">
                    <h1>Banco de Proyectos en Proceso
                        <small></small>
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">

                            </div>
                            <h3 class="panel-title">Control de Proyectos en Proceso</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover small" id="t_list_proceso">
                                <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Nombre</th>
                                    <th>Cliente</th>
                                    <th>Empresa Participante</th>
                                    <th>Descripción</th>
                                    <th>Prioridad</th>
                                    <th>Capital</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Término</th>
                                    <th>Fecha Limite Base</th>
                                    <th>Estatus</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($arrayObra as $obra)
                                    @if($obra->{ Obra_Model::$estatus } == 'EN PROCESO' )
                                        <tr class="info">

                                            <td>
                                                <a href="#" type="button" class="btn btn-warning btn-xs"
                                                   title="Ver Información Adicional"
                                                   data-toggle="modal"
                                                   data-target="#model-ver_adicional"
                                                   data-id_ver_adicional="{{ $obra->getAttribute(Obra_Model::$id) }}"><span
                                                            class="glyphicon glyphicon-eye-open"
                                                            aria-hidden="true"></span></a>
                                            </td>
                                            <td>{{ $obra->{ Obra_Model::$nombre } }}</td>
                                            <td>{{ $obra->{ "nombreCliente" } }}</td>
                                            <td>{{ $obra->{ "nombreEmpresa" } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$descripcion } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$prioridad } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$capitalMinimoContable } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaInicio } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaTermino } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaLimiteBase } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$estatus } }}</td>

                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- fin del tab 2 -->

        <div id="menu3" class="tab-pane fade">

            <div class="row">
                <div class="page-header">
                    <h1>Banco de Proyectos Terminados
                        <small></small>
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">

                            </div>
                            <h3 class="panel-title">Control de Proyectos Terminados</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-hover small" id="t_list_terminada">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cliente</th>
                                    <th>Empresa Participante</th>
                                    <th>Descripción</th>
                                    <th>Prioridad</th>
                                    <th>Capital</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Término</th>
                                    <th>Fecha Limite Base</th>
                                    <th>Estatus</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($arrayObra as $obra)
                                    @if($obra->{ Obra_Model::$estatus } == 'TERMINADA' )
                                        <tr class="success">

                                            <td>{{ $obra->{ Obra_Model::$nombre } }}</td>
                                            <td>{{ $obra->{ "nombreCliente" } }}</td>
                                            <td>{{ $obra->{ "nombreEmpresa" } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$descripcion } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$prioridad } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$capitalMinimoContable } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaInicio } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaTermino } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$fechaLimiteBase } }}</td>
                                            <td>{{ $obra->{ Obra_Model::$estatus } }}</td>

                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div><!-- fin del tab principal-->




@endsection

@include("Planeacion.Banco_Proyecto.modal.nuevo")
@include("Planeacion.Banco_Proyecto.modal.eliminar")
@include("Planeacion.Banco_Proyecto.modal.ver_adicional")
@include("Planeacion.Banco_Proyecto.modal.autorizar_obra")