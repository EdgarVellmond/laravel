<?php
use App\Models\Planeacion\Banco_Proyecto\Obra_Model;

?>

<div class="modal fade" id="model-autorizar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Autorizar Obra y Asignación de Empresa</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form_autorizar','onSubmit' => 'return autorizarProyecto()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label>Empresa:</label>
                    {!! Form::select( Obra_Model::$idEmpresa , $arrayEmpresa, 0, ['id' => Obra_Model::$idEmpresa, 'class' => "form-control select2"]) !!}
                </div>

                <div class="form-group">
                    <label for="nombre">Comentarios:</label>
                    <input type="text" name="{{ Obra_Model::$observaciones }}"
                           id="{{ Obra_Model::$observaciones }}" class="form-control" placeholder="Comentarios de la Autorización" required
                    >
                </div>

                <input type="hidden" name="{{ Obra_Model::$id }}" id="{{ Obra_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Autorizar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>