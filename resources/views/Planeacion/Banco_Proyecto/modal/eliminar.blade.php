<?php
use App\Models\Planeacion\Banco_Proyecto\Obra_Model;

?>

<div class="modal fade" id="model-cancelar_proyecto">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Cancelar Proyecto</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form', 'onSubmit' => 'return cancelarProyecto()']) !!}
            <div class="modal-body">

                <label>¿ Estás seguro que deseas cancelar este Proyecto ?</label>
                <br>
                <br>
                <div class="form-group">
                    <label>Comentarios:</label>
                    <input type="text" name="{{ Obra_Model::$observaciones }}" id="{{ Obra_Model::$observaciones }}"
                           class="form-control" placeholder="Comentarios" required>
                </div>

                <input type="hidden" name="{{ Obra_Model::$id  }}" id="{{ Obra_Model::$id  }}">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Cancelar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>