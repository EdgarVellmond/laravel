<?php
use App\Models\Planeacion\Banco_Proyecto\Obra_Model;

?>
<style type="text/css">

    .tabla {
        width: 100%;
    }

    .tabla th {
        text-align: center;
    }

</style>
<div class="modal fade" id="model-nuevo_proyecto">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Proyecto</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarProyecto()']) !!}
            <div class="modal-body">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#principal">Información Principal</a></li>
                        <li><a data-toggle="tab" href="#adicional">Información Adicional</a></li>
                </ul>

                <div class="tab-content">
                    <div id="principal" class="tab-pane fade in active">
                        <br>
                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" name="{{ Obra_Model::$nombre }}"
                                   id="{{ Obra_Model::$nombre }}" class="form-control" placeholder="Nombre"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Folio:</label>
                            <input type="text" name="{{ Obra_Model::$folio }}"
                                   id="{{ Obra_Model::$folio }}" class="form-control" placeholder="Folio de la Obra"
                            >
                        </div>
                        <div class="form-group">
                            <label for="nombre">Descripción:</label>
                            <input type="text" name="{{ Obra_Model::$descripcion }}"
                                   id="{{ Obra_Model::$descripcion }}" class="form-control" placeholder="Descripción de la Obra"
                            >
                        </div>
                        <div class="form-group">
                            <label>Cliente:</label>
                            {!! Form::select( Obra_Model::$idCliente , $arrayCliente, 0, ['id' => Obra_Model::$idCliente, 'class' => "form-control select2"]) !!}
                        </div>
                        <div class="form-group">
                            <label>Prioridad:</label>
                            {!! Form::select( Obra_Model::$prioridad , Config::get('enums.prioridad'), 0, ['id' => Obra_Model::$prioridad, 'class' => "form-control"]) !!}
                        </div>
                        <div class="form-group">
                            <label>Capital Mínimo Contable:</label>
                            <input type="text" name="{{ Obra_Model::$capitalMinimoContable }}" id="{{ Obra_Model::$capitalMinimoContable }}"
                                   class="form-control" placeholder="Capital Mínimo Contable" >
                        </div>
                    </div>
                    <div id="adicional" class="tab-pane fade">
                        <br>
                        <div class="form-group">
                            <label>Estado:</label>
                            {!! Form::select( "id_estado" , $arrayEstado, 0, ['id' => 'id_estado', 'class' => "form-control select2"]) !!}
                        </div>
                        <div class="form-group">
                            <label>Municipio:</label>
                            <select class="form-control select2" id="{{ Obra_Model::$idMunicipio }}"
                                    name="{{ Obra_Model::$idMunicipio }}">
                                <option value="0">N/D</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Localidad:</label>
                            <input type="text" name="{{ Obra_Model::$localidad }}" id="{{ Obra_Model::$localidad }}"
                                   class="form-control" placeholder="Localidad" >
                        </div>
                        <div class="form-group">
                            <label>Fecha de Inicio:</label>
                            <input type="date" name="{{ Obra_Model::$fechaInicio }}" id="{{ Obra_Model::$fechaInicio }}"
                                   class="form-control" placeholder="Fecha Estimada del Inicio de la Obra" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de Término:</label>
                            <input type="date" name="{{ Obra_Model::$fechaTermino }}" id="{{ Obra_Model::$fechaTermino }}"
                                   class="form-control" placeholder="Fecha Estimada del Termino de la Obra" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha Límite Base:</label>
                            <input type="date" name="{{ Obra_Model::$fechaLimiteBase }}" id="{{ Obra_Model::$fechaLimiteBase }}"
                                   class="form-control" placeholder="Fecha límite de la base" required>
                        </div>
                    </div>
                </div>




                <input type="hidden" name="{{ Obra_Model::$id }}" id="{{ Obra_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>