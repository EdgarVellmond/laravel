<?php
use App\Models\Planeacion\Banco_Proyecto\Obra_Model;

?>

<div class="modal fade" id="model-ver_adicional">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ver Información Adicional</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Estado:</span>
                    <input type="text" name="id_estado" id="id_estado"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Municipio:</span>
                    <input type="text" name="{{ Obra_Model::$idMunicipio }}" id="{{ Obra_Model::$idMunicipio }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Localidad:</span>
                    <input type="text" name="{{ Obra_Model::$localidad }}"
                           id="{{ Obra_Model::$localidad }}" class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Folio:</span>
                    <input type="text" name="{{ Obra_Model::$folio }}"
                           id="{{ Obra_Model::$folio }}" class="form-control"
                           disabled style="background-color: white">
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Observaciones:</span>
                    <input type="text" name="{{ Obra_Model::$observaciones }}" id="{{ Obra_Model::$observaciones }}"
                           class="form-control"
                           disabled style="background-color: white">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>