<?php
use App\Http\Helpers\Helpers;
use App\Models\Catalogos\Cliente_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Planeacion\Registro_Padron_Model;
use App\Models\Genericos\Documento_Model;

?>

@extends('layouts.listado')

@section('title')
    Registro de Padrón
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/planeacion/registro_padron.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
        var oTable;
        var urlBase = '{{url("/planeacion")}}';
    </script>



@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}">Principal</a></li>
        <li class="active">Catálogo de Faltas</li>
    </ol>
@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Registro de Padrón
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.recursos_humanos.falta"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar" data-toggle="modal"
                               data-target="#model-guardar"><span class="glyphicon glyphicon-plus"
                                                                  aria-hidden="true"></span> Nuevo Registro</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Registro de Padrón</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>ID local de Registro</th>
                            <th>Cliente</th>
                            <th>Empresa</th>
                            <th>Inscripción</th>
                            <th>Vigencia</th>
                            <th>Comentario</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayRegistroPadron as $registroPadron)
                            
                            <?php
                                $fechaInicial = new DateTime($registroPadron->{ Registro_Padron_Model::$inicioVigencia});
                                $diferencia = $fechaInicial->diff(new DateTime($registroPadron->{ Registro_Padron_Model::$finVigencia}));
                            ?> 

                            @if($registroPadron->{ Registro_Padron_Model::$estatus} == "Inscrito y vigente")

                                @if($diferencia->days > 30)
                                    <?php $clase = "success"; ?>
                                @elseif($diferencia->days <= 30 && $diferencia->days > 10)
                                    <?php $clase = "warning"; ?>
                                @elseif($diferencia->days <= 10)
                                    <?php $clase = "danger"; ?>
                                @else
                                    <?php $clase = ""; ?>
                                @endif
                                  
                            @else
                                <?php $clase = ""; ?>
                            @endif

                             
                            <tr class="{{$clase}}">
                                <td class="text-center">
                                    @if(Helpers::get_permiso("update.planeacion.registro_padron") && $registroPadron->{ Registro_Padron_Model::$estatus } == "Inscrito y vigente")
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Actualizar"
                                           data-toggle="modal" data-target="#modal-update"
                                           data-id="{{ $registroPadron->{Registro_Padron_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-refresh" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("edicion.planeacion.registro_padron") && $registroPadron->{ Registro_Padron_Model::$estatus } == "Inscrito y vigente")
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#model-guardar"
                                           data-id_editar="{{ $registroPadron->{Registro_Padron_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("vista.planeacion.registro_padron") && $registroPadron->{ Registro_Padron_Model::$estatus } == "Inscrito y vigente")
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Historial"
                                           data-toggle="modal" data-target="#modal-historial"
                                           data-id="{{ $registroPadron->{Registro_Padron_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-hdd" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("download.planeacion.registro_padron"))
                                            <a href="{{ url('/recursos_humanos').'/descargarDocumentos/'.$registroPadron->{Registro_Padron_Model::$id}.'/registro_padron/registro_padron'}}" class="btn btn-xs btn-success" 
                                            target="_blank" title="Descargar documento">
                                            <span class="glyphicon glyphicon-download-alt"></span></a>
                                        @endif
                                </td>
                                <td>{{ $registroPadron->{ Registro_Padron_Model::$id } }}</td>
                                <td>{{ $registroPadron->{ "nombreCliente" } }}</td>
                                <td>{{ $registroPadron->{ "nombreEmpresa" } }}</td>
                                <td>{{ $registroPadron->{ Registro_Padron_Model::$inscripcion } }}</td>
                                <td>{{ $registroPadron->{ Registro_Padron_Model::$inicioVigencia } . "  a  " . $registroPadron->{ Registro_Padron_Model::$finVigencia } }}</td></td>
                                <td>{{ $registroPadron->{ Registro_Padron_Model::$comentario } }}</td></td>
                                <td>{{ $registroPadron->{ Registro_Padron_Model::$estatus } }}</td></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@include("Planeacion.Registro_Padron.modal.form")
@include("Planeacion.Registro_Padron.modal.update")
@include("Planeacion.Registro_Padron.modal.historial")

@endsection
