<?php
use App\Models\Recursos_Humanos\Empleado_Model;

?>

<div class="modal fade" id="modal-historial">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Historial de Padrón</h4>
            </div>
            <div class="modal-body">


                <table class="table table-hover table-bordered small" id="historial_registro_padron">
                    <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>ID local de Registro</th>
                            <th>Vigencia</th>
                            <th>Comentario</th>
                            <th>Estatus</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table> 

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>