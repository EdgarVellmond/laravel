<?php
use App\Models\Planeacion\Registro_Padron_Model;
use App\Models\Catalogos\Cliente_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Genericos\Documento_Model;

?>

<div class="modal fade" id="modal-update">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Actualizar vigencias de Padrón</h4>
            </div>
            {!! Form::open(['url' => url('planeacion/actualizarRegistroPadron'),'class'=>'form','id'=>'add_edit_form', 'enctype'=>'multipart/form-data', 'file' => true]) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Inicio de Vigencia*</span>
                    <input type="date" value="" placeholder="" class="form-control"
                        id="modal-update-{{Registro_Padron_Model::$inicioVigencia}}"
                        name="{{Registro_Padron_Model::$inicioVigencia}}"
                        style="background-color: white" required>
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Fin de Vigencia*</span>
                    <input type="date" value="" placeholder="" class="form-control"
                        id="modal-update-{{Registro_Padron_Model::$finVigencia}}"
                        name="{{Registro_Padron_Model::$finVigencia}}"
                        style="background-color: white" required>
                </div>
                <br>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Comentario</span>
                    <input type="text" value="" placeholder="" class="form-control"
                        id="modal-update-{{Registro_Padron_Model::$comentario}}"
                        name="{{Registro_Padron_Model::$comentario}}"
                        style="background-color: white" >
                </div>
                <br>
                <div class="input-group input-group-sm" >
                    <span class="input-group-addon">Comprobante</span>
                    <input type="file" class="form-control" placeholder=""
                        id="modal-file-update-{{Documento_Model::$id}}"
                        name="{{Documento_Model::$id}}"
                        style="background-color: white;">
                </div>

                <input type="hidden" name="{{ Registro_Padron_Model::$id }}" id="{{ Registro_Padron_Model::$id }}" value>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>