<?php

use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Sistema\Usuarios_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Expediente_Digital_Model;
use App\Models\Recursos_Humanos\Expediente_Digital_Empleado_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Http\Helpers\Helpers;
use App\Models\Recursos_Humanos\Plantilla_Empleado_Model;
use App\Models\Recursos_Humanos\Falta_Model;
use App\Models\Genericos\Documento_Model;

?>
@extends('layouts.listado')

@section('title')
    Ficha del Empleado
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Recursos_Humanos/Empleado/ficha_empleado.js') !!}
    <script type="text/javascript">

        var oTable;
        var oTableIncidencias;
        var oTablePlantilla;
        var oTableExpediente;
        var idExpedienteDigitalEmpleado = "{{Expediente_Digital_Empleado_Model::$id}}";
        var idEmpleado = "{{Empleado_Model::$id}}";
        var idExpedienteDigital = "{{Expediente_Digital_Model::$id}}";
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var urlBase = "{{url('/')}}/";

    </script>
@endsection
@section('content')


    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Principal</a></li>
        <li><a href="{{ url('/recursos_humanos/empleado') }}">Control de Empleados</a></li>
        <li class="active">Ficha</li>
    </ol>

    <div class="row">
        <div class="page-header">
            <h1>Ficha del Empleado
                <small></small>
            </h1>
        </div>

        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="btn-group pull-right">

                        </div>
                        <h3 class="panel-title">Información General</h3>
                    </div>
                    <div class="panel-body">
                    <?php 
                        $permiso_vista_adicional = Helpers::get_permiso("vista_adicional.recursos_humanos.empleado");
                        $permiso_vista_licencia = Helpers::get_permiso("vista_licencia.recursos_humanos.empleado");
                        $permiso_vista_nomina = Helpers::get_permiso("vista_nomina.recursos_humanos.empleado");
                        $permiso_vista_domicilio = Helpers::get_permiso("vista_domicilio.recursos_humanos.empleado");
                        $permiso_vista_sucursal = Helpers::get_permiso("vista_sucursal.recursos_humanos.empleado");
                        $permiso_vista_fotografia = Helpers::get_permiso("vista_fotografia.recursos_humanos.empleado");

                    ?>
                        <table>
                            <tr>
                                <td><b>Nombre:&nbsp;&nbsp;</b></td>
                                <td>{{ $ficha[0]->{Empleado_Model::$nombre}. " " . $ficha[0]->{Empleado_Model::$apellidoPaterno} . " " . $ficha[0]->{Empleado_Model::$apellidoMaterno} }}</td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>Calle:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{Empleado_Model::$calle} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>No. Empleado:&nbsp;&nbsp;</b></td>
                                <td>{{ $ficha[0]->{Empleado_Model::$numeroEmpleado} }}</td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>No. Exterior:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{Empleado_Model::$numeroExterior} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Sucursal:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_sucursal)
                                        {{ $ficha[0]->{"nombreSucursal"} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>No. Interior:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{Empleado_Model::$numeroInterior} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Patrón:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_nomina)
                                        {{ $ficha[0]->{Empresa_Model::$razonSocial} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>Colonia:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{Empleado_Model::$colonia} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>R.F.C.:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{Empresa_Model::$rfc} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>C.P.:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{Empleado_Model::$cp} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>N.S.S.:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{Empleado_Model::$nss} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>Municipio:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{"nombreMunicipio"} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Curp:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{Empleado_Model::$curp} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>Estado:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_domicilio)
                                        {{ $ficha[0]->{"nombreEstado"} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Área:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{"nombreArea"} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>No. Licencia:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_licencia)
                                        {{ $ficha[0]->{Empleado_Model::$numeroLicencia} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Depto.:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{"nombreDepartamento"} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>Vigencia Licencia:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_licencia)
                                        {{ $ficha[0]->{Empleado_Model::$vigenciaLicencia} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Puesto:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{"nombrePuesto"} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Teléfono:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{Empleado_Model::$telefono} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><b>Correo:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_adicional)
                                        {{ $ficha[0]->{Empleado_Model::$correo} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="btn-group pull-right">
                            @if(Helpers::get_permiso("alta_contrato.recursos_humanos.empleado"))
                                <a href="#" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-id=""
                                   data-target="#modal-generar_contrato">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    Nuevo Contrato
                                </a>
                            @endif
                        </div>
                        <h3 class="panel-title">Contrato</h3>
                    </div>
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td><b>Sueldo Diario:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_nomina)
                                        {{ $ficha[0]->{Empleado_Model::$salarioBaseDiario} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>Salario Integrado:&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_nomina)
                                        {{ $ficha[0]->{Empleado_Model::$salarioDiarioIntegrado} }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><b>¿ Viatica ?&nbsp;&nbsp;</b></td>
                                <td>
                                    @if($permiso_vista_nomina)
                                        {{ $ficha[0]->{Empleado_Model::$viaticos} == 1 ? "Si":"No" }}
                                    @else
                                        {{"Sin permiso de Vista"}}
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table class="table table-hover table-bordered small" id="t_list">
                            <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Fecha de Captura</th>
                                <th>Fecha de Inicio</th>
                                <th>Fecha de Fin</th>
                                <th>Observaciones</th>
                                <th>Estatus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contrato as $row)
                                <?php $clase = "" ?>
                                @if($row->{Plantilla_Empleado_Model::$vigencia} == 1)
                                    <?php $clase = "success";?>
                                @endif
                                <tr class="{{$clase}}">
                                    <td>
                                        @if(Helpers::get_permiso("vista_previa_contrato.recursos_humanos.empleado"))
                                            <a href="#" data-src = "{{url('/catalogos/vistaPreviaCompleta/').'/'. $row->{Plantilla_Empleado_Model::$id} .'/'. $row->{Plantilla_Empleado_Model::$idEmpleado}  }}" data-toggle="modal" class="btn btn-xs btn-info" data-target="#modal-vista_previa">
                                            <span class="glyphicon glyphicon-eye-open"></span></a>
                                        @endif
                                    </td>
                                    <td>{{$row->{Plantilla_Empleado_Model::$createdAt} }}</td>
                                    <td>{{$row->{Plantilla_Empleado_Model::$fechaInicio} }}</td>
                                    <td>{{$row->{Plantilla_Empleado_Model::$fechaFin} }}</td>
                                    <td>{{$row->{Plantilla_Empleado_Model::$observaciones} }}</td>
                                    <td>
                                        @if($row->{Plantilla_Empleado_Model::$vigencia} == 1)
                                            {{"Vigente"}}
                                        @else
                                            {{"Historial"}}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="btn-group pull-right">
                        </div>
                        <h3 class="panel-title">Incidencias</h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-hover table-bordered small" id="t_list_incidencias">
                            <thead>
                            <tr>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th>Dias</th>
                                <th>Tipo</th>
                                <th>Observaciones</th>
                                <th>Estatus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($incidencias as $incidencia)
                                <tr>
                                    <td>
                                        {{$incidencia->{Falta_Model::$fechaInicio} }}
                                    </td>
                                    <td>
                                        {{$incidencia->{Falta_Model::$fechaFin} }}
                                    </td>
                                    <td>
                                        {{$incidencia->{Falta_Model::$dias} }}
                                    </td>
                                    <td>
                                        {{$incidencia->{"tipo"} }}
                                    </td>
                                    <td>
                                        {{$incidencia->{Falta_Model::$comentarios} }}
                                    </td>
                                    <td>
                                        {{$incidencia->{"estatus"} }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="btn-group pull-right">
                        </div>
                        <h3 class="panel-title">Expediente Único Digital</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover table-bordered small" id="t_list_expediente">
                            <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Documento</th>
                                <th>Archivo subido</th>
                                <th>Capturó/Modificó</th>
                                <th>Fecha</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $idRowAnterior = 0; ?>
                            @foreach($expediente as $row)
                                <?php $clase = "success";?>
                                
                                @if(!$row->{Documento_Model::$id})
                                    <?php $clase = "danger";?>
                                @endif
                                
                                @if($row->{Expediente_Digital_Model::$id} != $idRowAnterior)
                                <tr class="{{$clase}}">
                                    <td>
                                        @if(Helpers::get_permiso("download.expediente_digital.empleado"))
                                            <a href="{{ url('/recursos_humanos').'/descargarDocumentos/'.$ficha[0]->{Empleado_Model::$id}.'/empleado/expediente_digital_empleado'}}" class="btn btn-xs btn-success" 
                                            target="_blank" title="Descargar documento">
                                            <span class="glyphicon glyphicon-download-alt"></span></a>
                                        @endif
                                        @if(Helpers::get_permiso("vista_previa.expediente_digital.empleado"))
                                            <a data-src="{{ url('/').'/'.$row->{Documento_Model::$ruta}.$row->{Documento_Model::$uuid}.'.'.$row->{Documento_Model::$extension} }}" class="btn btn-xs btn-success" 
                                            title="Ver documento" data-toggle="modal" data-target="#modal-vista_previa">
                                            <span class="glyphicon glyphicon-eye-open"></span></a>
                                        @endif
                                        @if(Helpers::get_permiso("upload.expediente_digital.empleado"))
                                            <a href="#" data-id_empleado="{{$ficha[0]->{Empleado_Model::$id} }}" data-id_expediente_digital="{{$row->{Expediente_Digital_Model::$id} }}" data-id="{{$row->{Expediente_Digital_Empleado_Model::$id} }}" data-toggle="modal" class="btn btn-xs btn-info" data-target="#modal-subir" title="Subir documento">
                                            <span class="glyphicon glyphicon-upload"></span></a>
                                        @endif
                                    </td>
                                    <td>{{$row->{Expediente_Digital_Model::$nombre} }}</td>
                                    <td>
                                        @if(!$row->{Documento_Model::$id})
                                            {{"No hay documento"}}
                                        @else
                                            {{$row->{Documento_Model::$nombre . "_documento"} }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($row->{Documento_Model::$idUsuarioEdicion} )
                                            {{$row->{Usuarios_Model::$id . "_Edicion"} }}
                                        @else
                                            {{$row->{Usuarios_Model::$id} }}
                                        @endif
                                    </td>
                                    <td>{{$row->{Documento_Model::$updatedAt} }}</td>
                                </tr>
                                @endif
                                <?php $idRowAnterior = $row->{Expediente_Digital_Model::$id}; ?>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                    </div>
                    <h3 class="panel-title">Fotografía</h3>
                </div>
                <div class="panel-body">
                    @if($permiso_vista_fotografia)
                    <img src="{{ url('/')."/".$ficha[0]->{Empleado_Imagen_Model::$ruta}.$ficha[0]->{Empleado_Imagen_Model::$uuid}.".".$ficha[0]->{Empleado_Imagen_Model::$extension} }}"
                         style="width: 100%; height: auto;">
                    @else
                        {{"Sin permiso para ver la Fotografía"}}
                    @endif
                </div>
            </div>
        </div>
@if(Helpers::get_permiso("alta_contrato.recursos_humanos.empleado"))
    @include('Recursos_Humanos/Empleado/modal/generar_contrato')
@endif
@include('Recursos_Humanos/Empleado/modal/vista_previa')
@if(Helpers::get_permiso("upload.expediente_digital.empleado"))
    @include('Recursos_Humanos/Empleado/modal/subir_documento')
@endif
    </div>

@endsection


