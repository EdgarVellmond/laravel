<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Municipio_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;
use App\Http\Helpers\Helpers;
use App\Models\Genericos\Solicitud_Model;
use App\Models\Catalogos\Tipo_Solicitud_Model;

$ruta = "recursos_humanos/cambioEstatusSolicitud";

?>
@extends('layouts.listado')

@section('title')
    Catálogo Empleado
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Recursos_Humanos/Empleado/empleado.js') !!}
    {!! Html::script('js/app/Genericos/solicitud.js') !!}

    <script type="text/javascript">
        var oTable;
        var oTableSolicitud;
        var oTableSolicitudCancelado;
        var oTableSolicitudFinalizado;
        var token = '{{ csrf_token() }}';
        var urlLanguage = "{{ asset('vendor/datatables/lang/es_MX.json') }}";

        var idEmpleado = "{{Empleado_Model::$id}}";

        var nombre = "{{Empleado_Model::$nombre}}";
        var apellidoPaterno = "{{Empleado_Model::$apellidoPaterno}}";
        var apellidoMaterno = "{{Empleado_Model::$apellidoMaterno}}";
        var numeroEmpleado = "{{Empleado_Model::$numeroEmpleado}}";
        var rfc = "{{Empleado_Model::$rfc}}";
        var curp = "{{Empleado_Model::$curp}}";
        var nss = "{{Empleado_Model::$nss}}";
        var correo = "{{Empleado_Model::$correo}}";
        var telefono = "{{Empleado_Model::$telefono}}";
        var idEmpleadoImagen = "{{Empleado_Model::$idEmpleadoImagen}}";
        var idArea = "{{Departamento_Model::$idArea}}";
        var idDepartamento = "{{Departamento_Model::$id}}";
        var idPuesto = "{{Puesto_Model::$id}}";

        var calle = "{{Empleado_Model::$calle}}";
        var numeroExterior = "{{Empleado_Model::$numeroExterior}}";
        var numeroInterior = "{{Empleado_Model::$numeroInterior}}";
        var colonia = "{{Empleado_Model::$colonia}}";
        var cp = "{{Empleado_Model::$cp}}";
        var idMunicipio = "{{Empleado_Model::$idMunicipio}}";
        var idEstado = "{{Municipio_Model::$idEstado}}";

        var numeroLicencia = "{{Empleado_Model::$numeroLicencia}}";
        var vigenciaLicencia = "{{Empleado_Model::$vigenciaLicencia}}";

        var salarioBaseDiario = "{{Empleado_Model::$salarioBaseDiario}}";
        var salarioDiarioIntegrado = "{{Empleado_Model::$salarioDiarioIntegrado}}";
        var idPatron = "{{ Empleado_Model::$idPatron }}";
        var idPrimaRiesgo = "{{ Empleado_Model::$idPrimaRiesgo }}";
        var viaticos = "{{Empleado_Model::$viaticos}}";

        var idEmpresa = "{{Empresa_Model::$id}}";
        var nombreSucursal = "{{Sucursal_Model::$nombre}}";
        var clave = "{{Sucursal_Model::$clave}}";
        var calleSucursal = "{{Sucursal_Model::$calle}}";
        var coloniaSucursal = "{{Sucursal_Model::$colonia}}";
        var correoSucursal = "{{Sucursal_Model::$correo}}";
        var numeroInteriorSucursal = "{{Sucursal_Model::$numeroInterior}}";
        var numeroExteriorSucursal = "{{Sucursal_Model::$numeroExterior}}";
        var telefonoSucursal = "{{Sucursal_Model::$telefono}}";
        var cpSucursal = "{{Sucursal_Model::$cp}}";
        var diasVacaciones = "{{Empleado_Model::$diasVacaciones}}";

        var urlBase = "{{url('/')}}/";
    </script>

@endsection
@section('content')


    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Control de Empleados</a></li>
        @if(Helpers::get_permiso($permiso, 'F'))
            <li><a data-toggle="tab" href="#menu1">Solicitudes</a></li>
        @endif
        @if(Helpers::get_permiso($permiso, 'F'))
            <li><a data-toggle="tab" href="#menu2">Solicitudes Canceladas</a></li>
        @endif
        @if(Helpers::get_permiso($permiso, 'F'))
            <li><a data-toggle="tab" href="#menu3">Solicitudes Finalizadas</a></li>
        @endif
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active ">

            <div class="row">
                <div class="page-header">
                    <h1>Catálogo de Empleado
                        <small></small>
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="btn-group pull-right">
                                 @if(Helpers::get_permiso("alta.recursos_humanos.empleado"))
                                    <a href="#" type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                       data-id="" title="Nuevo Empleado"
                                       data-target="#modal-form">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Nuevo
                                    </a>
                                @endif
                            </div>
                            <h3 class="panel-title">Control de Empleado</h3>
                        </div>
                        <div class="panel-body">

                            <table class="table table-hover table-bordered small" id="t_list">
                                <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>N° Empleado</th>
                                    <th>Adicional</th>
                                    <th>Licencia</th>
                                    <th>Nómina</th>
                                    <th>Domicilio</th>
                                    <th>Sucursal</th>
                                    <th>Fotografía</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($empleados as $row)
                                    <tr>
                                        <td>
                                            @if(Helpers::get_permiso("edicion.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-success" data-toggle="modal"
                                                   data-target="#modal-form" title="Editar"
                                                   data-id="{{$row->{Empleado_Model::$id} }}">
                                                    <span class="glyphicon glyphicon-pencil"></span></a>
                                            @endif
                                            @if(Helpers::get_permiso("baja.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-danger" data-toggle="modal"
                                                   data-target="#modal-eliminar" title="Eliminar"
                                                   data-id="{{$row->{Empleado_Model::$id} }}">
                                                    <span class="glyphicon glyphicon-remove"></span></a>
                                            @endif
                                            @if(Helpers::get_permiso("ficha.recursos_humanos.empleado"))
                                                <a href="fichaEmpleado/{{$row->{Empleado_Model::$id} }}" title="Ficha"
                                                   class="btn btn-xs btn-warning">
                                                    <span class="glyphicon glyphicon-eye-open"></span></a>
                                            @endif
                                        </td>
                                        <td>{{$row->{Empleado_Model::$id} }}</td>
                                        <td>{{$row->{Empleado_Model::$nombre} . " " . $row->{Empleado_Model::$apellidoPaterno} . " " . $row->{Empleado_Model::$apellidoMaterno} }}</td>
                                        <td>{{$row->{Empleado_Model::$numeroEmpleado} }}</td>
                                        <td>
                                             @if(Helpers::get_permiso("vista_adicional.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-info" data-toggle="modal"
                                                   data-target="#modal-ver-adicional"
                                                   data-id="{{$row->{Empleado_Model::$id} }}" title="Ver">
                                                    <span class="glyphicon glyphicon-eye-open"></span></a>
                                            @else
                                                No tienes permiso de Vista
                                            @endif
                                        </td>
                                        <td>
                                             @if(Helpers::get_permiso("vista_licencia.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-info" data-toggle="modal" title="Ver"
                                                   data-target="#modal-ver-licencia"
                                                   data-id="{{$row->{Empleado_Model::$id} }}">
                                                    <span class="glyphicon glyphicon-eye-open"></span></a>
                                            @else
                                                No tienes permiso de Vista
                                            @endif
                                        </td>
                                        <td>
                                             @if(Helpers::get_permiso("vista_nomina.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-info" data-toggle="modal" title="Ver"
                                                   data-target="#modal-ver_nomina"
                                                   data-id="{{$row->{Empleado_Model::$id} }}">
                                                    <span class="glyphicon glyphicon-eye-open"></span></a>
                                            @else
                                                No tienes permiso de Vista
                                            @endif
                                        </td>
                                        <td>
                                             @if(Helpers::get_permiso("vista_domicilio.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-info" data-toggle="modal" title="Ver"
                                                   data-target="#modal-ver-direccion"
                                                   data-id="{{$row->{Empleado_Model::$id} }}">
                                                    <span class="glyphicon glyphicon-home"></span></a>
                                            @else
                                                No tienes permiso de Vista
                                            @endif
                                        </td>
                                        <td>
                                             @if(Helpers::get_permiso("vista_sucursal.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-info" data-toggle="modal" title="Ver"
                                                   data-target="#modal-ver_sucursal"
                                                   data-id="{{$row->{Empleado_Model::$idSucursal} }}">
                                                    <span class="glyphicon glyphicon-eye-open"></span></a>
                                            @else
                                                No tienes permiso de Vista
                                            @endif
                                        </td>
                                        <td>
                                             @if(Helpers::get_permiso("vista_fotografia.recursos_humanos.empleado"))
                                                <a href="#" class="btn btn-xs btn-info" data-toggle="modal" title="Ver"
                                                   data-target="#modal-ver-imagen"
                                                   data-id="{{$row->{Empleado_Model::$idEmpleadoImagen} }}">
                                                    <span class="glyphicon glyphicon-eye-open"></span></a>
                                            @else
                                                No tienes permiso de Vista
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- fin del primer tab -->

        @if(Helpers::get_permiso("vista.generico.solicitud"))
            <div id="menu1" class="tab-pane fade">

                <div class="row">
                    <div class="page-header">
                        <h1>Solicitudes
                            <small></small>
                        </h1>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="btn-group pull-right">

                                </div>
                                <h3 class="panel-title">Control de Solicitudes</h3>
                            </div>
                            <div class="panel-body">

                                @include("Genericos.Solicitud.table")

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if(Helpers::get_permiso("vista.generico.solicitud"))
            <div id="menu2" class="tab-pane fade">

                <div class="row">
                    <div class="page-header">
                        <h1>Solicitudes
                            <small></small>
                        </h1>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="btn-group pull-right">

                                </div>
                                <h3 class="panel-title">Control de Solicitudes</h3>
                            </div>
                            <div class="panel-body">

                                @include("Genericos.Solicitud.table_cancelado")

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if(Helpers::get_permiso("vista.generico.solicitud"))
            <div id="menu3" class="tab-pane fade">

                <div class="row">
                    <div class="page-header">
                        <h1>Solicitudes
                            <small></small>
                        </h1>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="btn-group pull-right">

                                </div>
                                <h3 class="panel-title">Control de Solicitudes</h3>
                            </div>
                            <div class="panel-body">

                                @include("Genericos.Solicitud.table_finalizado")

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if(Helpers::get_permiso("vista.generico.solicitud"))
            @if(Helpers::get_permiso("comenzar.generico.solicitud"))
                @include("Genericos.Solicitud.modal.comenzar")
            @endif
            @if(Helpers::get_permiso("rechazar.generico.solicitud"))
                @include("Genericos.Solicitud.modal.rechazar")
            @endif
            @if(Helpers::get_permiso("finalizar.generico.solicitud"))
                @include("Genericos.Solicitud.modal.finalizar")
            @endif
            @if(Helpers::get_permiso("cancelar.generico.solicitud"))
                @include("Genericos.Solicitud.modal.cancelar")
            @endif
        @endif

    </div>

    @include("Recursos_Humanos.Empleado.modal.form")
    @include("Recursos_Humanos.Empleado.modal.ver_adicional")
    @include("Recursos_Humanos.Empleado.modal.ver_licencia")
    @include("Recursos_Humanos.Empleado.modal.ver_nomina")
    @include("Recursos_Humanos.Empleado.modal.ver_direccion")
    @include("Recursos_Humanos.Empleado.modal.ver_sucursal")
    @include("Recursos_Humanos.Empleado.modal.ver_imagen")
    @include("Recursos_Humanos.Empleado.modal.eliminar")

@endsection