<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Estado_Model;
use App\Models\Catalogos\Empresa_Model;
use App\Models\Catalogos\Area_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Catalogos\Puesto_Model;
use App\Models\Catalogos\Municipio_Model;

?>
<style type="text/css">
    .max300 {
        max-width: 300px;
    }
</style>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Empleado</h4>
            </div>
            {!! Form::open(['id'=>'add_edit_form', 'url' => url('recursos_humanos/guardarEmpleado'),'role' => 'form', 'class' => 'form', 'enctype'=>'multipart/form-data', 'file'=>true, 'onSubmit' => 'return validarCombos()']) !!}
            <div class="modal-body">

                <!-- Tabs del formulario -->
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#general">General</a></li>
                    <li><a data-toggle="tab" href="#direccion">Dirección</a></li>
                    <li><a data-toggle="tab" href="#licencia">Licencia</a></li>
                </ul>

                <div class="tab-content">
                    <br>
                    <div id="general" class="tab-pane fade in active">

                        <table class="tabla" id="t_list">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Nombre</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$nombre}}"
                                               name="{{Empleado_Model::$nombre}}" style="background-color: white"
                                               required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Apellido paterno</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$apellidoPaterno}}"
                                               name="{{Empleado_Model::$apellidoPaterno}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Apellido materno</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$apellidoMaterno}}"
                                               name="{{Empleado_Model::$apellidoMaterno}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">N° de Empleado</span>
                                        <input type="number" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$numeroEmpleado}}"
                                               name="{{Empleado_Model::$numeroEmpleado}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm max300">
                                        <span class="input-group-addon">RFC</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$rfc}}" name="{{Empleado_Model::$rfc}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm max300">
                                        <span class="input-group-addon">CURP</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$curp}}"
                                               name="{{Empleado_Model::$curp}}" style="background-color: white"
                                               required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm max300">
                                        <span class="input-group-addon">NSS</span>
                                        <input type="number" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$nss}}" name="{{Empleado_Model::$nss}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Teléfono</span>
                                        <input type="number" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$telefono}}"
                                               name="{{Empleado_Model::$telefono}}" style="background-color: white"
                                               required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Correo</span>
                                        <input type="mail" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$correo}}"
                                               name="{{Empleado_Model::$correo}}" style="background-color: white"
                                               required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm max300">
                                        <span class="input-group-addon">Área</span>
                                        {!! Form::select( Area_Model::$id , $area, 0, ['id' => "modal-form-" . Area_Model::$id, 'class' => "form-control select2", "style" => "width: 100%;"]) !!}
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm max300">
                                        <span class="input-group-addon">Departamento</span>
                                        <select id="modal-form-{{Departamento_Model::$id}}"
                                                name="{{Departamento_Model::$id}}" class="form-control select2"
                                                style = "width: 100%;"
                                                required>
                                            <option value="0">Seleccionar departamento</option>
                                        </select>
                                        <input type="hidden" id="modal-form-{{Departamento_Model::$id}}_temporal">
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm max300">
                                        <span class="input-group-addon">Puesto</span>
                                        {!! Form::select( Puesto_Model::$id , $puesto, 0, ['id' => "modal-form-" . Puesto_Model::$id, 'class' => "form-control select2", "style" => "width: 100%;"]) !!}
                                    </div>
                                    <br>
                                </td>
                                <td>&nbsp;&nbsp;</td>
                                <td>
                                    <div class="thumbnail" style="min-width: 250px; max-width: 300px;">
                                        <img id="modal-form-{{Empleado_Model::$idEmpleadoImagen}}" src="" alt=""
                                             class="img-responsive " id='imagen' alt=""/>
                                    </div>
                                    <div class="input-group input-group-sm" style="max-width: 250px;">
                                        <span class="input-group-addon">Imagen</span>
                                        <input type="file" class="form-control" placeholder=""
                                               id="modal-file-{{Empleado_Model::$idEmpleadoImagen}}"
                                               name="{{Empleado_Model::$idEmpleadoImagen}}"
                                               onchange='verImagenAnetsSubir(this);' style="background-color: white;">
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <div id="direccion" class="tab-pane fade">


                        <table class="tabla" id="t_list">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="input-group input-group-sm ">
                                        <span class="input-group-addon">Calle</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$calle}}"
                                               name="{{Empleado_Model::$calle}}" style="background-color: white"
                                               required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">N° Exterior</span>
                                        <input type="number" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$numeroExterior}}"
                                               name="{{Empleado_Model::$numeroExterior}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">N° Interior</span>
                                        <input type="number" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$numeroInterior}}"
                                               name="{{Empleado_Model::$numeroInterior}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Colonia</span>
                                        <input type="text" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$colonia}}"
                                               name="{{Empleado_Model::$colonia}}" style="background-color: white"
                                               required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">C.P.</span>
                                        <input type="number" maxlength="5" value="" placeholder="" class="form-control"
                                               id="modal-form-{{Empleado_Model::$cp}}" name="{{Empleado_Model::$cp}}"
                                               style="background-color: white" required>
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Estado</span>
                                        {!! Form::select( Estado_Model::$id , $estado, 0, ['id' => "modal-form-" . Estado_Model::$id, 'class' => "form-control select2", "style" => "width: 100%;"]) !!}
                                    </div>
                                    <br>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Municipio</span>
                                        <select id="modal-form-{{Municipio_Model::$id}}" 
                                                name="{{Municipio_Model::$id}}"
                                                class="form-control select2" 
                                                style = "width: 100%;"
                                                required>
                                            <option value="0">Seleccionar municipio</option>
                                        </select>
                                        <input type="hidden" id="modal-form-{{Municipio_Model::$id}}_temporal">
                                    </div>
                                    <br>
                                </td>
                                <td>&nbsp;&nbsp;</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>


                    </div>

                    <div id="licencia" class="tab-pane fade">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">N° Licencia</span>
                            <input type="number" value="" placeholder="" class="form-control"
                                   id="modal-form-{{Empleado_Model::$numeroLicencia}}"
                                   name="{{Empleado_Model::$numeroLicencia}}" style="background-color: white" required>
                        </div>
                        <br>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">Vigencia de Licencia</span>
                            <input type="date" value="" placeholder="" class="form-control"
                                   id="modal-form-{{Empleado_Model::$vigenciaLicencia}}"
                                   name="{{Empleado_Model::$vigenciaLicencia}}" style="background-color: white"
                                   required>
                        </div>
                        <br>
                    </div>

                </div>

                <input type="hidden" name="{{Empleado_Model::$id}}" id="modal-form-{{Empleado_Model::$id}}">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>