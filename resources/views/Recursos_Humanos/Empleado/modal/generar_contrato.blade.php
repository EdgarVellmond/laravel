<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Plantilla_Model;
use App\Models\Recursos_Humanos\Plantilla_Empleado_Model;

?>

<style type="text/css">
    .tabla {
        width: 100%;
        text-align: center;
    }

    .encabezado {
        text-align: left;
    }

    .campoObligatorio{
        border: 2px solid red;
    }

    .campoListo{
        border: 0;
    }

</style>

<div class="modal fade" id="modal-generar_contrato">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Contrato</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'recursos_humanos/guardarPlantillaEmpleado','class'=>'form', 'method'=> 'POST', 'id' => 'form-generar_contrato-save']) !!}
                    <table>
                        <thead>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div id="divInicioContrato" class="input-group input-group-sm">
                                        <span class="input-group-addon">Inicio de Contrato</span>
                                        <input type="date" value="" placeholder="" class="form-control danger"
                                                id="modal-generar_contrato-{{Plantilla_Empleado_Model::$fechaInicio}}"
                                                name="{{Plantilla_Empleado_Model::$fechaInicio}}" style="background-color: white" required>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <div id="divFinContrato" class="input-group input-group-sm">
                                        <span class="input-group-addon">Fin de Contrato</span>
                                        <input type="date" value="" placeholder="" class="form-control"
                                                id="modal-generar_contrato-{{Plantilla_Empleado_Model::$fechaFin}}"
                                                name="{{Plantilla_Empleado_Model::$fechaFin}}" style="background-color: white" required>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                        <div id="divFinContrato" class="input-group input-group-sm">
                            <span class="input-group-addon">Observaciones</span>
                            <input type="text" value="" placeholder="" class="form-control"
                                    id="modal-generar_contrato-{{Plantilla_Empleado_Model::$observaciones}}"
                                    name="{{Plantilla_Empleado_Model::$observaciones}}" style="background-color: white">
                        </div>
                    <br>
                        <table class="table table-hover table-bordered small" id="t_list_plantilla">
                            <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Nombre de Plantilla</th>
                                    <th>Vista previa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($plantillas as $row)
                                    <tr style="text-align: center;">

                                        <td>
                                            <button id="{{ $row->{Plantilla_Model::$id} }}" type="submit" onclick="guardarId(this);" class="btn btn-xs btn-success" target="" ><span class="glyphicon glyphicon-floppy-saved">Guardar</span></button>
                                        </td>
                                        <td>{{$row->{Plantilla_Model::$nombre} }}</td>
                                        <td>
                                            <a href="{{url('/catalogos/vistaPreviaCompleta/').'/'. $row->{Plantilla_Model::$id} .'/'. $ficha[0]->{Empleado_Model::$id}  }}" id="vp" class="btn btn-xs btn-info" target="_blank" ><span class="glyphicon glyphicon-eye-open"></span></a>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <input type="hidden" id="hiddenId" name="{{Plantilla_Model::$id}}">
                        <input type="hidden" id="hiddenId" name="{{Plantilla_Empleado_Model::$id}}">
                        <input type="hidden" id="hiddenId" name="{{Empleado_Model::$id}}" value="{{ $ficha[0]->{Empleado_Model::$id} }}">

                    </div>
                {!!Form::close()!!}

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            <script type="text/javascript">
            
                /*function g(e){
                    e.event.preventDefault();preventDefault();
                }*/
            </script>
        </div>
    </div>
</div>