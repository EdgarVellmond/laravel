<?php
    use App\Models\Genericos\Documento_Model;
    use App\Models\Recursos_Humanos\Empleado_Model;
    use App\Models\Recursos_Humanos\Expediente_Digital_Empleado_Model;
    use App\Models\Catalogos\Expediente_Digital_Model;
?>
<div class="modal fade" id="modal-subir">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Vista previa</h4>
            </div>
            {!! Form::open(['url' => url('recursos_humanos/guardarDocumento'),'class'=>'form', 'enctype'=>'multipart/form-data', 'file'=>true]) !!}
            <div class="modal-body">
                <div class="thumbnail" style="width: 100%;">
                    <img id="modal-subir-vista_previa" src="" alt=""
                        class="img-responsive " id='imagen' alt=""/>
                </div>
                <div class="input-group input-group-sm" >
                    <span class="input-group-addon">Imagen</span>
                    <input type="file" class="form-control" placeholder=""
                        id="modal-file-{{Documento_Model::$id}}"
                        name="{{Documento_Model::$id}}"
                        onchange='verImagenAntesSubir(this);' style="background-color: white;">
                </div>
                <input type="hidden" id="modal-subir-{{ Expediente_Digital_Empleado_Model::$id }}" name="{{Expediente_Digital_Empleado_Model::$id}}" value>
                <input type="hidden" id="modal-subir-{{ Empleado_Model::$id }}" name="{{Empleado_Model::$id}}" value>
                <input type="hidden" id="modal-subir-{{ Expediente_Digital_Model::$id }}" name="{{Expediente_Digital_Model::$id}}" value>
            </div>

            <div class="modal-footer">
                <!--<a href="#" id="modal-vista_previa_blank" class="btn btn-xs btn-info" target="_blank" style="float: left;"><span class="glyphicon glyphicon-eye-open"></span> Ver vista previa en nueva pestaña</a>-->
                <button type="submit" class="btn btn-default" >Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>