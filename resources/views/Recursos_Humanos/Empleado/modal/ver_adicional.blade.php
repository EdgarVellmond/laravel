<?php
use App\Models\Recursos_Humanos\Empleado_Model;
use App\Models\Catalogos\Departamento_Model;
use App\Models\Catalogos\Puesto_Model;

?>

<style type="text/css">
    .tabla {
        width: 100%;
        text-align: center;
    }

    .encabezado {
        text-align: left;
    }
</style>

<div class="modal fade" id="modal-ver-adicional">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Información personal</h4>
            </div>
            <div class="modal-body">

                <table class="tabla" id="t_list">
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">RFC</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Empleado_Model::$rfc}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">CURP</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Empleado_Model::$curp}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">NSS</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Empleado_Model::$nss}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Teléfono</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Empleado_Model::$telefono}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Correo</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Empleado_Model::$correo}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Área</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Departamento_Model::$idArea}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Departamento</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Departamento_Model::$id}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Puesto</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver-adicional-{{Puesto_Model::$id}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->