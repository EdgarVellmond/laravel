<?php
use App\Models\Recursos_Humanos\Empleado_Model;

?>

<style type="text/css">
    .tabla {
        width: 100%;
        text-align: center;
    }

    .encabezado {
        text-align: left;
    }
</style>

<div class="modal fade" id="modal-ver_nomina">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Información de la Nómina</h4>
            </div>
            <div class="modal-body">

                <table class="tabla" id="t_list">
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Salario diario</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver_nomina_{{Empleado_Model::$salarioBaseDiario}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Salario diario integrado</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver_nomina_{{Empleado_Model::$salarioDiarioIntegrado}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Patrón</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver_nomina_{{Empleado_Model::$idPatron}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Viáticos</span>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="modal-ver_nomina_{{Empleado_Model::$viaticos}}" disabled
                                       style="background-color: white" required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->