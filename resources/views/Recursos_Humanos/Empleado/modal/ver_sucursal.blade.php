<?php
use App\Models\Catalogos\Sucursal_Model;
use App\Models\Catalogos\Municipio_Model;

?>
<style type="text/css">

    .tabla {
        width: 100%;
    }

    .tabla th {
        text-align: center;
    }

    .tabla td {
        max-width: 300px;
    }

</style>
<div class="modal fade" id="modal-ver_sucursal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sucursal</h4>
            </div>
            <div class="modal-body">
                <table class="tabla">
                    <thead>
                    <tr>
                        <th><h4>Datos Personales</h4></th>
                        <th></th>
                        <th><h4>Dirección</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Nombre:</span>
                                <input type="text" name="{{ Sucursal_Model::$nombre }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$nombre }}"
                                       style="background-color: white" class="form-control" placeholder="Nombre"
                                       required disabled>
                            </div>
                            <br>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Calle:</span>
                                <input type="text" name="{{ Sucursal_Model::$calle }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$calle }}"
                                       style="background-color: white" class="form-control" placeholder="Calle" required
                                       disabled>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Clave:</span>
                                <input type="text" name="{{ Sucursal_Model::$clave }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$clave }}"
                                       style="background-color: white" class="form-control" placeholder="Clave"
                                       disabled>
                            </div>
                            <br>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Colonia:</span>
                                <input type="text" name="{{ Sucursal_Model::$colonia }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$colonia }}"
                                       style="background-color: white" class="form-control" placeholder="Colonia"
                                       required disabled>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Correo:</span>
                                <input type="text" name="{{ Sucursal_Model::$correo }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$correo }}"
                                       style="background-color: white" class="form-control"
                                       placeholder="Apellido Materno" disabled>
                            </div>
                            <br>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Número Interior:</span>
                                <input type="text" name="{{ Sucursal_Model::$numeroInterior }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$numeroInterior }}"
                                       style="background-color: white" class="form-control"
                                       placeholder="Número interior" disabled>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Teléfono:</span>
                                <input type="text" name="{{ Sucursal_Model::$telefono }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$telefono }}"
                                       style="background-color: white" class="form-control" placeholder="RFC" disabled
                                       required>
                            </div>
                            <br>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Número exterior:</span>
                                <input type="text" name="{{ Sucursal_Model::$numeroExterior }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$numeroExterior }}"
                                       style="background-color: white" class="form-control"
                                       placeholder="Número exterior" disabled required>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Empresa:</span>
                                {!! Form::select( Sucursal_Model::$idEmpresa , $arrayEmpresa, 0, ['id' => 'modal-ver_sucursal_' . Sucursal_Model::$idEmpresa, 'class' => "form-control" ,"disabled", "style" => "background-color: white"]) !!}
                            </div>
                            <br>
                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Código Postal:</span>
                                <input type="number" name="{{ Sucursal_Model::$cp }}"
                                       id="modal-ver_sucursal_{{ Sucursal_Model::$cp }}" style="background-color: white"
                                       class="form-control" placeholder="Código Postal" required disabled>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Estado:</span>
                                <input type="text" name="{{ Municipio_Model::$idEstado }}"
                                       id="modal-ver_sucursal_{{ Municipio_Model::$idEstado }}"
                                       style="background-color: white" class="form-control" placeholder="Código Postal"
                                       required disabled>
                            </div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td width="25px"></td>
                        <td>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon">Municipio:</span>
                                <input type="text" name="{{ Municipio_Model::$id }}"
                                       id="modal-ver_sucursal_{{ Municipio_Model::$id }}"
                                       style="background-color: white" class="form-control" placeholder="Código Postal"
                                       required disabled>
                            </div>
                            <br>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>