
<div class="modal fade" id="modal-vista_previa">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Vista previa</h4>
            </div>
            <div class="modal-body">
                <iframe id="modal-vista_previa_iFrame" src="" style="width: 100%; min-height: 500px;"></iframe>
            </div>

            <div class="modal-footer">
                <a href="#" id="modal-vista_previa_blank" class="btn btn-xs btn-info" target="_blank" style="float: left;"><span class="glyphicon glyphicon-eye-open"></span> Ver vista previa en nueva pestaña</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>