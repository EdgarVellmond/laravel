<?php
use App\Models\Recursos_Humanos\Falta_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nueva Falta</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarFalta()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Fecha Inicio:</label>
                    <input type="date" name="{{ Falta_Model::$fechaInicio }}" id="{{ Falta_Model::$fechaInicio }}"
                           class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Fecha Fin:</label>
                    <input type="date" name="{{ Falta_Model::$fechaFin }}" id="{{ Falta_Model::$fechaFin }}"
                           class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Empleado:</label>
                    {!! Form::select( Falta_Model::$idEmpleado , $arrayEmpleado, 0, ['id' => Falta_Model::$idEmpleado, 'class' => "form-control select2"]) !!}
                </div>

                <input type="hidden" name="{{ Falta_Model::$id }}" id="{{ Falta_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>