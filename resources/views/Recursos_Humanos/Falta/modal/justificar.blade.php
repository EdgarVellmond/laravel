<?php
use App\Models\Recursos_Humanos\Falta_Model;

?>

<div class="modal fade" id="model-justificar">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Justificar Falta</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form_justificar','onSubmit' => 'return justificarFalta()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Comentarios:</label>
                    <input type="text" name="{{ Falta_Model::$comentarios }}" id="{{ Falta_Model::$comentarios }}"
                           class="form-control" required>
                </div>

                <input type="hidden" name="{{ Falta_Model::$id }}" id="{{ Falta_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Justificar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>