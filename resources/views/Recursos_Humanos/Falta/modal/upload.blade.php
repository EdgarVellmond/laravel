<?php
use App\Models\Recursos_Humanos\Falta_Model;

?>

<div class="modal fade" id="model-upload">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Subir Archivo de Falta</h4>
            </div>
            {!! Form::open(['url' => 'recursos_humanos/uploadArchivoFalta','enctype'=>'multipart/form-data', 'file'=>true,'class'=>'form','id'=>'add_form' ]) !!}
            <div class="modal-body">

                <div class="thumbnail" style="min-width: 250px; text-align: center; align-content: center;">
                    <img id="modal-form-{{Falta_Model::$idFaltaArchivo}}" src="" alt="" class="img-responsive " alt=""/>
                </div>
                <div class="input-group input-group-sm" style="max-width: 250px;">
                    <span class="input-group-addon">Imagen</span>
                    <input type="file" class="form-control" placeholder=""
                           id="modal-file-{{Falta_Model::$idFaltaArchivo}}" name="{{Falta_Model::$idFaltaArchivo}}"
                           onchange='verImagenAntesSubir(this);' style="background-color: white;">
                </div>

                <input type="hidden" name="{{ Falta_Model::$id  }}" id="{{ Falta_Model::$id  }}">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>