<?php
use App\Models\Recursos_Humanos\Incapacidad_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nueva Incapacidad</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarIncapacidad()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="date" name="{{ Incapacidad_Model::$fechaInicio }}"
                           id="{{ Incapacidad_Model::$fechaInicio }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="date" name="{{ Incapacidad_Model::$fechaFin }}" id="{{ Incapacidad_Model::$fechaFin }}"
                           class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Empleado:</label>
                    {!! Form::select( Incapacidad_Model::$idEmpleado , $arrayEmpleado, 0, ['id' => Incapacidad_Model::$idEmpleado, 'class' => "form-control select2"]) !!}
                </div>
                <div class="form-group">
                    <label>Tipo:</label>
                    {!! Form::select( Incapacidad_Model::$tipoIncapacidad , Config::get('enums.tipo_incapacidad'), 0, ['id' => Incapacidad_Model::$tipoIncapacidad, 'class' => "form-control"]) !!}
                </div>
                <div class="form-group">
                    <label for="nombre">Comentarios:</label>
                    <input type="text" name="{{ Incapacidad_Model::$comentarios }}"
                           id="{{ Incapacidad_Model::$comentarios }}" class="form-control" required>
                </div>


                <input type="hidden" name="{{ Incapacidad_Model::$id }}" id="{{ Incapacidad_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>