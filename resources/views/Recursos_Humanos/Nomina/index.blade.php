<?php
use App\Models\Recursos_Humanos\Nomina_Model;
use App\Http\Helpers\Helpers;
?>

@extends('layouts.listado')

@section('title')
    Nómina
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Recursos_Humanos/nomina.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
        var oTable;
        var urlBase = "{{url('/')}}/";
    </script>



@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}">Principal</a></li>
        <li class="active">Catálogo de Nómina</li>
    </ol>
@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Nómina
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.recursos_humanos.nomina"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar" data-toggle="modal"
                               data-target="#model-guardar"><span class="glyphicon glyphicon-plus"
                                                                  aria-hidden="true"></span> Nueva Nómina</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Nómina</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Periodo</th>
                            <th>Empresa</th>
                            <th>Pagado</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayNomina as $nomina)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.recursos_humanos.nomina"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#model-guardar"
                                           data-id_editar="{{ $nomina->getAttribute(Nomina_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.recursos_humanos.nomina"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#model-eliminar"
                                           data-id_eliminar="{{ $nomina->getAttribute(Nomina_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    @endif

                                </td>

                                <td>{{ $nomina->{ Nomina_Model::$fechaInicio } }}</td>
                                <td>{{ $nomina->{ Nomina_Model::$fechaFin } }}</td>
                                <td>{{ $nomina->{ Nomina_Model::$periodo } }}</td>
                                <td>{{ $nomina->{ Nomina_Model::$id_empresa  } }}</td>
                                <td>{{ $nomina->{ Nomina_Model::$totalPagado } }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('Recursos_Humanos/Nomina/modal/form')
