<?php
use App\Models\Recursos_Humanos\Nomina_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generar Nómina</h4>
            </div>
            {!! Form::open(['url' => url("recursos_humanos/generarNomina"),'class'=>'form','id'=>'add_form', 'target'=>'_blank']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Fecha Inicio:</label>
                    <input type="date" name="{{ Nomina_Model::$fechaInicio }}"
                           id="{{ Nomina_Model::$fechaInicio }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Fecha Final:</label>
                    <input type="date" name="{{ Nomina_Model::$fechaFin }}" id="{{ Nomina_Model::$fechaFin }}"
                           class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Patrón:</label>
                    {!! Form::select( Nomina_Model::$idEmpresa , $arrayEmpresa, 0, ['id' => Nomina_Model::$idEmpresa, 'class' => "form-control"]) !!}
                </div>

                <input type="hidden" name="{{ Nomina_Model::$id  }}" id="{{ Nomina_Model::$id  }}">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >Generar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>