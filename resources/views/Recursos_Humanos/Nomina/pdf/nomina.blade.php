<?php
use App\Models\Catalogos\Empresa_Model;
use App\Models\Recursos_Humanos\Empleado_Model;

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Reporte de Nómina</title>

    {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('vendor/bootstrap/css/bootstrap-theme.min.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::script('vendor/bootstrap/js/jquery.min.js') !!}
    {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
</head>

<body>

@foreach($arrayEmpresa as $empresa)

        <div class="row col-lg-12 col-md-12">
            <img src="{{ URL::asset('images/SISAC 2-02.png') }}" style="height: 100px; position: fixed;">
            <h3 style="text-align: center">RECIBO DE NOMINA</h3>
            <p style="text-align: center"><b>{{ $empresa->{Empresa_Model::$razonSocial} }}</b></p>
            <p style="text-align: center">{{ $empresa->{Empresa_Model::$calle}." ".$empresa->{Empresa_Model::$numeroExterior}." ".$empresa->{Empresa_Model::$numeroInterior}." ".$empresa->{Empresa_Model::$colonia} }}</p>
            <p style="text-align: center">C.P. {{ $empresa->{Empresa_Model::$cp} }}</p>
            <p style="text-align: center">R.F.C. {{ $empresa->{Empresa_Model::$rfc} }}</p>
            <p style="text-align: right">PERIODO DE:&nbsp; {{ $fechaInicio }}&nbsp; AL &nbsp;{{ $fechaFin }}</p>
        </div>

@endforeach

@foreach($arrayEmpleado as $empleado)
    <hr style="height:1px; background: black;">
    <p><b>{{ $empleado->{Empleado_Model::$numeroEmpleado} }} &nbsp;&nbsp;&nbsp; {{ $empleado->{Empleado_Model::$nombre} }}&nbsp;{{ $empleado->{Empleado_Model::$apellidoPaterno} }}&nbsp;{{ $empleado->{Empleado_Model::$apellidoMaterno} }}</b>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Periodo de: {{ $fechaInicio }} al {{ $fechaFin }}</p>
<table>
    <tr>
        <td>Puesto: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{"nombrePuesto"}  }} </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Departamento: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{"nombreDepartamento"}  }} </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>N.S.S.: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{Empleado_Model::$nss}  }} </td>
    </tr>
    <tr>
        <td>Salario: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{Empleado_Model::$salarioBaseDiario}  }} </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Salario Integrado: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{Empleado_Model::$salarioDiarioIntegrado}  }} </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Curp: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{Empleado_Model::$curp}  }} </td>
    </tr>
    <tr>
        <td>Ingreso: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{Empleado_Model::$inicioContrato}  }} </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Dias Pagados: </td>
        <td>&nbsp;</td>
        <td>{{ $array[$empleado->{Empleado_Model::$id}]["diasTotales"]  }} </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>RFC: </td>
        <td>&nbsp;</td>
        <td>{{ $empleado->{Empleado_Model::$rfc}  }} </td>
    </tr>
</table>

    <table class="table">
        <thead>
        <tr>
            <th>Percepción</th>
            <th>Importe</th>
            <th>Deducción</th>
            <th>Importe</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Sueldo</td>
            <td>{{ number_format($array[$empleado->{Empleado_Model::$id}]["sueldo"] , 2 , "." , ",") }}</td>
            <td>IMSS</td>
            <td>{{ number_format($array[$empleado->{Empleado_Model::$id}]["imss"] , 2 , "." , ",") }}</td>
        </tr>
        <tr>
            <td>Séptimo Dia</td>
            <td>{{ number_format($array[$empleado->{Empleado_Model::$id}]["septimoDia"] , 2 , "." , ",") }}</td>
            <td>ISPT</td>
            <td>{{ number_format($array[$empleado->{Empleado_Model::$id}]["ispt"] , 2 , "." , ",")  }}</td>
        </tr>

        </tbody>
    </table>
    <table class="table">
        <tr>
            <td></td>
            <td></td>
            <td>Total Percepciones:</td>
            <td>{{ number_format(($array[$empleado->{Empleado_Model::$id}]["sueldo"] + $array[$empleado->{Empleado_Model::$id}]["septimoDia"]) , 2 , "." , ",")  }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Total Deducciones:</td>
            <td>{{ number_format(($array[$empleado->{Empleado_Model::$id}]["imss"] + $array[$empleado->{Empleado_Model::$id}]["ispt"]) , 2 , "." , ",")  }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>Total:</b></td>
            <td>{{ number_format(($array[$empleado->{Empleado_Model::$id}]["sueldo"] + $array[$empleado->{Empleado_Model::$id}]["septimoDia"])-($array[$empleado->{Empleado_Model::$id}]["imss"] + $array[$empleado->{Empleado_Model::$id}]["ispt"]) , 2 , "." , ",")  }}</td>
        </tr>
    </table>
@endforeach
</body>
</html>