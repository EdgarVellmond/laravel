<?php
use App\Models\Recursos_Humanos\Permiso_Asistencia_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevo Permiso</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarPermisoAsistencia()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="date" name="{{ Permiso_Asistencia_Model::$fechaInicio }}"
                           id="{{ Permiso_Asistencia_Model::$fechaInicio }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="date" name="{{ Permiso_Asistencia_Model::$fechaFin }}"
                           id="{{ Permiso_Asistencia_Model::$fechaFin }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Empleado:</label>
                    {!! Form::select( Permiso_Asistencia_Model::$idEmpleado , $arrayEmpleado, 0, ['id' => Permiso_Asistencia_Model::$idEmpleado, 'class' => "form-control select2"]) !!}
                </div>
                <div class="form-group">
                    <label>Goce Sueldo:</label>
                    {!! Form::select( Permiso_Asistencia_Model::$goceSueldo , Config::get('enums.goce_sueldo'), 0, ['id' => Permiso_Asistencia_Model::$goceSueldo, 'class' => "form-control"]) !!}
                </div>
                <div class="form-group">
                    <label for="nombre">Comentarios:</label>
                    <input type="text" name="{{ Permiso_Asistencia_Model::$comentarios }}"
                           id="{{ Permiso_Asistencia_Model::$comentarios }}" class="form-control" required>
                </div>


                <input type="hidden" name="{{ Permiso_Asistencia_Model::$id }}" id="{{ Permiso_Asistencia_Model::$id }}"
                       value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>