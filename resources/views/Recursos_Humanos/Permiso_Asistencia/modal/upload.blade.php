<?php
use App\Models\Recursos_Humanos\Permiso_Asistencia_Model;

?>

<div class="modal fade" id="model-upload">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Subir Archivo de Permiso</h4>
            </div>
            {!! Form::open(['url' => 'recursos_humanos/uploadArchivoPermiso','enctype'=>'multipart/form-data', 'file'=>true,'class'=>'form','id'=>'add_form' ]) !!}
            <div class="modal-body">

                <div class="thumbnail" style="min-width: 250px; text-align: center; align-content: center;">
                    <img id="modal-form-{{Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo}}" src="" alt=""
                         class="img-responsive " alt=""/>
                </div>
                <div class="input-group input-group-sm" style="max-width: 250px;">
                    <span class="input-group-addon">Imagen</span>
                    <input type="file" class="form-control" placeholder=""
                           id="modal-file-{{Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo}}"
                           name="{{Permiso_Asistencia_Model::$idPermisoAsistenciaArchivo}}"
                           onchange='verImagenAntesSubir(this);' style="background-color: white;">
                </div>

                <input type="hidden" name="{{ Permiso_Asistencia_Model::$id  }}"
                       id="{{ Permiso_Asistencia_Model::$id  }}">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>