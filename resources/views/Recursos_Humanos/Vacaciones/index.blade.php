<?php
use App\Models\Recursos_Humanos\Vacaciones_Model;
use App\Http\Helpers\Helpers;

?>

@extends('layouts.listado')

@section('title')
    Vacaciones
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Recursos_Humanos/vacaciones.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = '{{ asset('vendor/datatables/lang/es_MX.json') }}';
        var oTable;
        var urlBase = "{{url('/')}}/";
    </script>

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}">Principal</a></li>
        <li class="active">Catálogo de Vacaciones</li>
    </ol>
@endsection
@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Vacaciones
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.recursos_humanos.vacaciones"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar" data-toggle="modal"
                               data-target="#model-guardar"><span class="glyphicon glyphicon-plus"
                                                                  aria-hidden="true"></span> Nuevas Vacaciones</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Vacaciones</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acciones</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Total Días</th>
                            <th>Empleado</th>
                            <th>Comentarios</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayVacaciones as $vacaciones)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.recursos_humanos.vacaciones"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#model-guardar"
                                           data-id_editar="{{ $vacaciones->getAttribute(Vacaciones_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.recursos_humanos.vacaciones"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#model-eliminar"
                                           data-id_eliminar="{{ $vacaciones->getAttribute(Vacaciones_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("upload.recursos_humanos.vacaciones"))
                                        <a href="#" type="button" class="btn btn-warning btn-xs" title="Subir Archivo"
                                           data-toggle="modal" data-target="#model-upload"
                                           data-id_upload="{{ $vacaciones->getAttribute(Vacaciones_Model::$id) }}"><span
                                                    class="glyphicon glyphicon-file" aria-hidden="true"></span></a>
                                    @endif

                                </td>

                                <td>{{ $vacaciones->{ Vacaciones_Model::$fechaInicio } }}</td>
                                <td>{{ $vacaciones->{ Vacaciones_Model::$fechaFin } }}</td>
                                <td>{{ $vacaciones->{ Vacaciones_Model::$dias } }}</td>
                                <td>{{ $vacaciones->{ "nombreEmpleado" } }}</td>
                                <td>{{ $vacaciones->{ Vacaciones_Model::$comentarios } }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('Recursos_Humanos/Vacaciones/modal/form')
@include('Recursos_Humanos/Vacaciones/modal/eliminar')
@include('Recursos_Humanos/Vacaciones/modal/upload')