<?php
use App\Models\Recursos_Humanos\Vacaciones_Model;

?>

<div class="modal fade" id="model-guardar">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Nuevas Vacaciones</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_form','onSubmit' => 'return guardarVacaciones()']) !!}
            <div class="modal-body">

                <div class="form-group">
                    <label for="nombre">Fecha Inicial:</label>
                    <input type="date" name="{{ Vacaciones_Model::$fechaInicio }}"
                           id="{{ Vacaciones_Model::$fechaInicio }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nombre">Fecha Final:</label>
                    <input type="date" name="{{ Vacaciones_Model::$fechaFin }}" id="{{ Vacaciones_Model::$fechaFin }}"
                           class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Empleado:</label>
                    {!! Form::select( Vacaciones_Model::$idEmpleado , $arrayEmpleado, 0, ['id' => Vacaciones_Model::$idEmpleado, 'class' => "form-control select2"]) !!}
                </div>
                <div class="form-group">
                    <label for="nombre">Comentarios:</label>
                    <input type="text" name="{{ Vacaciones_Model::$comentarios }}"
                           id="{{ Vacaciones_Model::$comentarios }}" class="form-control" required>
                </div>


                <input type="hidden" name="{{ Vacaciones_Model::$id }}" id="{{ Vacaciones_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>