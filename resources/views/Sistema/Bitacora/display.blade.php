<?php
use App\Http\Helpers\Helpers;
?>
@extends('layouts.display')
@section('title')
Bitacora Display
@endsection
@section('links')

@endsection
@section('scripts')

@endsection
@section('content')
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    		<div class="panel panel-primary">
    			  <div class="panel-heading">
    					<h3 class="panel-title">Datos del Registro</h3>
    			  </div>
    			  <div class="panel-body">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                          <dl class="dl-horizontal">
                              <dt>Tabla:</dt>
                              <dd>{{ $bitacora->tabla }}&nbsp;</dd>
                              <dt>ID Cambiado:</dt>
                              <dd>{{ $bitacora->id_cambiado }}&nbsp;</dd>
                              <dt>Usuario:</dt>
                              <dd>{{ $bitacora->relacionUsuario->nombre }}&nbsp;</dd>
                          </dl>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                          <dl class="dl-horizontal">
                              <dt>Servidor:</dt>
                              <dd>{{ $bitacora->servidor }}&nbsp;</dd>
                              <dt>IP Cliente:</dt>
                              <dd>{{ $bitacora->ip_cliente }}&nbsp;</dd>
                              <dt>Fecha del Cambio:</dt>
                              <dd>{{ Helpers::datetimecas($bitacora->created_at) }}&nbsp;</dd>
                          </dl>
                      </div>
    			  </div>
    		</div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos Anterior</h3>
                    </div>
                    <div class="panel-body">
                        @if(empty($datosAnterior))
                            <div class="alert alert-info">
                            	Este es un registro Nuevo
                            </div>
                        @else
                        <table class="table table-hover">
                        	<tbody>
                            @foreach($datosAnterior as $keyDato => $valDato)
                                <tr>
                                    <td>{{ $keyDato }}</td>
                                    <td>{{ $valDato }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos Nuevos</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tbody>
                            @foreach($datosNuevo as $keyDato => $valDato)
                                <tr>
                                    <td>@if($datosModificados[$keyDato]) <strong> @endif{{ ($datosModificados[$keyDato])? '*':'' }}{{ $keyDato }}@if($datosModificados[$keyDato]) </strong> @endif</td>
                                    <td>{{ $valDato }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    	</div>
    </div>
@endsection