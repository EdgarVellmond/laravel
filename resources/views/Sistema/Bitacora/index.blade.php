<?php
use App\Models\Sistema\Historial_Model;
use App\Models\Sistema\Usuarios_Model;
?>
@extends('layouts.listado')
@section('title')
Bitacora
@endsection
@section('links')

@endsection
@section('scripts')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("/") }}">Principal</a></li>
    <li class="active">Menus</li>
</ol>
@endsection
@section('content')
    <div class="row">
    	<div class="col-md-12">
    		<div class="page-header">
    		  <h1>Bitacora del Sistema<small> </small></h1>
    		</div>
			<!-- TAB NAVIGATION -->
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a href="#tab1" role="tab" data-toggle="tab">Tablas</a></li>
				<li><a href="#tab2" role="tab" data-toggle="tab">Usuarios</a></li>
			</ul>
			<!-- TAB CONTENT -->
			<div class="tab-content">
				<div class="active tab-pane fade in" id="tab1">
					<br>
					<table class="table table-bordered table-hover" id="t_list1">
						<thead>
							<tr>
								<th>Acciones</th>
								<th>Tabla</th>
							</tr>
						</thead>
						<tbody>
						@foreach($tablas as $rowTabla)
							<tr>
								<td><a class="btn btn-xs btn-default" href="{{ url('bitacora/tabla/'.$rowTabla->getAttribute(Historial_Model::$tabla)) }}"><span class="glyphicon glyphicon-eye-open"></span></a></td>
								<td>{{ $rowTabla->getAttribute(Historial_Model::$tabla) }}</td>
							</tr>
						</tbody>
						@endforeach
					</table>
					<script type="text/javascript">
						$('#t_list1').dataTable({
							"dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
							language: {
								'url':'{{ asset('vendor/datatables/lang/es_MX.json') }}'
							},
							order: [],
							columnDefs: [
								{"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center ', 'width':'10%'},
								{"targets": [1], 'className': ' sinwarp'}
							]
						});
					</script>
				</div>
				<div class="tab-pane fade" id="tab2">
					<br>
					<table class="table table-bordered table-hover" id="t_list2" width="100%">
						<thead>
							<tr>
								<th>Acciones</th>
								<th>Usuario</th>
							</tr>
						</thead>
						<tbody>
							@foreach($usuarios as $rowUsuario)
							<tr>
								<td width="10%"><a class="btn btn-xs btn-default" href="{{ url('bitacora/usuario/'.$rowUsuario->getAttribute(Usuarios_Model::$id)) }}"><span class="glyphicon glyphicon-eye-open"></span></a></td>
								<td>{{ $rowUsuario->getAttribute(Usuarios_Model::$nombre) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<script type="text/javascript">
						$('#t_list2').dataTable({
							"dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
							language: {
								'url':'{{ asset('vendor/datatables/lang/es_MX.json') }}'
							},
							order: [],
							columnDefs: [
								{"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center ', 'width':'10%'},
								{"targets": [1], 'className': ' sinwarp'}
							]
						});
					</script>
				</div>
			</div>
		</div>
    </div>
@endsection