<?php
use App\Models\Sistema\Historial_Model;
use App\Models\Sistema\Usuarios_Model;
use App\Http\Helpers\Helpers;
?>
@extends('layouts.listado')
@section('title')
Bitacora Tabla
@endsection
@section('links')

@endsection
@section('scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('#t_list').dataTable({
				"dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
				language: {
					'url':'{{ asset('vendor/datatables/lang/es_MX.json') }}'
				},
				order: [],
				columnDefs: [
					{"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center ', 'width':'10%'},
					{"targets": [1,2,3], 'className': ' sinwarp'}
				]
			});
		});
	</script>
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("/") }}">Principal</a></li>
    <li class="active">Menus</li>
</ol>
@endsection
@section('content')
    <div class="row">
    	<div class="col-md-12">
    		<div class="page-header">
    		  <h1>Bitacora del Sistema<small> </small></h1>
    		</div>
			<table class="table table-bordered table-hover" id="t_list">
				<thead>
					<tr>
						<th>Acciones</th>
						<th>ID Cambiado</th>
						<th>Tabla</th>
						<th>Fecha del Cambio</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bitacoras as $rowBitacora)
					<tr>
						<td valign="top"><a class="btn btn-xs btn-default" href="{{ url('bitacora/display/'.$rowBitacora->getAttribute(Historial_Model::$id)) }}"><span class="glyphicon glyphicon-eye-open"></span></a></td>
						<td>ID:{{ $rowBitacora->getAttribute(Historial_Model::$idCambiado) }}</td>
						<td>{{ $rowBitacora->tabla }}</td>
						<td>{{ Helpers::datetimecas($rowBitacora->created_at) }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
    </div>
@endsection