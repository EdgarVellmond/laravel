<?php
use App\Models\Sistema\Categoria_Variables_Sistema_Model;
use App\Http\Helpers\Helpers;
?>

@extends('layouts.listado')

@section('title')
    Categoría Variables
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.full.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('js/app/Sistema/categoria_variables_sistema.js') !!}

    <script type="text/javascript">
        var token = '{{ csrf_token() }}';
        var urlTable = "{{ asset('vendor/datatables/lang/es_MX.json') }}";
        var oTable;

        var idCategoriaVariablesSistema = '{{Categoria_Variables_Sistema_Model::$id}}';
        var nombreCategoriaVariablesSistema = '{{Categoria_Variables_Sistema_Model::$nombre}}';
        
    </script>



@endsection

@section('content')

    <div class="row">
        <div class="page-header">
            <h1>Catálogo de Categoría de Variables de Sistema
                <small></small>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        @if(Helpers::get_permiso("alta.sistema.categoria_variables_sistema"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Nuevo" data-toggle="modal"
                               data-target="#modal-form"><span class="glyphicon glyphicon-plus"
                                                                  aria-hidden="true"></span>Nuevo</a>
                        @endif
                    </div>
                    <h3 class="panel-title">Control de Categoría de Variables de Sistema</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover small" id="t_list">
                        <thead>
                        <tr>
                            <th>Acción</th>
                            <th>Nombre</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arrayCategoriaVariablesSistema as $categoria_variables_sistema)
                            <tr>
                                <td class="text-center">
                                    @if(Helpers::get_permiso("edicion.sistema.categoria_variables_sistema"))
                                        <a href="#" type="button" class="btn btn-success btn-xs" title="Editar"
                                           data-toggle="modal" data-target="#modal-form"
                                           data-id="{{ $categoria_variables_sistema->{Categoria_Variables_Sistema_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-pencil" ></span></a>
                                    @endif
                                    @if(Helpers::get_permiso("baja.sistema.categoria_variables_sistema"))
                                        <a href="#" type="button" class="btn btn-danger btn-xs" title="Eliminar"
                                           data-toggle="modal" data-target="#modal-eliminar"
                                           data-id="{{ $categoria_variables_sistema->{Categoria_Variables_Sistema_Model::$id} }}"><span
                                                    class="glyphicon glyphicon-remove" ></span></a>
                                    @endif

                                </td>

                                <td>{{ $categoria_variables_sistema->{ Categoria_Variables_Sistema_Model::$nombre } }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('Sistema.Categoria_Variables_Sistema.modal.form')
@include('Sistema.Categoria_Variables_Sistema.modal.eliminar')