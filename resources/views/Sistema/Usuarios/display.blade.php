<?php
use App\Http\Helpers\Helpers;
use App\Models\Sistema\Usuarios_Model;
use App\Models\Sistema\Submenu_Model;
use App\Models\Sistema\Menu_Model;
use App\Models\Sistema\Permiso_Model;
?>
@extends('layouts.display')
@section('title')
    Detalles del Usuario
@endsection
@section('links')
    {!! Html::style('vendor/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendor/select2-bootstrap3-css/select2-bootstrap.min.css') !!}
    {!! Html::style('vendor/bootstrap-duallistbox/src/bootstrap-duallistbox.css') !!}
@endsection
@section('scripts')
    {!! Html::script('vendor/select2/dist/js/select2.min.js') !!}
    {!! Html::script('vendor/select2/dist/js/i18n/es.js') !!}
    {!! Html::script('vendor/bootstrap-duallistbox/src/jquery.bootstrap-duallistbox.js') !!}
    {!! Html::script('js/app/Sistema/usuario.js') !!}
    <script type="text/javascript">
       var oTable;
       var token = '{{ csrf_token() }}';
       var urlTable = "{{ asset('vendor/datatables/lang/es_MX.json') }}";

        function cambiarPassword(){
            if($('#e_password').attr('disabled')){
                $('#e_password').removeAttr('disabled');
            }else{
                $('#e_password').attr('disabled',true);
            }
        }
        function uf_check(check,permiso){
            elemento = $('.'+permiso);
            if(check == 1){
                elemento.prop("checked", true);
            }else{
                elemento.prop("checked", false);
            }
        }
    </script>
@endsection
@section('buttons_r')
    <li>
        <p class="navbar-text"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Estado: @if($usuario->getAttribute(Usuarios_Model::$estatus) == 1) Activo @else Inactivo @endif</p>
    </li>
    <li>
        @if($usuario->getAttribute(Usuarios_Model::$estatus) == 1)
            @if(Helpers::get_permiso("desactivar.sistema.control_usuarios"))
            <a href="{{ url('usuarios/estatusUsuario/'.($usuario->getAttribute(Usuarios_Model::$id)).'/0') }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span> Desactivar Usuario</a>
                @endif
        @else
            @if(Helpers::get_permiso("activar.sistema.control_usuarios"))
            <a href="{{ url('usuarios/estatusUsuario/'.($usuario->getAttribute(Usuarios_Model::$id)).'/1') }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-check"></span> Activar Usuario</a>
                @endif
        @endif
    </li>
@endsection
@section('content')

    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}">Principal</a></li>
        <li><a href="{{ url("/usuarios") }}">Catálogo de Usuarios</a></li>
        <li class="active">Ficha</li>
    </ol>

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#DatosPrincipales" aria-controls="DatosPrincipales" role="tab" data-toggle="tab">Datos Principales</a>
            </li>
            <li role="presentation">
                <a href="#Permisos" aria-controls="Permisos" role="tab" data-toggle="tab">Permisos de Acceso</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="DatosPrincipales">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="btn-group pull-right">
                            @if(Helpers::get_permiso("edicion.sistema.control_usuarios"))
                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-edit_user"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar Usuario</button>
                                @endif
                        </div>
                        <h3 class="panel-title">Datos Generales del Usuario</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <table class="table">
                                    <tr>
                                        <td class="col-md-3"><strong>Nombre:</strong></td>
                                        <td class="col-md-9">{{ $usuario->getAttribute(Usuarios_Model::$nombre) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3"><strong>Correo:</strong></td>
                                        <td class="col-md-9">{{ $usuario->getAttribute(Usuarios_Model::$correo) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3"><strong>Nombre Usuario:</strong></td>
                                        <td class="col-md-9">{{ $usuario->getAttribute(Usuarios_Model::$usuario) }}</td>
                                        <input type="hidden" name="id_usuario_permiso" id="id_usuario_permiso" value="{{$usuario->getAttribute(Usuarios_Model::$id)}}">
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="Permisos">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Permisos del Usuario</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel-group" id="AcordionPermisos" role="tablist" aria-multiselectable="true">



                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-hover small" id="t_list">
                                    <thead>
                                    <tr>
                                        <th>Permiso</th>
                                        <th>Modulo</th>
                                        <th>Descripcion</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($permisos as $row)

                                        @if(array_key_exists($row->{Permiso_Model::$id} , json_decode($usuarioPermiso ,true)))
                                            <tr class="success" id="{{ $row->{Permiso_Model::$id} }}">
                                        @else
                                            <tr class="danger" id="{{ $row->{Permiso_Model::$id} }}">
                                        @endif

                                            <td>
                                                {{ $row->{Permiso_Model::$nombre} }}
                                            </td>
                                            <td>
                                                {{ $row->{Permiso_Model::$modulo} }}
                                            </td>
                                            <td>
                                                {{ $row->{Permiso_Model::$descripcion} }}
                                            </td>
                                            @if(array_key_exists($row->{Permiso_Model::$id} , json_decode($usuarioPermiso ,true)))
                                                    <td>
                                                        <input type="checkbox" name="{{ $row->{Permiso_Model::$id} }}" id="{{ $row->{Permiso_Model::$id} }}" checked onchange="check(this);">
                                                    </td>
                                            @else
                                                    <td>
                                                        <input type="checkbox" name="{{ $row->{Permiso_Model::$id} }}" id="{{ $row->{Permiso_Model::$id} }}" onchange="check(this)">
                                                    </td>
                                                    @endif


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>









                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="modal-edit_user">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Editar Usuario</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('url' => url('usuarios/fichaUsuario'),'class'=>'form-horizontal','files'=>'true','id'=>'edit_form')) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre Completo:</label>
                                    <input type="text" name="{{ Usuarios_Model::$nombre }}" id="e_{{ Usuarios_Model::$nombre }}" class="form-control" placeholder="Nombre completo del usuario" required value="{{ $usuario->getAttribute(Usuarios_Model::$nombre) }}">
                                </div>
                                <div class="form-group">
                                    <label for="usuario">Usuario:</label>
                                    <input type="text" name="{{ Usuarios_Model::$usuario }}" id="e_{{ Usuarios_Model::$usuario }}" class="form-control" placeholder="Nombre de Usuario" required value="{{ $usuario->getAttribute(Usuarios_Model::$usuario) }}">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input type="password" name="{{ Usuarios_Model::$password }}" id="e_{{ Usuarios_Model::$password }}" class="form-control" placeholder="Contraseña" required disabled>
                                </div>
                                <div class="form-group">
                                    <a class="btn btn-primary btn-sm" onclick="cambiarPassword();">Cambiar Contraseña</a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="correo">Correo Electrónico:</label>
                                    <input type="email" name="{{ Usuarios_Model::$correo }}" id="e_{{ Usuarios_Model::$correo }}" class="form-control" placeholder="Correo Electronico" required value="{{ $usuario->getAttribute(Usuarios_Model::$correo) }}">
                                </div>
                                <div class="form-group">
                                    <label>Empleado:</label>
                                    {!! Form::select( "id_empleado" , $empleados, 0, ['id' => 'id_empleado', 'class' => "form-control"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="{{ Usuarios_Model::$id }}" id="e_{{ Usuarios_Model::$id }}" value="{{ $usuario->getAttribute(Usuarios_Model::$id) }}" />
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
@endsection