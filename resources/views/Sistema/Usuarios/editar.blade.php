@extends('layouts.layout')
 
@section('content')


<div class="container">
	<div class="row clearfix">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="page-header">Usuarios <small>Editar</small></h2>
			<br>
			
			{!! Form::open(array('action' => 'UsuariosController@actualizar','class'=>'form','files'=>'true','id'=>'edit_form')) !!}
			<div class="form-group">
 				{!! Form::label('Nombre Completo: ') !!}	
 				<div class="input-group col-sm-8">
 					{!! Form::text('nombre',$usuario->nombre,array( 'class' => 'form-control', 'placeholder' => 'Nombre completo del usuario')) !!} 
 				</div>
				<p class="text-danger">	{!! $errors->first('nombre')!!} </p>
 			</div>
 			
 			<div class="form-group">
 				{!! Form::label('Nombre de Usuario :') !!}
 				<div class="input-group col-sm-6">
 					{!! Form::text('username',$usuario->username,array( 'class' => 'form-control','placeholder' => 'Nombre de usuario')) !!}
 				</div>
 				<p class="text-danger">	{!! $errors->first('username')!!} </p>
 			</div>
			
 			<div class="form-group">
 				{!! Form::label('Password :') !!}
 				<div class="input-group col-sm-6">
 					{!! Form::text('password','',array( 'class' => 'form-control','placeholder' => 'Password')) !!}
 				</div>
 				<p class="text-danger">	{!! $errors->first('password')!!} </p>
 			</div>

			<div class="form-group">
 				{!! Form::label('Correo Electrónico :') !!}
 				<div class="input-group col-sm-6">
 					{!! Form::email('email',$usuario->email,array( 'class' => 'form-control','placeholder' => 'Correo Electronico')) !!}
 				</div>
 				<p class="text-danger">	{!! $errors->first('email')!!} </p>

			</div>	

		    {!! form::hidden('id',$usuario->id) !!}
 			<br>
 				{!! link_to('usuarios', $title = " Regresar", $attributes = array('class' => "btn btn-default fa fa-arrow-left" ), $secure = null) !!}
			<button class="btn fa fa-save fa4x" id="xboton_grabar" type="submit"> Grabar</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection