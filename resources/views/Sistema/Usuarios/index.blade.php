<?php
use App\Models\Sistema\Usuarios_Model;
use App\Http\Helpers\Helpers;
?>
@extends('layouts.listado')
@section('title')
Catalogo de Usuarios
@endsection
@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function() {
            oTable = $('#t_list').dataTable({
                "dom": "<'row'<'col-lg-6'li><'col-lg-6'fp>>t<'row'<'col-lg-6'i><'col-lg-6'>>",
                language: {
                    'url':'{{ asset('vendor/datatables/lang/es_MX.json') }}'
                },
                columnDefs: [ 
                    {"targets": [0], 'searchable': false, "orderable": false, 'className': 'text-center ', 'width':'5%'},
                    {"targets": [1,2,3], 'className': ' sinwarp'}
                ]
            });

          /*  $('.select2').select2({
                width: '100%',
                class: 'form-control'
            });*/

        });
    </script>
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
          <li><a href="{{ url("/") }}">Principal</a></li>
          <li class="active">Catalogo de Usuarios</li>
        </ol>
@endsection
@section('content')
    <div class="row">
        <div class="page-header">
          <h1>Catalogo de Usuarios<small> </small></h1>
        </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="btn-group pull-right">
                            @if(Helpers::get_permiso("alta.sistema.control_usuarios"))
                            <a href="#" type="button" class="btn btn-success btn-xs" title="Agregar" data-toggle="modal" data-target="#modal-new_usuario"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo Usuario</a>
                                @endif
                        </div>
                        <h3 class="panel-title">Control de Usuarios</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover small" id="t_list">
                            <thead>
                            <tr>
                                <th>Acciones</th>
                                <th>Usuario</th>
                                <th>Nombre Completo</th>
                                <th>Correo Electrónico</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuarios as $rowUsuario)
                                <tr class="{{ ($rowUsuario->isOnline())? 'success':'' }} {{ ($rowUsuario->getAttribute(Usuarios_Model::$estatus) == 0)? 'danger':'' }}">
                                    <td class="text-center">
                                        @if(Helpers::get_permiso("edicion.sistema.control_usuarios"))
                                        <a href="{{ url('usuarios/fichaUsuario/'.$rowUsuario->getAttribute(Usuarios_Model::$id))  }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            @endif
                                    </td>
                                    <td>{{ $rowUsuario->getAttribute(Usuarios_Model::$usuario) }}</td>
                                    <td>{{ $rowUsuario->getAttribute(Usuarios_Model::$nombre) }}</td>
                                    <td>{{ $rowUsuario->getAttribute(Usuarios_Model::$correo) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <div class="modal fade" id="modal-new_usuario">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo Usuario</h4>
                </div>
                {!! Form::open(['url' => url('usuarios/nuevoUsuario'),'class'=>'form-horizontal','files'=>'true','id'=>'add_form']) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre">Nombre Completo:</label>
                        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre completo del usuario" required>
                    </div>
                    <div class="form-group">
                        <label for="usuario">Usuario:</label>
                        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Nombre de Usuario" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" required>
                    </div>
                    <div class="form-group">
                        <label for="correo">Correo Electrónico:</label>
                        <input type="correo" name="correo" id="correo" class="form-control" placeholder="Correo Electronico2" required>
                    </div>
                    <div class="form-group">
                        <label>Empleado:</label>
                        {!! Form::select( "id_empleado" , $empleados, 0, ['id' => 'id_empleado', 'class' => "form-control"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

@endsection

