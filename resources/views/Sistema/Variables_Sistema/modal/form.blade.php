<?php
use App\Models\Sistema\Variables_Sistema_Model;
?>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Variable de Sistema</h4>
            </div>
            {!! Form::open(['url' => null,'class'=>'form','id'=>'add_edit_form','onSubmit' => 'return guardarVariablesSistema()']) !!}
            <div class="modal-body">

                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Nombre</span>
                    <input type="text" name="{{ Variables_Sistema_Model::$nombre }}" id="modal-form-{{ Variables_Sistema_Model::$nombre }}" class="form-control" placeholder="Nombre" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Tabla</span>
                    <input type="text" name="{{ Variables_Sistema_Model::$tablaOrigen }}" id="modal-form-{{ Variables_Sistema_Model::$tablaOrigen }}" class="form-control" placeholder="Tabla" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Alias de la Tabla</span>
                    <input type="text" name="{{ Variables_Sistema_Model::$aliasTablaOrigen }}" id="modal-form-{{ Variables_Sistema_Model::$aliasTablaOrigen }}" class="form-control" placeholder="Alias de la Tabla" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300" >
                    <span class="input-group-addon">Campo</span>
                    <input type="text" name="{{ Variables_Sistema_Model::$campo }}" id="modal-form-{{ Variables_Sistema_Model::$campo }}" class="form-control" placeholder="Campo" required>
                </div>
                <br>
                <div class="input-group input-group-sm max300">
                    <span class="input-group-addon">Rol</span>
                    {!! Form::select( Variables_Sistema_Model::$idRol , $roles, 0, ['id' => "modal-form-" . Variables_Sistema_Model::$idRol, 'class' => "form-control select2"]) !!}
                </div>
                <br>
                <div class="input-group input-group-sm max300">
                    <span class="input-group-addon">Categoría</span>
                    {!! Form::select( Variables_Sistema_Model::$idCategoria , $categoriaVariablesSistema, 0, ['id' => "modal-form-" . Variables_Sistema_Model::$idCategoria, 'class' => "form-control select2"]) !!}
                </div>

                <input type="hidden" name="{{ Variables_Sistema_Model::$id }}" id="modal-form-{{ Variables_Sistema_Model::$id }}" value="0">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>