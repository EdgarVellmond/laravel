<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SISAC - Login</title>
    <meta name="description" content="Sistema semilla de Laravel">
    <meta name="author" content="Oscar Vargas & Erik Villareal!">
    {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}
    {!! Html::style('vendor/bootstrap/css/bootstrap-theme.min.css') !!}
    {!! Html::style('css/style.css') !!}

    {!! Html::script('vendor/bootstrap/js/jquery.min.js') !!}
    {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('js/scripts.js') !!}
</head>

<style>

    body {
        padding-top: 100px;
        padding-bottom: 40px;
        background-color: #fff;
    }
    .form-signin {
        max-width: 400px;
        padding: 15px;
        margin: 0 auto;
        background-color: #eee
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    #ladoizq { width:50%; height:590px; float:left; }
    #ladoder { width:50%; height:590px; float:left; }
    #pie{ width:100%; height:5px; background-color: #35495e; margin-bottom: 5px;}
</style>

<body style="padding-top: 10px">
<div class="container" >
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4" style="height: 300px">
            <img src="{{ URL::asset('images/SISAC 2-02.png') }}" class="img-responsive" style="height: 100%">
        </div>
        <div class="col-md-4"></div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Acceso al Sistema</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => '/login', 'class' => 'form')) !!}
                    <div class="form-group">
                        <label for="usuario">Usuario</label>
                        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" />
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Ingresar al Sistema</button>
                    </div>
                    {!! Form::close() !!}
                    @if (Session::has('errors'))
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <strong>Oops! Algo salio mal. : </strong>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-info">
                        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        	<strong>Aviso:</strong> {{ Session::get('message') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h1>{{ str_replace('_',' ',env('NOMBRE_SISTEMA')) }} 1.0v<br>
                <small>SISTEMA</small>
            </h1>
            <p class="small">DESCRIPCION DEL SISTEMA<br>
                Sistema de Administración en la Construcción</p>
            <div>{!! \App\Http\Helpers\Helpers::loginCount() !!}</div>
        </div>
    </div>
</div>
</body>
</html>