<?php
use App\Http\Helpers\Helpers;
use App\Models\Recursos_Humanos\Empleado_Imagen_Model;

?>

    <div class="menuLateral gradiante esconder" >
        <h3><a href="{{url('/')}}" style="text-decoration: none;">{{ str_replace('_',' ',env('NOMBRE_SISTEMA')) }}</a></h3>
        
        <ul class="nav nav-pills nav-stacked">
            <br>
            <li>
                <?php 
                    $usuario = Helpers::get_imagenUsuario(); 
                    $path = url($usuario->{Empleado_Imagen_Model::$ruta}.$usuario->{Empleado_Imagen_Model::$uuid}.'.'.$usuario->{Empleado_Imagen_Model::$extension} );
                    $path = str_replace("\\",'/',$path);
                ?>
                <div class="foto" >
                    @if($usuario->{Empleado_Imagen_Model::$ruta} != null && $usuario->{Empleado_Imagen_Model::$uuid} != null && $usuario->{Empleado_Imagen_Model::$extension} != null)
                        <img style="
                                    background-image: url('{{ $path }}'); 
                                    background-repeat: no-repeat; 
                                    background-size: cover;
                                    border-radius: 50%;">
                    @else
                        <span class="glyphicon glyphicon-user" style="font-size: 40px;"></span>
                    @endif
                    {{ (!is_null(Auth::user()))? Auth::user()->usuario:'' }}
                </div>
            </li>
            <hr>
            @include("layouts.menus.catalogos")
            @include("layouts.menus.recursos_humanos")
            @include("layouts.menus.sistema")
            @include("layouts.menus.solicitud")
            <hr>
            <h4>Obra</h4>
            @include("layouts.menus.planeacion")
            @include("layouts.menus.indicadores")
        </ul>
    </div>

<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
            <div class="esconder" style="display: none;">
            <div class="foto" >
                    @if($usuario->{Empleado_Imagen_Model::$ruta} != null && $usuario->{Empleado_Imagen_Model::$uuid} != null && $usuario->{Empleado_Imagen_Model::$extension} != null)
                        <img style="
                                    background-image: url('{{ $path }}'); 
                                    background-repeat: no-repeat; 
                                    background-size: cover;
                                    border-radius: 50%;">
                    @else
                        <span class="glyphicon glyphicon-user" style="font-size: 40px;"></span>
                    @endif
                    {{ (!is_null(Auth::user()))? Auth::user()->nombre:'' }}
                </div>
            </div>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        {{--Opciones a la Izquierda--}}
        <ul class="nav navbar-nav small">
        </ul>
        {{--Opciones a la Derecha--}}
        <ul class="nav navbar-nav navbar-right small">

            <li>
                <a  title="Desplazar menú" data-toggle="offcanvas" style="cursor: pointer;" onclick="$('.esconder').animate({width: 'toggle'});$('.mover').animate({height: 'toggle'});"><span style="font-size: 20px;" class="glyphicon glyphicon-transfer"></span> </a>
            </li>

            <li>
                <a href="{{ url('logout') }}"><span class="glyphicon glyphicon-ban-circle"></span> Salir</a>
            </li>

        </ul>
    </div>
</nav>
