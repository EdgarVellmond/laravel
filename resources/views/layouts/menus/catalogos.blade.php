<?php
use App\Http\Helpers\Helpers;

?>

@if(Helpers::get_permiso("vista.catalogos"))


        <li><a data-toggle="collapse" data-target="#catalogos" style="cursor: pointer;" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');" >Catálogos<span class="glyphicon glyphicon-triangle-right"></span></a>

              <div id="catalogos" class="collapse">
                  <ul class="nav nav-pills nav-stacked submenu">
                      


                    @if(Helpers::get_permiso("vista.catalogos.area"))
                <li><a href="{{ url("catalogos/area") }}" data-toggle="tooltip"
                       data-placement="left">Área</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.categoria_expediente_digital"))
                <li><a href="{{ url("catalogos/categoria_expediente_digital") }}" data-toggle="tooltip"
                       data-placement="left">Categoria Expediente Digital</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.clasificacion_obra"))
                <li><a href="{{ url("catalogos/clasificacion_obra") }}" data-toggle="tooltip"
                       data-placement="left">Clasificación de Obra</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.clave_giro_nomina"))
                <li><a href="{{ url("catalogos/clave_giro_nomina") }}" data-toggle="tooltip"
                       data-placement="left">Clave Giro Nómina</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.cliente"))
                <li><a href="{{ url("catalogos/cliente") }}" data-toggle="tooltip"
                       data-placement="left">Clientes</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.cuentas_nomina_sat"))
                <li><a href="{{ url("catalogos/cuentas_nomina_sat") }}" data-toggle="tooltip"
                       data-placement="left">Cuentas Nómina SAT</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.cuota_obrero_patronal"))
                <li><a href="{{ url("catalogos/cuota_obrero_patronal") }}" data-toggle="tooltip"
                       data-placement="left">Cuota Obrero Patronal</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.departamento"))
                <li><a href="{{ url("catalogos/departamento") }}" data-toggle="tooltip"
                       data-placement="left">Departamento</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.dia_festivo"))
                <li><a href="{{ url("catalogos/dia_festivo") }}" data-toggle="tooltip"
                       data-placement="left">Día Festivo</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.empresa"))
                <li><a href="{{ url("catalogos/empresa") }}" data-toggle="tooltip"
                       data-placement="left">Empresa</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.estado"))
                <li><a href="{{ url("/estado") }}" data-toggle="tooltip"
                       data-placement="left">Estado</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.expediente_digital"))
                <li><a href="{{ url("catalogos/expediente_digital") }}" data-toggle="tooltip"
                       data-placement="left">Doctos. Expediente Digital</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.impuesto_isr"))
                <li><a href="{{ url("catalogos/impuesto_isr") }}" data-toggle="tooltip"
                       data-placement="left">Impuesto ISR</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.metodo_pago"))
                <li><a href="{{ url("catalogos/metodo_pago") }}" data-toggle="tooltip"
                       data-placement="left">Método de Pago</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.municipio"))
                <li><a href="{{ url("/municipio") }}" data-toggle="tooltip"
                       data-placement="left">Municipio</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.plantilla"))
                <li><a href="{{ url("catalogos/plantilla") }}" data-toggle="tooltip"
                       data-placement="left">Plantillas</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.prima_riesgo"))
                <li><a href="{{ url("catalogos/prima_riesgo") }}" data-toggle="tooltip"
                       data-placement="left">Prima de Riesgo</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.proveedor"))
                <li><a href="{{ url("/proveedor") }}" data-toggle="tooltip"
                       data-placement="left">Proveedor</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.puesto"))
                <li><a href="{{ url("catalogos/puesto") }}" data-toggle="tooltip"
                       data-placement="left">Puesto</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.rol"))
                <li><a href="{{ url("catalogos/rol") }}" data-toggle="tooltip"
                       data-placement="left">Rol</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.salario_minimo"))
                <li><a href="{{ url("catalogos/salario_minimo") }}" data-toggle="tooltip"
                       data-placement="left">Salario Mínimo</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.sucursal"))
                <li><a href="{{ url("/sucursal") }}" data-toggle="tooltip"
                       data-placement="left">Sucursal</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.tipo_plantilla"))
                <li><a href="{{ url("catalogos/tipo_plantilla") }}" data-toggle="tooltip"
                       data-placement="left">Tipo Plantilla</a></li>
            @endif
            @if(Helpers::get_permiso("vista.catalogos.tipo_solicitud"))
                <li><a href="{{ url("catalogos/tipo_solicitud") }}" data-toggle="tooltip"
                       data-placement="left">Tipo Solicitud</a></li>
            @endif






                  </ul>
              </div>
        </li>
        
        
@endif