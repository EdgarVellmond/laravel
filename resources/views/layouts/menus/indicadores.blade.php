<?php
use App\Http\Helpers\Helpers;

?>

@if(Helpers::get_permiso("vista.indicadores"))
    <li><a data-toggle="collapse" data-target="#indicadores" style="cursor: pointer;" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');">Indicadores<span class="glyphicon glyphicon-triangle-right"></span></a>

        <div id="indicadores" class="collapse">
            <ul class="nav nav-pills nav-stacked submenu">

                @if(Helpers::get_permiso("vista.indicadores.proyecto"))
                    <li><a href="{{ url("indicadores/bancoProyectos") }}" data-toggle="tooltip"
                           data-placement="left">Banco de Proyectos</a></li>
                @endif
                @if(Helpers::get_permiso("alta.indicadores.proyecto"))
                    <li><a href="{{ url("indicadores/reportes") }}" data-toggle="tooltip"
                           data-placement="left">Reportes</a></li>
                @endif

            </ul>
        </div>
    </li>
@endif