<?php
use App\Http\Helpers\Helpers;

?>

@if(Helpers::get_permiso("vista.planeacion"))
    <li><a data-toggle="collapse" data-target="#planeacion" style="cursor: pointer;" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');">Planeación<span class="glyphicon glyphicon-triangle-right"></span></a>

        <div id="planeacion" class="collapse">
            <ul class="nav nav-pills nav-stacked submenu">

                @if(Helpers::get_permiso("vista.planeacion.banco_proyecto"))
                    <li><a href="{{ url("planeacion/bancoProyecto") }}" data-toggle="tooltip"
                           data-placement="left">Banco de Proyectos</a></li>
                @endif
                @if(Helpers::get_permiso("vista.planeacion.registro_padron"))
                    <li><a href="{{ url("planeacion/registroPadron") }}" data-toggle="tooltip"
                           data-placement="left">Registro de Padrón</a></li>
                @endif

            </ul>
        </div>
    </li>
@endif