<?php
use App\Http\Helpers\Helpers;

?>

@if(Helpers::get_permiso("vista.recursos_humanos"))

        <li><a data-toggle="collapse" data-target="#recursos_humanos" style="cursor: pointer;" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');">Recursos Humanos<span class="glyphicon glyphicon-triangle-right"></span></a>

              <div id="recursos_humanos" class="collapse">
                  <ul class="nav nav-pills nav-stacked submenu">
                      


                    @if(Helpers::get_permiso("vista.recursos_humanos.empleado"))
                <li><a href="{{ url("recursos_humanos/empleado") }}" data-toggle="tooltip"
                       data-placement="left">Control de Empleados</a></li>
            @endif
            @if(Helpers::get_permiso("vista.recursos_humanos.falta"))
                <li><a href="{{ url("recursos_humanos/falta") }}" data-toggle="tooltip"
                       data-placement="left">Faltas</a></li>
            @endif
            @if(Helpers::get_permiso("vista.recursos_humanos.incapacidad"))
                <li><a href="{{ url("recursos_humanos/incapacidad") }}" data-toggle="tooltip"
                       data-placement="left">Incapacidades</a></li>
            @endif
            @if(Helpers::get_permiso("vista.recursos_humanos.nomina"))
                <li><a href="{{ url("recursos_humanos/nomina") }}" data-toggle="tooltip"
                       data-placement="left">Nómina</a></li>
            @endif
            @if(Helpers::get_permiso("vista.recursos_humanos.permiso_asistencia"))
                <li><a href="{{ url("recursos_humanos/permiso_asistencia") }}" data-toggle="tooltip"
                       data-placement="left">Permisos de Asistencia</a></li>
            @endif
            @if(Helpers::get_permiso("vista.recursos_humanos.vacaciones"))
                <li><a href="{{ url("recursos_humanos/vacaciones") }}" data-toggle="tooltip"
                       data-placement="left">Vacaciones</a></li>
            @endif



                  </ul>
              </div>
        </li>
@endif