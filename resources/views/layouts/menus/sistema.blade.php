<?php
use App\Http\Helpers\Helpers;

?>

@if(Helpers::get_permiso("vista.sistema"))
    <li><a data-toggle="collapse" data-target="#sistema" style="cursor: pointer;" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');">Sistema<span class="glyphicon glyphicon-triangle-right"></span></a>

              <div id="sistema" class="collapse">
                  <ul class="nav nav-pills nav-stacked submenu">
                      


                    @if(Helpers::get_permiso("vista.sistema.control_usuarios"))
                <li><a href="{{ url("usuarios") }}" data-toggle="tooltip"
                       data-placement="left">Control de Usuarios</a></li>
            @endif
            @if(Helpers::get_permiso("vista.sistema.variables_sistema"))
                <li><a href="{{ url("sistema/variables_sistema") }}" data-toggle="tooltip"
                       data-placement="left">Variables del Sistema</a></li>
            @endif
            @if(Helpers::get_permiso("vista.sistema.categoria_variables_sistema"))
                <li><a href="{{ url("sistema/categoria_variables_sistema") }}" data-toggle="tooltip"
                       data-placement="left">Categoria de Variables del Sistema</a></li>
            @endif



                  </ul>
              </div>
        </li>
@endif