<?php
use App\Http\Helpers\Helpers;

?>

@if(Helpers::get_permiso("vista.solicitud"))

      <li><a data-toggle="collapse" data-target="#solicitud" style="cursor: pointer;" onclick=" $(this).find('span').toggleClass('glyphicon glyphicon-triangle-bottom').toggleClass('glyphicon glyphicon-triangle-right');">Solicitud<span class="glyphicon glyphicon-triangle-right"></span></a>

              <div id="solicitud" class="collapse">
                  <ul class="nav nav-pills nav-stacked submenu">
                      


                    @if(Helpers::get_permiso("vista.generico.solicitud"))
                <li><a href="{{ url("solicitud/generarSolicitud") }}" data-toggle="tooltip"
                       data-placement="left">Generar Solicitud</a></li>
            @endif
            @if(Helpers::get_permiso("vista.generico.bitacora_solicitud"))
                <li><a href="{{ url("solicitud/bitacoraSolicitud") }}" data-toggle="tooltip"
                       data-placement="left">Bitacora Solicitud</a></li>
            @endif



                  </ul>
              </div>
        </li>
        
@endif